PROJECT STATUSES:

STATUS_ACTIVE         - after some developer evaluate it
STATUS_IN_WORK        - after author add some developer to it
STATUS_WITHOUT_ANSWER - until any developer  haven't evaluate it(default)
STATUS_DELETED        - after project is deleted
STATUS_COMPLETED      - after project is completed