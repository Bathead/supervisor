<?php

use yii\db\Schema;
use yii\db\Migration;

class m160106_071252_create_project_developer_table extends Migration
{
    public function up()
    {
        $this->createTable('project_developer', [
            'id' => Schema::TYPE_PK,
            'id_project' => Schema::TYPE_INTEGER . ' NOT NULL',
            'id_developer' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);

        $this->addForeignKey('FK_project_id', 'project_developer', 'id_project', 'project', 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('FK_developer_id', 'project_developer', 'id_developer', 'user', 'id', 'NO ACTION', 'NO ACTION');

    }

    public function down()
    {
        $this->dropTable('project_developer');
    }

}
