<?php

use yii\db\Migration;

class m160706_074832_change_types_in_hr_ontact extends Migration
{
    protected $tn_hr_contact = '{{%hr_contact}}';

    public function up()
    {
        $this->alterColumn($this->tn_hr_contact, 'monthly_rate', $this->double());
        $this->alterColumn($this->tn_hr_contact, 'hourly_rate', $this->double());
        $this->alterColumn($this->tn_hr_contact, 'years', $this->double());
    }

    public function down()
    {
        $this->alterColumn($this->tn_hr_contact, 'monthly_rate', $this->decimal(10, 2));
        $this->alterColumn($this->tn_hr_contact, 'hourly_rate', $this->decimal(10, 2));
        $this->alterColumn($this->tn_hr_contact, 'years', $this->decimal(10, 2));
    }
}
