<?php

use yii\db\Migration;

class m160226_133437_add_test_data_to_hr_module extends Migration
{
    protected $tn_hr_mailing = '{{%hr_mailing}}';
    protected $tn_hr_mailing_contact = '{{%hr_mailing_contact}}';
    protected $tn_hr_contact = '{{%hr_contact}}';
    protected $tn_hr_contact_technology = '{{%hr_contact_technology}}';
    protected $hr_black_list = '{{%hr_black_list}}';

    public function safeUp()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');

        $created_at = $updated_at = date('Y-m-d H:i:s');

        // insert contacts
        $contactRows = ['id', 'name', 'email', 'contacts', 'main_technology_id', 'city', 'creator_id'];
        $contacts = [
            [1, 'Firstname Lastname 1', 'email1@gmail.com', 'Phone 1', 1, 'Rivne', 1],
            [2, 'Firstname Lastname 2', 'email2@gmail.com', 'Phone 2', 5, 'Kiev', 6],
            [3, 'Firstname Lastname 3', 'email3@gmail.com', 'Phone 3', 5, 'Rivne', 6],
            [4, 'Firstname Lastname 4', 'email4@gmail.com', 'Phone 4', 5, 'Lviv', 1],
            [5, 'Firstname Lastname 5', 'email5@gmail.com', 'Phone 5', 5, 'Rivne', 1],
            [6, 'Firstname Lastname 6', 'email6@gmail.com', 'Phone 6', 5, 'Kiev', 6],
            [7, 'Firstname Lastname 7', 'email7@gmail.com', 'Phone 7', 5, 'Lviv', 1],
            [8, 'Firstname Lastname 8', 'email8@gmail.com', 'Phone 8', 5, 'Rivne', 6],
            [9, 'Firstname Lastname 9', 'email9@gmail.com', 'Phone 9', 1, 'Lviv', 6],
            [10, 'Firstname Lastname 10', 'email10@gmail.com', 'Phone 10', 1, 'Rivne', 6],
            [11, 'Firstname Lastname 11', 'email11@gmail.com', 'Phone 11', 6, 'Rivne', 6],
            [12, 'Firstname Lastname 12', 'email12@gmail.com', 'Phone 12', 6, 'Rivne', 6],
            [13, 'Firstname Lastname 13', 'email13@gmail.com', 'Phone 13', 6, 'Rivne', 6],
            [14, 'Firstname Lastname 14', 'email14@gmail.com', 'Phone 14', 6, 'Rivne', 6],
            [15, 'Firstname Lastname 15', 'email15@gmail.com', 'Phone 15', 6, 'Kiev', 1],
            [16, 'Firstname Lastname 16', 'email16@gmail.com', 'Phone 16', 6, 'Rivne', 1],
            [17, 'Firstname Lastname 17', 'email17@gmail.com', 'Phone 17', 6, 'Lviv', 1],
            [18, 'Firstname Lastname 18', 'email18@gmail.com', 'Phone 18', 6, 'Rivne', 1],
            [19, 'Firstname Lastname 19', 'email19@gmail.com', 'Phone 19', 1, 'Lviv', 6],
            [20, 'Firstname Lastname 20', 'email20@gmail.com', 'Phone 20', 3, 'Lviv', 6],
            [21, 'Firstname Lastname 21', 'email21@gmail.com', 'Phone 21', 3, 'Lviv', 6],
            [22, 'Firstname Lastname 22', 'email22@gmail.com', 'Phone 22', 3, 'Lviv', 6],
            [23, 'Firstname Lastname 23', 'email23@gmail.com', 'Phone 23', 3, 'Lviv', 6],
            [24, 'Firstname Lastname 24', 'email24@gmail.com', 'Phone 24', 1, 'Lviv', 6],
            [25, 'Firstname Lastname 25', 'email25@gmail.com', 'Phone 25', 1, 'Rivne', 1],
            [26, 'Firstname Lastname 26', 'email26@gmail.com', 'Phone 26', 1, 'Kiev', 1],
            [27, 'Firstname Lastname 27', 'email27@gmail.com', 'Phone 27', 7, 'Rivne', 6],
            [28, 'Firstname Lastname 28', 'email28@gmail.com', 'Phone 28', 7, 'Rivne', 6],
            [29, 'Firstname Lastname 29', 'email29@gmail.com', 'Phone 29', 7, 'Kiev', 6],
            [30, 'Firstname Lastname 30', 'email30@gmail.com', 'Phone 30', 7, 'Kiev', 6],
        ];
        $this->batchInsert($this->tn_hr_contact, $contactRows, $contacts);

        //insert mailings
        $contactAdditionalTechnologiesRows = ['contact_id', 'technology_id'];
        $contactAdditionalTechnologies = [
            // contact 1
            [1, 2],
            [1, 3],
            [1, 4],

            // contact 2
            [2, 2],
            [2, 3],
            [2, 4],

            // contact 3
            [3, 2],
            [3, 4],

            // contact 4
            [4, 2],
            [4, 3],
            [4, 4],

            // contact 5
            [5, 2],
            [5, 3],

            // contact 6
            [6, 3],
            [6, 4],

            // contact 7
            [7, 2],
            [7, 3],
            [7, 4],

            // contact 8
            [8, 1],
            [8, 2],
            [8, 4],

            // contact 9
            [9, 1],
            [9, 3],

            // contact 10
            [10, 7],
            [10, 3],
            [10, 4],
        ];
        $this->batchInsert($this->tn_hr_contact_technology, $contactAdditionalTechnologiesRows, $contactAdditionalTechnologies);

        //insert mailings
        $mailingRows = ['id', 'description', 'subject', 'message', 'from_name', 'from_email', 'created_at', 'created_by_id'];
        $mailing = [
            [1, 'Mailing 1', 'Mailing 1 subject', 'Mailing 1 message', 'from_name1', 'from_email@gmail.com', $created_at, 1],
            [2, 'Mailing 2', 'Mailing 2 subject', 'Mailing 2 message', 'from_name1', 'from_email@gmail.com', $created_at, 6],
            [3, 'Mailing 3', 'Mailing 3 subject', 'Mailing 3 message', 'from_name1', 'from_email@gmail.com', $created_at, 6],
            [4, 'Mailing 4', 'Mailing 4 subject', 'Mailing 4 message', 'from_name1', 'from_email@gmail.com', $created_at, 6]
        ];
        $this->batchInsert($this->tn_hr_mailing, $mailingRows, $mailing);

        //insert mailing contacts
        $mailingContactsRows = ['mailing_id', 'email'];
        $mailingContacts = [
            // mailing 1
            [1, 'email1@gmail.com'],
            [1, 'email2@gmail.com'],
            [1, 'email3@gmail.com'],
            [1, 'email4@gmail.com'],
            [1, 'email6@gmail.com'],
            [1, 'email7@gmail.com'],

            // mailing 2
            [2, 'email11@gmail.com'],
            [2, 'email13@gmail.com'],
            [2, 'email14@gmail.com'],
            [2, 'email16@gmail.com'],
            [2, 'email17@gmail.com'],
            [2, 'email22@gmail.com'],

            // mailing 3
            [3, 'email11@gmail.com'],
            [3, 'email13@gmail.com'],
            [3, 'email14@gmail.com'],
            [3, 'email16@gmail.com'],
            [3, 'email17@gmail.com'],
            [3, 'email22@gmail.com'],

            // mailing 4
            [4, 'email1@gmail.com'],
            [4, 'email2@gmail.com'],
            [4, 'email3@gmail.com'],
            [4, 'email4@gmail.com'],
            [4, 'email6@gmail.com'],
            [4, 'email7@gmail.com'],
            [4, 'email11@gmail.com'],
            [4, 'email13@gmail.com'],
            [4, 'email14@gmail.com'],
            [4, 'email16@gmail.com'],
            [4, 'email17@gmail.com'],
            [4, 'email22@gmail.com'],

        ];
        $this->batchInsert($this->tn_hr_mailing_contact, $mailingContactsRows, $mailingContacts);

        //insert blacklist
        $blacklistRows = ['email', 'reason', 'created_at', 'created_by_id'];
        $blackList = [
            ['email11@gmail.com', 'Unsubscribed', $created_at, NULL],
            ['email22@gmail.com', 'Unsubscribed', $created_at, NULL],
            ['email23@gmail.com', 'Reason 1', $created_at, 6],
            ['email24@gmail.com', 'Reason 2', $created_at, 1],
        ];
        $this->batchInsert($this->hr_black_list, $blacklistRows, $blackList);

    }

    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->execute('TRUNCATE TABLE ' . $this->tn_hr_contact);
        $this->execute('TRUNCATE TABLE ' . $this->tn_hr_contact_technology);
        $this->execute('TRUNCATE TABLE ' . $this->tn_hr_mailing);
        $this->execute('TRUNCATE TABLE ' . $this->tn_hr_mailing_contact);
        $this->execute('TRUNCATE TABLE ' . $this->hr_black_list);
    }
}
