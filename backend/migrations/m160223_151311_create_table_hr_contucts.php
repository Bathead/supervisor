<?php

use yii\db\Schema;
use yii\db\Migration;

class m160223_151311_create_table_hr_contucts extends Migration
{
    protected $tn_hr_contact = '{{%hr_contact}}';
    protected $tn_technology = '{{%technology}}';
    protected $tn_english_language_level = '{{%english_language_level}}';
    protected $tn_user = '{{%user}}';

    public function safeUp()
    {
        $this->createTable($this->tn_hr_contact, [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'email' => $this->string()->unique(),
            'contacts' => $this->string(),
            'main_technology_id' => $this->integer(),
            'monthly_rate' => $this->decimal(10,2),
            'hourly_rate' => $this->decimal(10,2),
            'english_level_id' => $this->integer(),
            'city' => $this->string(),
            'years' => $this->decimal(10, 2),
            'attached_resume_path' => $this->string(),
            'attached_sg_resume_path' => $this->string(),
            'creator_id' => $this->integer(),
        ]);

        $this->addForeignKey('hr_contacts_main_technology_id', $this->tn_hr_contact, 'main_technology_id', $this->tn_technology, 'id', 'SET NULL', 'NO ACTION');
        $this->addForeignKey('hr_contacts_english_level_id', $this->tn_hr_contact, 'english_level_id', $this->tn_english_language_level, 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('hr_contacts_creator_id', $this->tn_hr_contact, 'creator_id', $this->tn_user, 'id', 'NO ACTION', 'NO ACTION');
    }

    public function safeDown()
    {
        $this->dropForeignKey('hr_contacts_creator_id', $this->tn_hr_contact);
        $this->dropForeignKey('hr_contacts_english_level_id', $this->tn_hr_contact);
        $this->dropForeignKey('hr_contacts_main_technology_id', $this->tn_hr_contact);
        $this->dropTable($this->tn_hr_contact);
    }
}
