<?php

use yii\db\Schema;
use yii\db\Migration;

class m160106_074004_add_permision_hasRating extends Migration
{
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';

    public function up()
    {   
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            // user permissions
            ['user.hasRating', 2, 'Has rating'],
        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

//        $rolePermissionsRows = ['parent', 'child'];
//        $rolePermissions = [
//            // developer permissions
//            ['Developer', 'user.hasRating'],
//        ];
//        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function down()
    {
//        $this->delete($this->tn_auth_item_child, "parent = 'Developer' AND child = 'user.hasRating'");
        $this->delete($this->tn_auth_item, "name = 'user.hasRating' AND type = '2' AND description = 'Has rating'");
    }
}
