<?php

use yii\db\Schema;
use yii\db\Migration;

class m160203_081848_create_dialog_tables extends Migration
{
    protected $tn_dialog = '{{%dialog}}';
    protected $tn_dialog_message = '{{%dialog_message}}';
    protected $tn_dialog_user = '{{%dialog_user}}';
    protected $tn_dialog_message_unread = '{{%dialog_message_unread}}';
    protected $tn_user = '{{%user}}';

    public function safeUp()
    {

        $this->createTable($this->tn_dialog, [
            'id' => $this->primaryKey(),
            'subject' => $this->text()->defaultValue(null),
            'author_id' => $this->integer(),
            'created_at' => $this->timestamp()->defaultValue(0),
            'last_message_id' => $this->integer(),
            'last_action_date' => $this->timestamp()->defaultValue(0),
            'is_deleted' => $this->boolean()->defaultValue(0),
        ]);

        $this->createTable($this->tn_dialog_message, [
            'id' => $this->primaryKey(),
            'dialog_id' => $this->integer(),
            'user_id' => $this->integer(),
            'message' => $this->text(),
            'parent_id' => $this->integer()->defaultValue(null),
            'updated_at' => $this->timestamp()->defaultValue(0),
            'created_at' => $this->timestamp()->defaultValue(0),
            'is_deleted' => $this->boolean()->defaultValue(0)
        ]);

        $this->createTable($this->tn_dialog_user, [
            'dialog_id' => $this->integer(),
            'user_id' => $this->integer(),
            'PRIMARY KEY (dialog_id, user_id)'
        ]);

        $this->createTable($this->tn_dialog_message_unread, [
            'message_id' => $this->integer(),
            'user_id' => $this->integer(),
            'PRIMARY KEY (message_id, user_id)'
        ]);

        $this->addForeignKey('fk_dialog_author_id', $this->tn_dialog, 'author_id', $this->tn_user, 'id', 'SET NULL', 'NO ACTION');
        $this->addForeignKey('fk_dialog_last_message_id', $this->tn_dialog, 'last_message_id', $this->tn_dialog_message, 'id', 'SET NULL', 'NO ACTION');

        $this->addForeignKey('fk_dialog_message_dialog_id', $this->tn_dialog_message, 'dialog_id', $this->tn_dialog, 'id', 'CASCADE', 'NO ACTION');
        $this->addForeignKey('fk_dialog_message_user_id', $this->tn_dialog_message, 'user_id', $this->tn_user, 'id', 'SET NULL', 'NO ACTION');
        $this->addForeignKey('fk_dialog_message_parent_id', $this->tn_dialog_message, 'parent_id', $this->tn_dialog_message, 'id', 'CASCADE', 'NO ACTION');

        $this->addForeignKey('fk_dialog_user_dialog_id', $this->tn_dialog_user, 'dialog_id', $this->tn_dialog, 'id', 'CASCADE', 'NO ACTION');
        $this->addForeignKey('fk_dialog_user_user_id', $this->tn_dialog_user, 'user_id', $this->tn_user, 'id', 'CASCADE', 'NO ACTION');

        $this->addForeignKey('fk_dialog_message_unread_message_id', $this->tn_dialog_message_unread, 'message_id', $this->tn_dialog_message, 'id', 'CASCADE', 'NO ACTION');
        $this->addForeignKey('fk_dialog_message_unread_user_id', $this->tn_dialog_message_unread, 'user_id', $this->tn_user, 'id', 'CASCADE', 'NO ACTION');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_dialog_author_id', $this->tn_dialog);
        $this->dropForeignKey('fk_dialog_last_message_id', $this->tn_dialog);

        $this->dropForeignKey('fk_dialog_message_dialog_id', $this->tn_dialog_message);
        $this->dropForeignKey('fk_dialog_message_user_id', $this->tn_dialog_message);
        $this->dropForeignKey('fk_dialog_message_parent_id', $this->tn_dialog_message);

        $this->dropForeignKey('fk_dialog_user_dialog_id', $this->tn_dialog_user);
        $this->dropForeignKey('fk_dialog_user_user_id', $this->tn_dialog_user);

        $this->dropForeignKey('fk_dialog_message_unread_message_id', $this->tn_dialog_message_unread);
        $this->dropForeignKey('fk_dialog_message_unread_user_id', $this->tn_dialog_message_unread);

        $this->dropTable($this->tn_dialog);
        $this->dropTable($this->tn_dialog_message);
        $this->dropTable($this->tn_dialog_user);
        $this->dropTable($this->tn_dialog_message_unread);
    }

}
