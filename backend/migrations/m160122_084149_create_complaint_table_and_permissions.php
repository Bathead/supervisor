<?php

use yii\db\Schema;
use yii\db\Migration;
use app\modules\user\models\User;
use app\modules\complaint\models\Complaint;

class m160122_084149_create_complaint_table_and_permissions extends Migration
{
    protected $tn_user = '{{%user}}';
    protected $tn_complaint = '{{%complaint}}';
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';
    protected $tn_auth_assignment = '{{%auth_assignment}}';

    public function safeUp()
    {
        $this->createTable($this->tn_complaint, [
            'id' => $this->primaryKey(),
            'who_id' => $this->integer(),
	        'whom_id' => $this->integer(),
	        'date' => $this->timestamp()->defaultValue(0),
	        'reason' => $this->text(),
	        'status' => $this->integer()->defaultValue(Complaint::STATUS_NEW)
        ]);

        $this->addForeignKey('who_user_id', $this->tn_complaint, 'who_id', $this->tn_user, 'id', 'RESTRICT', 'NO ACTION');
        $this->addForeignKey('whom_user_id', $this->tn_complaint, 'whom_id', $this->tn_user, 'id', 'RESTRICT', 'NO ACTION');

        $created_at = date('Y-m-d H:i:s');

        $complaintsRows = ['who_id', 'whom_id', 'date', 'reason', 'status'];
        $complaints = [
            [4, 3, $created_at, 'Test complaint 1 reason', Complaint::STATUS_REVIEWED],
            [5, 3, $created_at, 'Test complaint 2 reason', Complaint::STATUS_REVIEWED],
            [6, 3, $created_at, 'Test complaint 3 reason', Complaint::STATUS_NEW],
            [7, 3, $created_at, 'Test complaint 4 reason', Complaint::STATUS_NEW],
        ];
        $this->batchInsert($this->tn_complaint, $complaintsRows, $complaints);

        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            ['complaint.create', 2, 'Create complaint'],
            ['complaint.read',   2, 'Read complaint'],
            ['complaint.update', 2, 'Update complaint'],
            ['complaint.delete', 2, 'Delete complaint'],

        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            // supervisor permissions
            ['Supervisor', 'complaint.create'],
            ['Supervisor', 'complaint.read'],
            ['Supervisor', 'complaint.update'],
            ['Supervisor', 'complaint.delete'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function safeDown()
    {
        $this->dropForeignKey('who_user_id', $this->tn_complaint);
        $this->dropForeignKey('whom_user_id', $this->tn_complaint);
        $this->dropTable($this->tn_complaint);

        $this->delete($this->tn_auth_item, "name = 'complaint.create'");
        $this->delete($this->tn_auth_item, "name = 'complaint.read'");
        $this->delete($this->tn_auth_item, "name = 'complaint.update'");
        $this->delete($this->tn_auth_item, "name = 'complaint.delete'");

        $this->delete($this->tn_auth_item_child, "child = 'complaint.create'");
        $this->delete($this->tn_auth_item_child, "child = 'complaint.read'");
        $this->delete($this->tn_auth_item_child, "child = 'complaint.update'");
        $this->delete($this->tn_auth_item_child, "child = 'complaint.delete'");
    }

}
