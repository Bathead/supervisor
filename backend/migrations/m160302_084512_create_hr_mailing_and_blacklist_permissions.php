<?php

use yii\db\Migration;

class m160302_084512_create_hr_mailing_and_blacklist_permissions extends Migration
{
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';
    protected $tn_auth_assignment = '{{%auth_assignment}}';

    public function safeUp()
    {
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            ['hrBlackList.create', 2, 'Create black list record'],
            ['hrBlackList.read',   2, 'Read black list record'],
            ['hrBlackList.update', 2, 'Update black list record'],
            ['hrBlackList.delete', 2, 'Delete black list record'],

            ['hrMailing.create', 2, 'Create mailing'],
            ['hrMailing.read',   2, 'Read mailing'],
            ['hrMailing.update', 2, 'Update mailing'],
            ['hrMailing.delete', 2, 'Delete mailing'],
        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            // supervisor permissions
            ['Supervisor', 'hrBlackList.create'],
            ['Supervisor', 'hrBlackList.read'],
            ['Supervisor', 'hrBlackList.update'],
            ['Supervisor', 'hrBlackList.delete'],
            ['Supervisor', 'hrMailing.create'],
            ['Supervisor', 'hrMailing.read'],
//            ['Supervisor', 'hrMailing.update'],
            ['Supervisor', 'hrMailing.delete'],

            // hr permissions
            ['Hr', 'hrBlackList.create'],
            ['Hr', 'hrBlackList.read'],
            ['Hr', 'hrBlackList.update'],
            ['Hr', 'hrBlackList.delete'],
            ['Hr', 'hrMailing.create'],
            ['Hr', 'hrMailing.read'],
//            ['Hr', 'hrMailing.update'],
            ['Hr', 'hrMailing.delete'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function safeDown()
    {
        $this->delete($this->tn_auth_item, "name = 'hrBlackList.create'");
        $this->delete($this->tn_auth_item, "name = 'hrBlackList.read'");
        $this->delete($this->tn_auth_item, "name = 'hrBlackList.update'");
        $this->delete($this->tn_auth_item, "name = 'hrBlackList.delete'");
        $this->delete($this->tn_auth_item, "name = 'hrMailing.create'");
        $this->delete($this->tn_auth_item, "name = 'hrMailing.read'");
//        $this->delete($this->tn_auth_item, "name = 'hrMailing.update'");
        $this->delete($this->tn_auth_item, "name = 'hrMailing.delete'");

        $this->delete($this->tn_auth_item_child, "child = 'hrBlackList.create'");
        $this->delete($this->tn_auth_item_child, "child = 'hrBlackList.read'");
        $this->delete($this->tn_auth_item_child, "child = 'hrBlackList.update'");
        $this->delete($this->tn_auth_item_child, "child = 'hrBlackList.delete'");
        $this->delete($this->tn_auth_item_child, "child = 'hrMailing.create'");
        $this->delete($this->tn_auth_item_child, "child = 'hrMailing.read'");
//        $this->delete($this->tn_auth_item_child, "child = 'hrMailing.update'");
        $this->delete($this->tn_auth_item_child, "child = 'hrMailing.delete'");
    }
}
