<?php

use yii\db\Migration;

class m160217_084015_add_additional_fields_to_profile_table extends Migration
{
    protected $tn_profile = '{{%profile}}';

    public function up()
    {
        $this->addColumn($this->tn_profile, 'email_home', $this->string());
        $this->addColumn($this->tn_profile, 'birthday', $this->date());
    }

    public function down()
    {
        $this->dropColumn($this->tn_profile, 'email_home');
        $this->dropColumn($this->tn_profile, 'birthday');
    }
}
