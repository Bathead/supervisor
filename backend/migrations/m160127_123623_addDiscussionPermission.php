<?php

use yii\db\Migration;
use app\modules\rbac\models\AuthItem;
use yii\rbac\Item;

class m160127_123623_addDiscussionPermission extends Migration
{
    public function safeUp()
    {
        /** @var \app\modules\rbac\components\DbManager $authManager */
        $authManager = Yii::$app->getAuthManager();

        $readRule = new \app\modules\rbac\models\BizRule(null, [
            'name' => 'isProjectDeveloper',
            'className' => '\app\modules\project\rules\CheckAssignedToProject'
        ]);
        $readRule->save();

        $permissionAll = new AuthItem(null, [
            'name' => 'discussion.all',
            'type' => Item::TYPE_PERMISSION,
            'description' => 'All discussion permission',
        ]);
        $permissionAll->save();

        $permissionRead = new AuthItem(null, [
            'name' => 'discussion.read',
            'type' => Item::TYPE_PERMISSION,
            'description' => 'Read discussion',
        ]);
        $permissionRead->save();

        $permissionAccess = new AuthItem(null, [
            'name' => 'discussion.hasAccess',
            'type' => Item::TYPE_PERMISSION,
            'description' => 'All discussion permission',
            'ruleName' => 'isProjectDeveloper'
        ]);
        $permissionAccess->save();

        $permissionAdd = new AuthItem(null, [
            'name' => 'discussion.add',
            'type' => Item::TYPE_PERMISSION,
            'description' => 'Add new message',
        ]);
        $permissionAdd->save();

        $permissionEdit = new AuthItem(null, [
            'name' => 'discussion.edit',
            'type' => Item::TYPE_PERMISSION,
            'description' => 'Edit message',
        ]);
        $permissionEdit->save();

        $permissionDelete = new AuthItem(null, [
            'name' => 'discussion.delete',
            'type' => Item::TYPE_PERMISSION,
            'description' => 'Delete message',
        ]);
        $permissionDelete->save();

        $permissionRestore = new AuthItem(null, [
            'name' => 'discussion.restore',
            'type' => Item::TYPE_PERMISSION,
            'description' => 'Restore message',
        ]);
        $permissionRestore->save();

        $authManager->addChild($permissionAll->getItem(), $permissionRead->getItem());
        $authManager->addChild($permissionAll->getItem(), $permissionAdd->getItem());
        $authManager->addChild($permissionAll->getItem(), $permissionEdit->getItem());
        $authManager->addChild($permissionAll->getItem(), $permissionDelete->getItem());
        $authManager->addChild($permissionAll->getItem(), $permissionRestore->getItem());
        $authManager->addChild($permissionAccess->getItem(), $permissionRead->getItem());


        $supervisorRole = $authManager->getRole('Supervisor');
        $pmRole = $authManager->getRole('Pm');
        $salesRole = $authManager->getRole('Sales');
        $developerRole = $authManager->getRole('Developer');

        $authManager->addChild($pmRole, $permissionAccess->getItem());
        $authManager->addChild($salesRole, $permissionAccess->getItem());
        $authManager->addChild($developerRole, $permissionAccess->getItem());
        $authManager->addChild($supervisorRole, $permissionAll->getItem());
    }

    public function safeDown()
    {
        /** @var \app\modules\rbac\components\DbManager $authManager */
        $authManager = Yii::$app->getAuthManager();

        $authManager->remove(new \app\modules\project\rules\CheckAssignedToProject);

        $list['all'] = $authManager->getPermission('discussion.all');
        $list['add'] = $authManager->getPermission('discussion.add');
        $list['read'] = $authManager->getPermission('discussion.read');
        $list['hasAccess'] = $authManager->getPermission('discussion.hasAccess');
        $list['delete'] = $authManager->getPermission('discussion.delete');
        $list['edit'] = $authManager->getPermission('discussion.edit');
        $list['restore'] = $authManager->getPermission('discussion.restore');

        $roleSupervisor = $authManager->getRole('Supervisor');
        $rolePM = $authManager->getItem('Pm');
        $roleDev = $authManager->getItem('Developer');
        $roleSales = $authManager->getItem('Sales');

        $authManager->removeChild($roleSupervisor, $list['all']);
        $authManager->removeChild($rolePM, $list['hasAccess']);
        $authManager->removeChild($roleDev, $list['hasAccess']);
        $authManager->removeChild($roleSales, $list['hasAccess']);

        foreach ($list as $permission) {
            $authManager->removeChildren($permission);
            $authManager->remove($permission);
        }

    }
}
