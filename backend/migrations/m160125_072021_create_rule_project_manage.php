<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\rbac;

class m160125_072021_create_rule_project_manage extends Migration
{
    protected $t_rule = '{{%auth_rule}}';
    protected $t_auth_item = '{{%auth_item}}';
    protected $t_auth_item_child = '{{%auth_item_child}}';
    protected $rule;

    public function __construct()
    {
        parent::__construct();
        $this->rule = new \app\modules\project\rules\ProjectManageRule();
    }

    public function up()
    {
        /*
         * Create ProjectManageRule
         */
        $this->insert($this->t_rule, [
           'name' => $this->rule->name,
           'data' => serialize($this->rule),
        ]);
        /*
         * Create permission "project.manage"
         */
        $this->insert($this->t_auth_item, [
          'name' => "project.manage",
          'type' => 2,
          'description' => 'User can manage his project',
          'rule_name' => $this->rule->name,
        ]);
        /*
         * Add permission to roles
         */
        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
          ['Sales', 'project.manage'],
          ['Pm', 'project.manage'],
          ['Supervisor', 'project.manage'],
        ];

        $this->batchInsert($this->t_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function down()
    {
        // delete rule "ProjectManage"
        $this->delete($this->t_rule, "name = '" . $this->rule->name . "'");
        // delete permission "project.manage"
        $this->delete($this->t_auth_item, "name = 'project.manage'");
        // delete permission "project.manage" from role
        $this->delete($this->t_auth_item_child, "parent = 'Sales' AND child = 'project.manage'");
        $this->delete($this->t_auth_item_child, "parent = 'Pm' AND child = 'project.manage'");
        $this->delete($this->t_auth_item_child, "parent = 'Supervisor' AND child = 'project.manage'");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
