<?php

use yii\db\Schema;
use yii\db\Migration;

class m160201_091458_create_news_comments_permissions extends Migration
{
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';

    public function safeUp()
    {
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            ['newsComments.create', 2, 'Create news comments'],
            ['newsComments.read',   2, 'Read news comments'],
            ['newsComments.update', 2, 'Update news comments'],
            ['newsComments.delete', 2, 'Delete news comments'],

        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            ['Supervisor', 'newsComments.create'],
            ['Supervisor', 'newsComments.read'],
            ['Supervisor', 'newsComments.update'],
            ['Supervisor', 'newsComments.delete'],

            ['Developer', 'newsComments.read'],
            ['Developer', 'newsComments.create'],

            ['Hr', 'newsComments.read'],
            ['Hr', 'newsComments.create'],

            ['Pm', 'newsComments.read'],
            ['Pm', 'newsComments.create'],

            ['Qa', 'newsComments.read'],
            ['Qa', 'newsComments.create'],

            ['Sales', 'newsComments.read'],
            ['Sales', 'newsComments.create'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function safeDown()
    {
        $this->delete($this->tn_auth_item, "name = 'newsComments.create'");
        $this->delete($this->tn_auth_item, "name = 'newsComments.read'");
        $this->delete($this->tn_auth_item, "name = 'newsComments.update'");
        $this->delete($this->tn_auth_item, "name = 'newsComments.delete'");

        $this->delete($this->tn_auth_item_child, "child = 'newsComments.create'");
        $this->delete($this->tn_auth_item_child, "child = 'newsComments.read'");
        $this->delete($this->tn_auth_item_child, "child = 'newsComments.update'");
        $this->delete($this->tn_auth_item_child, "child = 'newsComments.delete'");
    }

}
