<?php

use yii\db\Migration;

class m160225_081725_create_mailing_tables extends Migration
{
    protected $tn_hr_mailing = '{{%hr_mailing}}';
    protected $tn_hr_mailing_contact = '{{%hr_mailing_contact}}';
    protected $tn_hr_contact = '{{%hr_contact}}';
    protected $tn_user = '{{%user}}';

    public function up()
    {
        $this->createTable($this->tn_hr_mailing, [
            'id' => $this->primaryKey(),
            'description' => $this->text()->notNull(),
            'subject' => $this->text()->notNull(),
            'message' => $this->text()->notNull(),
            'from_name' => $this->text()->notNull(),
            'from_email' => $this->text()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'created_by_id' => $this->integer()
        ]);

        $this->addForeignKey('hr_mailing_created_by_id', $this->tn_hr_mailing, 'created_by_id', $this->tn_user, 'id', 'SET NULL', 'NO ACTION');

        $this->createTable($this->tn_hr_mailing_contact, [
            'mailing_id' => $this->integer()->notNull(),
            'email' => $this->string()->notNull(),
            'PRIMARY KEY (mailing_id, email)'
        ]);

        $this->addForeignKey('hr_mailing_mailing_id', $this->tn_hr_mailing_contact, 'mailing_id', $this->tn_hr_mailing, 'id', 'CASCADE', 'NO ACTION');
    }

    public function down()
    {
        $this->dropForeignKey('hr_mailing_created_by_id', $this->tn_hr_mailing);
        $this->dropForeignKey('hr_mailing_mailing_id', $this->tn_hr_mailing_contact);

        $this->dropTable($this->tn_hr_mailing);
        $this->dropTable($this->tn_hr_mailing_contact);
    }
}
