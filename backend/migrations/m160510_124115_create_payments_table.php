<?php

use yii\db\Migration;

/**
 * Handles the creation for table `payments_table`.
 */
class m160510_124115_create_payments_table extends Migration
{
    protected $tn_payment = '{{%payment}}';
    protected $tn_billing_card = '{{%billing_card}}';
    protected $tn_user = '{{%user}}';

    public function safeUp()
    {
        $this->createTable($this->tn_payment, [
            'id' => $this->primaryKey(),
            'receiver_id' => $this->integer()->notNull(),
            'payment_sum_uah' => $this->double()->defaultValue(0),
            'payment_sum_usd' => $this->double()->defaultValue(0),
            'bonuses_sum_usd' => $this->double()->defaultValue(0),
            'other_sum_usd' => $this->double()->defaultValue(0),
            'sum_uah' => $this->double()->defaultValue(0),
            'sum_usd' => $this->double()->defaultValue(0),
            'uah_billing_card_number' => $this->string(),
            'usd_billing_card_number' => $this->string(),
            'comment'=> $this->string(255),
            'status' => $this->integer(),
            'version' => $this->integer()->defaultValue(0),
            'parent' => $this->integer(),
            'period' => $this->date()->notNull(),
            'created_at' => $this->timestamp()->defaultValue(0),
            'updated_at' => $this->timestamp()->defaultValue(0)
        ]);

        $paymentRows = ['id', 'receiver_id', 'payment_sum_uah', 'payment_sum_usd', 'bonuses_sum_usd', 'other_sum_usd', 'sum_uah', 'sum_usd', 'uah_billing_card_number', 'usd_billing_card_number', 'comment', 'status', 'version', 'parent', 'period', 'created_at', 'updated_at'];
        $paymentData = [
            [1, 1, 2000, 200, 20, 0, 2000, 220, '1111-1111-1111-1111', '1111-1111-1111-1112', 'comment', 1, 0, null, '2016-05-01', '2016-05-07', '2016-05-07'],
            [2, 2, 10000, 0, 0, 0, 10000, 0, '2222-2222-2222-2221', '2222-2222-2222-2222', '', 1, 0, null, '2016-05-01', '2016-05-07', '2016-05-07'],
            [3, 3, 0, 1000, 50, 10, 0, 1060, '3333-3333-3333-3331', '3333-3333-3333-3332', '', 1, 0, null, '2016-05-01', '2016-05-07', '2016-05-07'],
            [4, 4, 4000, 300, 0, 0, 4000, 300, '4444-4444-4444-4441', '4444-4444-4444-4442', '', 2, 0, null, '2016-05-01', '2016-05-07', '2016-05-07'],
            [5, 5, 3000, 100, 80, 5, 3000, 185, '5555-5555-5555-5551', '5555-5555-5555-5552', '', 2, 0, null, '2016-05-01', '2016-05-07', '2016-05-07'],
            [6, 6, 8000, 500, 100, 70, 8000, 670, '6666-6666-6666-6661', '6666-6666-6666-6662', '', 1, 0, null, '2016-05-01', '2016-05-07', '2016-05-07'],
            [7, 7, 2000, 1250, 0, 0, 2000, 1250, '7777-7777-7777-7771', '7777-7777-7777-7772', '', 2, 0, null, '2016-05-01', '2016-05-07', '2016-05-07'],
        ];

        $this->batchInsert($this->tn_payment, $paymentRows, $paymentData);

        //$this->addForeignKey('fk_payment_uah_billing_card_number', $this->tn_payment, 'uah_billing_card_number', $this->tn_billing_card, 'number', 'CASCADE', 'NO ACTION');
        //$this->addForeignKey('fk_payment_usd_billing_card_number', $this->tn_payment, 'usd_billing_card_number', $this->tn_billing_card, 'number', 'CASCADE', 'NO ACTION');
        $this->addForeignKey('fk_payment_owner_id', $this->tn_payment, 'receiver_id', $this->tn_user, 'id', 'CASCADE', 'NO ACTION');
        $this->addForeignKey('fk_payment_parent', $this->tn_payment, 'parent', $this->tn_payment, 'id', 'CASCADE', 'NO ACTION');
    }

    public function safeDown()
    {
        //$this->dropForeignKey('fk_payment_uah_billing_card_number', $this->tn_payment);
        //$this->dropForeignKey('fk_payment_usd_billing_card_number', $this->tn_payment);
        $this->dropForeignKey('fk_payment_owner_id', $this->tn_payment);
        $this->dropForeignKey('fk_payment_parent', $this->tn_payment);

        $this->dropTable($this->tn_payment);
    }
}
