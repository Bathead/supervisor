<?php

use yii\db\Schema;
use yii\db\Migration;

class m160106_121754_add_new_permisions_for_technologies_currencies_languages extends Migration
{
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';

    public function up()
    {   
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            //currency permissions
            ['currency.create', 2, 'Create currency'],
            ['currency.read', 2, 'Read currency'],
            ['currency.update', 2, 'Update currency'],
            ['currency.delete', 2, 'Delete currency'],

            //technologies permissions
            ['technology.create', 2, 'Create technology'],
            ['technology.read', 2, 'Read technology'],
            ['technology.update', 2, 'Update technology'],
            ['technology.delete', 2, 'Delete technology'],
            ['technology.ratingManage', 2, 'User technology rating management'],

            //languages permissions
            ['language.create', 2, 'Create language'],
            ['language.read', 2, 'Read language'],
            ['language.update', 2, 'Update language'],
            ['language.delete', 2, 'Delete language'],
        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            // supervisor permissions
            ['Supervisor', 'currency.create'],
            ['Supervisor', 'currency.read'],
            ['Supervisor', 'currency.update'],
            ['Supervisor', 'currency.delete'],
            ['Supervisor', 'technology.create'],
            ['Supervisor', 'technology.read'],
            ['Supervisor', 'technology.update'],
            ['Supervisor', 'technology.delete'],
            ['Supervisor', 'technology.ratingManage'],
            ['Supervisor', 'language.create'],
            ['Supervisor', 'language.read'],
            ['Supervisor', 'language.update'],
            ['Supervisor', 'language.delete'],

            ['Developer', 'technology.read'],
            ['Sales',     'technology.read'],
            ['Qa',        'technology.read'],
            ['Pm',        'technology.read'],
            ['Hr',        'technology.read'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function down()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->execute('TRUNCATE TABLE ' . $this->tn_auth_item);
        $this->execute('TRUNCATE TABLE ' . $this->tn_auth_item_child);
    }
}
