<?php

use yii\db\Migration;

class m160728_060154_clean_db_for_production extends Migration
{
    protected $tn_auth_assignment       = '{{%auth_assignment}}';
    protected $tn_billing_card          = '{{%billing_card}}';
    protected $tn_complaint             = '{{%complaint}}';
    protected $tn_dialog                = '{{%dialog}}';
    protected $tn_dialog_message        = '{{%dialog_message}}';

    protected $tn_dialog_message_unread = '{{%dialog_message_unread}}';
    protected $tn_dialog_user           = '{{%dialog_user}}';
    protected $tn_hr_black_list         = '{{%hr_black_list}}';
    protected $tn_hr_contact            = '{{%hr_contact}}';
    protected $tn_hr_contact_technology = '{{%hr_contact_technology}}';

    protected $tn_hr_mailing            = '{{%hr_mailing}}';
    protected $tn_hr_mailing_contact    = '{{%hr_mailing_contact}}';
    protected $tn_news                  = '{{%news}}';
    protected $tn_news_comment          = '{{%news_comment}}';
    protected $tn_news_unread           = '{{%news_unread}}';

    protected $tn_payment               = '{{%payment}}';
    protected $tn_profile               = '{{%profile}}';
    protected $tn_project               = '{{%project}}';
    protected $tn_project_comm          = '{{%project_comm}}';
    protected $tn_project_developer     = '{{%project_developer}}';

    protected $tn_project_select        = '{{%project_select}}';
    protected $tn_project_technologies  = '{{%project_technologies}}';
    protected $tn_technology            = '{{%technology}}';
    protected $tn_ticket                = '{{%ticket}}';
    protected $tn_ticket_comment        = '{{%ticket_comment}}';

    protected $tn_user                  = '{{%user}}';
    protected $tn_user_technologies     = '{{%user_technologies}}';

    public function up()
    {

        if (YII_ENV_PROD) {
            $this->execute('SET FOREIGN_KEY_CHECKS = 0');

            $this->execute('TRUNCATE TABLE ' . $this->tn_auth_assignment);
            $this->execute('TRUNCATE TABLE ' . $this->tn_billing_card);
            $this->execute('TRUNCATE TABLE ' . $this->tn_complaint);
            $this->execute('TRUNCATE TABLE ' . $this->tn_dialog);
            $this->execute('TRUNCATE TABLE ' . $this->tn_dialog_message);

            $this->execute('TRUNCATE TABLE ' . $this->tn_dialog_message_unread);
            $this->execute('TRUNCATE TABLE ' . $this->tn_dialog_user);
            $this->execute('TRUNCATE TABLE ' . $this->tn_hr_black_list);
            $this->execute('TRUNCATE TABLE ' . $this->tn_hr_contact);
            $this->execute('TRUNCATE TABLE ' . $this->tn_hr_contact_technology);

            $this->execute('TRUNCATE TABLE ' . $this->tn_hr_mailing);
            $this->execute('TRUNCATE TABLE ' . $this->tn_hr_mailing_contact);
            $this->execute('TRUNCATE TABLE ' . $this->tn_news);
            $this->execute('TRUNCATE TABLE ' . $this->tn_news_comment);
            $this->execute('TRUNCATE TABLE ' . $this->tn_news_unread);

            $this->execute('TRUNCATE TABLE ' . $this->tn_payment);
            $this->execute('TRUNCATE TABLE ' . $this->tn_profile);
            $this->execute('TRUNCATE TABLE ' . $this->tn_project);
            $this->execute('TRUNCATE TABLE ' . $this->tn_project_comm);
            $this->execute('TRUNCATE TABLE ' . $this->tn_project_developer);

            $this->execute('TRUNCATE TABLE ' . $this->tn_project_select);
            $this->execute('TRUNCATE TABLE ' . $this->tn_project_technologies);
            $this->execute('TRUNCATE TABLE ' . $this->tn_technology);
            $this->execute('TRUNCATE TABLE ' . $this->tn_ticket);
            $this->execute('TRUNCATE TABLE ' . $this->tn_ticket_comment);

            $this->execute('TRUNCATE TABLE ' . $this->tn_user);
            $this->execute('TRUNCATE TABLE ' . $this->tn_user_technologies);


            // add test supervisor account

            // create profile

            // add profiles for test accounts
            $profileRows = [
                'id',
                'first_name',
                'last_name',
            ];
            $profiles = [
                [
                    1,
                    'sup first name',
                    'sup last name'
                ]
            ];
            $this->batchInsert($this->tn_profile, $profileRows, $profiles);

            // generate default password
            $pass = 'asdf';
            $passHash = Yii::$app->security->generatePasswordHash($pass);

            // add user
            $userRows = ['id', 'email', 'username', 'password', 'auth_key', 'access_token', 'profile_id'];
            $users = [
                [1, 'supervisor@mail.com', 'sup',    $passHash, 'v3BvS-yYAS4AUEknZ64rQVadzXHW3dVa', 'v3Bvz-yYAS4AUEknZ64rQVadzXHW3dVa', 1],
            ];
            $this->batchInsert($this->tn_user, $userRows, $users);

            // assign supervisor role
            $userRolesRows = ['item_name', 'user_id'];
            $userRoles = [
                ['Supervisor',       1],
            ];
            $this->batchInsert($this->tn_auth_assignment, $userRolesRows, $userRoles);
        }
    }

    public function down()
    {
    }
}
