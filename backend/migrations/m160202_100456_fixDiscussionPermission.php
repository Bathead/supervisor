<?php

use yii\db\Schema;
use yii\db\Migration;

class m160202_100456_fixDiscussionPermission extends Migration
{

    public function safeUp()
    {
        /** @var \app\modules\rbac\components\DbManager $auth */
        $auth = Yii::$app->authManager;

        $permissionAccess = $auth->getPermission('discussion.hasAccess');

        $permissionAdd = $auth->getPermission('discussion.add');
        $auth->addChild($permissionAccess, $permissionAdd);

        $permissionEdit = $auth->getPermission('discussion.edit');
        $auth->addChild($permissionAdd, $permissionEdit);

        $permissionDelete = $auth->getPermission('discussion.delete');
        $auth->addChild($permissionAdd, $permissionDelete);

        $permissionRestore = $auth->getPermission('discussion.restore');
        $auth->addChild($permissionAdd, $permissionRestore);
    }

    public function safeDown()
    {
        /** @var \app\modules\rbac\components\DbManager $auth */
        $auth = Yii::$app->authManager;

        $permissionAccess = $auth->getPermission('discussion.hasAccess');

        $permissionAdd = $auth->getPermission('discussion.add');
        $auth->removeChild($permissionAccess, $permissionAdd);

        $permissionEdit = $auth->getPermission('discussion.edit');
        $auth->removeChild($permissionAdd, $permissionEdit);

        $permissionDelete = $auth->getPermission('discussion.delete');
        $auth->removeChild($permissionAdd, $permissionDelete);

        $permissionRestore = $auth->getPermission('discussion.restore');
        $auth->removeChild($permissionAdd, $permissionRestore);
    }
}
