<?php

use yii\db\Schema;
use yii\db\Migration;

class m160104_133203_create_profile_table extends Migration
{
    protected $tn_user = '{{%user}}';
    protected $tn_profile = '{{%profile}}';
    protected $tn_rank = '{{%rank}}';
    protected $tn_currency = '{{%currency}}';

    public function up()
    {
        $this->createTable($this->tn_profile, [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'middle_name' => $this->string(),
            'phone' => $this->string(),
            'skype' => $this->string(),
            'icq' => $this->string(),
            'monthly_rate' => $this->double()->defaultValue(0),
            'monthly_rate_currency_id' => $this->integer(),
            'external_hourly_rate' => $this->double()->defaultValue(0),
            'external_hourly_rate_currency_id' => $this->integer(),
            'internal_hourly_rate' => $this->double()->defaultValue(0),
            'internal_hourly_rate_currency_id' => $this->integer(),
            'rating' => $this->double(),
            'rank_id' => $this->integer(),
            'status' => $this->integer(),
            'status_description' => $this->string(64),
            'status_free_start_date' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL ',
            'status_free_oriented_date' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT NULL ',
        ]);

        $this->addColumn($this->tn_user, 'profile_id', $this->integer());
        $this->addForeignKey('user_profile_id', $this->tn_user, 'profile_id', $this->tn_profile, 'id', 'NO ACTION', 'NO ACTION');

        $this->addForeignKey('profile_rank_id', $this->tn_profile, 'rank_id', $this->tn_rank, 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('profile_monthly_rate_currency_id', $this->tn_profile, 'monthly_rate_currency_id', $this->tn_currency, 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('profile_external_hourly_rate_currency_id', $this->tn_profile, 'external_hourly_rate_currency_id', $this->tn_currency, 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('profile_internal_hourly_rate_currency_id', $this->tn_profile, 'internal_hourly_rate_currency_id', $this->tn_currency, 'id', 'NO ACTION', 'NO ACTION');

        // add profiles for test accounts
        $profileRows = [
            'id',
            'first_name',
            'last_name',
            'middle_name',
            'phone',
            'skype',
            'icq',
            'monthly_rate',
            'monthly_rate_currency_id',
            'external_hourly_rate',
            'external_hourly_rate_currency_id',
            'internal_hourly_rate',
            'internal_hourly_rate_currency_id',
            'rating',
            'rank_id',
            'status',
            'status_description',
            'status_free_start_date',
            'status_free_oriented_date'
        ];
        $profiles = [
            [
                1,
                'supervisor first name',
                'supervisor last name',
                'supervisor middle name',
                'supervisor phone',
                'supervisor skype',
                'supervisor icq',
                300,
                1,
                2,
                1,
                13,
                2,
                10,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
            ],
            [
                2,
                'sysadmin first name',
                'sysadmin last name',
                'sysadmin middle name',
                'sysadmin phone',
                'sysadmin skype',
                'sysadmin icq',
                300,
                1,
                2,
                1,
                15,
                2,
                10,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
            ],
            [
                3,
                'developer first name',
                'developer last name',
                'developer middle name',
                'developer phone',
                'developer skype',
                'developer icq',
                7000,
                2,
                2,
                1,
                10,
                2,
                10,
                2,
                NULL,
                NULL,
                NULL,
                NULL
            ],
            [
                4,
                'sales first name',
                'sales last name',
                'sales middle name',
                'sales phone',
                'sales skype',
                'sales icq',
                200,
                1,
                1,
                1,
                20,
                2,
                9,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
            ],
            [
                5,
                'pm first name',
                'pm last name',
                'pm middle name',
                'pm phone',
                'pm skype',
                'pm icq',
                200,
                1,
                1,
                1,
                15,
                2,
                9,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
            ],
            [
                6,
                'hr first name',
                'hr last name',
                'hr middle name',
                'hr phone',
                'hr skype',
                'hr icq',
                200,
                1,
                1,
                1,
                14,
                2,
                9,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
            ],
            [
                7,
                'qa first name',
                'qa last name',
                'qa middle name',
                'qa phone',
                'qa skype',
                'qa icq',
                200,
                1,
                1,
                1,
                18,
                2,
                9,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
            ],
        ];
        $this->batchInsert($this->tn_profile, $profileRows, $profiles);

        $this->update($this->tn_user, ['profile_id' => 1], 'id = 1');
        $this->update($this->tn_user, ['profile_id' => 2], 'id = 2');
        $this->update($this->tn_user, ['profile_id' => 3], 'id = 3');
        $this->update($this->tn_user, ['profile_id' => 4], 'id = 4');
        $this->update($this->tn_user, ['profile_id' => 5], 'id = 5');
        $this->update($this->tn_user, ['profile_id' => 6], 'id = 6');
        $this->update($this->tn_user, ['profile_id' => 7], 'id = 7');

    }

    public function down()
    {
        $this->dropForeignKey('user_profile_id', $this->tn_user);
        $this->dropColumn($this->tn_user, 'profile_id');

        $this->dropForeignKey('profile_rank_id', $this->tn_profile);
        $this->dropForeignKey('profile_monthly_rate_currency_id', $this->tn_profile);
        $this->dropForeignKey('profile_external_hourly_rate_currency_id', $this->tn_profile);
        $this->dropForeignKey('profile_internal_hourly_rate_currency_id', $this->tn_profile);
        $this->dropTable($this->tn_profile);
    }
}
