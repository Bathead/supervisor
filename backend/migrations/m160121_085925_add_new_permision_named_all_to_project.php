<?php

use yii\db\Schema;
use yii\db\Migration;

class m160121_085925_add_new_permision_named_all_to_project extends Migration
{
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';

    public function up()
    {   
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            // user permissions
            ['project.all', 2, 'Full access'],
        ];

        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            // supervisor permissions
            ['Supervisor', 'project.all'],
        ];

        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function down()
    {
        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'project.all'");
        $this->delete($this->tn_auth_item, "name = 'project.all' AND type = '2' AND description = 'Full access'");
    }
}
