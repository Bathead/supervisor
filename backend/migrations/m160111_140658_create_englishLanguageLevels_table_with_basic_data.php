<?php

use yii\db\Schema;
use yii\db\Migration;
use app\modules\englishLanguageLevel\models\EnglishLanguageLevel;

class m160111_140658_create_englishLanguageLevels_table_with_basic_data extends Migration
{
    protected $tn_englishLanguageLevel = '{{%english_language_level}}';

    public function safeUp()
    {
        $this->createTable($this->tn_englishLanguageLevel, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()
        ]);

        $this->insert($this->tn_englishLanguageLevel, array('name' => 'Beginner'));
        $this->insert($this->tn_englishLanguageLevel, array('name' => 'Elementary'));
        $this->insert($this->tn_englishLanguageLevel, array('name' => 'Pre-Intermediate'));
        $this->insert($this->tn_englishLanguageLevel, array('name' => 'Intermediate'));
        $this->insert($this->tn_englishLanguageLevel, array('name' => 'Upper intermediate'));
        $this->insert($this->tn_englishLanguageLevel, array('name' => 'Advanced'));
        $this->insert($this->tn_englishLanguageLevel, array('name' => 'Proficient'));
    }

    public function safeDown()
    {
        $this->dropTable($this->tn_englishLanguageLevel);
    }
}
