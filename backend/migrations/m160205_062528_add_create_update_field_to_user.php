<?php

use yii\db\Schema;
use yii\db\Migration;

class m160205_062528_add_create_update_field_to_user extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'created_at', $this->dateTime());
        $this->addColumn('user', 'updated_at', $this->dateTime());
    }

    public function down()
    {
        $this->dropColumn('user', 'created_at');
        $this->dropColumn('user', 'updated_at');
    }
}
