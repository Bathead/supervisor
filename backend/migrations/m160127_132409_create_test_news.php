<?php

use yii\db\Schema;
use yii\db\Migration;
use app\modules\news\models\News;

class m160127_132409_create_test_news extends Migration
{
    protected $tn_news = '{{%news}}';
    protected $tn_news_unread = '{{%news_unread}}';

    public function safeUp()
    {
        $newsRows = ['id', 'title', 'excerpt', 'content', 'author_id', 'type'];
        $news = [
            [
                1,
                'Yii 2.0.4 is released',
                'We are very pleased to announce the release of Yii Framework version 2.0.4.
                Please refer to the instructions at http://www.yiiframework.com/download/ to install
                or upgrade to this version.',
                'Version 2.0.4 is a patch release of Yii 2.0 which contains over 100 minor new features
                and bug fixes. It contains a fix for the security issue CVE-2015-3397 that may allow XSS
                attacks to IE6/IE7 users. For this reason, you are urged to upgrade to this version if your
                applications are required to support IE6/IE7. We have added a new helper method
                yii\helpers\Json::htmlEncode() which should be used instead of Json::encode() if you use the result
                in Javascript code that is embedded in a HTML page to avoid the problem described above.
                The release further adds a new core validator named EachValidator which allows you to validate
                every element in an array input. A new method named attributeHints() is added to yii\base\Model,
                which let you to configure input hints (in addition to input labels). And finally you can use indexBy()
                together with column() when performing database query with yii\db\Query. It will return an array of
                scalar values indexed by the specified column or computed indices. There are many other useful additions
                and changes to this release. Please refer to the core framework change log and the extension change logs.',
                1,
                News::TYPE_IMPORTANT
            ],
            [
                2,
                'Laravel 5.1.11 introduces ACL system',
                'Another great news for Laravel community – more and more often repeated functions become a part of
                Laravel framework itself. Today a new addition is Authorization or ACL functionality.',
                'Already retweeted and favorited by hundreds of Laravel fans, this feature adds some new functionality
                to Auth mechanism. Here are just a few example from new official documentation:
                <br/>
                New Gate facade:
                <br/>
                if (Gate::forUser($user)->allows(\'update-post\', $post)) {
                <br/>
                    //
                <br/>
                }<br/>
                if (Gate::forUser($user)->allows(\'update-post\', $post)) {
                <br/>
                    //
                <br/>
                }<br/>
                Using User model in request:
                <br/>
                if ($request->user()->can(\'update-post\', $post)) {
                <br/>
                    //
                <br/>
                }<br/>
                if ($request->user()->can(\'update-post\', $post)) {
                <br/>
                    //
                <br/>
                }<br/>
                ...',
                1,
                News::TYPE_USUAL
            ],
        ];
        $this->batchInsert($this->tn_news, $newsRows, $news);

        $unreadNews = [
            [1, 1],
            [1, 3],
            [1, 4],
            [1, 5],
            [1, 6],
            [1, 7],
        ];
        $this->batchInsert($this->tn_news_unread, ['news_id', 'user_id'], $unreadNews);
    }

    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->execute('TRUNCATE TABLE ' . $this->tn_news);
        $this->execute('TRUNCATE TABLE ' . $this->tn_news_unread);
    }

}
