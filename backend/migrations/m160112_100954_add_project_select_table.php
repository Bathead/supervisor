<?php

use yii\db\Schema;
use yii\db\Migration;

class m160112_100954_add_project_select_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%project_select}}', [
            'id' => $this->primaryKey(),
            'id_project' => $this->integer()->notNull(),
            'id_user' => $this->integer()->notNull(),
            'time' => $this->string(),
            'reject_reason' => $this->string(),
            'evaluate' => $this->integer(),
        ]);

        $this->addForeignKey('fk_project_select_user', '{{%project_select}}', '[[id_user]]', '{{%user}}', '[[id]]',
            'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_project_select_project', '{{%project_select}}', '[[id_project]]', '{{%project}}', '[[id]]',
            'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%project_select}}');
    }
}
