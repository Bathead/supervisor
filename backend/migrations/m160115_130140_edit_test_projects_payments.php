<?php

use yii\db\Schema;
use yii\db\Migration;

class m160115_130140_edit_test_projects_payments extends Migration
{
    public function up()
    {
        $this->execute("UPDATE project SET payment_method = '1' WHERE payment_method = 'Погодинно'");
    }

    public function down()
    {
        $this->execute("UPDATE project SET payment_method = 'Погодинно' WHERE payment_method = '1'");
    }
}
