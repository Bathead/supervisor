<?php

use yii\db\Schema;
use yii\db\Migration;

class m160215_072855_add_department_permission extends Migration
{
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';
    protected $tn_auth_assignment = '{{%auth_assignment}}';

    public function safeUp()
    {
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            // rank permissions
            ['department.create', 2, 'Create department'],
            ['department.read',   2, 'Read department'],
            ['department.update', 2, 'Update department'],
            ['department.delete', 2, 'Delete department'],
        ];

        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            ['Supervisor', 'department.create'],
            ['Supervisor', 'department.read'],
            ['Supervisor', 'department.update'],
            ['Supervisor', 'department.delete'],

            ['Developer', 'department.read'],

            ['Pm', 'department.read'],

            ['Qa', 'department.read'],

            ['Sales', 'department.read'],

            ['Hr', 'department.read']
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function safeDown()
    {
        $this->delete($this->tn_auth_item, "name = 'department.create'");
        $this->delete($this->tn_auth_item, "name = 'department.read'");
        $this->delete($this->tn_auth_item, "name = 'department.update'");
        $this->delete($this->tn_auth_item, "name = 'department.delete'");

        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'department.create'");
        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'department.read'");
        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'department.update'");
        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'department.delete'");

        $this->delete($this->tn_auth_item_child, "parent = 'Developer' AND child = 'department.read'");

        $this->delete($this->tn_auth_item_child, "parent = 'Pm' AND child = 'department.read'");

        $this->delete($this->tn_auth_item_child, "parent = 'Qa' AND child = 'department.read'");

        $this->delete($this->tn_auth_item_child, "parent = 'Sales' AND child = 'department.read'");

        $this->delete($this->tn_auth_item_child, "parent = 'Hr' AND child = 'department.read'");
    }
}
