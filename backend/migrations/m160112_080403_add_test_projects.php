<?php

use yii\db\Schema;
use yii\db\Migration;

class m160112_080403_add_test_projects extends Migration
{
    public function up()
    { 
        $this->insert('project',array(
            'id' => '1',
            'id_sales' => '4',
            'title'=>'First Project',
            'description' => 'description of the project number 1',
            'payment_method' => 'Погодинно',
            'start_date' => '2016-01-06',
            'end_date' => '2016-01-09',
            'internal_price' => '12',
            'external_price' => '20',
            'hours_developers' => '5',
            'hours_sales' => '5',
            'hours_pm' => '5',
            'hours_qa' => '5',
            'publish_date' => '2016-01-06 12:01:40',
            'status' => 'Активні',
            'version' => '0',
        ));

        $this->insert('project',array(
            'id' => '2',
            'id_sales' => '4',
            'title'=>'Second',
            'description' => 'second one description',
            'payment_method' => 'Погодинно',
            'start_date' => '2016-01-07',
            'end_date' => '2016-01-12',
            'internal_price' => '12',
            'external_price' => '20',
            'hours_developers' => '5',
            'hours_sales' => '5',
            'hours_pm' => '5',
            'hours_qa' => '5',
            'publish_date' => '2016-01-06 14:02:39',
            'status' => 'Активні',
            'version' => '0',
        ));

        $this->insert('project',array(
            'id' => '3',
            'id_sales' => '4',
            'title'=>'Кращий проект',
            'description' => 'опис найкращого проекту',
            'payment_method' => 'Погодинно',
            'start_date' => '2016-01-08',
            'end_date' => '2016-01-30',
            'internal_price' => '11',
            'external_price' => '54',
            'hours_developers' => '6',
            'hours_sales' => '7',
            'hours_pm' => '8',
            'hours_qa' => '9',
            'publish_date' => '2016-01-06 15:23:15',
            'status' => 'Активні',
            'version' => '0',
        ));

        $this->insert('project',array(
            'id' => '4',
            'id_sales' => '4',
            'title'=>'gsdfh',
            'description' => 'fdg  f gb',
            'payment_method' => 'Погодинно',
            'start_date' => '2016-01-04',
            'end_date' => '2016-01-28',
            'internal_price' => '15',
            'external_price' => '524',
            'hours_developers' => '3',
            'hours_sales' => '2',
            'hours_pm' => '5',
            'hours_qa' => '7',
            'publish_date' => '2016-01-06 15:25:15',
            'status' => 'Активні',
            'version' => '0',
        ));


        $this->insert('project_developer', array(
            'id' => '1',
            'id_project' =>'1',
            'id_developer' => '3',
        ));
    }

    public function down()
    {
        $this->execute('DELETE FROM project_developer WHERE id = 1');
        $this->execute('DELETE FROM project WHERE id = 1 OR id = 2 OR id = 3 OR id = 4');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
