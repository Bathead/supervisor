<?php

use yii\db\Migration;

class m160302_104542_additional_tickets_permissions extends Migration
{
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';

    public function up()
    {
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            // ticket permissions
            ['ticket.update', 2, 'Can update ticket'],
            ['ticket.delete', 2, 'Can delete ticket'],

        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            ['Developer', 'ticket.update'],
            ['Developer', 'ticket.delete'],
            ['Hr', 'ticket.update'],
            ['Hr', 'ticket.delete'],
            ['Pm', 'ticket.update'],
            ['Pm', 'ticket.delete'],
            ['Qa', 'ticket.update'],
            ['Qa', 'ticket.delete'],
            ['Sales', 'ticket.update'],
            ['Sales', 'ticket.delete'],
            ['Supervisor', 'ticket.update'],
            ['Supervisor', 'ticket.delete'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);


    }

    public function down()
    {
        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'ticket.delete'");
        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'ticket.update'");
        $this->delete($this->tn_auth_item_child, "parent = 'Sales' AND child = 'ticket.delete'");
        $this->delete($this->tn_auth_item_child, "parent = 'Sales' AND child = 'ticket.update'");
        $this->delete($this->tn_auth_item_child, "parent = 'Qa' AND child = 'ticket.delete'");
        $this->delete($this->tn_auth_item_child, "parent = 'Qa' AND child = 'ticket.update'");
        $this->delete($this->tn_auth_item_child, "parent = 'Pm' AND child = 'ticket.delete'");
        $this->delete($this->tn_auth_item_child, "parent = 'Pm' AND child = 'ticket.update'");
        $this->delete($this->tn_auth_item_child, "parent = 'Hr' AND child = 'ticket.delete'");
        $this->delete($this->tn_auth_item_child, "parent = 'Hr' AND child = 'ticket.update'");
        $this->delete($this->tn_auth_item_child, "parent = 'Developer' AND child = 'ticket.delete'");
        $this->delete($this->tn_auth_item_child, "parent = 'Developer' AND child = 'ticket.update'");

        $this->delete($this->tn_auth_item, "name = 'ticket.delete' AND type = '2' AND description = 'Can delete ticket'");
        $this->delete($this->tn_auth_item, "name = 'ticket.update' AND type = '2' AND description = 'Can update ticket'");
    }
}
