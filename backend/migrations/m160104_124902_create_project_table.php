<?php

use yii\db\Schema;
use yii\db\Migration;

class m160104_124902_create_project_table extends Migration
{
    public function up()
    {
        $this->createTable('project', [
            'id' => Schema::TYPE_PK,
            'id_sales' => Schema::TYPE_INTEGER . ' NOT NULL',// Schema::TYPE_INTEGER . ' NOT NULL',
            'title' => Schema::TYPE_STRING,
            'description' => Schema::TYPE_TEXT,
            'payment_method' => Schema::TYPE_STRING,
            'start_date' => $this->date(),
            'end_date' => $this->date(),
            'internal_price' => $this->decimal(10, 2),
            'external_price' => $this->decimal(10, 2),
            'hours_developers' => $this->decimal(10, 2),
            'hours_sales' => $this->decimal(10, 2),
            'hours_pm' => $this->decimal(10, 2),
            'hours_qa' => $this->decimal(10, 2),
            'publish_date' => $this->dateTime(),
            'status' => Schema::TYPE_STRING,
            'version' => Schema::TYPE_INTEGER . ' NOT NULL',
            'parent' => Schema::TYPE_INTEGER,
        ]);

        $this->addForeignKey('FK_project_sale_id', 'project', 'id_sales', 'user', 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('FK_project_parent', 'project', 'parent', 'project', 'id', 'CASCADE', 'NO ACTION');

        $this->execute("ALTER TABLE project CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;");
    }

    public function down()
    {
        $this->dropForeignKey('FK_project_sale_id', 'project');
        $this->dropForeignKey('FK_project_parent', 'project');
        $this->dropTable('project');
    }

}
