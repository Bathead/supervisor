<?php

use yii\db\Schema;
use yii\db\Migration;
use app\modules\user\models\User;
use app\modules\user\models\Profile;
use app\modules\technology\models\Technology;
use app\modules\currency\models\Currency;
use app\modules\technology\models\UserTechnology;

class m160118_093853_profile_and_currency_foreign_keys_fixing extends Migration
{
    protected $tn_user = '{{%user}}';
    protected $tn_profile = '{{%profile}}';
    protected $tn_currency = '{{%currency}}';
    protected $tn_technology = '{{%technology}}';
    protected $tn_user_technologies = '{{%user_technologies}}';

    public function safeUp()
    {
        $this->dropForeignKey('profile_external_hourly_rate_currency_id', $this->tn_profile);
        $this->dropForeignKey('profile_internal_hourly_rate_currency_id', $this->tn_profile);
        $this->dropForeignKey('profile_monthly_rate_currency_id', $this->tn_profile);
        $this->dropForeignKey('technology_user_id', $this->tn_user_technologies);
        $this->dropForeignKey('technology_id', $this->tn_user_technologies);

        $this->addForeignKey('profile_monthly_rate_currency_id', $this->tn_profile, 'monthly_rate_currency_id', $this->tn_currency, 'id', 'SET NULL', 'NO ACTION');
        $this->addForeignKey('profile_external_hourly_rate_currency_id', $this->tn_profile, 'external_hourly_rate_currency_id', $this->tn_currency, 'id', 'SET NULL', 'NO ACTION');
        $this->addForeignKey('profile_internal_hourly_rate_currency_id', $this->tn_profile, 'internal_hourly_rate_currency_id', $this->tn_currency, 'id', 'SET NULL', 'NO ACTION');
        $this->addForeignKey('technology_user_id', $this->tn_user_technologies, 'user_id', $this->tn_user, 'id', 'RESTRICT', 'NO ACTION');
        $this->addForeignKey('technology_id', $this->tn_user_technologies, 'technology_id', $this->tn_technology, 'id', 'RESTRICT', 'NO ACTION');
    }

    public function safeDown()
    {
        $this->dropForeignKey('profile_external_hourly_rate_currency_id', $this->tn_profile);
        $this->dropForeignKey('profile_internal_hourly_rate_currency_id', $this->tn_profile);
        $this->dropForeignKey('profile_monthly_rate_currency_id', $this->tn_profile);
        $this->dropForeignKey('technology_user_id', $this->tn_user_technologies);
        $this->dropForeignKey('technology_id', $this->tn_user_technologies);

        $this->addForeignKey('profile_monthly_rate_currency_id', $this->tn_profile, 'monthly_rate_currency_id', $this->tn_currency, 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('profile_external_hourly_rate_currency_id', $this->tn_profile, 'external_hourly_rate_currency_id', $this->tn_currency, 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('profile_internal_hourly_rate_currency_id', $this->tn_profile, 'internal_hourly_rate_currency_id', $this->tn_currency, 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('technology_user_id', $this->tn_user_technologies, 'user_id', $this->tn_user, 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('technology_id', $this->tn_user_technologies, 'technology_id', $this->tn_technology, 'id', 'NO ACTION', 'NO ACTION');
    }

}
