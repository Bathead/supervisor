<?php

use yii\db\Schema;
use yii\db\Migration;

class m160128_062529_create_ticket_table extends Migration
{
    public function up()
    {
        $this->createTable('ticket', [
            'id' => Schema::TYPE_PK,
            'author_id' => $this->integer(),
            'title' => Schema::TYPE_STRING,
            'description' => Schema::TYPE_TEXT,
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'status' => Schema::TYPE_STRING,
            'priority' => Schema::TYPE_STRING,
            'file_path' => Schema::TYPE_STRING,
        ]);

        $this->addForeignKey('FK_ticket_author_id', 'ticket', 'author_id', 'user', 'id', 'SET NULL', 'SET NULL');
    }

    public function down()
    {
        $this->dropForeignKey('FK_ticket_author_id', 'ticket');
        $this->dropTable('ticket');
    }
}
