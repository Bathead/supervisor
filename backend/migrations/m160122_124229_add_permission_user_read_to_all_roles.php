<?php

use yii\db\Schema;
use yii\db\Migration;

class m160122_124229_add_permission_user_read_to_all_roles extends Migration
{
    protected $t_auth_item_child = '{{%auth_item_child}}';

    public function up()
    {
        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
          ['Sales', 'user.read'],
          ['Qa', 'user.read'],
          ['Pm', 'user.read'],
          ['Developer', 'user.read'],
          ['Hr', 'user.read'],
        ];
        $this->batchInsert($this->t_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function down()
    {
        // delete permission "user.read" from role
        $this->delete($this->t_auth_item_child, "parent = 'Sales' AND child = 'user.read'");
        $this->delete($this->t_auth_item_child, "parent = 'Qa' AND child = 'user.read'");
        $this->delete($this->t_auth_item_child, "parent = 'Pm' AND child = 'user.read'");
        $this->delete($this->t_auth_item_child, "parent = 'Developer' AND child = 'user.read'");
        $this->delete($this->t_auth_item_child, "parent = 'Hr' AND child = 'user.read'");
    }

}
