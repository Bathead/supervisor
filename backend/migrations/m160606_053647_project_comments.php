<?php

use yii\db\Migration;

class m160606_053647_project_comments extends Migration
{
    protected $tn_user = '{{%user}}';
    protected $tn_project = '{{%project}}';
    protected $tn_project_comm = '{{%project_comm}}';

    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';

    public function up()
    {
        // table
        $this->createTable($this->tn_project_comm, [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer(),
            'user_id' => $this->integer(),
            'message' => $this->text(),
            'parent_id' => $this->integer()->defaultValue(null),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp(),
            'is_deleted' => $this->boolean()->defaultValue(0)
        ]);
        // foreign keys
        $this->addForeignKey('fk_proj_id', $this->tn_project_comm, 'project_id', $this->tn_project, 'id', 'CASCADE', 'NO ACTION');
        $this->addForeignKey('fk_project_comm_user_id', $this->tn_project_comm, 'user_id', $this->tn_user, 'id', 'CASCADE', 'NO ACTION');

        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            ['projectComments.create', 2, 'Create project comments'],
            ['projectComments.read',   2, 'Read project comments'],
            ['projectComments.update', 2, 'Update project comments'],
            ['projectComments.delete', 2, 'Delete project comments'],

        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);
        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            ['Supervisor', 'projectComments.create'],
            ['Supervisor', 'projectComments.read'],
            ['Supervisor', 'projectComments.update'],
            ['Supervisor', 'projectComments.delete'],

            ['Developer', 'projectComments.read'],
            ['Developer', 'projectComments.create'],

            ['Sales', 'projectComments.read'],
            ['Sales', 'projectComments.create'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function down()
    {
        $this->dropForeignKey('fk_proj_id', $this->tn_project_comm);
        $this->dropForeignKey('fk_project_comm_user_id', $this->tn_project_comm);
        $this->dropTable($this->tn_project_comm);

        $this->delete($this->tn_auth_item, "name = 'projectComments.create'");
        $this->delete($this->tn_auth_item, "name = 'projectComments.read'");
        $this->delete($this->tn_auth_item, "name = 'projectComments.update'");
        $this->delete($this->tn_auth_item, "name = 'projectComments.delete'");
        $this->delete($this->tn_auth_item_child, "child = 'projectComments.create'");
        $this->delete($this->tn_auth_item_child, "child = 'projectComments.read'");
        $this->delete($this->tn_auth_item_child, "child = 'projectComments.update'");
        $this->delete($this->tn_auth_item_child, "child = 'projectComments.delete'");
    }

}
