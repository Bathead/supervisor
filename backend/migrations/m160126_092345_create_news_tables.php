<?php

use yii\db\Schema;
use yii\db\Migration;

class m160126_092345_create_news_tables extends Migration
{
    protected $tn_user = '{{%user}}';
    protected $tn_news = '{{%news}}';
    protected $tn_news_comment = '{{%news_comment}}';
    protected $tn_news_unread = '{{%news_unread}}';
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';

    public function safeUp()
    {
        // tables

        $this->createTable($this->tn_news, [
            'id' => $this->primaryKey(),
            'title' => $this->text(),
            'excerpt' => $this->text(),
            'content' => $this->text(),
            'author_id' => $this->integer(),
            'created_at' => $this->timestamp()->notNull(),
            'type' => $this->integer(),
        ]);

        $this->createTable($this->tn_news_comment, [
            'id' => $this->primaryKey(),
            'news_id' => $this->integer(),
            'user_id' => $this->integer(),
            'message' => $this->text(),
            'parent_id' => $this->integer()->defaultValue(null),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp(),
            'is_deleted' => $this->boolean()->defaultValue(0)
        ]);

        $this->createTable($this->tn_news_unread, [
            'news_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'PRIMARY KEY (news_id, user_id)'
        ]);

        // foreign keys

        $this->addForeignKey('fk_news_author_id', $this->tn_news, 'author_id', $this->tn_user, 'id', 'SET NULL', 'NO ACTION');

        $this->addForeignKey('fk_news_id', $this->tn_news_comment, 'news_id', $this->tn_news, 'id', 'CASCADE', 'NO ACTION');
        $this->addForeignKey('fk_news_comment_user_id', $this->tn_news_comment, 'user_id', $this->tn_user, 'id', 'CASCADE', 'NO ACTION');

        $this->addForeignKey('fk_news_readed_id', $this->tn_news_unread, 'news_id', $this->tn_news, 'id', 'CASCADE', 'NO ACTION');
        $this->addForeignKey('fk_user_id', $this->tn_news_unread, 'user_id', $this->tn_user, 'id', 'CASCADE', 'NO ACTION');

        // permissions

        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            ['news.create', 2, 'Create news'],
            ['news.read',   2, 'Read news'],
            ['news.update', 2, 'Update news'],
            ['news.delete', 2, 'Delete news'],

        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            ['Supervisor', 'news.create'],
            ['Supervisor', 'news.read'],
            ['Supervisor', 'news.update'],
            ['Supervisor', 'news.delete'],

            ['Developer', 'news.read'],
            ['Hr', 'news.read'],
            ['Pm', 'news.read'],
            ['Qa', 'news.read'],
            ['Sales', 'news.read'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_news_author_id', $this->tn_news);
        $this->dropForeignKey('fk_news_id', $this->tn_news_comment);
        $this->dropForeignKey('fk_news_comment_user_id', $this->tn_news_comment);
        $this->dropForeignKey('fk_news_readed_id', $this->tn_news_unread);
        $this->dropForeignKey('fk_user_id', $this->tn_news_unread);

        $this->dropTable($this->tn_news);
        $this->dropTable($this->tn_news_comment);
        $this->dropTable($this->tn_news_unread);

        $this->delete($this->tn_auth_item, "name = 'news.create'");
        $this->delete($this->tn_auth_item, "name = 'news.read'");
        $this->delete($this->tn_auth_item, "name = 'news.update'");
        $this->delete($this->tn_auth_item, "name = 'news.delete'");

        $this->delete($this->tn_auth_item_child, "child = 'news.create'");
        $this->delete($this->tn_auth_item_child, "child = 'news.read'");
        $this->delete($this->tn_auth_item_child, "child = 'news.update'");
        $this->delete($this->tn_auth_item_child, "child = 'news.delete'");
    }
}
