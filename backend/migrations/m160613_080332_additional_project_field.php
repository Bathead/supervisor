<?php

use yii\db\Schema;
use yii\db\Migration;
use app\modules\project\models\Project;

class m160613_080332_additional_project_field extends Migration
{
    protected $tn_project = '{{%project}}';

    public function safeUp()
    {
        $this->addColumn($this->tn_project, 'dialog_id', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn($this->tn_project, 'dialog_id');
    }
}
