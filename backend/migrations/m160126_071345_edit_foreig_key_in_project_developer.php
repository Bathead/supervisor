<?php

use yii\db\Schema;
use yii\db\Migration;

class m160126_071345_edit_foreig_key_in_project_developer extends Migration
{
    public function up()
    {
        $this->dropForeignKey('FK_project_id', 'project_developer');
        $this->dropForeignKey('FK_developer_id', 'project_developer');
        $this->addForeignKey('FK_project_id', 'project_developer', 'id_project', 'project', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_developer_id', 'project_developer', 'id_developer', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('FK_project_id', 'project_developer');
        $this->dropForeignKey('FK_developer_id', 'project_developer');
        $this->addForeignKey('FK_project_id', 'project_developer', 'id_project', 'project', 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('FK_developer_id', 'project_developer', 'id_developer', 'user', 'id', 'NO ACTION', 'NO ACTION');
    }
}
