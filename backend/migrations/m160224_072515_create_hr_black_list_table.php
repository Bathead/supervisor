<?php

use yii\db\Migration;

class m160224_072515_create_hr_black_list_table extends Migration
{
    protected $tn_hr_contact = '{{%hr_contact}}';
    protected $tn_hr_black_list = '{{%hr_black_list}}';
    protected $tn_user = '{{%user}}';

    public function up()
    {
        $this->createTable($this->tn_hr_black_list, [
            'id' => $this->primaryKey(),
            'email' => $this->string()->unique(),
            'reason' => $this->string(),
            'created_at' => $this->timestamp()->notNull(),
            'created_by_id' => $this->integer()
        ]);

        $this->addForeignKey('hr_created_by_id', $this->tn_hr_black_list, 'created_by_id', $this->tn_user, 'id', 'SET NULL', 'NO ACTION');
    }

    public function down()
    {
        $this->dropForeignKey('hr_created_by_id', $this->tn_hr_black_list);
        $this->dropTable($this->tn_hr_black_list);
    }
}
