<?php

use yii\db\Migration;

class m160606_081456_test_project_comments extends Migration
{
    protected $tn_project_comm = '{{%project_comm}}';

    public function up()
    {
        $created_at = $updated_at = date('Y-m-d H:i:s');
        $commentsRows = ['project_id', 'user_id', 'message', 'created_at', 'updated_at'];
        $comments = [
            [1, 1, 'Test comment 1', $created_at, $updated_at],
            [1, 1, 'Test comment 2', $created_at, $updated_at],
            [1, 1, 'Test comment 3', $created_at, $updated_at],
            [1, 3, 'Test comment 4', $created_at, $updated_at],
            [1, 1, 'Test comment 5', $created_at, $updated_at],
            [1, 3, 'Test comment 6', $created_at, $updated_at],
            [1, 1, 'Test comment 7', $created_at, $updated_at],
            [1, 1, 'Test comment 8', $created_at, $updated_at],
            [1, 1, 'Test comment 9', $created_at, $updated_at],
            [1, 1, 'Test comment 10', $created_at, $updated_at],
            [1, 1, 'Test comment 11', $created_at, $updated_at],
            [1, 1, 'Test comment 12', $created_at, $updated_at],
            [1, 1, 'Test comment 13', $created_at, $updated_at],
            [1, 1, 'Test comment 14', $created_at, $updated_at],
            [1, 1, 'Test comment 15', $created_at, $updated_at],
            [2, 1, 'Test comment 1', $created_at, $updated_at],
            [2, 1, 'Test comment 2', $created_at, $updated_at],
            [2, 1, 'Test comment 3', $created_at, $updated_at],
            [2, 3, 'Test comment 4', $created_at, $updated_at],
            [2, 1, 'Test comment 5', $created_at, $updated_at],
            [2, 3, 'Test comment 6', $created_at, $updated_at],
            [2, 1, 'Test comment 7', $created_at, $updated_at],
            [2, 1, 'Test comment 8', $created_at, $updated_at],
            [2, 1, 'Test comment 9', $created_at, $updated_at],
            [2, 1, 'Test comment 10', $created_at, $updated_at],
            [2, 1, 'Test comment 11', $created_at, $updated_at],
            [2, 1, 'Test comment 12', $created_at, $updated_at],
            [2, 1, 'Test comment 13', $created_at, $updated_at],
            [2, 1, 'Test comment 14', $created_at, $updated_at],
            [2, 1, 'Test comment 15', $created_at, $updated_at],
        ];
        $this->batchInsert($this->tn_project_comm, $commentsRows, $comments);
    }

    public function down()
    {
        $this->execute("DELETE FROM $this->tn_project_comm WHERE project_id = 1 OR project_id = 2");
    }

}
