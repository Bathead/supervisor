<?php

use yii\db\Schema;
use yii\db\Migration;

class m160115_102011_edit_project extends Migration
{
    public function up()
    {
        $this->execute("UPDATE project SET status = '1' WHERE status = 'Активні'");
    }

    public function down()
    {
        $this->execute("UPDATE project SET status = 'Активні' WHERE status = '1'");
    }

}
