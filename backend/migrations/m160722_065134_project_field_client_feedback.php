<?php

use yii\db\Migration;

class m160722_065134_project_field_client_feedback extends Migration
{
    protected $tn_project = '{{%project}}';

    public function safeUp()
    {
        $this->addColumn($this->tn_project, 'client_feedback', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn($this->tn_project, 'client_feedback');
    }
}
