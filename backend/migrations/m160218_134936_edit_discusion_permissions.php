<?php

use yii\db\Migration;

class m160218_134936_edit_discusion_permissions extends Migration
{
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';

    public function safeUp()
    {
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            ['discussion.discuss', 2, 'Can discuss projects'],
        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            ['Sales', 'discussion.discuss'],
            ['Developer', 'discussion.discuss'],
            ['Pm', 'discussion.discuss'],
            ['Qa', 'discussion.discuss'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);

    }

    public function safeDown()
    {
        $this->delete($this->tn_auth_item, "name = 'discussion.discuss'");
        $this->delete($this->tn_auth_item_child, "child = 'discussion.discuss'");
    }
}
