<?php

use yii\db\Schema;
use yii\db\Migration;

class m160204_062859_create_ticket_comments_permissions extends Migration
{

    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';

    public function safeUp()
    {
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            ['ticketComments.create', 2, 'Create ticket comments'],
            ['ticketComments.read',   2, 'Read ticket comments'],
            ['ticketComments.update', 2, 'Update ticket comments'],
            ['ticketComments.delete', 2, 'Delete ticket comments'],

        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            ['Supervisor', 'ticketComments.create'],
            ['Supervisor', 'ticketComments.read'],
            ['Supervisor', 'ticketComments.update'],
            ['Supervisor', 'ticketComments.delete'],

            ['Developer', 'ticketComments.read'],
            ['Developer', 'ticketComments.create'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function safeDown()
    {
        $this->delete($this->tn_auth_item, "name = 'ticketComments.create'");
        $this->delete($this->tn_auth_item, "name = 'ticketComments.read'");
        $this->delete($this->tn_auth_item, "name = 'ticketComments.update'");
        $this->delete($this->tn_auth_item, "name = 'ticketComments.delete'");

        $this->delete($this->tn_auth_item_child, "child = 'ticketComments.create'");
        $this->delete($this->tn_auth_item_child, "child = 'ticketComments.read'");
        $this->delete($this->tn_auth_item_child, "child = 'ticketComments.update'");
        $this->delete($this->tn_auth_item_child, "child = 'ticketComments.delete'");
    }
}
