<?php

use yii\db\Schema;
use yii\db\Migration;
use app\modules\project\models\Project;

class m160120_074539_add_new_fields_to_project extends Migration
{

    protected $tn_project = '{{%project}}';

    public function up()
    {
        $this->addColumn($this->tn_project, 'client', $this->string());
        $this->addColumn($this->tn_project, 'payment_sum', $this->decimal(10, 2));
        $this->addColumn($this->tn_project, 'time_spent', $this->decimal(10, 2));
        $this->addColumn($this->tn_project, 'quality', $this->string());
    }

    public function down()
    {
        $this->dropColumn($this->tn_project, 'client');
        $this->dropColumn($this->tn_project, 'payment_sum');
        $this->dropColumn($this->tn_project, 'time_spent');
        $this->dropColumn($this->tn_project, 'quality');
    }
}