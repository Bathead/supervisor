<?php

use yii\db\Schema;
use yii\db\Migration;

class m160104_131930_create_developer_rank_table extends Migration
{
    protected $tn_user = '{{%user}}';
    protected $tn_rank = '{{%rank}}';

    public function safeUp()
    {
        $this->createTable($this->tn_rank, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()
        ]);

        $this->insert($this->tn_rank, array('name' => 'Trainee'));
        $this->insert($this->tn_rank, array('name' => 'Junior'));
        $this->insert($this->tn_rank, array('name' => 'Strong junior'));
        $this->insert($this->tn_rank, array('name' => 'Middle'));
        $this->insert($this->tn_rank, array('name' => 'Strong middle'));
        $this->insert($this->tn_rank, array('name' => 'Senior'));
        $this->insert($this->tn_rank, array('name' => 'Strong senior'));
        $this->insert($this->tn_rank, array('name' => 'Team lead'));
    }

    public function safeDown()
    {
        $this->dropTable($this->tn_rank);
    }
}
