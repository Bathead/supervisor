<?php

use yii\db\Migration;

class m160726_110420_add_salary_rate_permission extends Migration
{
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';
    protected $tn_auth_assignment = '{{%auth_assignment}}';

    public function safeUp()
    {
        // add permissions
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            ['user.canReadSalaryRate', 2, 'Can read user salary rate'],
        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        // add new permissions to roles
        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            ['Supervisor', 'user.canReadSalaryRate'],
            ['Sales',      'user.canReadSalaryRate'],
            ['Pm',         'user.canReadSalaryRate'],
            ['Hr',         'user.canReadSalaryRate'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);

    }

    public function safeDown()
    {
        $this->delete($this->tn_auth_item_child, "child = 'user.canReadSalaryRate'");
        $this->delete($this->tn_auth_item, "name = 'user.canReadSalaryRate'");
    }
}
