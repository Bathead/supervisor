<?php

use yii\db\Migration;

class m160524_121417_chenge_types_in_project_table extends Migration
{
    protected $tn_project = '{{%project}}';

    public function up()
    {
        $this->alterColumn($this->tn_project, 'internal_price', $this->double());
        $this->alterColumn($this->tn_project, 'external_price', $this->double());
        $this->alterColumn($this->tn_project, 'hours_developers', $this->double());
        $this->alterColumn($this->tn_project, 'hours_sales', $this->double());
        $this->alterColumn($this->tn_project, 'hours_pm', $this->double());
        $this->alterColumn($this->tn_project, 'hours_qa', $this->double());
        $this->alterColumn($this->tn_project, 'payment_sum', $this->double());
        $this->alterColumn($this->tn_project, 'time_spent', $this->double());
    }

    public function down()
    {
        $this->alterColumn($this->tn_project, 'internal_price', $this->decimal(10, 2));
        $this->alterColumn($this->tn_project, 'external_price', $this->decimal(10, 2));
        $this->alterColumn($this->tn_project, 'hours_developers', $this->decimal(10, 2));
        $this->alterColumn($this->tn_project, 'hours_sales', $this->decimal(10, 2));
        $this->alterColumn($this->tn_project, 'hours_pm', $this->decimal(10, 2));
        $this->alterColumn($this->tn_project, 'hours_qa', $this->decimal(10, 2));
        $this->alterColumn($this->tn_project, 'payment_sum', $this->decimal(10, 2));
        $this->alterColumn($this->tn_project, 'time_spent', $this->decimal(10, 2));
    }



}
