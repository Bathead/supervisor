<?php

use yii\db\Schema;
use yii\db\Migration;
use app\modules\project\models\Project;
use app\modules\user\models\User;

class m160126_061946_edit_foreign_key_in_project extends Migration
{
    protected $tn_project = '{{%project}}';
    protected $tn_user = '{{%user}}';

    public function up()
    {
        $this->dropForeignKey('FK_project_sale_id', $this->tn_project);
        $this->alterColumn($this->tn_project, 'id_sales', $this->integer());
        $this->addForeignKey('FK_project_sale_id', $this->tn_project, 'id_sales', $this->tn_user, 'id', 'SET NULL', 'SET NULL');
    }

    public function down()
    {
        $this->dropForeignKey('FK_project_sale_id', $this->tn_project);
        $this->alterColumn($this->tn_project, 'id_sales', $this->integer()->notNull());
        $this->addForeignKey('FK_project_sale_id', $this->tn_project, 'id_sales', $this->tn_user, 'id', 'NO ACTION', 'NO ACTION');
    }
}
