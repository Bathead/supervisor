<?php

use yii\db\Schema;
use yii\db\Migration;

class m160210_155938_add_new_permissions_for_payments_module extends Migration
{
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';

    public function up()
    {
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            // ticket permissions
            ['payment.create', 2, 'Can create new payment'],
            ['payment.read', 2, 'Can read tickets'],
            ['payment.update', 2, 'Can update tickets'],
            ['payment.delete', 2, 'Can delete tickets'],
        ];

        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            // supervisor permissions
            ['Supervisor', 'payment.read'],
            ['Supervisor', 'payment.create'],
            ['Supervisor', 'payment.update'],
            ['Supervisor', 'payment.delete'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function down()
    {
        $this->delete($this->tn_auth_item, "name = 'payment.read'");
        $this->delete($this->tn_auth_item, "name = 'payment.create'");
        $this->delete($this->tn_auth_item, "name = 'payment.update'");
        $this->delete($this->tn_auth_item, "name = 'payment.delete'");
        $this->delete($this->tn_auth_item_child, "child = 'payment.read'");
        $this->delete($this->tn_auth_item_child, "child = 'payment.create'");
        $this->delete($this->tn_auth_item_child, "child = 'payment.update'");
        $this->delete($this->tn_auth_item_child, "child = 'payment.delete'");
    }
}
