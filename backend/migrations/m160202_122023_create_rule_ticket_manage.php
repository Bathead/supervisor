<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\rbac;

class m160202_122023_create_rule_ticket_manage extends Migration
{
    protected $t_rule = '{{%auth_rule}}';
    protected $t_auth_item = '{{%auth_item}}';
    protected $t_auth_item_child = '{{%auth_item_child}}';
    protected $rule;

    public function __construct()
    {
        parent::__construct();
        $this->rule = new \app\modules\ticket\rules\TicketManageRule();
    }

    public function up()
    {
        /*
         * Create TicketManageRule
         */
        $this->insert($this->t_rule, [
           'name' => $this->rule->name,
           'data' => serialize($this->rule),
        ]);
        /*
         * Create permission "ticket.manage"
         */
        $this->insert($this->t_auth_item, [
          'name' => "ticket.manage",
          'type' => 2,
          'description' => 'User can manage his ticket',
          'rule_name' => $this->rule->name,
        ]);
        /*
         * Add permission to roles
         */
        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
          ['Developer', 'ticket.manage'],
          ['Supervisor', 'ticket.manage'],
        ];

        $this->batchInsert($this->t_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function down()
    {
        // delete rule "TicketManage"
        $this->delete($this->t_rule, "name = '" . $this->rule->name . "'");
        // delete permission "ticket.manage"
        $this->delete($this->t_auth_item, "name = 'ticket.manage'");
        // delete permission "project.manage" from role
        $this->delete($this->t_auth_item_child, "parent = 'Developer' AND child = 'ticket.manage'");
        $this->delete($this->t_auth_item_child, "parent = 'Supervisor' AND child = 'ticket.manage'");
    }
}
