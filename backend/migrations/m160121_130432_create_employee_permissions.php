<?php

use yii\db\Schema;
use yii\db\Migration;
use app\modules\user\models\User;

class m160121_130432_create_employee_permissions extends Migration
{
    protected $tn_users = '{{%user}}';
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';
    protected $tn_auth_assignment = '{{%auth_assignment}}';

    public function safeUp()
    {
        // add permissions
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            ['user.haveEmploymentStatus', 2, 'User can have (and change) employment status'],
            ['user.canSendMessage', 2, 'Send message to other users'],
            ['user.canComplain', 2, 'Report user'],
        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            // supervisor permissions
            ['Supervisor', 'user.canSendMessage'],
            ['Supervisor', 'user.canComplain'],

            // developer permissions
            ['Developer', 'user.canSendMessage'],
            ['Developer', 'user.canComplain'],
            ['Developer', 'user.haveEmploymentStatus'],

            // hr permissions
            ['Hr', 'user.canSendMessage'],
            ['Hr', 'user.canComplain'],

            // pm permissions
            ['Pm', 'user.canSendMessage'],
            ['Pm', 'user.canComplain'],

            // qa permissions
            ['Qa', 'user.canSendMessage'],
            ['Qa', 'user.canComplain'],

            // sales permissions
            ['Sales', 'user.canSendMessage'],
            ['Sales', 'user.canComplain'],

        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function safeDown()
    {
        $this->delete($this->tn_auth_item, "name = 'user.canSendMessage'");
        $this->delete($this->tn_auth_item, "name = 'user.canComplain'");
        $this->delete($this->tn_auth_item, "name = 'user.haveEmploymentStatus'");
        $this->delete($this->tn_auth_item_child, "child = 'user.canSendMessage'");
        $this->delete($this->tn_auth_item_child, "child = 'user.canComplain'");
        $this->delete($this->tn_auth_item_child, "child = 'user.haveEmploymentStatus'");
    }

}
