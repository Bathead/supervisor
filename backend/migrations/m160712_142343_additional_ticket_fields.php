<?php

use yii\db\Migration;

class m160712_142343_additional_ticket_fields extends Migration
{
    protected $tn_ticket = '{{%ticket}}';
    protected $tn_user = '{{%user}}';

    public function safeUp()
    {
        $this->addColumn($this->tn_ticket, 'receiver_id', $this->integer());
        $this->addForeignKey('FK_ticket_receiver_id', $this->tn_ticket, 'receiver_id', $this->tn_user, 'id', 'SET NULL', 'SET NULL');
        $this->execute("UPDATE ticket SET receiver_id = '1'");
    }

    public function safeDown()
    {
        $this->execute("UPDATE ticket SET receiver_id = NULL");
        $this->dropForeignKey('FK_ticket_receiver_id', $this->tn_ticket);
        $this->dropColumn($this->tn_ticket, 'receiver_id');

    }
}
