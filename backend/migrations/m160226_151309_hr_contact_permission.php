<?php

use yii\db\Migration;

class m160226_151309_hr_contact_permission extends Migration
{
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';
    protected $tn_auth_assignment = '{{%auth_assignment}}';

    public function safeUp()
    {
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            // rank permissions
            ['hrContact.create', 2, 'Create hr contact'],
            ['hrContact.read',   2, 'Read hr contact'],
            ['hrContact.update', 2, 'Update hr contact'],
            ['hrContact.delete', 2, 'Delete hr contact'],
        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            ['Supervisor', 'hrContact.create'],
            ['Supervisor', 'hrContact.read'],
            ['Supervisor', 'hrContact.update'],
            ['Supervisor', 'hrContact.delete'],
            ['Hr', 'hrContact.create'],
            ['Hr', 'hrContact.read'],
            ['Hr', 'hrContact.update'],
            ['Hr', 'hrContact.delete'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function safeDown()
    {
        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'hrContact.create'");
        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'hrContact.read'");
        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'hrContact.update'");
        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'hrContact.delete'");

        $this->delete($this->tn_auth_item_child, "parent = 'Hr' AND child = 'hrContact.create'");
        $this->delete($this->tn_auth_item_child, "parent = 'Hr' AND child = 'hrContact.read'");
        $this->delete($this->tn_auth_item_child, "parent = 'Hr' AND child = 'hrContact.update'");
        $this->delete($this->tn_auth_item_child, "parent = 'Hr' AND child = 'hrContact.delete'");

        $this->delete($this->tn_auth_item, "name = 'hrContact.create'");
        $this->delete($this->tn_auth_item, "name = 'hrContact.read'");
        $this->delete($this->tn_auth_item, "name = 'hrContact.update'");
        $this->delete($this->tn_auth_item, "name = 'hrContact.delete'");
    }
}
