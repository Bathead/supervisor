<?php

use yii\db\Schema;
use yii\db\Migration;

class m160104_092956_add_test_accounts_roles_and_permisions extends Migration
{
    protected $tn_users = '{{%user}}';
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';
    protected $tn_auth_assignment = '{{%auth_assignment}}';

    public function safeUp()
    {
        // add test accounts
        $pass = 'asdf';
        $passHash = Yii::$app->security->generatePasswordHash($pass);

        $userRows = ['id', 'email', 'username', 'password', 'auth_key', 'access_token'];
        $users = [
            [1, 'supervisor@mail.com', 'sup',    $passHash, 'v3BvS-yYAS4AUEknZ64rQVadzXHW3dVa', 'v3Bvz-yYAS4AUEknZ64rQVadzXHW3dVa'],
            [2, 'sysadmin@mail.com', 'sysadmin', $passHash, 'v3BvS-yYAS4AUEknZ64rQVadzXHW3dVb', 'v3Bvz-yYAS4AUEknZ64rQVadzXHW3dVb'],
            [3, 'dev@mail.com',   'dev',         $passHash, 'v3BvS-yYAS4AUEknZ64rQVadzXHW3dVc', 'v3Bvc-yYAS4AUEknZ64rQVadzXHW3dVc'],
            [4, 'sales@mail.com', 'sales',       $passHash, 'v3BvS-yYAS4AUEknZ64rQVadzXHW3dVd', 'v3Bvv-yYAS4AUEknZ64rQVadzXHW3dVd'],
            [5, 'pm@mail.com',    'pm',          $passHash, 'v3BvS-yYAS4AUEknZ64rQVadzXHW3dVe', 'v3Bvb-yYAS4AUEknZ64rQVadzXHW3dVe'],
            [6, 'hr@mail.com',    'hr',          $passHash, 'v3BvS-yYAS4AUEknZ64rQVadzXHW3dVf', 'v3Bvn-yYAS4AUEknZ64rQVadzXHW3dVf'],
            [7, 'qa@mail.com',    'qa',          $passHash, 'v3BvS-yYAS4AUEknZ64rQVadzXHW3dVg', 'v3Bvm-yYAS4AUEknZ64rQVadzXHW3dVg'],
        ];
        $this->batchInsert($this->tn_users, $userRows, $users);

        // add roles
        $roleRows = ['name', 'type'];
        $roles = [
            ['Supervisor',      1],
            ['Sysadmin',      1],
            ['Developer',  1],
            ['Sales',      1],
            ['Pm',         1],
            ['Hr',         1],
            ['Qa',         1],
        ];
        $this->batchInsert($this->tn_auth_item, $roleRows, $roles);

        // add permissions
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            // user permissions
            ['user.create', 2, 'Create user'],
            ['user.read',   2, 'Read user'],
            ['user.update', 2, 'Update user'],
            ['user.delete', 2, 'Delete user'],

            // project permissions
            ['project.create', 2, 'Create project'],
            ['project.read',   2, 'Read project'],
            ['project.update', 2, 'Update project'],
            ['project.delete', 2, 'Delete project'],
        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            // supervisor permissions
            ['Supervisor', 'user.create'],
            ['Supervisor', 'user.read'],
            ['Supervisor', 'user.update'],
            ['Supervisor', 'user.delete'],
            ['Supervisor', 'project.create'],
            ['Supervisor', 'project.read'],
            ['Supervisor', 'project.update'],
            ['Supervisor', 'project.delete'],

            // sysadmin permissions
            ['Sysadmin', 'user.create'],
            ['Sysadmin', 'user.read'],
            ['Sysadmin', 'user.update'],
            ['Sysadmin', 'user.delete'],
            ['Sysadmin', 'project.create'],
            ['Sysadmin', 'project.read'],
            ['Sysadmin', 'project.update'],
            ['Sysadmin', 'project.delete'],

            // sales permissions
            ['Sales', 'project.create'],
            ['Sales', 'project.read'],
            ['Sales', 'project.update'],
            ['Sales', 'project.delete'],

        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);

        $userRolesRows = ['item_name', 'user_id'];
        $userRoles = [
            ['Supervisor',       1],
            ['Sysadmin',         2],
            ['Developer',        3],
            ['Sales',            4],
            ['Pm',               5],
            ['Hr',               6],
            ['Qa',               7],
        ];
        $this->batchInsert($this->tn_auth_assignment, $userRolesRows, $userRoles);

    }

    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->execute('TRUNCATE TABLE ' . $this->tn_users);
        $this->execute('TRUNCATE TABLE ' . $this->tn_auth_item);
        $this->execute('TRUNCATE TABLE ' . $this->tn_auth_item_child);
        $this->execute('TRUNCATE TABLE ' . $this->tn_auth_assignment);
    }
}
