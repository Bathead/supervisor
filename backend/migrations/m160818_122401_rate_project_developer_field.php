<?php

use yii\db\Migration;

class m160818_122401_rate_project_developer_field extends Migration
{
    protected $tn_project_developer = '{{%project_developer}}';
    protected $tn_currency = '{{%currency}}';


    public function safeUp()
    {
        $this->addColumn($this->tn_project_developer, 'rate', $this->double());
        $this->addColumn($this->tn_project_developer, 'currency_id', $this->integer());
        $this->addForeignKey('FK_project_developer_currency', $this->tn_project_developer, 'currency_id', $this->tn_currency, 'id', 'SET NULL', 'SET NULL');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_project_developer_currency', $this->tn_project_developer);
        $this->dropColumn($this->tn_project_developer, 'currency_id');
        $this->dropColumn($this->tn_project_developer, 'rate');
    }
}
