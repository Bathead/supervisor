<?php

use yii\db\Schema;
use yii\db\Migration;

class m160203_133810_add_test_dialogs extends Migration
{
    protected $tn_dialog = '{{%dialog}}';
    protected $tn_dialog_message = '{{%dialog_message}}';
    protected $tn_dialog_user = '{{%dialog_user}}';
    protected $tn_dialog_message_unread = '{{%dialog_message_unread}}';

    public function safeUp()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');

        $created_at = $updated_at = date('Y-m-d H:i:s');

        $dialogRows = ['id', 'subject', 'author_id', 'created_at', 'last_message_id', 'last_action_date'];
        $dialogs = [
            [1, 'Test dialog 1', 1, $created_at, 3, $updated_at],
            [2, 'Test dialog 2', 1, $created_at, 6, $updated_at],
            [3, 'Test dialog 3', 2, $created_at, 18, $updated_at],
        ];

        $this->batchInsert($this->tn_dialog, $dialogRows, $dialogs);

        $messageRows = ['id', 'dialog_id', 'user_id', 'message', 'parent_id', 'updated_at', 'created_at'];
        $messages = [
            // dialog 1
            [1, 1, 1, 'Dialog 1 test message 1', null, $updated_at, $created_at],
            [2, 1, 2, 'Dialog 1 test message 2', null, $updated_at, $created_at],
            [3, 1, 3, 'Dialog 1 test message 3', null, $updated_at, $created_at],

            // dialog 2
            [4, 2, 1, 'Dialog 2 test message 1', null, $updated_at, $created_at],
            [5, 2, 2, 'Dialog 2 test message 2', null, $updated_at, $created_at],
            [6, 2, 3, 'Dialog 2 test message 3', null, $updated_at, $created_at],

            // dialog 3
            [7, 3, 1, 'Dialog 3 test message 1', null, $updated_at, $created_at],
            [8, 3, 2, 'Dialog 3 test message 2', null, $updated_at, $created_at],
            [9, 3, 3, 'Dialog 3 test message 3', null, $updated_at, $created_at],
            [10, 3, 3, 'Dialog 3 test message 4', null, $updated_at, $created_at],
            [11, 3, 3, 'Dialog 3 test message 5', null, $updated_at, $created_at],
            [12, 3, 3, 'Dialog 3 test message 6', null, $updated_at, $created_at],
            [13, 3, 3, 'Dialog 3 test message 7', null, $updated_at, $created_at],
            [14, 3, 3, 'Dialog 3 test message 8', null, $updated_at, $created_at],
            [15, 3, 3, 'Dialog 3 test message 9', null, $updated_at, $created_at],
            [16, 3, 3, 'Dialog 3 test message 10', null, $updated_at, $created_at],
            [17, 3, 3, 'Dialog 3 test message 11', null, $updated_at, $created_at],
            [18, 3, 3, 'Dialog 3 test message 12', null, $updated_at, $created_at],
        ];

        $this->batchInsert($this->tn_dialog_message, $messageRows, $messages);

        $dialogUserRows = ['dialog_id', 'user_id'];
        $dialogUser = [
            // dialog 1
            [1, 1],
            [1, 2],
            [1, 3],

            // dialog 2
            [2, 1],
            [2, 2],
            [2, 3],

            // dialog 3
            [3, 1],
            [3, 2],
            [3, 3],
        ];
        $this->batchInsert($this->tn_dialog_user, $dialogUserRows, $dialogUser);

        $unreadMessageRows = ['message_id', 'user_id'];
        $unreadMessage = [
            // user 1
            [3, 1],     // dialog 1
            [6, 1],     // dialog 2
            [12, 1],    // dialog 3
            [13, 1],    // dialog 3
            [14, 1],    // dialog 3
            [15, 1],    // dialog 3
            [16, 1],    // dialog 3
            [17, 1],    // dialog 3
            [18, 1],    // dialog 3

            // user 2
            [3, 2],     // dialog 1
            [6, 2],     // dialog 2
            [12, 2],    // dialog 3
            [13, 2],    // dialog 3
            [14, 2],    // dialog 3
            [15, 2],    // dialog 3
            [16, 2],    // dialog 3
            [17, 2],    // dialog 3
            [18, 2],    // dialog 3

            // user 3
            [3, 3],     // dialog 1
            [6, 3],     // dialog 2
            [12, 3],    // dialog 3
            [13, 3],    // dialog 3
            [14, 3],    // dialog 3
            [15, 3],    // dialog 3
            [16, 3],    // dialog 3
            [17, 3],    // dialog 3
            [18, 3],    // dialog 3
        ];
        $this->batchInsert($this->tn_dialog_message_unread, $unreadMessageRows, $unreadMessage);

    }

    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->execute('TRUNCATE TABLE ' . $this->tn_dialog);
        $this->execute('TRUNCATE TABLE ' . $this->tn_dialog_message);
        $this->execute('TRUNCATE TABLE ' . $this->tn_dialog_user);
        $this->execute('TRUNCATE TABLE ' . $this->tn_dialog_message_unread);
    }

}
