<?php

use yii\db\Schema;
use yii\db\Migration;

class m160120_112746_add_project_technology_table extends Migration
{
    protected $tn_project = '{{%project}}';
    protected $tn_technology = '{{%technology}}';
    protected $tn_project_technologies = '{{%project_technologies}}';

    public function safeUp()
    {
        $this->createTable($this->tn_project_technologies, [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer()->notNull(),
            'technology_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey('technology_project_id', $this->tn_project_technologies, 'project_id', $this->tn_project, 'id', 'CASCADE', 'NO ACTION');
        $this->addForeignKey('technology_p_id', $this->tn_project_technologies, 'technology_id', $this->tn_technology, 'id', 'CASCADE', 'NO ACTION');

        $projectTechnologies = [
            [1, 1],
            [2, 2],
            [3, 3],
            [2, 4],
            [1, 7],
        ];

        $this->batchInsert($this->tn_project_technologies, ['project_id', 'technology_id'], $projectTechnologies);
    }

    public function safeDown()
    {
        $this->dropForeignKey('technology_project_id', $this->tn_project_technologies);
        $this->dropForeignKey('technology_p_id', $this->tn_project_technologies);
        $this->dropTable($this->tn_project_technologies);
    }
}

