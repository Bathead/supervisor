<?php

use yii\db\Schema;
use yii\db\Migration;

class m160112_131111_add_permissions_to_developer_role extends Migration
{
    protected $tn_auth_item_child = '{{%auth_item_child}}';

    public function safeUp()
    {
        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            ['Developer', 'project.read'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function safeDown()
    {
        $this->delete($this->tn_auth_item_child, "parent = 'Developer' AND child = 'project.read'");
    }

}
