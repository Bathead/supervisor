<?php

use yii\db\Schema;
use yii\db\Migration;

class m160113_111020_add_permision_selectProject extends Migration
{
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';

    public function up()
    {   
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            // user permissions
            ['project.select', 2, 'Select project'],
        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            // developer permissions
            ['Developer', 'project.select'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function down()
    {
        $this->delete($this->tn_auth_item_child, "parent = 'Developer' AND child = 'project.select'");
        $this->delete($this->tn_auth_item, "name = 'project.select' AND type = '2' AND description = 'Select project'");
    }
}
