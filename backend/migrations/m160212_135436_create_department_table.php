<?php

use yii\db\Schema;
use yii\db\Migration;

class m160212_135436_create_department_table extends Migration
{
    protected $tn_user;
    protected $tn_department;
    protected $tn_user_department;

    public function __construct()
    {
        parent::__construct();
        $this->tn_user = '{{%user}}';
        $this->tn_department = '{{%department}}';
        $this->tn_user_department = '{{%user_department}}';
    }

    public function safeUp()
    {
        $this->createTable($this->tn_department, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'head_id' => $this->integer()
        ]);

        $this->createTable($this->tn_user_department, [
            'id'            => $this->primaryKey(),
            'user_id'       => $this->integer()->notNull(),
            'department_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('FK_user_department_user_id', $this->tn_user_department, 'user_id', $this->tn_user, 'id', 'CASCADE', 'NO ACTION');
        $this->addForeignKey('FK_user_department_department_id', $this->tn_user_department, 'department_id', $this->tn_department, 'id', 'CASCADE', 'NO ACTION');
        $this->addForeignKey('FK_department_head_id', $this->tn_department, 'head_id', $this->tn_user, 'id', 'SET NULL', 'NO ACTION');

        $departments = [
            [1, 'PHP'],
            [2, 'WordPress'],
            [3, 'Drupal'],
            [4, 'Frontend'],
            [5, 'iOS'],
            [6, 'Java'],
            [7, 'Android'],
            [8, 'Ruby']
        ];
        $this->batchInsert($this->tn_department, ['id', 'name'], $departments);

        $userDepartments = [
            [3, 1],
            [3, 2],
            [3, 4],
            [3, 5]
        ];
        $this->batchInsert($this->tn_user_department, ['user_id', 'department_id'], $userDepartments);
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_user_department_user_id', $this->tn_user_department);
        $this->dropForeignKey('FK_user_department_department_id', $this->tn_user_department);
        $this->dropForeignKey('FK_department_head_id', 'department');
        $this->dropTable($this->tn_department);
        $this->dropTable($this->tn_user_department);
    }
}
