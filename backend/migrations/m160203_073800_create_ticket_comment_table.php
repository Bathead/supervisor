<?php

use yii\db\Schema;
use yii\db\Migration;

class m160203_073800_create_ticket_comment_table extends Migration
{
    protected $tn_user = '{{%user}}';
    protected $tn_ticket = '{{%ticket}}';
    protected $tn_ticket_comment = '{{%ticket_comment}}';

    public function safeUp()
    {
        // table
        $this->createTable($this->tn_ticket_comment, [
            'id' => $this->primaryKey(),
            'ticket_id' => $this->integer(),
            'user_id' => $this->integer(),
            'message' => $this->text(),
            'parent_id' => $this->integer()->defaultValue(null),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp(),
            'is_deleted' => $this->boolean()->defaultValue(0)
        ]);

        // foreign keys
        $this->addForeignKey('fk_ticket_id', $this->tn_ticket_comment, 'ticket_id', $this->tn_ticket, 'id', 'CASCADE', 'NO ACTION');
        $this->addForeignKey('fk_ticket_comment_user_id', $this->tn_ticket_comment, 'user_id', $this->tn_user, 'id', 'CASCADE', 'NO ACTION');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_ticket_id', $this->tn_ticket_comment);
        $this->dropForeignKey('fk_ticket_comment_user_id', $this->tn_ticket_comment);

        $this->dropTable($this->tn_ticket_comment);
    }
}
