<?php

use yii\db\Schema;
use yii\db\Migration;

class m160118_102737_rank_foreign_key_fixing extends Migration
{

    protected $tn_profile = '{{%profile}}';
    protected $tn_rank = '{{%rank}}';

    public function safeUp()
    {
        $this->dropForeignKey('profile_rank_id', $this->tn_profile);
        $this->addForeignKey('profile_rank_id', $this->tn_profile, 'rank_id', $this->tn_rank, 'id', 'SET NULL', 'NO ACTION');
    }

    public function safeDown()
    {
        $this->dropForeignKey('profile_rank_id', $this->tn_profile);
        $this->addForeignKey('profile_rank_id', $this->tn_profile, 'rank_id', $this->tn_rank, 'id', 'NO ACTION', 'NO ACTION');
    }
}
