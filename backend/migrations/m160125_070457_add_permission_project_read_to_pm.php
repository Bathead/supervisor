<?php

use yii\db\Schema;
use yii\db\Migration;

class m160125_070457_add_permission_project_read_to_pm extends Migration
{
    protected $tn_auth_item_child = '{{%auth_item_child}}';

    public function up()
    {
        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            // Pm permissions
            ['Pm', 'project.read'],
        ];

        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function down()
    {
        $this->delete($this->tn_auth_item_child, "parent = 'Pm' AND child = 'project.read'");
    }
}
