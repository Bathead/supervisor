<?php

use yii\db\Schema;
use yii\db\Migration;

class m160211_141003_create_billing_cards_table_with_test_data extends Migration
{
    protected $tn_billing_card = '{{%billing_card}}';
    protected $tn_user = '{{%user}}';
    protected $tn_currency = '{{%currency}}';

    public function safeUp()
    {
        // tables
        $this->createTable($this->tn_billing_card, [
            'id' => $this->primaryKey(),
            'owner_id' => $this->integer(),
            'number' => $this->string(),
            'currency_id' => $this->integer(),
            'expiration_date' => $this->date(),
        ]);

        // indexes
        $this->createIndex('billing_card_unique_number', $this->tn_billing_card, 'number', true);

        // add test data
        $billingCardsRows = ['id', 'owner_id', 'number', 'currency_id', 'expiration_date'];
        $billingCards = [
            [
                1,
                1,
                '1111-1111-1111-1111',
                1,
                '2018-03-16',
            ],
            [
                2,
                1,
                '1111-1111-1111-1112',
                2,
                '2016-03-16',
            ],

            [
                3,
                2,
                '2222-2222-2222-2221',
                1,
                '2017-05-28',
            ],
            [
                4,
                2,
                '2222-2222-2222-2222',
                2,
                '2019-11-10',
            ],

            [
                5,
                3,
                '3333-3333-3333-3331',
                1,
                '2018-02-01',
            ],
            [
                6,
                3,
                '3333-3333-3333-3332',
                2,
                '2018-07-30',
            ],

            [
                7,
                4,
                '4444-4444-4444-4441',
                1,
                '2019-06-23',
            ],
            [
                8,
                4,
                '4444-4444-4444-4442',
                2,
                '2016-12-16',
            ],

            [
                9,
                5,
                '5555-5555-5555-5551',
                1,
                '2017-10-04',
            ],
            [
                10,
                5,
                '5555-5555-5555-5552',
                2,
                '2017-09-16',
            ],

            [
                11,
                6,
                '6666-6666-6666-6661',
                1,
                '2018-01-25',
            ],
            [
                12,
                6,
                '6666-6666-6666-6662',
                2,
                '2018-02-12',
            ],

            [
                13,
                7,
                '7777-7777-7777-7771',
                1,
                '2018-11-21',
            ],
            [
                14,
                7,
                '7777-7777-7777-7772',
                2,
                '2019-10-22',
            ],
        ];

        $this->batchInsert($this->tn_billing_card, $billingCardsRows, $billingCards);

        // foreign keys
        $this->addForeignKey('fk_card_owner_id', $this->tn_billing_card, 'owner_id', $this->tn_user, 'id', 'CASCADE', 'NO ACTION');
        $this->addForeignKey('fk_card_currency_id', $this->tn_billing_card, 'currency_id', $this->tn_currency, 'id', 'CASCADE', 'NO ACTION');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_card_owner_id', $this->tn_billing_card);
        $this->dropForeignKey('fk_card_currency_id', $this->tn_billing_card);

        $this->dropIndex('billing_card_unique_number', $this->tn_billing_card);

        $this->dropTable($this->tn_billing_card);
    }
}
