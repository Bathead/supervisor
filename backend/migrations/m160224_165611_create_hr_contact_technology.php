<?php

use yii\db\Migration;

class m160224_165611_create_hr_contact_technology extends Migration
{
    protected $tn_hr_contact_technology = '{{%hr_contact_technology}}';
    protected $tn_hr_contact = '{{%hr_contact}}';
    protected $tn_technology = '{{%technology}}';

    public function up()
    {
        $this->createTable($this->tn_hr_contact_technology, [
            'id' => $this->primaryKey(),
            'contact_id' => $this->integer(),
            'technology_id' => $this->integer(),
        ]);

        $this->addForeignKey('hr_contact_users_id', $this->tn_hr_contact_technology, 'contact_id', $this->tn_hr_contact, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('hr_contact_technologies_id', $this->tn_hr_contact_technology, 'technology_id', $this->tn_technology, 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('hr_contact_technologies_id', $this->tn_hr_contact_technology);
        $this->dropForeignKey('hr_contact_users_id', $this->tn_hr_contact_technology);
        $this->dropTable($this->tn_hr_contact_technology);
    }
}
