<?php

use yii\db\Schema;
use yii\db\Migration;

class m160118_083351_add_profile_image_to_profile extends Migration
{
    public function up()
    {
        $this->addColumn('profile', 'profile_image', $this->string(255));
    }

    public function down()
    {
        $this->dropColumn('profile', 'profile_image');
    }
}
