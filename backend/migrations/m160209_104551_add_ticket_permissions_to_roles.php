<?php

use yii\db\Schema;
use yii\db\Migration;

class m160209_104551_add_ticket_permissions_to_roles extends Migration
{
    protected $tn_auth_item_child = '{{%auth_item_child}}';

    public function up()
    {
        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            ['Hr', 'ticket.read'],
            ['Pm', 'ticket.read'],
            ['Qa', 'ticket.read'],
            ['Sales', 'ticket.read'],

            ['Hr', 'ticket.create'],
            ['Pm', 'ticket.create'],
            ['Qa', 'ticket.create'],
            ['Sales', 'ticket.create'],
            ['Supervisor', 'ticket.create'],

            ['Hr', 'ticket.manage'],
            ['Pm', 'ticket.manage'],
            ['Qa', 'ticket.manage'],
            ['Sales', 'ticket.manage'],

            ['Hr', 'ticketComments.create'],
            ['Pm', 'ticketComments.create'],
            ['Qa', 'ticketComments.create'],
            ['Sales', 'ticketComments.create'],

            ['Hr', 'ticketComments.delete'],
            ['Pm', 'ticketComments.delete'],
            ['Qa', 'ticketComments.delete'],
            ['Sales', 'ticketComments.delete'],

            ['Hr', 'ticketComments.read'],
            ['Pm', 'ticketComments.read'],
            ['Qa', 'ticketComments.read'],
            ['Sales', 'ticketComments.read'],

            ['Hr', 'ticketComments.update'],
            ['Pm', 'ticketComments.update'],
            ['Qa', 'ticketComments.update'],
            ['Sales', 'ticketComments.update'],

        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function down()
    {
        $this->delete($this->tn_auth_item_child, "parent = 'Hr' AND child = 'ticket.read'");
        $this->delete($this->tn_auth_item_child, "parent = 'Pm' AND child = 'ticket.read'");
        $this->delete($this->tn_auth_item_child, "parent = 'Qa' AND child = 'ticket.read'");
        $this->delete($this->tn_auth_item_child, "parent = 'Sales' AND child = 'ticket.read'");

        $this->delete($this->tn_auth_item_child, "parent = 'Hr' AND child = 'ticket.create'");
        $this->delete($this->tn_auth_item_child, "parent = 'Pm' AND child = 'ticket.create'");
        $this->delete($this->tn_auth_item_child, "parent = 'Qa' AND child = 'ticket.create'");
        $this->delete($this->tn_auth_item_child, "parent = 'Sales' AND child = 'ticket.create'");
        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'ticket.create'");


        $this->delete($this->tn_auth_item_child, "parent = 'Hr' AND child = 'ticket.manage'");
        $this->delete($this->tn_auth_item_child, "parent = 'Pm' AND child = 'ticket.manage'");
        $this->delete($this->tn_auth_item_child, "parent = 'Qa' AND child = 'ticket.manage'");
        $this->delete($this->tn_auth_item_child, "parent = 'Sales' AND child = 'ticket.manage'");

        $this->delete($this->tn_auth_item_child, "parent = 'Hr' AND child = 'ticketComments.create'");
        $this->delete($this->tn_auth_item_child, "parent = 'Pm' AND child = 'ticketComments.create'");
        $this->delete($this->tn_auth_item_child, "parent = 'Qa' AND child = 'ticketComments.create'");
        $this->delete($this->tn_auth_item_child, "parent = 'Sales' AND child = 'ticketComments.create'");

        $this->delete($this->tn_auth_item_child, "parent = 'Hr' AND child = 'ticketComments.delete'");
        $this->delete($this->tn_auth_item_child, "parent = 'Pm' AND child = 'ticketComments.delete'");
        $this->delete($this->tn_auth_item_child, "parent = 'Qa' AND child = 'ticketComments.delete'");
        $this->delete($this->tn_auth_item_child, "parent = 'Sales' AND child = 'ticketComments.delete'");

        $this->delete($this->tn_auth_item_child, "parent = 'Hr' AND child = 'ticketComments.read'");
        $this->delete($this->tn_auth_item_child, "parent = 'Pm' AND child = 'ticketComments.read'");
        $this->delete($this->tn_auth_item_child, "parent = 'Qa' AND child = 'ticketComments.read'");
        $this->delete($this->tn_auth_item_child, "parent = 'Sales' AND child = 'ticketComments.read'");

        $this->delete($this->tn_auth_item_child, "parent = 'Hr' AND child = 'ticketComments.update'");
        $this->delete($this->tn_auth_item_child, "parent = 'Pm' AND child = 'ticketComments.update'");
        $this->delete($this->tn_auth_item_child, "parent = 'Qa' AND child = 'ticketComments.update'");
        $this->delete($this->tn_auth_item_child, "parent = 'Sales' AND child = 'ticketComments.update'");
    }
}
