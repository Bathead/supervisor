<?php

use yii\db\Migration;

class m160420_140656_create_permisions_for_billing_card_module extends Migration
{
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';
    protected $tn_auth_assignment = '{{%auth_assignment}}';

    public function safeUp()
    {
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            // rank permissions
            ['billingCard.create', 2, 'Create billing card'],
            ['billingCard.read',   2, 'Read billing card'],
            ['billingCard.update', 2, 'Update billing card'],
            ['billingCard.delete', 2, 'Delete billing card'],
        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            ['Supervisor', 'billingCard.create'],
            ['Supervisor', 'billingCard.read'],
            ['Supervisor', 'billingCard.update'],
            ['Supervisor', 'billingCard.delete'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function safeDown()
    {
        $this->delete($this->tn_auth_item, "name = 'billingCard.create'");
        $this->delete($this->tn_auth_item, "name = 'billingCard.read'");
        $this->delete($this->tn_auth_item, "name = 'billingCard.update'");
        $this->delete($this->tn_auth_item, "name = 'billingCard.delete'");

        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'billingCard.create'");
        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'billingCard.read'");
        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'billingCard.update'");
        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'billingCard.delete'");
    }
}
