<?php

use yii\db\Migration;

/**
 * Handles adding permissions to table `sysadmin`.
 */
class m160712_122323_add_permissions_to_sysadmin extends Migration
{
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';
    protected $tn_auth_assignment = '{{%auth_assignment}}';

    public function safeUp()
    {
        // add new permissions to sysadmin role
        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            ['Sysadmin',  'user.hasRating'],
            ['Sysadmin',  'user.canSendMessage'],
            ['Sysadmin',  'user.canComplain'],
            ['Sysadmin',  'ticketComments.update'],
            ['Sysadmin',  'ticketComments.read'],
            ['Sysadmin',  'ticketComments.delete'],
            ['Sysadmin',  'ticketComments.create'],
            ['Sysadmin',  'ticket.update'],
            ['Sysadmin',  'ticket.read'],
            ['Sysadmin',  'ticket.manage'],
            ['Sysadmin',  'ticket.delete'],
            ['Sysadmin',  'ticket.create'],
            ['Sysadmin',  'ticket.consider'],
            ['Sysadmin',  'technology.update'],
            ['Sysadmin',  'technology.read'],
            ['Sysadmin',  'technology.delete'],
            ['Sysadmin',  'technology.create'],
            ['Sysadmin',  'rank.update'],
            ['Sysadmin',  'rank.read'],
            ['Sysadmin',  'rank.delete'],
            ['Sysadmin',  'rank.create'],
            ['Sysadmin',  'projectComments.update'],
            ['Sysadmin',  'projectComments.read'],
            ['Sysadmin',  'projectComments.delete'],
            ['Sysadmin',  'projectComments.create'],
            ['Sysadmin',  'project.select'],
            ['Sysadmin',  'project.manage'],
            ['Sysadmin',  'project.all'],
            ['Sysadmin',  'newsComments.update'],
            ['Sysadmin',  'newsComments.read'],
            ['Sysadmin',  'newsComments.delete'],
            ['Sysadmin',  'newsComments.create'],
            ['Sysadmin',  'news.read'],
            ['Sysadmin',  'language.update'],
            ['Sysadmin',  'language.read'],
            ['Sysadmin',  'language.delete'],
            ['Sysadmin',  'language.create'],
            ['Sysadmin',  'department.update'],
            ['Sysadmin',  'department.read'],
            ['Sysadmin',  'department.delete'],
            ['Sysadmin',  'department.create'],
            ['Sysadmin',  'currency.read'],
            ['Sysadmin',  'complaint.update'],
            ['Sysadmin',  'complaint.read'],
            ['Sysadmin',  'complaint.delete'],
            ['Sysadmin',  'complaint.create'],
            ['Sysadmin',  'billingCard.read'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);

    }

    public function safeDown()
    {
        $this->delete($this->tn_auth_item_child, "parent = 'Sysadmin'");
    }
}
