<?php

use yii\db\Schema;
use yii\db\Migration;

class m160112_091109_create_rank_and_rbac_permissions extends Migration
{
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';
    protected $tn_auth_assignment = '{{%auth_assignment}}';

    public function safeUp()
    {
        // add permissions
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            // rank permissions
            ['rank.create', 2, 'Create rank'],
            ['rank.read',   2, 'Read rank'],
            ['rank.update', 2, 'Update rank'],
            ['rank.delete', 2, 'Delete rank'],

            // rbac permissions
            ['rbac.all', 2, 'Role based access control'],
        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        // add new permissions to supervisor role
        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            ['Supervisor', 'rank.create'],
            ['Supervisor', 'rank.read'],
            ['Supervisor', 'rank.update'],
            ['Supervisor', 'rank.delete'],
            ['Supervisor', 'rbac.all'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);

    }

    public function safeDown()
    {
        $this->delete($this->tn_auth_item, "name = 'rank.create'");
        $this->delete($this->tn_auth_item, "name = 'rank.read'");
        $this->delete($this->tn_auth_item, "name = 'rank.update'");
        $this->delete($this->tn_auth_item, "name = 'rank.delete'");
        $this->delete($this->tn_auth_item, "name = 'rbac.all'");

        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'rank.create'");
        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'rank.read'");
        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'rank.update'");
        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'rank.delete'");
        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'rbac.all'");
    }

}
