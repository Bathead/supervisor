<?php

use yii\db\Schema;
use yii\db\Migration;

class m160104_124807_create_currency_table_with_test_data extends Migration
{
    protected $tn_currency = '{{%currency}}';

    public function safeUp()
    {
        $this->createTable($this->tn_currency, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'rate' => $this->double()->notNull()->defaultValue(1.0)
        ]);

        $this->insert($this->tn_currency, array('name' => 'USD', 'rate' => 1.0));
        $this->insert($this->tn_currency, array('name' => 'UAH', 'rate' => 24));
    }

    public function safeDown()
    {
        $this->dropTable($this->tn_currency);
    }
}
