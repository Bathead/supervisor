<?php

use yii\db\Schema;
use yii\db\Migration;
use app\modules\user\models\Profile;

class m160112_091106_add_englishLanguageLevel_id_to_profile extends Migration
{
    protected $tn_profile = '{{%profile}}';

    public function up()
    {
        $this->addColumn($this->tn_profile, 'englishLanguageLevel_id', $this->integer());
    }

    public function down()
    {
        $this->dropColumn($this->tn_profile, 'englishLanguageLevel_id');
    }
}
