<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\rbac;

class m160129_060956_create_permissions_for_ticket extends Migration
{
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';

    public function up()
    {
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            // ticket permissions
            ['ticket.create', 2, 'Can create new ticket'],
        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            // developer permissions
            ['Developer', 'ticket.create'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);

        //consider permission
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            ['ticket.consider', 2, 'Can answer to the tickets'],
        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            ['Supervisor', 'ticket.consider'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);

        //read permission
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            ['ticket.read', 2, 'Read tickets'],
        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            ['Supervisor', 'ticket.read'],
            ['Developer', 'ticket.read'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function down()
    {
        $this->delete($this->tn_auth_item_child, "parent = 'Developer' AND child = 'ticket.create'");
        $this->delete($this->tn_auth_item, "name = 'ticket.create' AND type = '2' AND description = 'Can create new ticket'");

        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'ticket.consider'");
        $this->delete($this->tn_auth_item, "name = 'ticket.consider' AND type = '2' AND description = 'Can answer to the tickets'");

        $this->delete($this->tn_auth_item_child, "parent = 'Supervisor' AND child = 'ticket.read'");
        $this->delete($this->tn_auth_item_child, "parent = 'Developer' AND child = 'ticket.read'");
        $this->delete($this->tn_auth_item, "name = 'ticket.read' AND type = '2' AND description = 'Read tickets'");
    }
}
