<?php

use yii\db\Schema;
use yii\db\Migration;
use app\modules\user\models\User;
use app\modules\technology\models\Technology;
use app\modules\technology\models\UserTechnology;

class m160106_110314_create_technology extends Migration
{
    protected $tn_user;
    protected $tn_technology;
    protected $tn_user_technologies;

    public function __construct()
    {
        parent::__construct();
        $this->tn_user = '{{%user}}';
        $this->tn_technology = '{{%technology}}';
        $this->tn_user_technologies = '{{%user_technologies}}';
    }

    public function safeUp()
    {
        $this->createTable($this->tn_technology, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()
        ]);

        $this->createTable($this->tn_user_technologies, [
            'id'            => $this->primaryKey(),
            'user_id'       => $this->integer()->notNull(),
            'technology_id' => $this->integer()->notNull(),
            'rating'        => $this->integer()->defaultValue(null),
            'active'        => $this->boolean()->defaultValue(true)->notNull()
        ]);

        $this->addForeignKey('technology_user_id', $this->tn_user_technologies, 'user_id', $this->tn_user, 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('technology_id', $this->tn_user_technologies, 'technology_id', $this->tn_technology, 'id', 'NO ACTION', 'NO ACTION');

        $technologies = [
            [1, 'PHP'],
            [2, 'Laravel'],
            [3, 'Yii2'],
            [4, 'Codeigniter'],
            [5, 'Drupal'],
            [6, 'Wordpress'],
            [7, 'AngularJs'],
        ];
        $this->batchInsert($this->tn_technology, ['id', 'name'], $technologies);

        $userTechnologies = [
            [3, 1, 7],
            [3, 2, 4],
            [3, 3, null],
            [3, 4, 3],
            [3, 7, null],
        ];
        $this->batchInsert($this->tn_user_technologies, ['user_id', 'technology_id', 'rating'], $userTechnologies);

    }

    public function safeDown()
    {
        $this->dropForeignKey('technology_user_id', $this->tn_user_technologies);
        $this->dropForeignKey('technology_id', $this->tn_user_technologies);
        $this->dropTable($this->tn_technology);
        $this->dropTable($this->tn_user_technologies);
    }
}
