<?php

use yii\db\Schema;
use yii\db\Migration;
use app\modules\ticket\models\Ticket;

class m160205_130428_create_test_tickets_comments extends Migration
{
    protected $tn_ticket = '{{%ticket}}';
    protected $tn_ticket_comment = '{{%ticket_comment}}';

    public function safeUp()
    {
        $ticketRows = ['id', 'author_id', 'title', 'description', 'created_at', 'status', 'priority'];
        $ticket = [
            [
                1,
                3,
                'Дуже потрібний тікет',
                'Добавте мені час, який не врахували',
                '2016-01-06 12:04:39',
                Ticket::STATUS_ACTIVE,
                Ticket::PRIORITY_IMPORTANT,
            ],
            [
                2,
                3,
                'Test ticket',
                'Very cool ticket',
                '2016-02-04 15:53:02',
                Ticket::STATUS_ACTIVE,
                Ticket::PRIORITY_NORMAL,
            ],
            [
                3,
                3,
                'Ще один тестовий тікет',
                'Вміст тестового тікета',
                '2016-02-04 15:53:02',
                Ticket::STATUS_ACTIVE,
                Ticket::PRIORITY_NORMAL,
            ],
        ];
        $this->batchInsert($this->tn_ticket, $ticketRows, $ticket);




        $created_at = $updated_at = date('Y-m-d H:i:s');
        $commentsRows = ['ticket_id', 'user_id', 'message', 'created_at', 'updated_at'];
        $comments = [
            [1, 1, 'Test comment 1', $created_at, $updated_at],
            [1, 1, 'Test comment 2', $created_at, $updated_at],
            [1, 1, 'Test comment 3', $created_at, $updated_at],
            [1, 3, 'Test comment 4', $created_at, $updated_at],
            [1, 1, 'Test comment 5', $created_at, $updated_at],
            [1, 3, 'Test comment 6', $created_at, $updated_at],
            [1, 1, 'Test comment 7', $created_at, $updated_at],
            [1, 1, 'Test comment 8', $created_at, $updated_at],
            [1, 1, 'Test comment 9', $created_at, $updated_at],
            [1, 1, 'Test comment 10', $created_at, $updated_at],
            [1, 1, 'Test comment 11', $created_at, $updated_at],
            [1, 1, 'Test comment 12', $created_at, $updated_at],
            [1, 1, 'Test comment 13', $created_at, $updated_at],
            [1, 1, 'Test comment 14', $created_at, $updated_at],
            [1, 1, 'Test comment 15', $created_at, $updated_at],

            [2, 1, 'Test comment 1', $created_at, $updated_at],
            [2, 1, 'Test comment 2', $created_at, $updated_at],
            [2, 1, 'Test comment 3', $created_at, $updated_at],
            [2, 3, 'Test comment 4', $created_at, $updated_at],
            [2, 1, 'Test comment 5', $created_at, $updated_at],
            [2, 3, 'Test comment 6', $created_at, $updated_at],
            [2, 1, 'Test comment 7', $created_at, $updated_at],
            [2, 1, 'Test comment 8', $created_at, $updated_at],
            [2, 1, 'Test comment 9', $created_at, $updated_at],
            [2, 1, 'Test comment 10', $created_at, $updated_at],
            [2, 1, 'Test comment 11', $created_at, $updated_at],
            [2, 1, 'Test comment 12', $created_at, $updated_at],
            [2, 1, 'Test comment 13', $created_at, $updated_at],
            [2, 1, 'Test comment 14', $created_at, $updated_at],
            [2, 1, 'Test comment 15', $created_at, $updated_at],

        ];
        $this->batchInsert($this->tn_ticket_comment, $commentsRows, $comments);
    }

    public function safeDown()
    {
        $this->execute("DELETE FROM $this->tn_ticket_comment WHERE ticket_id = 1 OR ticket_id = 2");
        $this->execute("DELETE FROM $this->tn_ticket WHERE id = 1 OR id = 2 OR id = 3");
    }
}
