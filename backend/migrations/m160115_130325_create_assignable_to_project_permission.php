<?php

use yii\db\Schema;
use yii\db\Migration;

class m160115_130325_create_assignable_to_project_permission extends Migration
{
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';
    protected $tn_auth_assignment = '{{%auth_assignment}}';

    public function safeUp()
    {
        $this->insert($this->tn_auth_item, [
            'name' => "user.assignableToProject",
            'type' => 2,
            'description' => 'User can be assigned to project'
        ]);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            ['Developer', 'user.assignableToProject'],
            ['Pm', 'user.assignableToProject'],
            ['Sales', 'user.assignableToProject'],
            ['Qa', 'user.assignableToProject'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function safeDown()
    {
        $this->delete($this->tn_auth_item, "name = 'user.assignableToProject'");
        $this->delete($this->tn_auth_item_child, "child = 'user.assignableToProject'");
    }
}
