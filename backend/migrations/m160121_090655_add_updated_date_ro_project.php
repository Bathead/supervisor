<?php

use yii\db\Schema;
use yii\db\Migration;
use app\modules\project\models\Project;

class m160121_090655_add_updated_date_ro_project extends Migration
{

    protected $tn_project = '{{%project}}';

    public function up()
    {
        $this->addColumn($this->tn_project, 'updated_date', $this->dateTime());
    }

    public function down()
    {
        $this->dropColumn($this->tn_project, 'updated_date');
    }
}
