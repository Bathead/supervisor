<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\db\Exception as dbException;
use yii\helpers\Console;

class AppController extends Controller
{
    public $showMigrationProcess = false;

    public function options($actionID)
    {
        return array_merge(
            ['showMigrationProcess', false]
        );
    }

    /**
     * Drop database and deploy migrate
     * @return int ExitCode
     * @internal param int $showProcessMigrate
     */
    public function actionCleardb()
    {
        $startTime = microtime(true);

        if (!YII_DEBUG) {
            $this->stderr('This command not allowed in prod envelopment', Console::FG_RED);
            return Controller::EXIT_CODE_ERROR;
        }

        if (!$this->confirm("Do you really want to delete the database?")) {
            return Controller::EXIT_CODE_NORMAL;
        }

        $dbConnection = Yii::$app->db;
        $dbName = $this->getDBName($dbConnection->dsn);

        try {
            $this->stdout("Try delete database:" . PHP_EOL, Console::FG_GREEN);

            if ($dbConnection->createCommand("DROP DATABASE `{$dbName}`")->execute()) {
                $this->stdout("\tDatabase deleted\n", Console::FG_GREEN);
            }
            if ($dbConnection->createCommand("CREATE DATABASE `{$dbName}` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci")->execute()) {
                $this->stdout("\tNew database created\n", Console::FG_GREEN);
            }
        } catch (dbException $e) {
            $this->stdout("Database error:\n", Console::FG_RED);
            $this->stdout($e->getMessage(), Console::FG_RED);
            return Controller::EXIT_CODE_ERROR;
        }

        $this->stdout("Start deploy migrate:\n", Console::FG_GREEN);

        if ($this->showMigrationProcess) {
            system('php yii migrate/up --interactive=0', $result);
        } else {
            $this->stdout("\tprocessing...\r", Console::FG_YELLOW);
            Console::hideCursor();
            exec('php yii migrate/up --interactive=0', $outputParams, $result);
            Console::showCursor();
        }

        if ($result !== Controller::EXIT_CODE_NORMAL) {
            $this->stdout("\n\nMigration error. Check stacktrace log.\n", Console::FG_RED);
        } else {
            $this->stdout("\tAll migration applied" . PHP_EOL, Console::FG_GREEN);

            $processTime = microtime(true) - $startTime;
            $this->stdout("All successful. Time: {$processTime}s" . PHP_EOL, Console::FG_GREEN);
        }

        return $result;
    }

    /**
     * @param $dsn
     * @return string|null
     */
    private function getDBName($dsn)
    {
        if (preg_match('/' . 'dbname' . '=([^;]*)/', $dsn, $match)) {
            return $match[1];
        } else {
            return null;
        }
    }
}