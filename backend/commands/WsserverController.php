<?php

namespace app\commands;

use yii;
use yii\console\Controller;
use yii\helpers\Console;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use app\modules\dialog\components\dialogManager\DialogsManager;

class WsserverController extends Controller
{
    private $_port = 8080;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->stdout("Ratchet web socket server started." . PHP_EOL, Console::FG_GREEN);
    }

    public function actionRun() {
        $this->startServer();
        return Controller::EXIT_CODE_NORMAL;
    }

    public function actionRunbg() {
        Yii::$app->consoleBackgroundRunner->run('wsserver/run');
    }

    private function startServer() {
        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    new DialogsManager()
                )
            ),
            $this->_port
        );

        $server->run();
    }
}