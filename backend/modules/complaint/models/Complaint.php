<?php
namespace app\modules\complaint\models;

use Yii;
use yii\db\ActiveRecord;
use app\modules\user\models\User;
use ReflectionClass;
use yii\helpers\Inflector;

/**
 * This is the model class for table "complaint".
 *
 * @property integer $id
 * @property integer $who_id
 * @property integer $whom_id
 * @property string $date
 * @property string $reason
 * @property integer $status
 * @property array $statuses
 * @property string $statusAsString
 * @property string $whoFullName
 * @property string $whomFullName
 *
 * @property User $whom
 * @property User $who
 */
class Complaint extends ActiveRecord
{
    const STATUS_NEW = 1;
    const STATUS_REVIEWED = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'complaint';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['who_id', 'whom_id', 'status'], 'integer'],
            [['date'], 'safe'],
            [['reason'], 'string']
        ];
    }

    public function fields()
    {
        $fields = array_merge(parent::fields(), [
            'whomFullName',
            'whoFullName',
            'who',
            'whom',
            'statusAsString'
        ]);

        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'             => 'ID',
            'who_id'         => 'Who',
            'whomFullName'   => 'Whom',
            'whom_id'        => 'Whom',
            'whoFullName'    => 'Who',
            'date'           => 'Date',
            'reason'         => 'Reason',
            'status'         => 'Status',
            'statusAsString' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWhom()
    {
        return $this->hasOne(User::className(), ['id' => 'whom_id']);
    }

    public function getWhomFullName()
    {
        return $this->whom->fullName;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWho()
    {
        return $this->hasOne(User::className(), ['id' => 'who_id']);
    }

    public function getWhoFullName()
    {
        return $this->who->fullName;
    }

    public static function getStatuses() {
        $result = [];
        $constPrefix = "STATUS_";

        // create a reflection class to get constants
        $reflClass = new ReflectionClass(get_called_class());
        $constants = $reflClass->getConstants();

        // check for status constants (e.g., STATUS_ACTIVE)
        foreach ($constants as $constantName => $constantValue) {

            // add prettified id and name to result
            if (strpos($constantName, $constPrefix) === 0) {
                $prettyName = str_replace($constPrefix, "", $constantName);
                $prettyName = Inflector::humanize(strtolower($prettyName));
                $result[] = [
                    'id' => $constantValue,
                    'title' => $prettyName
                ];
            }
        }

        return $result;
    }

    public function getStatusAsString()
    {
        if ($this->status === null) {
            return null;
        } else {
            $statuses = self::getStatuses();
            $status = array_search($this->status, array_column($statuses, 'id'));
            return $statuses[$status]['title'];
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->who_id = Yii::$app->user->id;
                $this->date = date('Y-m-d H:i:s');
            }
            return true;
        } else {
            return false;
        }
    }

    public static function getNewComplaints() {
        return self::find()->where(['status' => self::STATUS_NEW]);
    }

    public static function getComplaintsToUser($userId) {
        return self::find()
            ->where(['whom_id' => $userId])
            ->orderBy(['date' => SORT_DESC])
            ->all();
    }

    public static function getComplaintsFromUser($userId) {
        return self::find()
            ->where(['who_id' => $userId])
            ->orderBy(['date' => SORT_DESC])
            ->all();
    }
}