<?php

namespace app\modules\complaint\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\complaint\models\Complaint;
use app\modules\user\models\User;
use app\modules\user\models\Profile;

/**
 * SearchComplaint represents the model behind the search form about `app\modules\complaint\models\Complaint`.
 */
class SearchComplaint extends Complaint
{
    public $status;
    public $whoFullName;
    public $whomFullName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'who_id', 'whom_id', 'status'], 'integer'],
            [['date', 'reason', 'status', 'whoFullName', 'whomFullName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $tn_user = User::tableName();
        $tn_profile = Profile::tableName();
        $tn_complaint = Complaint::tableName();

        $query = Complaint::find()
            ->from("$tn_complaint as c");

        $query->leftJoin("$tn_user as u1", 'u1.id = c.who_id');
        $query->leftJoin("$tn_profile as p1", 'p1.id = u1.profile_id');
        $query->leftJoin("$tn_user as u2", 'u2.id = c.whom_id');
        $query->leftJoin("$tn_profile as p2", 'p2.id = u2.profile_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> [
                'attributes' => [
                    'status' => [
                        'asc' => ['status' => SORT_ASC],
                        'desc' => ['status' => SORT_DESC],
                    ],
                    'date' => [
                        'asc' => ['date' => SORT_ASC],
                        'desc' => ['date' => SORT_DESC],
                    ],
                    'reason' => [
                        'asc' => ['reason' => SORT_ASC],
                        'desc' => ['reason' => SORT_DESC],
                    ],
                    'whoFullName' => [
                        'asc' => ['p1.first_name' => SORT_ASC, 'p1.last_name' => SORT_ASC, 'p1.middle_name' => SORT_ASC],
                        'desc' => ['p1.first_name' => SORT_DESC, 'p1.last_name' => SORT_DESC], 'p1.middle_name' => SORT_DESC,
                        'label' => 'Who',
                        'default' => SORT_ASC
                    ],
                    'whomFullName' => [
                        'asc' => ['p2.first_name' => SORT_ASC, 'p2.last_name' => SORT_ASC, 'p2.middle_name' => SORT_ASC],
                        'desc' => ['p2.first_name' => SORT_DESC, 'p2.last_name' => SORT_DESC], 'p2.middle_name' => SORT_DESC,
                        'label' => 'Whom',
                        'default' => SORT_ASC
                    ],
                ],
                'defaultOrder' => ['status' => SORT_ASC, 'date' => SORT_DESC]
            ],
            'pagination' => [
                'pageSize' => $params['count'],
                'page' => $params['page'] - 1
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->whoFullName)) {
            $query->andFilterWhere([
                'or',
                ['like', 'p1.first_name', $this->whoFullName],
                ['like', 'p1.last_name', $this->whoFullName],
                ['like', 'p1.middle_name', $this->whoFullName],
            ]);
        }

        if (!empty($this->whomFullName)) {
            $query->andFilterWhere([
                'or',
                ['like', 'p2.first_name', $this->whomFullName],
                ['like', 'p2.last_name', $this->whomFullName],
                ['like', 'p2.middle_name', $this->whomFullName],
            ]);
        }

        $query->andFilterWhere(['c.status' => $this->status]);
        $query->andFilterWhere(['like', 'reason', $this->reason]);
        $query->andFilterWhere(['like', 'date', $this->date]);

        return $dataProvider;
    }
}
