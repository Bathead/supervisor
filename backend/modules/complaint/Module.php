<?php

namespace app\modules\complaint;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\complaint\controllers';

    public function init()
    {
        parent::init();
    }
}
