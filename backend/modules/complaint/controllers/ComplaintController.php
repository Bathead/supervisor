<?php

namespace app\modules\complaint\controllers;

use Yii;
use app\modules\complaint\models\Complaint;
use app\modules\complaint\models\search\SearchComplaint;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;

/**
 * ComplaintController implements the CRUD actions for Complaint model.
 */
class ComplaintController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    public function actionGetComplaints() {
        if (!Yii::$app->user->can('complaint.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $searchModel = new SearchComplaint();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return [
            'list' => $dataProvider->getModels(),
            'totalCount' => $dataProvider->totalCount
        ];
    }

    public function actionGetLatestComplaints() {
        if (!Yii::$app->user->can('complaint.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $count = Yii::$app->request->post('count');

        $models = Complaint::find()
            ->orderBy([
                'date' => SORT_DESC
            ])
            ->limit($count)
            ->all();

        return [
            'list' => $models
        ];
    }

    public function actionGetComplaint($id) {
        if (!Yii::$app->user->can('complaint.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        return [
            'entity' => $this->findModel($id)
        ];
    }

    public function actionGetComplaintStatuses() {
        return [
            'list' => Complaint::getStatuses()
        ];
    }

    public function actionGetCountOpened(){
        if (!Yii::$app->user->can('complaint.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $count = Complaint::find()
            ->where(['status' => Complaint::STATUS_NEW])
            ->count();

        return [
            'count' => $count,
        ];
    }

    public function actionCreate() {
        if (!Yii::$app->user->can('user.canComplain') && !Yii::$app->user->can('complaint.create')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $model = new Complaint();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->who->cleanImportantFields(true);
            $model->whom->cleanImportantFields(true);
            return [
                'success'   => true,
                'newRecord' => $model
            ];
        }
        return ['success' => false];
    }

    public function actionUpdate($id) {
        if (!Yii::$app->user->can('complaint.update')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['success' => true];
        }
        return ['success' => false];
    }

    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('complaint.delete')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $success = $this->findModel($id)->delete();
        return ['success' => $success];
    }

    public function actionMarkAsReviewed($id) {
        if (!Yii::$app->user->can('complaint.update')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $complaint = $this->findModel($id);
        $complaint->status = Complaint::STATUS_REVIEWED;
        return [
            'success' => $complaint->save()
        ];
    }

    public function actionGetNewComplaintsCount() {
        $count = 0;
        if (Yii::$app->user->can('complaint.read')) {
            $count = Complaint::getNewComplaints()->count();
        }
        return [
            'count' => $count
        ];
    }

    protected function findModel($id)
    {
        if (($model = Complaint::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
