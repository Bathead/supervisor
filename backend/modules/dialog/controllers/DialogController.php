<?php

namespace app\modules\dialog\controllers;

use app\modules\dialog\models\Dialog;
use app\modules\dialog\models\DialogMessage;
use app\modules\dialog\models\DialogUser;
use app\modules\dialog\models\DialogMessageUnread;
use app\modules\dialog\models\upload\UploadMessageAttachmentForm;
use app\modules\user\models\Profile;
use app\modules\user\models\User;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;

class DialogController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionGetDialogs() {
        $query = Dialog::find()
            ->with('lastMessage')
            ->from(Dialog::tableName() . ' as d')
            ->rightJoin(DialogUser::tableName() . ' as du', 'd.id = du.dialog_id')
            ->where(['du.user_id' => Yii::$app->user->id])
            ->orderBy(['d.last_action_date' => SORT_DESC]);

        return [
            'list' => $query->all()
        ];
    }

    public function actionGetDialog($id) {
        // check is user assigned to dialog
        if (!Dialog::isUserAssignedToDialog(Yii::$app->user->id, $id)) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $dialog = $this->findModel($id);
        $allMessagesLoaded = false;

        // get last <messagesPerPage> messages
        $messagesCount = DialogMessage::find()
            ->where(['dialog_id' => $id])
            ->count();

        $limit = $this->module->messagesPerPage;
        $offset = $messagesCount - $limit;
        if ($offset < 0) {
            $offset = 0;
        }

        if ($offset == 0) {
            $allMessagesLoaded = true;
        }

        $messages = DialogMessage::find()
            ->with(['user', 'user.profile'])
            ->where(['dialog_id' => $id])
            ->orderBy(['created_at' => SORT_ASC])
            ->limit($limit)
            ->offset($offset)
            ->all();

        // get unread messages before dialog opened
        $unreadMessagesIds = $dialog->unreadMessagesIds;

        return [
            'entity' => $dialog,
            'messages' => $messages,
            'unreadMessagesIds' => $unreadMessagesIds,
            'allMessagesLoaded' => $allMessagesLoaded,
            'accessToken' => Yii::$app->user->identity->access_token,
            'isDialogEditable' => ($dialog->author->id == Yii::$app->user->id)
        ];
    }

    public function actionGetMoreMessages() {

        $dialogId = Yii::$app->request->post('dialogId');
        $loadedMessagesCount = Yii::$app->request->post('loadedMessagesCount');

        // check is user assigned to dialog
        if (!Dialog::isUserAssignedToDialog(Yii::$app->user->id, $dialogId)) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $allMessagesLoaded = false;

        // get messages
        $messagesCount = DialogMessage::find()
            ->where(['dialog_id' => $dialogId])
            ->count();

        $limit = $messagesCount - $loadedMessagesCount;
        if ($limit > $this->module->messagesPerPage) {
            $limit = $this->module->messagesPerPage;
        }

        $offset = $messagesCount - $loadedMessagesCount - $limit;
        if ($offset < 0) {
            $offset = 0;
        }

        if ($offset == 0) {
            $allMessagesLoaded = true;
        }


        $dialog = Dialog::findOne($dialogId);
        $messages = DialogMessage::find()
            ->with(['user', 'user.profile'])
            ->where(['dialog_id' => $dialogId])
            ->orderBy(['created_at' => SORT_ASC])
            ->limit($limit)
            ->offset($offset)
            ->all();

        // get unread messages before dialog opened
        $unreadMessagesIds = $dialog->unreadMessagesIds;

        return [
            'messages' => $messages,
            'unreadMessagesIds' => $unreadMessagesIds,
            'allMessagesLoaded' => $allMessagesLoaded
        ];
    }

    // get users, which not assigned to dialog already
    public function actionGetOtherUsersByName() {
        if (!Yii::$app->user->can('user.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $name = Yii::$app->request->post('name');
        $dialogId = Yii::$app->request->post('dialogId');
        $dialogUserIds = $this->findModel($dialogId)->dialogUsersIds;

        $users = User::find()
            ->from(User::tableName() . ' as u')
            ->leftJoin(Profile::tableName() . ' as p', 'p.id = u.profile_id')
            ->where(['not in' , 'u.id', $dialogUserIds])
            ->andFilterWhere([
                'or',
                ['like', 'p.first_name', $name],
                ['like', 'p.last_name', $name]
            ])
            ->andWhere(['u.status' => User::STATUS_ACTIVE])
            ->all();

        return [
            'list' => $users
        ];
    }

    // remove unread mark from messages
    public function actionMarkMessagesAsRead() {
        $dialogId = Yii::$app->request->post('dialogId');
        $unreadMessagesIds = Yii::$app->request->post('unreadMessagesIds');

        // check is user assigned to dialog
        if (!Dialog::isUserAssignedToDialog(Yii::$app->user->id, $dialogId)) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $success = DialogMessageUnread::deleteAll([
            'user_id' => Yii::$app->user->id,
            'message_id' => $unreadMessagesIds
        ]);

        return [
            'success' => $success,
        ];
    }

    public function actionCreate() {
        $subject = Yii::$app->request->post('subject');
        $message = Yii::$app->request->post('message');
        $userIds = [Yii::$app->request->post('userId')];

        if ($newDialogId = Dialog::createDialog($subject, $userIds, $message)) {
            return [
                'success'  => true,
                'dialogId' => $newDialogId
            ];
        }
        return ['success'  => false];
    }

    public function actionLeave($id) {
        // check is user assigned to dialog
        if (!Dialog::isUserAssignedToDialog(Yii::$app->user->id, $id)) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        return [
            'success' => Dialog::leaveDialog($id)
        ];
    }

    public function actionAttachFilesToMessage($id) {
        if ($message = DialogMessage::findOne($id)) {
            // check, is current user the message author
            if ($message->user_id == Yii::$app->user->id) {

                $uploadModel = new UploadMessageAttachmentForm();
                $success = $uploadModel->upload($id);
                return [
                    'success'       => $success,
                    'updatedRecord' => $message
                ];

            } else {
                throw new ForbiddenHttpException('You are not allowed to perform this action.');
            }
        }
        return ['success'  => false];
    }

    protected function findModel($id)
    {
        if (($model = Dialog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
