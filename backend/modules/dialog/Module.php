<?php

namespace app\modules\dialog;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\dialog\controllers';

    public $messagesPerPage = 20;

    public function init()
    {
        parent::init();
    }
}