<?php

namespace app\modules\dialog\components\dialogManager;

use app\modules\dialog\components\dialogManager\models\DmUser;
use Yii;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use yii\helpers\Json;
use app\modules\dialog\components\dialogManager\models\DmDialog;
use app\modules\user\models\User;

/**
 * Dialogs manager
 *
 * @property DmDialog[] $dialogs
 */
class DialogsManager implements MessageComponentInterface {

    protected $dialogs = [];

    public function onOpen(ConnectionInterface $conn) {
        // data initialization
        $getParameters = $conn->WebSocket->request->getQuery()->toArray();
        $this->arrayHtmlspecialchars($getParameters);
        $accessToken = $getParameters['accessToken'];
        $dialogId = $getParameters['dialogId'];

        // check access token and assigning to dialog
        if ($user = DmUser::isUserValid($accessToken, $dialogId)) {
            $dmUser = new DmUser($user, $conn);

            // check, is dialog already exist in dialogs list
            if (!array_key_exists($dialogId, $this->dialogs)) {
                $this->dialogs[$dialogId] = new DmDialog($dialogId);
            }

            $this->dialogs[$dialogId]->attachUser($dmUser);

            echo "New connection ({$conn->resourceId}). Dialog id: {$dialogId}. User id: {$user->id}.\n";
        } else {
            $conn->close();
        }
    }

    public function onMessage(ConnectionInterface $from, $data) {
        // data initialization
        $getParameters = $from->WebSocket->request->getQuery()->toArray();
        $accessToken = $getParameters['accessToken'];
        $dialogId = $getParameters['dialogId'];

        $data = Json::decode($data, true);
        $this->arrayHtmlspecialchars($data);
        $eventType = $data['eventType'];
        $dialog = $this->dialogs[$dialogId];

        // check is user valid
        if ($user = DmUser::isUserValid($accessToken, $dialog->id)) {
            if (method_exists($this, $eventType)) {
                $this->$eventType($from, $user, $dialog, $data);
            }
        }
    }

    private function event_new_message(ConnectionInterface $from, User $user, DmDialog $dialog, $data) {
        if ($message = $dialog->addMessage($data['message'], $user->id)) {

            // format message and send other dialog partisipants
            $author = [
                'fullName' => $user->fullName,
                'profile'  => [
                    'profileImageFullPath' => $user->profile->profileImageFullPath
                ]
            ];

            $message = [
                'id'         => $message->id,
                'updated_at' => $message->updated_at,
                'message'    => $message->message,
                'user'       => $author
            ];

            $response = [
                'message' => $message
            ];
            $dialog->sendAllOtherUsers($this->formatResponse($response, $user), $from);

            // update counters for users, connected to others dialogs
            $this->event_update_other_dialogs_counters($from, $user, $dialog, $data);

            echo sprintf('Connection %d (user id: %d) sending message "%s" to %d other connections' . "\n"
                , $from->resourceId, $user->id, $data['message'], $dialog->getUsersCount() - 1);
        }
    }

    private function event_update_other_dialogs_counters(ConnectionInterface $from, User $user, DmDialog $dialog, $data) {
        $response = [
            'dialogId'   => $dialog->id,
            'counterInc' => 1
        ];

        foreach($this->dialogs as $d) {
            if ($d->id !== $dialog->id) {
                $d->sendAllOtherUsers($this->formatResponse($response, $user), $from);
            }
        }
    }

    private function event_update_message(ConnectionInterface $from, User $user, DmDialog $dialog, $data) {
        $response = [
            'updatedMessage' => $data['updatedMessage'],
        ];

        $dialog->sendAllOtherUsers($this->formatResponse($response, $user), $from);

        echo sprintf('Connection %d (user id: %d) update message (message id: %d)' . "\n"
            , $from->resourceId, $user->id, $data['updatedMessage']->id);
    }

    private function event_change_dialog_subject(ConnectionInterface $from, User $user, DmDialog $dialog, $data) {
        $dialog->entity->subject = $data['subject'];

        if ($dialog->entity->save()) {

            $response = [
                'newSubject' => $data['subject'],
            ];
            $dialog->sendAllOtherUsers($this->formatResponse($response, $user), $from);

            echo sprintf('Connection %d (user id: %d) change dialog %d subject (send to %d other connection)' . "\n"
                , $from->resourceId, $user->id, $dialog->id, $dialog->getUsersCount() - 1);
        }
    }

    private function event_add_user(ConnectionInterface $from, User $user, DmDialog $dialog, $data) {
        $userIds = $data['userIds'];
        if ($newUsers = $dialog->addUsersToDialog($userIds)) {

            $response = [];
            foreach ($newUsers as $user){
                $temp = [
                    'id'       => $user->id,
                    'fullName' => $user->fullName,
                    'profile'  => [
                        'profileImageFullPath' => $user->profile->profileImageFullPath
                    ]
                ];
                $response['newUsers'][] = $temp;
            }
            $dialog->sendAllOtherUsers($this->formatResponse($response, $user), $from);

            echo sprintf('Connection %d (user id: %d) add to dialog "%d" users with ids: %s (send to %d other connection)' . "\n",
                $from->resourceId, $user->id, $dialog->id, implode($userIds, ', '), $dialog->getUsersCount() - 1);
        }
    }

    private function event_user_write_message(ConnectionInterface $from, User $user, DmDialog $dialog, $data) {
        $type = $data['type']; // start | stop
        if ($type == 'start') {
            $dialog->usersWriteMessageAdd($user);
        } elseif ($type == 'stop') {
            $dialog->usersWriteMessageRemove($user);
        }

        $response['usersWriteMessage'] = $dialog->getUsersWriteMessageList();
        $dialog->sendAllOtherUsers($this->formatResponse($response, $user), $from);
    }

    private function event_remove_user(ConnectionInterface $from, User $user, DmDialog $dialog, $data) {
        $currentUserId = $user->id;
        $removedUserId = $data['userId'];
        if (($dialog->entity->author_id == $currentUserId) || ($removedUserId == $currentUserId)) {
            if ($removedUser = $dialog->removeUserFromDialog($removedUserId)) {

                $response = [
                    'removedUser' => [
                        'id'       => $removedUser->id,
                        'fullName' => $removedUser->fullName,
                    ]
                ];
                $dialog->sendAllOtherUsers($this->formatResponse($response, $user), $from);

                echo sprintf('Connection %d (user id: %d) remove from dialog "%d" user with id: %s (send to %d other connection)' . "\n",
                    $from->resourceId, $user->id, $dialog->id, $removedUserId, $dialog->getUsersCount() - 1);
            }
        }
    }

    /**
     * Add to response event type and information about user, which create event
     *
     * @param array $data
     * @param User $user
     * @return null
     */
    private function formatResponse($data, $user = null) {
        $dbt = debug_backtrace();
        $responseInfo['eventType'] = $dbt[1]['function']; // get name of function, which call formatResponse()

        if ($user !== null) {
            $responseInfo['userId'] = $user->id;
            $responseInfo['username'] = $user->fullName;
        }

        return array_merge($responseInfo, $data);
    }


    public function onClose(ConnectionInterface $conn) {

        $getParameters = $conn->WebSocket->request->getQuery()->toArray();
        $dialog = $this->dialogs[$getParameters['dialogId']];
        $dialog->usersWriteMessageRemoveByConn($conn);

        $response['usersWriteMessage'] = $dialog->getUsersWriteMessageList();
        $dialog->sendAllOtherUsers($this->formatResponse($response), $conn);

        $dialog->detachUserByConnection($conn);

        echo "Connection {$conn->resourceId} has disconnected. Dialog id: {$dialog->id}.\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }

    private function arrayHtmlspecialchars(&$arr) {
        foreach($arr as &$value) {
            if (!is_array($value)) {
                $value = htmlspecialchars($value);
            }
        }
        unset($value);
    }
}