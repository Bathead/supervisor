<?php

namespace app\modules\dialog\components\dialogManager\models;

use Yii;
use Ratchet\ConnectionInterface;
use yii\helpers\Json;
use app\modules\dialog\models\DialogMessage;
use app\modules\dialog\models\Dialog;
use app\modules\user\models\User;

/**
 * Dialog manager Dialog
 *
 * @property integer $id
 * @property DmUser[] $users
 * @property Dialog $entity
 */
class DmDialog {
    public $id;
    public $users;
    public $entity;
    private $_usersWriteMessage = [];

    public function __construct($id) {
        $this->id = $id;
        $this->entity = Dialog::findOne($id);
    }

    /**
     * Add message to current dialog
     *
     * @param string $message
     * @param integer $userId
     * @return DialogMessage|false
     */
    public function addMessage($message, $userId) {
        $dm = new DialogMessage();
        $dm->message = $message;
        $dm->dialog_id = $this->id;
        $dm->user_id = $userId;
        return ($dm->save()) ? $dm : false;
    }

    public function attachUser($dmUser) {
        $this->users[] = $dmUser;
    }

    public function getUsersCount() {
        return count($this->users);
    }

    /**
     * Send data (in JSON) to all users
     *
     * @param array $data
     * @param ConnectionInterface $from
     * @return null
     */
    public function sendAllOtherUsers($data, $from) {
        foreach($this->users as $id => $user) {
            $data['isMyRequest'] = ($from === $user->conn);
            $user->send(Json::encode($data));
        }
    }

    /**
     * Remove user from dialog users list
     *
     * @param ConnectionInterface $conn
     * @return null
     */
    public function detachUserByConnection($conn) {
        foreach($this->users as $key => $user) {
            if ($user->conn === $conn) {
                unset($this->users[$key]);
            }
        }
    }

    /**
     * Add users to dialog
     *
     * @param integer[] $userIds
     * @return User[]|false
     */
    public function addUsersToDialog($userIds) {
        if ($this->entity->addUsersToDialog($userIds)) {
            // return data about users, if success
            return User::find()
                ->with('profile')
                ->where(['id' => $userIds])
                ->all();
        }
        return false;
    }

    /**
     * Remove user from dialog
     *
     * @param integer $userId
     * @return User|false
     */
    public function removeUserFromDialog($userId) {
        if (Dialog::leaveDialog($this->entity->id, $userId)) {
            return User::find()
                ->with('profile')
                ->where(['id' => $userId])
                ->one();
        }
        return false;
    }

    /**
     * Returns list of users, who write message
     *
     * @return array
     */
    public function getUsersWriteMessageList() {
        $result = [];
        foreach ($this->_usersWriteMessage as $id => $name) {
            $result[] = [
                'id' => $id,
                'fullName' => $name
            ];
        }
        return $result;
    }

    /**
     * Add user to list of users, who write message
     *
     * @param User $user
     * @return null
     */
    public function usersWriteMessageAdd($user) {
        $this->_usersWriteMessage[$user->id] = $user->fullName;
    }

    /**
     * Remove user from list of users, who write message
     *
     * @param User $user
     * @return null
     */
    public function usersWriteMessageRemove($user) {
         unset($this->_usersWriteMessage[$user->id]);
    }

    /**
     * Remove user from list of users, who write message
     *
     * @param ConnectionInterface $conn
     * @return null
     */
    public function usersWriteMessageRemoveByConn($conn) {
        foreach($this->users as $key => $user) {
            if ($user->conn === $conn) {
                unset($this->_usersWriteMessage[$user->entity->id]);
            }
        }
    }
}

