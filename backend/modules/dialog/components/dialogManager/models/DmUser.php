<?php

namespace app\modules\dialog\components\dialogManager\models;

use Yii;
use Ratchet\ConnectionInterface;
use app\modules\user\models\User;
use app\modules\dialog\models\DialogUser;

/**
 * Dialog manager dialog User
 *
 * @property User $entity
 * @property ConnectionInterface $conn
 * @property string $accessToken
 */

class DmUser {
    public $entity;
    public $conn;        // ConnectionInterface interface object

    public function __construct($entity, $conn) {
        $this->entity = $entity;
        $this->conn = $conn;
    }

    public function send($data) {
        $this->conn->send($data);
    }

    /**
     * Search user in database by access token, and check, is user assigned to dialog
     * Close connection, if user not exist
     *
     * @param string $accessToken
     * @return User|false
     */
    public static function isUserValid($accessToken, $dialogId) {
        $user = User::find()
            ->from(User::tableName() . ' as u')
            ->with(['profile'])
            ->rightJoin(DialogUser::tableName() . ' as du', 'u.id = du.user_id')
            ->where([
                'u.access_token' => $accessToken,
                'du.dialog_id'   => $dialogId,
                'u.status'       => User::STATUS_ACTIVE
            ])
            ->one();

        return ($user === null) ? false : $user;
    }

}