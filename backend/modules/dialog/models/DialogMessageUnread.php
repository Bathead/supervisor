<?php

namespace app\modules\dialog\models;

use Yii;
use app\modules\user\models\User;

/**
 * This is the model class for table "{{%dialog_message_unread}}".
 *
 * @property integer $message_id
 * @property integer $user_id
 *
 * @property User $user
 * @property DialogMessage $message
 */
class DialogMessageUnread extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dialog_message_unread}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message_id', 'user_id'], 'required'],
            [['message_id', 'user_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'message_id' => 'Message ID',
            'user_id'    => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessage()
    {
        return $this->hasOne(DialogMessage::className(), ['id' => 'message_id']);
    }
}
