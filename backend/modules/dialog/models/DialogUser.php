<?php

namespace app\modules\dialog\models;

use Yii;
use app\modules\user\models\User;

/**
 * This is the model class for table "{{%dialog_user}}".
 *
 * @property integer $dialog_id
 * @property integer $user_id
 *
 * @property User $user
 * @property Dialog $dialog
 */
class DialogUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dialog_user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dialog_id', 'user_id'], 'required'],
            [['dialog_id', 'user_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dialog_id' => 'Dialog ID',
            'user_id'   => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDialog()
    {
        return $this->hasOne(Dialog::className(), ['id' => 'dialog_id']);
    }
}
