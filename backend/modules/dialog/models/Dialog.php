<?php

namespace app\modules\dialog\models;

use Yii;
use app\modules\user\models\User;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "{{%dialog}}".
 *
 * @property integer $id
 * @property string $subject
 * @property integer $author_id
 * @property string $created_at
 * @property integer $last_message_id
 * @property string $last_action_date
 * @property integer $is_deleted
 *
 * @property integer $unreadMessagesCount
 * @property DialogMessage $lastMessage
 * @property User $author
 * @property DialogMessage[] $messages
 * @property User[] $dialogUsers
 * @property int[] DialogUsersIds
 * @property DialogMessage[] $unreadMessages
 * @property integer[] $unreadMessagesIds
 */
class Dialog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dialog}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject'], 'string'],
            [['author_id', 'last_message_id', 'is_deleted'], 'integer'],
            [['created_at'], 'safe']
        ];
    }

    public function fields()
    {
        $fields = array_merge(parent::fields(), [
            'lastMessage',
            'unreadMessagesCount',
            'dialogUsers'
        ]);

        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => 'ID',
            'subject'         => 'Subject',
            'author_id'       => 'Author ID',
            'created_at'      => 'Created At',
            'last_message_id' => 'Last Message ID',
            'is_deleted'      => 'Is Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastMessage()
    {
        return $this->hasOne(DialogMessage::className(), ['id' => 'last_message_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDialogUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])
            ->viaTable(DialogUser::tableName(), ['dialog_id' => 'id']);
    }

    public function getDialogUsersIds() {
        $result = [];
        foreach ($this->dialogUsers as $user){
            $result[] = $user->id;
        }
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(DialogMessage::className(), ['dialog_id' => 'id']);
    }

    public function getUnreadMessages() {
        return DialogMessage::find()
            ->from(DialogMessage::tableName() . ' as dm')
            ->rightJoin(DialogMessageUnread::tableName() . ' as dmu', 'dmu.message_id = dm.id')
            ->where(['dm.dialog_id' => $this->id])
            ->andWhere(['dmu.user_id' => Yii::$app->user->id])
            ->all();
    }

    public function getUnreadMessagesIds() {
        $result = [];
        foreach($this->unreadMessages as $m) {
            $result[] = $m->id;
        }
        return $result;
    }

    public function getUnreadMessagesCount() {
        return count($this->unreadMessages);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->author_id = Yii::$app->user->id;
                $this->created_at = date('Y-m-d H:i:s');
            }
            return true;
        } else {
            return false;
        }
    }

    public function addUsersToDialog($userIds) {
        $data = [];
        foreach ($userIds as $userId) {
            $data[] = [$this->id, $userId];
        }

        // save dialog -> user records
        $success = Yii::$app->db->createCommand()
            ->batchInsert(DialogUser::tableName(), ['dialog_id', 'user_id'], $data)
            ->execute();

        return $success;
    }

    public function removeUserFromDialog($userId) {
        return DialogUser::deleteAll(['dialog_id' => $this->id, 'user_id' => $userId]);
    }

    public static function createDialog($subject, $newDialogUsersIds, $firstMessage = null) {
        $transaction = Yii::$app->db->beginTransaction();
        try {

            // create dialog
            $dialog = new Dialog();
            $dialog->subject = $subject;
            $dialog->save();

            // save dialog users
            $userIds = $newDialogUsersIds;
            $data[] = [$dialog->id, Yii::$app->user->id];
            foreach ($userIds as $userId) {
                $data[] = [$dialog->id, $userId];
            }
            Yii::$app->db->createCommand()->batchInsert(DialogUser::tableName(), ['dialog_id', 'user_id'], $data)->execute();

            if ($firstMessage !== null) {
                // save first message
                $message = new DialogMessage();
                $message->dialog_id = $dialog->id;
                $message->message = $firstMessage;
                $message->save();
            }

            $transaction->commit();
            return $dialog->id;
        } catch (\Exception $e) {
            $transaction->rollBack();
            return false;
        }
    }

    public static function leaveDialog($dialogId, $userId = null) {
        if ($userId === null) {
            $userId = Yii::$app->user->id;
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {

            // remove user assigning to dialog
            $dialogUserModel = DialogUser::find()
                ->where(['dialog_id' => $dialogId, 'user_id' => $userId])
                ->one();
            if ($dialogUserModel !== null) {
                $dialogUserModel->delete();
            }

            // remove all unread messages for abandoned dialog and current user
            $unreadMessages = DialogMessageUnread::find()
                ->from(DialogMessageUnread::tableName() . ' as dmu')
                ->leftJoin(DialogMessage::tableName() . ' as dm', 'dmu.message_id = dm.id')
                ->where([
                    'dmu.user_id' => $userId,
                    'dm.dialog_id' => $dialogId
                ])
                ->all();
            foreach ($unreadMessages as $um) {
                $um->delete();
            }

            // if last user leave dialog -> remove dialog
            $dialogUsersCount = DialogUser::find()
                ->where(['dialog_id' => $dialogId])
                ->count();
            if ($dialogUsersCount == 0) {

                // delete dialog messages attachments
                $messagesIds = DialogMessage::find()
                    ->select('id')
                    ->where(['dialog_id' => $dialogId])
                    ->indexBy('id')
                    ->column();
                
                foreach($messagesIds as $id) {
                    $path = Yii::$app->params['backendPublicDirPath'] . DialogMessage::$attachmentsPath . $id;
                    if (file_exists($path)) {
                        FileHelper::removeDirectory($path);
                    }
                }

                $dialog = Dialog::findOne($dialogId);
                $dialog->delete();
            }

            $transaction->commit();
            return true;
        } catch (\Exception $e) {
            $transaction->rollBack();
            return false;
        }
    }

    public static function isUserAssignedToDialog($userId, $dialogId) {
        $count = DialogUser::find()
            ->where(['user_id' => $userId, 'dialog_id' => $dialogId])
            ->count();
        return ($count > 0);
    }

}