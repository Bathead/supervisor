<?php

namespace app\modules\dialog\models;

use Yii;
use app\modules\user\models\User;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "{{%dialog_message}}".
 *
 * @property integer $id
 * @property integer $dialog_id
 * @property integer $user_id
 * @property string $message
 * @property integer $parent_id
 * @property string $updated_at
 * @property string $created_at
 * @property integer $is_deleted
 *
 * @property DialogMessage $parent
 * @property Dialog $dialog
 * @property User $user
 * @property string $updatedTime
 * @property string $isUnread
 * @property string[] $fileAttachments
 * @property array $fullPathFileAttachments
 */
class DialogMessage extends \yii\db\ActiveRecord
{
    public static $attachmentsPath = 'uploads/dialogMessagesAttachments/';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dialog_message}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dialog_id', 'user_id', 'parent_id', 'is_deleted'], 'integer'],
            [['message'], 'string'],
            [['updated_at', 'created_at'], 'safe']
        ];
    }

    public function fields()
    {
        $fields = array_merge(parent::fields(), [
            'user',
            'isUnread',
            'updatedTime',
            'fullPathFileAttachments'
        ]);

        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'dialog_id'  => 'Dialog ID',
            'user_id'    => 'User ID',
            'message'    => 'Message',
            'parent_id'  => 'Parent ID',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(DialogMessage::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDialog()
    {
        return $this->hasOne(Dialog::className(), ['id' => 'dialog_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUpdatedTime() {
        $date = strtotime($this->updated_at);
        $result = date('H:i', $date);
        return $result;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $currentTime = date('Y-m-d H:i:s');
            if ($insert) {
                if ($this->user_id === null) {
                    $this->user_id = Yii::$app->user->id;
                }
                $this->created_at = $currentTime;
            }
            $this->updated_at = $currentTime;
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes) {
        if ($insert) {
            // change last dialog message id
            $this->dialog->last_message_id = $this->id;

            // add unread messages records
            $userIds = DialogUser::find()
                ->select('user_id')
                ->where(['dialog_id' => $this->dialog_id])
                ->andWhere(['!=', 'user_id', $this->user_id])
                ->indexBy('user_id')
                ->column();

            if (count($userIds) > 0) {
                $data = [];
                foreach ($userIds as $userId) {
                    $data[] = [$this->id, $userId];
                }

                Yii::$app->db->createCommand()->batchInsert(DialogMessageUnread::tableName(), ['message_id', 'user_id'], $data)->execute();
            }
        }
        // change last dialog action date
        $this->dialog->last_action_date = $this->updated_at;
        $this->dialog->save();
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $newLastMessage = $this->find()
                ->where(['dialog_id' => $this->dialog_id])
                ->andWhere(['!=', 'id', $this->id])
                ->orderBy('created_at DESC')
                ->one();

            $this->dialog->last_message_id = ($newLastMessage === null) ? null : $newLastMessage->id;
            $this->dialog->last_action_date = date('Y-m-d H:i:s');
            $this->dialog->save();

            return true;
        } else {
            return false;
        }
    }

    public function getIsUnread() {
        $count = DialogMessageUnread::find()
            ->where(['message_id' => $this->id, 'user_id' => Yii::$app->user->id])
            ->count();
        return ($count > 0);
    }

    public static function getUnreadMessagesCount() {
        $count = DialogMessageUnread::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->count();
        return $count;
    }

    public function getFileAttachments() {
        $path = FileHelper::normalizePath(self::$attachmentsPath . $this->id);
        if (file_exists($path)) {
            return FileHelper::findFiles($path);
        }
        return false;
    }

    public function getFullPathFileAttachments() {
        if ($files = $this->fileAttachments) {
            $result = [];
            foreach ($files as $file) {
                $result[] = [
                    'name' => basename($file),
                    'path' => Yii::$app->params['backendPublicDirPath'] . $file
                ];
            }
            return $result;
        }
        return false;
    }
}