<?php

namespace app\modules\payment;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\payment\controllers';

    public function init()
    {
        parent::init();
    }
}
