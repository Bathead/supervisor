<?php

namespace app\modules\payment\controllers;

use Yii;
use app\modules\payment\models\BillingCard;
use app\modules\payment\models\search\SearchBillingCard;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;
use app\modules\user\models\User;
use app\modules\user\models\Profile;

/**
 * BillingCardController implements the CRUD actions for BillingCard model.
 */
class BillingCardController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionGetBillingCards(){
        if (!Yii::$app->user->can('billingCard.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $searchModel = new SearchBillingCard();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return [
            'list' => $dataProvider->getModels(),
            'totalCount' => $dataProvider->totalCount
        ];
    }

    public function actionGetBillingCard($id){
        if(!Yii::$app->user->can('billingCard.read')){
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        return [
            'entity' => BillingCard::findOne($id)
        ];
    }

    public function actionGetUsersByName() {
        if (!Yii::$app->user->can('user.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $name = Yii::$app->request->post('name');

        $users = User::find()
            ->from(User::tableName() . ' as u')
            ->leftJoin(Profile::tableName() . ' as p', 'p.id = u.profile_id')
            ->where(['u.status' => User::STATUS_ACTIVE])
            ->andFilterWhere([
                'or',
                ['like', 'p.first_name', $name],
                ['like', 'p.last_name', $name]
            ])
            ->all();

        return [
            'list' => $users
        ];
    }

    public function actionGetStates() {
        return [
            'list' => BillingCard::getStates()
        ];
    }

    public function actionCreate(){
        if(!Yii::$app->user->can('billingCard.create')){
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = new BillingCard();

        if($model->load(Yii::$app->request->post()) && $model->save()){
            return ['success' => true];
        }

        return ['success' => false];
    }

    public function actionUpdate($id){
        if(!Yii::$app->user->can('billingCard.update')){
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = $this->findModel($id);

        if($model->load(Yii::$app->request->post()) && $model->save()){
            return ['success' => true];
        }

        return ['success' => true];
    }

    public function actionDelete($id){
        if(!Yii::$app->user->can('billingCard.delete')){
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $success = $this->findModel($id)->delete();
        return ['success' => $success];
    }

    public function actionCheckBillingCardUniqueField() {
        $fieldName = Yii::$app->request->post('fieldName');
        $fieldValue = Yii::$app->request->post('fieldValue');

        $count = BillingCard::find()
            ->where([$fieldName => $fieldValue])
            ->count();

        return [
            'isUnique' => !($count > 0)
        ];
    }

    /**
     * Finds the BillingCard model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BillingCard the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BillingCard::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
