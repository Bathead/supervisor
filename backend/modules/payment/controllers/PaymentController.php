<?php

namespace app\modules\payment\controllers;

use Yii;
use app\modules\payment\models\Payment;
use app\modules\payment\models\search\SearchPayment;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use app\modules\user\models\User;
use app\modules\user\models\Profile;

/**
 * PaymentController implements the CRUD actions for Payment model.
 */
class PaymentController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionGetPayments(){
        if(!Yii::$app->user->can('payment.read')){
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $searchModel = new SearchPayment();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return [
            'list' => $dataProvider->getModels(),
            'totalCount' => $dataProvider->totalCount
        ];
    }

    public function actionGetFilteredPayments(){
        if(!Yii::$app->user->can('payment.read')){
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $searchModel = new SearchPayment();
        $dataProvider = $searchModel->searchFiltered(Yii::$app->request->queryParams);

        return [
            'list' => $dataProvider->getModels()
        ];
    }

    public function actionGetPayment($id){
        if(!Yii::$app->user->can('payment.read')){
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        return [
            'entity' => Payment::findOne($id)
        ];
    }

    public function actionGetPaymentVersions($id) {
        if (!Yii::$app->user->can('payment.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $searchModel = new SearchPayment();
        $dataProvider = $searchModel->searchVersions(Yii::$app->request->queryParams, $id);

        return [
            'list' => $dataProvider->getModels(),
            'totalCount' => $dataProvider->totalCount
        ];
    }

    public function actionGetPaymentStatuses() {
        return [
            'list' => Payment::getPaymentStatuses()
        ];
    }

    public function actionGetPaymentVersion() {

        $id = Yii::$app->request->post('id');
        $version = Yii::$app->request->post('version');

        return [
            'entity' => Payment::findOne(['parent' => $id, 'version' => $version])
        ];
    }

    public function actionGetUsersByName() {
        if (!Yii::$app->user->can('user.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $name = Yii::$app->request->post('name');

        $users = User::find()
            ->from(User::tableName() . ' as u')
            ->leftJoin(Profile::tableName() . ' as p', 'p.id = u.profile_id')
            ->where(['u.status' => User::STATUS_ACTIVE])
            ->andFilterWhere([
                'or',
                ['like', 'p.first_name', $name],
                ['like', 'p.last_name', $name]
            ])
            ->all();

        return [
            'list' => $users
        ];
    }

    public function actionCreate(){
        if(!Yii::$app->user->can('payment.create')){
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = new Payment();

        if($model->load(Yii::$app->request->post()) && $model->save()){
            return ['success' => true];
        }

        return ['success' => false];
    }

    public function actionUpdateList(){
        if(!Yii::$app->user->can('payment.update')){
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $ids = Yii::$app->request->post('ids');
        $status = Yii::$app->request->post('status');

        foreach($ids as $id)
        {
            $model = $this->findModel($id);

            if (!($model->isOriginalVersion())) {
                throw new ForbiddenHttpException('You are not allowed to perform this action.');
            }

            $oldModel = new Payment();
            $oldModel->attributes = $model->attributes;
            $oldModel->version = $model->versionsCount + 1;
            $oldModel->parent = $model->id;

            $model->status = $status;

            $oldModel->save(false);
            $success = $model->save(false);
        }

        return ['success' => $success];
    }

    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('payment.update')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = $this->findModel($id);

        if (!($model->isOriginalVersion())) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $oldModel = new Payment();
        $oldModel->attributes = $model->attributes;
        $oldModel->version = $model->versionsCount + 1;
        $oldModel->parent = $model->id;

        if ($model->load(Yii::$app->request->post())) {
            $oldModel->save(false);
            $model->save(false);
            return ['success' => true];
        }

        return ['success' => false];
    }

    public function actionDelete($id){
        if(!Yii::$app->user->can('payment.delete')){
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = $this->findModel($id);
        $success = $model->delete();

        return ['success' => $success];
    }

    public function actionDeleteList(){
        if(!Yii::$app->user->can('payment.delete')){
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $ids = Yii::$app->request->post();

        $ids = implode(', ', $ids);
        $success = Payment::deleteAll('id IN (' . $ids . ')');

        return ['success' => $success];
    }

    protected function findModel($id)
    {
        if (($model = Payment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
