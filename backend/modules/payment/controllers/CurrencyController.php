<?php

namespace app\modules\payment\controllers;

use Yii;
use app\modules\payment\models\Currency;
use app\modules\payment\models\search\SearchCurrency;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

/**
 * CurrencyController implements the CRUD actions for Currency model.
 */
class CurrencyController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionGetCurrencies(){
        if(!Yii::$app->user->can('currency.read')){
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $searchModel = new SearchCurrency();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return [
            'list' => $dataProvider->getModels(),
            'totalCount' => $dataProvider->totalCount
        ];
    }

    public function actionGetAllCurrencies(){
        return [
            'list' => Currency::find()->all(),
        ];
    }

    public function actionGetCurrency($id){
        if(!Yii::$app->user->can('currency.read')){
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        return [
            'entity' => Currency::findOne($id)
        ];
    }

    public function actionCheckCurrencyUniqueField() {
        if (!Yii::$app->user->can('currency.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $fieldName = Yii::$app->request->post('fieldName');
        $fieldValue = Yii::$app->request->post('fieldValue');

        $count = Currency::find()
            ->where([$fieldName => $fieldValue])
            ->count();

        return [
            'isUnique' => !($count > 0)
        ];
    }

    public function actionCreate(){
        if(!Yii::$app->user->can('currency.create')){
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = new Currency();

        if($model->load(Yii::$app->request->post()) && $model->save()){
            return ['success' => true];
        }

        return ['success' => false];
    }

    public function actionUpdate($id){
        if(!Yii::$app->user->can('currency.update')){
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = $this->findModel($id);

        if($model->load(Yii::$app->request->post()) && $model->save()){
            return ['success' => true];
        }

        return ['success' => true];
    }

    public function actionDelete($id){
        if(!Yii::$app->user->can('currency.delete')){
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $success = $this->findModel($id)->delete();

        return ['success' => $success];
    }

    protected function findModel($id)
    {
        if (($model = Currency::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
