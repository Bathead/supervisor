<?php

namespace app\modules\payment\models;

use Yii;
use app\modules\user\models\User;
use yii\db\ActiveRecord;
use ReflectionClass;
use yii\helpers\Inflector;

/**
 * This is the model class for table "billing_card".
 *
 * @property integer $id
 * @property integer $owner_id
 * @property string $number
 * @property integer $currency_id
 * @property string $expiration_date
 *
 * @property Currency $currency
 * @property User $owner
 */

class BillingCard extends ActiveRecord
{
    const STATE_ACTIVE = 2;
    const STATE_WARNING = 1;
    const STATE_INACTIVE = 0;

    const MONTH_PERIOD = 30;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'billing_card';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'currency_id'], 'integer'],
            [['number', 'currency_id', 'expiration_date'], 'required'],
            [['number'], 'string', 'max' => 255],
            [['number'], 'unique'],
            [['expiration_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'owner_id' => 'Owner',
            'number' => 'Number',
            'currency_id' => 'Currency',
            'expiration_date' => 'Expiration date',
            'currencyAsString' => 'Currency',
        ];
    }

    public function fields()
    {
        $fields = array_merge(parent::fields(), [
            'currency',
            'owner',
            'number',
            'state'
        ]);

        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    public function getState(){
        $todayDate = new \DateTime();
        $expirationDate = new \DateTime($this->expiration_date);
        $interval =  $expirationDate->diff($todayDate);
        $formattedInterval = $interval->format('%R%a');

        if (
            $formattedInterval < (- self::MONTH_PERIOD)
        ) {
            return self::STATE_ACTIVE;
        } else if (
            ($formattedInterval >= (- self::MONTH_PERIOD)) && ($formattedInterval <= +0)
        ) {
           return self::STATE_WARNING;
        } else {
            return self::STATE_INACTIVE;
        }
    }

    public static function getStates() {
        $result = [];
        $constPrefix = "STATE_";

        // create a reflection class to get constants
        $reflClass = new ReflectionClass(get_called_class());
        $constants = $reflClass->getConstants();

        // check for status constants (e.g., STATE_ACTIVE)
        foreach ($constants as $constantName => $constantValue) {

            // add prettified id and name to result
            if (strpos($constantName, $constPrefix) === 0) {
                $prettyName = str_replace($constPrefix, "", $constantName);
                $prettyName = Inflector::humanize(strtolower($prettyName));
                $result[] = [
                    'id' => $constantValue,
                    'title' => $prettyName
                ];
            }
        }

        return $result;
    }
}
