<?php
namespace app\modules\payment\models;

use yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "currency".
 *
 * @property integer $id
 * @property string $name
 * @property double $rate
 */
class Currency extends ActiveRecord
{
    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return 'currency';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['rate'], 'number'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('payment', 'ID'),
            'name' => Yii::t('payment', 'Name'),
            'rate' => Yii::t('payment', 'Rate'),
        ];
    }

    /**
     * Get currency by id
     * @param integer $currencyId
     * @return array|null|ActiveRecord
     */
    public static function getCurrencyById($currencyId)
    {
        return self::find()->where(['id' => $currencyId])->one();
    }
}