<?php

namespace app\modules\payment\models;

use Yii;
use app\modules\user\models\User;
use ReflectionClass;
use yii\db\ActiveRecord;
use yii\helpers\Inflector;

/**
 * This is the model class for table "payment".
 *
 * @property integer $id
 * @property integer $receiver_id
 * @property integer $payer_id
 * @property string $payment_sum
 * @property integer $currency_id
 * @property string $created_at
 *
 * @property User $receiver
 * @property User $payer
 * @property mixed billing_card_id
 */
class Payment extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_WILLING_TO_PAY = 1;
    const STATUS_PAYED = 2;

    public static function tableName()
    {
        return 'payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['receiver_id', 'status'], 'required'],
            [['receiver_id'], 'integer'],
            [['comment', 'uah_billing_card_number', 'usd_billing_card_number'], 'string'],
            [['payment_sum_uah', 'payment_sum_usd', 'bonuses_sum_usd', 'other_sum_usd', 'sum_uah', 'sum_usd'], 'number'],
            [['status', 'version', 'parent'], 'integer'],
            [['period'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'receiver_id' => 'Receiver',
            'created_at' => 'Created At',
            'receiverFullName' => 'Receiver',
            'status' => 'Status',
        ];
    }

    public function fields()
    {
        $fields = array_merge(parent::fields(), [
            'receiver',
            'billingCardUAH',
            'billingCardUSD',
            'status',
            'statusAsString',
            'periodAsString'
        ]);

        return $fields;
    }

    /**
     * Get payment receiver
     * @return \yii\db\ActiveQuery
     */
    public function getReceiver()
    {
        return $this->hasOne(User::className(), ['id' => 'receiver_id']);
    }

    /**
     * Get uah payment destination billing card
     * @return \yii\db\ActiveQuery
     */
    public function getBillingCardUAH()
    {
        return $this->hasOne(BillingCard::className(), ['number' => 'uah_billing_card_number']);
    }

    /**
     * Get usd payment destination billing card
     * @return \yii\db\ActiveQuery
     */
    public function getBillingCardUSD()
    {
        return $this->hasOne(BillingCard::className(), ['number' => 'usd_billing_card_number']);
    }

    /**
     * Get payment sum uah as string
     * @return string
     */
    public function getPaymentSumUAHAsString() {
        return $this->payment_sum_uah . ' UAH';
    }

    /**
     * Get payment sum usd as string
     * @return string
     */
    public function getPaymentSumUSDAsString() {
        return $this->payment_sum_usd . ' USD';
    }

    /**
     * Get bonuses sum usd as string
     * @return string
     */
    public function getBonusesSumUSDAsString() {
        return $this->bonuses_sum_usd . ' USD';
    }

    /**
     * Get other sum usd as string
     * @return string
     */
    public function getOtherSumUSDAsString() {
        return $this->other_sum_usd . ' USD';
    }

    public function getPeriodAsString() {
        return substr($this->period, 0, 7);
    }

    public static function getPaymentStatuses() {
        $result = [];
        $constPrefix = "STATUS_";

        // create a reflection class to get constants
        $reflClass = new ReflectionClass(get_called_class());
        $constants = $reflClass->getConstants();

        // check for status constants (e.g., STATUS_PAYED)
        foreach ($constants as $constantName => $constantValue) {

            // add prettified name to dropdown
            if (strpos($constantName, $constPrefix) === 0) {
                $prettyName = str_replace($constPrefix, "", $constantName);
                $prettyName = Inflector::humanize(strtolower($prettyName));
                $result[] = [
                    'id' => $constantValue,
                    'title' => $prettyName
                ];
            }
        }
        return $result;
    }

    public function getStatusAsString() {
        $statuses = $this->getPaymentStatuses();
        return $statuses[$this->status-1]['title'];
    }

    public function isOriginalVersion(){
        return $this->version == 0;
    }

    public function getVersionsCount(){
        return Payment::find()
            ->where(['parent' => $this->id])
            ->count();
    }

    public function getLastVersion(){
        return Payment::findOne(['parent' => $this->id, 'version' => $this->versionsCount]);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->created_at = date('Y-m-d H:i:s');
            }
            $this->updated_at = date('Y-m-d H:i:s');
            $this->sum_uah = $this->payment_sum_uah ? $this->payment_sum_uah : 0;
            $this->sum_usd = $this->payment_sum_usd + $this->bonuses_sum_usd + $this->other_sum_usd;
            $this->uah_billing_card_number = $this->receiver->billingCardUAH->number;
            $this->usd_billing_card_number = $this->receiver->billingCardUSD->number;
            return true;
        } else {
            return false;
        }
    }
}
