<?php

namespace app\modules\payment\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\payment\models\Currency;

/**
 * SearchCurrency represents the model behind the search form about `app\modules\currency\models\Currency`.
 */
class SearchCurrency extends Currency
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'rate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Currency::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $params['count'],
                'page' => $params['page'] - 1
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (isset($this->rate) && $this->rate != '') {
            if ($c = $this->isComparable($this->rate)) {
                $query->andWhere([$c['condition'], 'rate', $c['value']]);
            } else {
                $query->andWhere('rate LIKE "%' . $this->rate . '%" ');
            }
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    // check, is string contain comparable symbols ('>=', '<=', '>', '<')
    // and return false, or ['condition' => '...', 'value' => '...']
    private function isComparable($string) {
        $conditions = ['>=', '<=', '>', '<'];
        foreach ($conditions as $condition) {
            if (strpos($string, $condition) === 0) {
                return [
                    'condition' => $condition,
                    'value' => str_replace($condition, '', $string)
                ];
            }
        }
        return false;
    }
}
