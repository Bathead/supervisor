<?php

namespace app\modules\payment\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\payment\models\Currency;
use app\modules\payment\models\BillingCard;
use app\modules\user\models\User;
use app\modules\user\models\Profile;

/**
 * BillingCardSearch represents the model behind the search form about `app\modules\payment\models\BillingCard`.
 */
class SearchBillingCard extends BillingCard
{

    public $ownerFullName;
    public $currencyName;
    public $currencyId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'owner_id', 'currency_id'], 'integer'],
            [[
                'number',
                'ownerFullName',
                'currencyName',
                'currencyId',
                'expiration_date',
            ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $tn_billing_card = BillingCard::tableName();
        $tn_user = User::tableName();
        $tn_profile = Profile::tableName();
        $tn_currency = Currency::tableName();

        $query = BillingCard::find()
            ->from("$tn_billing_card as bc")
            ->leftJoin("$tn_user as u", 'u.id = bc.owner_id')
            ->leftJoin("$tn_profile as p", 'p.id = u.profile_id')
            ->leftJoin("$tn_currency as c", 'c.id = bc.currency_id')
            ->where(['u.status' => User::STATUS_ACTIVE]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $params['count'],
                'page' => $params['page'] - 1
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'number' => [
                    'asc' => ['bc.number' => SORT_ASC],
                    'desc' => ['bc.number' => SORT_DESC]
                ],
                'ownerFullName' => [
                    'asc' => ['p.first_name' => SORT_ASC, 'p.last_name' => SORT_ASC, 'p.middle_name' => SORT_ASC],
                    'desc' => ['p.first_name' => SORT_DESC, 'p.last_name' => SORT_DESC], 'p.middle_name' => SORT_DESC,
                    'label' => Yii::t('user', 'Name'),
                    'default' => SORT_ASC
                ],
                'currencyName' => [
                    'asc' => ['c.name' => SORT_ASC],
                    'desc' => ['c.name' => SORT_DESC]
                ],
                'expiration_date' => [
                    'asc' => ['bc.expiration_date' => SORT_ASC],
                    'desc' => ['bc.expiration_date' => SORT_DESC]
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (!empty($this->ownerFullName)) {
            $query->andWhere('p.first_name LIKE "%' . $this->ownerFullName . '%" ' .
                'OR p.last_name LIKE "%' . $this->ownerFullName . '%"' .
                'OR p.middle_name LIKE "%' . $this->ownerFullName . '%"'
            );
        }

        if (!empty($this->number)) {
            $query->andWhere('bc.number LIKE "%' . $this->number . '%" ');
        }

        if (!empty($this->currencyId)) {
            $query->andWhere(['c.id' => $this->currencyId]);
        }

        if (!empty($this->expiration_date)) {
            $query->andWhere('bc.expiration_date LIKE "%' . $this->expiration_date . '%" ');
        }

        return $dataProvider;
    }
}
