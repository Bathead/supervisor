<?php

namespace app\modules\payment\models\search;

use app\modules\department\models\Department;
use app\modules\user\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\payment\models\Payment;
use app\modules\user\models\Profile;
use app\modules\department\models\UserDepartment;

/**
 * SearchPayment represents the model behind the search form about `app\modules\payment\models\Payment`.
 */
class SearchPayment extends Payment
{
    public $receiverFullName;
//    public $receiverDepartmentName;
//    public $receiverDepartmentId;
    public $departmentsIds;
    public $paymentSumUAHAsString;
    public $paymentSumUSDAsString;
    public $bonusesSumUSDAsString;
    public $otherSumUSDAsString;
    public $sumUAHAsString;
    public $sumUSDAsString;
    public $billingCardUAH;
    public $billingCardUSD;
    public $status;
    public $comment;
    public $period;

    public $date_from;
    public $date_to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'receiver_id'], 'integer'],
            [[
                'created_at',
                'receiverFullName',
//                'receiverDepartmentName',
//                'receiverDepartmentId',
                'departmentsIds',
                'uah_billing_card_number',
                'usd_billing_card_number',
                'paymentSumUAHAsString',
                'paymentSumUSDAsString',
                'bonusesSumUSDAsString',
                'otherSumUSDAsString',
                'sumUAHAsString',
                'sumUSDAsString',
                'billingCardUAH',
                'billingCardUSD',
                'status',
                'comment',
                'period',

                'date_from',
                'date_to',
            ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $tn_user = User::tableName();
        $tn_payment = Payment::tableName();
        $tn_profile = Profile::tableName();
        $tn_user_department = UserDepartment::tableName();

        $query = Payment::find()
            ->from("$tn_payment as pay")
            ->leftJoin("$tn_user as u", 'u.id = pay.receiver_id')
            ->leftJoin("$tn_profile as p", 'p.id = u.profile_id')
            ->where(['u.status' => User::STATUS_ACTIVE]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $params['count'],
                'page' => $params['page'] - 1
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'receiverFullName' => [
                    'asc' => ['p.first_name' => SORT_ASC, 'p.last_name' => SORT_ASC, 'p.middle_name' => SORT_ASC],
                    'desc' => ['p.first_name' => SORT_DESC, 'p.last_name' => SORT_DESC, 'p.middle_name' => SORT_DESC],
                ],
//                'receiverDepartmentName' => [
//                    'asc' => ['d.name' => SORT_ASC],
//                    'desc' => ['d.name' => SORT_DESC]
//                ],
                'paymentSumUAHAsString' => [
                    'asc' => ['payment_sum_uah' => SORT_ASC],
                    'desc' => ['payment_sum_uah' => SORT_DESC],
                ],
                'paymentSumUSDAsString' => [
                    'asc' => ['payment_sum_usd' => SORT_ASC],
                    'desc' => ['payment_sum_usd' => SORT_DESC],
                ],
                'bonusesSumUSDAsString' => [
                    'asc' => ['bonuses_sum_usd' => SORT_ASC],
                    'desc' => ['bonuses_sum_usd' => SORT_DESC],
                ],
                'otherSumUSDAsString' => [
                    'asc' => ['other_sum_usd' => SORT_ASC],
                    'desc' => ['other_sum_usd' => SORT_DESC],
                ],
                'sumUAHAsString' => [
                    'asc' => ['sum_uah' => SORT_ASC],
                    'desc' => ['sum_uah' => SORT_DESC],
                ],
                'sumUSDAsString' => [
                    'asc' => ['sum_usd' => SORT_ASC],
                    'desc' => ['sum_usd' => SORT_DESC],
                ],
                'billingCardUAH' => [
                    'asc' => ['uah_billing_card_number' => SORT_ASC],
                    'desc' => ['uah_billing_card_number' => SORT_DESC],
                ],
                'billingCardUSD' => [
                    'asc' => ['usd_billing_card_number' => SORT_ASC],
                    'desc' => ['usd_billing_card_number' => SORT_DESC],
                ],
                'status' => [
                    'asc' => ['pay.status' => SORT_ASC],
                    'desc' => ['pay.status' => SORT_DESC],
                ],
                'comment' => [
                    'asc' => ['comment' => SORT_ASC],
                    'desc' => ['comment' => SORT_DESC],
                ],
                'created_at' => [
                    'asc' => ['created_at' => SORT_ASC],
                    'desc' => ['created_at' => SORT_DESC],
                ],
                'period' => [
                    'asc' => ['period' => SORT_ASC],
                    'desc' => ['period' => SORT_DESC],
                ],
            ],
            'defaultOrder' => ['created_at'=>SORT_DESC]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (!empty($this->receiverFullName)) {
            $query->andFilterWhere([
                'or',
                ['like', 'p.first_name', $this->receiverFullName],
                ['like', 'p.last_name', $this->receiverFullName],
                ['like', 'p.middle_name', $this->receiverFullName],
            ]);
        }
//        if (!empty($this->receiverDepartmentId)) {
//            $query->andWhere(['d.id' => $this->receiverDepartmentId]);
//        }

        if (!empty($this->departmentsIds)) {
            $query
                ->leftJoin("$tn_user_department as ud", 'u.id = ud.user_id')
                ->andWhere(['in', 'ud.department_id', $this->departmentsIds]);
        }
        
        if(isset($this->paymentSumUAHAsString) && $this->paymentSumUAHAsString != ''){
            if($c = $this->isComparable($this->paymentSumUAHAsString)){
                $query->andWhere([$c['condition'], 'payment_sum_uah', $c['value']]);
            } else {
                $query->andWhere(['=', 'payment_sum_uah', $this->paymentSumUAHAsString]);
            }
        }
        if(!empty($this->status)){
            $query->andWhere(['pay.status' => $this->status]);
        }
        if(isset($this->paymentSumUSDAsString) && $this->paymentSumUSDAsString != '') {
            if ($c = $this->isComparable($this->paymentSumUSDAsString)) {
                $query->andWhere([$c['condition'], 'payment_sum_usd', $c['value']]);
            } else {
                $query->andWhere(['=', 'payment_sum_usd', $this->paymentSumUSDAsString]);
            }
        }
        if(isset($this->bonusesSumUSDAsString) && $this->bonusesSumUSDAsString != ''){
            if($c = $this->isComparable($this->bonusesSumUSDAsString))
            {
                $query->andWhere([$c['condition'], 'bonuses_sum_usd', $c['value']]);
            } else {
                $query->andWhere(['=', 'bonuses_sum_usd', $this->bonusesSumUSDAsString]);
            }
        }
        if(isset($this->otherSumUSDAsString) && $this->otherSumUSDAsString != '') {
            if ($c = $this->isComparable($this->otherSumUSDAsString)) {
                $query->andWhere([$c['condition'], 'other_sum_usd', $c['value']]);
            } else {
                $query->andWhere(['=', 'other_sum_usd', $this->otherSumUSDAsString]);
            }
        }
        if(isset($this->sumUAHAsString) && $this->sumUAHAsString != '') {
            if ($c = $this->isComparable($this->sumUAHAsString)) {
                $query->andWhere([$c['condition'], 'sum_uah', $c['value']]);
            } else {
                $query->andWhere(['=', 'sum_uah', $this->sumUAHAsString]);
            }
        }
        if(isset($this->sumUSDAsString) && $this->sumUSDAsString != '') {
            if ($c = $this->isComparable($this->sumUSDAsString)) {
                $query->andWhere([$c['condition'], 'sum_usd', $c['value']]);
            } else {
                $query->andWhere(['=', 'sum_usd', $this->sumUSDAsString]);
            }
        }
        if (!empty($this->comment)) {
            $query->andWhere(['like', 'comment', $this->comment]);
        }
        if(!empty($this->billingCardUAH)){
            $query->andWhere(['like', 'uah_billing_card_number', $this->billingCardUAH]);
        }
        if(!empty($this->billingCardUSD)){
            $query->andWhere(['like', 'usd_billing_card_number', $this->billingCardUSD]);
        }

        if(!empty($this->date_from) && empty($this->date_to)){
            $query->andWhere(['>=', 'period', $this->date_from]);
        }
        else if(!empty($this->date_to) && empty($this->date_from)){
            $query->andWhere(['<=', 'period', $this->date_to]);
        }
        else if(!empty($this->date_from) && !empty($this->date_to)){
            $df = new \DateTime($this->date_from);
            $dt = new \DateTime($this->date_to);
            $days = $df->diff($dt, false)->days;

            if($this->date_from > $this->date_to){
                $query->andWhere(['<=', 'period', $this->date_from]);
                $query->andWhere(['>=', 'period', $this->date_to]);
            }
            else if($days == 0){
                $query->andWhere(['=', 'period', $this->date_from]);
            }
            else if($this->date_from < $this->date_to){
                $query->andWhere(['>=', 'period', $this->date_from]);
                $query->andWhere(['<=', 'period', $this->date_to]);
            }
        }

        $query->andWhere(['version' => 0]);

        return $dataProvider;
    }

    public function searchVersions($params, $id){
        $tn_user = User::tableName();
        $tn_payment = Payment::tableName();
        $tn_profile = Profile::tableName();
        $tn_user_department = UserDepartment::tableName();

        $query = Payment::find()
            ->from("$tn_payment as pay")
            ->leftJoin("$tn_user as u", 'u.id = pay.receiver_id')
            ->leftJoin("$tn_profile as p", 'p.id = u.profile_id')
            ->where(['u.status' => User::STATUS_ACTIVE]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $params['count'],
                'page' => $params['page'] - 1
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'receiverFullName' => [
                    'asc' => ['p.first_name' => SORT_ASC, 'p.last_name' => SORT_ASC, 'p.middle_name' => SORT_ASC],
                    'desc' => ['p.first_name' => SORT_DESC, 'p.last_name' => SORT_DESC, 'p.middle_name' => SORT_DESC],
                ],
//                'receiverDepartmentName' => [
//                    'asc' => ['d.name' => SORT_ASC],
//                    'desc' => ['d.name' => SORT_DESC]
//                ],
                'paymentSumUAHAsString' => [
                    'asc' => ['payment_sum_uah' => SORT_ASC],
                    'desc' => ['payment_sum_uah' => SORT_DESC],
                ],
                'paymentSumUSDAsString' => [
                    'asc' => ['payment_sum_usd' => SORT_ASC],
                    'desc' => ['payment_sum_usd' => SORT_DESC],
                ],
                'bonusesSumUSDAsString' => [
                    'asc' => ['bonuses_sum_usd' => SORT_ASC],
                    'desc' => ['bonuses_sum_usd' => SORT_DESC],
                ],
                'otherSumUSDAsString' => [
                    'asc' => ['other_sum_usd' => SORT_ASC],
                    'desc' => ['other_sum_usd' => SORT_DESC],
                ],
                'sumUAHAsString' => [
                    'asc' => ['sum_uah' => SORT_ASC],
                    'desc' => ['sum_uah' => SORT_DESC],
                ],
                'sumUSDAsString' => [
                    'asc' => ['sum_usd' => SORT_ASC],
                    'desc' => ['sum_usd' => SORT_DESC],
                ],
                'billingCardUAH' => [
                    'asc' => ['uah_billing_card_number' => SORT_ASC],
                    'desc' => ['uah_billing_card_number' => SORT_DESC],
                ],
                'billingCardUSD' => [
                    'asc' => ['usd_billing_card_number' => SORT_ASC],
                    'desc' => ['usd_billing_card_number' => SORT_DESC],
                ],
                'status' => [
                    'asc' => ['pay.status' => SORT_ASC],
                    'desc' => ['pay.status' => SORT_DESC],
                ],
                'comment' => [
                    'asc' => ['comment' => SORT_ASC],
                    'desc' => ['comment' => SORT_DESC],
                ],
                'created_at' => [
                    'asc' => ['created_at' => SORT_ASC],
                    'desc' => ['created_at' => SORT_DESC],
                ],
            ],
            'defaultOrder' => ['created_at'=>SORT_DESC]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (!empty($this->receiverFullName)) {
            $query->andFilterWhere([
                'or',
                ['like', 'p.first_name', $this->receiverFullName],
                ['like', 'p.last_name', $this->receiverFullName],
                ['like', 'p.middle_name', $this->receiverFullName],
            ]);
        }
//        if (!empty($this->receiverDepartmentId)) {
//            $query->andWhere(['d.id' => $this->receiverDepartmentId]);
//        }

        if (!empty($this->departmentsIds)) {
            $query
                ->leftJoin("$tn_user_department as ud", 'u.id = ud.user_id')
                ->andWhere(['in', 'ud.department_id', $this->departmentsIds]);
        }

        if(isset($this->paymentSumUAHAsString) && $this->paymentSumUAHAsString != ''){
            if($c = $this->isComparable($this->paymentSumUAHAsString)){
                $query->andWhere([$c['condition'], 'payment_sum_uah', $c['value']]);
            } else {
                $query->andWhere(['=', 'payment_sum_uah', $this->paymentSumUAHAsString]);
            }
        }
        if(!empty($this->status)){
            $query->andWhere(['pay.status' => $this->status]);
        }
        if(isset($this->paymentSumUSDAsString) && $this->paymentSumUSDAsString != '') {
            if ($c = $this->isComparable($this->paymentSumUSDAsString)) {
                $query->andWhere([$c['condition'], 'payment_sum_usd', $c['value']]);
            } else {
                $query->andWhere(['=', 'payment_sum_usd', $this->paymentSumUSDAsString]);
            }
        }
        if(isset($this->bonusesSumUSDAsString) && $this->bonusesSumUSDAsString != ''){
            if($c = $this->isComparable($this->bonusesSumUSDAsString))
            {
                $query->andWhere([$c['condition'], 'bonuses_sum_usd', $c['value']]);
            } else {
                $query->andWhere(['=', 'bonuses_sum_usd', $this->bonusesSumUSDAsString]);
            }
        }
        if(isset($this->otherSumUSDAsString) && $this->otherSumUSDAsString != '') {
            if ($c = $this->isComparable($this->otherSumUSDAsString)) {
                $query->andWhere([$c['condition'], 'other_sum_usd', $c['value']]);
            } else {
                $query->andWhere(['=', 'other_sum_usd', $this->otherSumUSDAsString]);
            }
        }
        if(isset($this->sumUAHAsString) && $this->sumUAHAsString != '') {
            if ($c = $this->isComparable($this->sumUAHAsString)) {
                $query->andWhere([$c['condition'], 'sum_uah', $c['value']]);
            } else {
                $query->andWhere(['=', 'sum_uah', $this->sumUAHAsString]);
            }
        }
        if(isset($this->sumUSDAsString) && $this->sumUSDAsString != '') {
            if ($c = $this->isComparable($this->sumUSDAsString)) {
                $query->andWhere([$c['condition'], 'sum_usd', $c['value']]);
            } else {
                $query->andWhere(['=', 'sum_usd', $this->sumUSDAsString]);
            }
        }
        if (!empty($this->comment)) {
            $query->andWhere(['like', 'comment', $this->comment]);
        }
        if(!empty($this->billingCardUAH)){
            $query->andWhere(['like', 'uah_billing_card_number', $this->billingCardUAH]);
        }
        if(!empty($this->billingCardUSD)){
            $query->andWhere(['like', 'usd_billing_card_number', $this->billingCardUSD]);
        }

        return $dataProvider;
    }

    public function searchFiltered($params)
    {
        $tn_user = User::tableName();
        $tn_payment = Payment::tableName();
        $tn_profile = Profile::tableName();
        $tn_user_department = UserDepartment::tableName();

        $query = Payment::find()
            ->from("$tn_payment as pay")
            ->leftJoin("$tn_user as u", 'u.id = pay.receiver_id')
            ->leftJoin("$tn_profile as p", 'p.id = u.profile_id')
            ->where(['u.status' => User::STATUS_ACTIVE]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => INF,
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'receiverFullName' => [
                    'asc' => ['p.first_name' => SORT_ASC, 'p.last_name' => SORT_ASC, 'p.middle_name' => SORT_ASC],
                    'desc' => ['p.first_name' => SORT_DESC, 'p.last_name' => SORT_DESC, 'p.middle_name' => SORT_DESC],
                ],
//                'receiverDepartmentName' => [
//                    'asc' => ['d.name' => SORT_ASC],
//                    'desc' => ['d.name' => SORT_DESC]
//                ],
                'paymentSumUAHAsString' => [
                    'asc' => ['payment_sum_uah' => SORT_ASC],
                    'desc' => ['payment_sum_uah' => SORT_DESC],
                ],
                'paymentSumUSDAsString' => [
                    'asc' => ['payment_sum_usd' => SORT_ASC],
                    'desc' => ['payment_sum_usd' => SORT_DESC],
                ],
                'bonusesSumUSDAsString' => [
                    'asc' => ['bonuses_sum_usd' => SORT_ASC],
                    'desc' => ['bonuses_sum_usd' => SORT_DESC],
                ],
                'otherSumUSDAsString' => [
                    'asc' => ['other_sum_usd' => SORT_ASC],
                    'desc' => ['other_sum_usd' => SORT_DESC],
                ],
                'sumUAHAsString' => [
                    'asc' => ['sum_uah' => SORT_ASC],
                    'desc' => ['sum_uah' => SORT_DESC],
                ],
                'sumUSDAsString' => [
                    'asc' => ['sum_usd' => SORT_ASC],
                    'desc' => ['sum_usd' => SORT_DESC],
                ],
                'billingCardUAH' => [
                    'asc' => ['uah_billing_card_number' => SORT_ASC],
                    'desc' => ['uah_billing_card_number' => SORT_DESC],
                ],
                'billingCardUSD' => [
                    'asc' => ['usd_billing_card_number' => SORT_ASC],
                    'desc' => ['usd_billing_card_number' => SORT_DESC],
                ],
                'status' => [
                    'asc' => ['pay.status' => SORT_ASC],
                    'desc' => ['pay.status' => SORT_DESC],
                ],
                'comment' => [
                    'asc' => ['comment' => SORT_ASC],
                    'desc' => ['comment' => SORT_DESC],
                ],
                'created_at' => [
                    'asc' => ['created_at' => SORT_ASC],
                    'desc' => ['created_at' => SORT_DESC],
                ],
                'period' => [
                    'asc' => ['period' => SORT_ASC],
                    'desc' => ['period' => SORT_DESC],
                ],
            ],
            'defaultOrder' => ['created_at'=>SORT_DESC]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (!empty($this->receiverFullName)) {
            $query->andFilterWhere([
                'or',
                ['like', 'p.first_name', $this->receiverFullName],
                ['like', 'p.last_name', $this->receiverFullName],
                ['like', 'p.middle_name', $this->receiverFullName],
            ]);
        }
//        if (!empty($this->receiverDepartmentId)) {
//            $query->andWhere(['d.id' => $this->receiverDepartmentId]);
//        }

        if (!empty($this->departmentsIds)) {
            $query
                ->leftJoin("$tn_user_department as ud", 'u.id = ud.user_id')
                ->andWhere(['in', 'ud.department_id', $this->departmentsIds]);
        }

        if(isset($this->paymentSumUAHAsString) && $this->paymentSumUAHAsString != ''){
            if($c = $this->isComparable($this->paymentSumUAHAsString)){
                $query->andWhere([$c['condition'], 'payment_sum_uah', $c['value']]);
            } else {
                $query->andWhere(['=', 'payment_sum_uah', $this->paymentSumUAHAsString]);
            }
        }
        if(!empty($this->status)){
            $query->andWhere(['pay.status' => $this->status]);
        }
        if(isset($this->paymentSumUSDAsString) && $this->paymentSumUSDAsString != '') {
            if ($c = $this->isComparable($this->paymentSumUSDAsString)) {
                $query->andWhere([$c['condition'], 'payment_sum_usd', $c['value']]);
            } else {
                $query->andWhere(['=', 'payment_sum_usd', $this->paymentSumUSDAsString]);
            }
        }
        if(isset($this->bonusesSumUSDAsString) && $this->bonusesSumUSDAsString != ''){
            if($c = $this->isComparable($this->bonusesSumUSDAsString))
            {
                $query->andWhere([$c['condition'], 'bonuses_sum_usd', $c['value']]);
            } else {
                $query->andWhere(['=', 'bonuses_sum_usd', $this->bonusesSumUSDAsString]);
            }
        }
        if(isset($this->otherSumUSDAsString) && $this->otherSumUSDAsString != '') {
            if ($c = $this->isComparable($this->otherSumUSDAsString)) {
                $query->andWhere([$c['condition'], 'other_sum_usd', $c['value']]);
            } else {
                $query->andWhere(['=', 'other_sum_usd', $this->otherSumUSDAsString]);
            }
        }
        if(isset($this->sumUAHAsString) && $this->sumUAHAsString != '') {
            if ($c = $this->isComparable($this->sumUAHAsString)) {
                $query->andWhere([$c['condition'], 'sum_uah', $c['value']]);
            } else {
                $query->andWhere(['=', 'sum_uah', $this->sumUAHAsString]);
            }
        }
        if(isset($this->sumUSDAsString) && $this->sumUSDAsString != '') {
            if ($c = $this->isComparable($this->sumUSDAsString)) {
                $query->andWhere([$c['condition'], 'sum_usd', $c['value']]);
            } else {
                $query->andWhere(['=', 'sum_usd', $this->sumUSDAsString]);
            }
        }
        if (!empty($this->comment)) {
            $query->andWhere(['like', 'comment', $this->comment]);
        }
        if(!empty($this->billingCardUAH)){
            $query->andWhere(['like', 'uah_billing_card_number', $this->billingCardUAH]);
        }
        if(!empty($this->billingCardUSD)){
            $query->andWhere(['like', 'usd_billing_card_number', $this->billingCardUSD]);
        }

        if(!empty($this->date_from) && empty($this->date_to)){
            $query->andWhere(['>=', 'period', $this->date_from]);
        }
        else if(!empty($this->date_to) && empty($this->date_from)){
            $query->andWhere(['<=', 'period', $this->date_to]);
        }
        else if(!empty($this->date_from) && !empty($this->date_to)){
            $df = new \DateTime($this->date_from);
            $dt = new \DateTime($this->date_to);
            $days = $df->diff($dt, false)->days;

            if($this->date_from > $this->date_to){
                $query->andWhere(['<=', 'period', $this->date_from]);
                $query->andWhere(['>=', 'period', $this->date_to]);
            }
            else if($days == 0){
                $query->andWhere(['=', 'period', $this->date_from]);
            }
            else if($this->date_from < $this->date_to){
                $query->andWhere(['>=', 'period', $this->date_from]);
                $query->andWhere(['<=', 'period', $this->date_to]);
            }
        }

        $query->andWhere(['version' => 0]);

        return $dataProvider;
    }

    // check, is string contain comparable symbols ('>=', '<=', '>', '<')
    // and return false, or ['condition' => '...', 'value' => '...']
    private function isComparable($string) {
        $conditions = ['>=', '<=', '>', '<'];
        foreach ($conditions as $condition) {
            if (strpos($string, $condition) === 0) {
                return [
                    'condition' => $condition,
                    'value' => str_replace($condition, '', $string)
                ];
            }
        }
        return false;
    }
}
