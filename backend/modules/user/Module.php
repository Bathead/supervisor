<?php

namespace app\modules\user;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\user\controllers';

    public $loginDuration = 2592000; // 1 month

    public $userPerPage = 15;

    public function init()
    {
        parent::init();
    }
}
