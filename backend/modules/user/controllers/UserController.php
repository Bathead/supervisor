<?php

namespace app\modules\user\controllers;

use app\modules\department\models\UserDepartment;
use app\modules\dialog\models\DialogMessage;
use app\modules\payment\models\BillingCard;
use app\modules\technology\models\UserTechnology;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\rbac\Role;
use app\modules\user\models\User;
use app\modules\user\models\Profile;
use app\modules\user\models\search\SearchUser;
use yii\web\ForbiddenHttpException;
use app\modules\user\models\upload\uploadUserPhotoForm;
use app\modules\news\models\News;
use app\modules\complaint\models\Complaint;
use app\modules\ticket\models\Ticket;
use app\modules\user\behaviors\ActiveUserBehavior;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['get-employment-statuses', 'update-counters'],
                        'roles' => ['?'],
                    ],
                ],
            ],
//            'activeUser' => [
//                'class' => ActiveUserBehavior::className(),
//                'except' => ['actionGetEmploymentStatuses', 'actionUpdateCounters']
//            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionGetUsers() {
        if (!Yii::$app->user->can('user.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $searchModel = new SearchUser();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $users = $dataProvider->getModels();

        $removePaymentsInfo = (!Yii::$app->user->can('payment.read') && !Yii::$app->user->can('billingCard.read'));
        $this->formatUserToResponse($users, $removePaymentsInfo);

        return [
            'list' => $users,
            'totalCount' => $dataProvider->totalCount
        ];
    }

    public function actionGetAllUsers() {
        if (!Yii::$app->user->can('user.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $users = User::find()
            ->where(['status' => User::STATUS_ACTIVE])
            ->all();
        $removePaymentsInfo = (!Yii::$app->user->can('payment.read') && !Yii::$app->user->can('billingCard.read'));
        $this->formatUserToResponse($users, $removePaymentsInfo);

        return [
            'list' => $users
        ];
    }

    public function actionGetAllOtherUsers() {
        if (!Yii::$app->user->can('user.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $users = User::find()
            ->where(['!=', 'id', Yii::$app->user->id])
            ->andWhere(['status' => User::STATUS_ACTIVE])
            ->all();
        $removePaymentsInfo = (!Yii::$app->user->can('payment.read') && !Yii::$app->user->can('billingCard.read'));
        $this->formatUserToResponse($users, $removePaymentsInfo);

        return [
            'list' => $users
        ];
    }

    public function actionGetOtherUsersByName() {
        if (!Yii::$app->user->can('user.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $name = Yii::$app->request->post('name');

        $users = User::find()
            ->from(User::tableName() . ' as u')
            ->leftJoin(Profile::tableName() . ' as p', 'p.id = u.profile_id')
            ->where(['!=' , 'u.id', Yii::$app->user->id])
            ->andWhere(['u.status' => User::STATUS_ACTIVE])
            ->andFilterWhere([
                'or',
                ['like', 'p.first_name', $name],
                ['like', 'p.last_name', $name]
            ])
            ->all();

        $removePaymentsInfo = (!Yii::$app->user->can('payment.read') && !Yii::$app->user->can('billingCard.read'));
        $this->formatUserToResponse($users, $removePaymentsInfo);

        return [
            'list' => $users
        ];
    }

    public function actionGetUser($id) {
        if (!Yii::$app->user->can('user.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $user = User::findOne($id);
        $this->formatUserToResponse($user);
        return [
            'entity' => $user
        ];
    }

    public function actionGetUserBillingCards($id) {
        if (!Yii::$app->user->can('user.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $cards = BillingCard::find()
            ->where(['owner_id' => $id])
            ->all();
        return [
            'list' => $cards
        ];
    }

    public function actionGetUserStatuses() {
        return [
            'list' => User::getUserStatuses()
        ];
    }

    public function actionGetEmploymentStatuses() {
        return [
            'list' => Profile::statusDropdown()
        ];
    }

    public function actionCheckUserUniqueField() {
        if (!Yii::$app->user->can('user.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $fieldName = Yii::$app->request->post('fieldName');
        $fieldValue = Yii::$app->request->post('fieldValue');

        $count = User::find()
            ->where([$fieldName => $fieldValue])
            ->count();
        return [
            'isUnique' => !($count > 0)
        ];
    }

    public function actionCreate()
    {
        if (!Yii::$app->user->can('user.create')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $data = json_decode(Yii::$app->request->post('data'), true);
        $userModel = new User();
        $profileModel = new Profile();

        if ($userModel->load($data) && $profileModel->load($data)) {
            $isValid = $userModel->validate();
            $isValid = $profileModel->validate() && $isValid;

            if ($isValid) {

                //save profile image
                $uploadUserPhotoForm = new uploadUserPhotoForm();
                $profileImageFileName = $uploadUserPhotoForm->upload();
                if ($profileImageFileName === false) {
                    return [
                        'success' => false,
                        'message' => 'Image upload error'
                    ];
                }

                $transaction = Yii::$app->db->beginTransaction();
                try {

                    // save profile
                    $profileModel->profile_image = $profileImageFileName;
                    $profileModel->save(false);
                    // save user
                    $userModel->profile_id = $profileModel->id;
                    $userModel->password = Yii::$app->security->generatePasswordHash($userModel->password);
                    $userModel->save(false);

                    // save user roles
                    if (!empty($data['User']['rolesAsArray'])) {
                        foreach ($data['User']['rolesAsArray'] as $roleName) {
                            $role = new Role();
                            $role->name = $roleName;
                            Yii::$app->authManager->assign($role, $userModel->id);
                        }
                    }

                    // save technologies
                    if (!empty($data['User']['userTechnologiesFullList'])) {
                        $canManageRating = Yii::$app->user->can('technology.ratingManage');
                        foreach($data['User']['userTechnologiesFullList'] as $t) {
                            if ($t['active']) {
                                $ut = new UserTechnology();
                                $ut->user_id = $userModel->id;
                                $ut->technology_id = $t['id'];
                                $ut->rating = ($canManageRating) ? $t['rating'] : null;
                                $ut->save();
                            }
                        }
                    }

                    // save departments
                    if (!empty($data['User']['userDepartmentsFullList'])) {
                        foreach($data['User']['userDepartmentsFullList'] as $d) {
                            if ($d['chosen']) {
                                $ud = new UserDepartment();
                                $ud->user_id = $userModel->id;
                                $ud->department_id = $d['id'];
                                $ud->save();
                            }
                        }
                    }

                    // save billing cards
                    if (!empty($data['User']['billingCards'])) {
                        foreach ($data['User']['billingCards'] as $card) {
                            $newCard = new BillingCard();
                            $newCard->number = $card['number'];
                            $newCard->currency_id = $card['currency_id'];
                            $newCard->expiration_date = $card['expiration_date'];
                            $newCard->owner_id = $userModel->id;
                            $newCard->save();
                        }
                    }

                    $transaction->commit();
                    return ['success' => true];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    // delete uploaded profile image, if transaction error
                    @unlink(Profile::$profileImagesPath . $profileImageFileName);
                    return [
                        'success' => false,
                        'message' => 'Save data fail (transaction rollBack)'
                    ];
                }
            }
            return [
                'success' => false,
                'message' => 'Not valid'
            ];
        }
        return [
            'success' => false,
            'message' => "Can't load data from post"
        ];
    }

    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('user.update') && (Yii::$app->user->id != $id)) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $userModel = $this->findModel($id);
        $profileModel = $userModel->profile;

        if (!Yii::$app->user->can('user.update')){
            $userModel->scenario = User::SCENARIO_PROFILE;
            $profileModel->scenario = Profile::SCENARIO_PROFILE;
        }

        $data = json_decode(Yii::$app->request->post('data'), true);

        $oldProfileImage = User::findOne($id)->profile->profile_image;

        if ($userModel->load($data) && $profileModel->load($data)) {
            $isValid = $userModel->validate();
            $isValid = $profileModel->validate() && $isValid;

            if ($isValid) {

                //save new profile image, if selected
                $uploadUserPhotoForm = new uploadUserPhotoForm();
                $profileImageFileName = $uploadUserPhotoForm->upload();
                if ($profileImageFileName === false) {
                    return [
                        'success' => false,
                        'message' => 'Image upload error'
                    ];
                }

                $transaction = Yii::$app->db->beginTransaction();
                try {
                    // save profile and user
                    if ($profileImageFileName !== null) {
                        // set new profile photo, if selected
                        $profileModel->profile_image = $profileImageFileName;
                    }
                    $profileModel->save(false);
                    if (isset($data['User']['password']) && ($data['User']['password'] !== null)) {
                        // generate new password hash (if set)
                        $userModel->password = Yii::$app->security->generatePasswordHash($userModel->password);
                    }
                    $userModel->save(false);

                    // save user roles
                    $userRoles = Yii::$app->authManager->getRolesByUser($id);
                    foreach($userRoles as $role){
                        // remove all roles from user
                        Yii::$app->authManager->revoke($role, $id);
                    }
                    if (!empty($data['User']['rolesAsArray'])) {
                        foreach($data['User']['rolesAsArray'] as $roleName) {
                            $role = new Role();
                            $role->name = $roleName;
                            Yii::$app->authManager->assign($role, $userModel->id);
                        }
                    }

                    // --- save technologies ---

                    $oldUserTechnologies = UserTechnology::find()
                        ->where(['user_id' => $id])
                        ->all();

                    $canManageTechnologyRating = Yii::$app->user->can('technology.ratingManage');
                    // update old user technology records
                    $oldTechnologiesIds = [];
                    foreach ($oldUserTechnologies as $oldUt) {
                        foreach($data['User']['userTechnologiesFullList'] as $newUt) {
                            if ($oldUt->technology_id == $newUt['id']) {
                                $oldUt->active = $newUt['active'];
                                if ($canManageTechnologyRating) {
                                    $oldUt->rating = $newUt['rating'];
                                }
                                $oldUt->save();
                                break;
                            }
                        }
                        $oldTechnologiesIds[] = $oldUt->technology_id;
                    }

                    // add records for new user technologies, which was not added before
                    foreach($data['User']['userTechnologiesFullList'] as $newUt) {
                        if (!in_array($newUt['id'], $oldTechnologiesIds) && $newUt['active']) {
                            $ut = new UserTechnology();
                            $ut->user_id = $id;
                            $ut->technology_id = $newUt['id'];
                            if ($canManageTechnologyRating) {
                                $ut->rating = $newUt['rating'];
                            }
                            $ut->save();
                        }
                    }

                    // --- end save technologies ---

                    // --- save departments ---
                    UserDepartment::deleteAll(['user_id' => $id]);

                    if (!empty($data['User']['userDepartmentsFullList'])) {
                        foreach($data['User']['userDepartmentsFullList'] as $d) {
                            if ($d['chosen']) {
                                $ud = new UserDepartment();
                                $ud->user_id = $id;
                                $ud->department_id = $d['id'];
                                $ud->save();
                            }
                        }
                    }

                    // --- end save departments ---

                    // --- save billing cards ---

                    $oldCardsIds = BillingCard::find()
                        ->select('id')
                        ->where(['owner_id' => $id])
                        ->column();

                    $cards = $data['User']['billingCards'];
                    $cardsIdsToRemove = [];

                    // format old cards list (to remove)
                    $notDeletedOldCardsIds = [];
                    foreach($cards as $card) {
                        // get only that cards, which already exist in database
                        if (isset($card['id'])) {
                            $notDeletedOldCardsIds[] = $card['id'];
                        }
                    }
                    foreach($oldCardsIds as $oldCardId) {
                        if (!in_array($oldCardId, $notDeletedOldCardsIds)) {
                            $cardsIdsToRemove[] = $oldCardId;
                        }
                    }
                    BillingCard::deleteAll([
                        'id' => $cardsIdsToRemove
                    ]);

                    // add new cards
                    foreach($cards as $card) {
                        // get only that cards, which already not exist in database
                        if (!isset($card['id'])) {
                            $newCard = new BillingCard();
                            $newCard->number = $card['number'];
                            $newCard->currency_id = $card['currency_id'];
                            $newCard->expiration_date = $card['expiration_date'];
                            $newCard->owner_id = $userModel->id;
                            $newCard->save();
                        }
                    }

                    // --- end save billing cards ---

                    $transaction->commit();

                    // delete old profile photo, if new is selected, and all changes was successfully
                    if ($profileImageFileName !== null) {
                        @unlink(Profile::$profileImagesPath . $oldProfileImage);
                    }

                    return ['success' => true];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    // delete uploaded profile image, if transaction error
                    @unlink(Profile::$profileImagesPath . $profileImageFileName);
                    return [
                        'success' => false,
                        'message' => 'Save data fail (transaction rollBack)'
                    ];
                }
            }
            return [
                'success' => false,
                'message' => 'Not valid'
            ];
        }
        return [
            'success' => false,
            'message' => "Can't load data from post"
        ];
    }

    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('user.delete')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $success = $this->findModel($id)->delete();
        return ['success' => $success];
    }

    public function actionUpdateEmploymentStatus($id) {
        if (($id != Yii::$app->user->id) || !Yii::$app->user->can('user.haveEmploymentStatus')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $user = $this->findModel($id);
        $post = Yii::$app->request->post();
        if (($post['status'] == Profile::STATUS_PROFILE_FREE) && ($user->profile->status != Profile::STATUS_PROFILE_FREE)) {
            // status will be changed to FREE from NOT FREE
            $user->profile->status_free_start_date = date('Y-m-d H:i:s');
            $user->profile->status_free_oriented_date = null;
        }
        if ($post['status'] != Profile::STATUS_PROFILE_FREE) {
            $user->profile->status_free_start_date = null;
            $user->profile->status_free_oriented_date = $post['status_free_oriented_date'];
        }

        $user->profile->status = $post['status'];
        $user->profile->status_description = $post['status_description'];

        return ['success' => $user->profile->save()];
    }

    public function actionGetComplaintsToUser($id) {
        if (!Yii::$app->user->can('complaint.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        return [
            'list' => Complaint::getComplaintsToUser($id)
        ];
    }

    public function actionUpdateCounters() {
        $counters = [
            'importantUnreadNewsCounter' => (int)News::getUnreadNews(News::TYPE_IMPORTANT)->count(),
            'newComplaintsCounter'       => (int)Complaint::getNewComplaints()->count(),
            'newTicketsCounter'          => (int)Ticket::getNewTickets()->count(),
            'newSentTicketsCounter'      => (int)Ticket::getNewSentTickets()->count(),
            'unreadMessagesCounter'      => (int)DialogMessage::getUnreadMessagesCount(),
            'onlineUsersCount'           => (int)Yii::$app->user->getOnlineUsersCount(),
            'onlineUsers'                => Yii::$app->user->getOnlineUsers(),
        ];
        $counters['allTicketsCounter'] = $counters['newTicketsCounter'] + $counters['newSentTicketsCounter'];

        return $counters;
    }

    // prepare models to response (remove important fields from models)
    protected function formatUserToResponse(&$data, $removePaymentsInfo = false) {
        if (is_array($data)){
            foreach ($data as &$user) {
                $user->cleanImportantFields($removePaymentsInfo);
            }
            unset($user);
        } else {
            $data->cleanImportantFields($removePaymentsInfo);
        }
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
