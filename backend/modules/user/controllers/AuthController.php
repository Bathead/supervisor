<?php

namespace app\modules\user\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use app\modules\user\models\forms\LoginForm;

class AuthController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function actionLogin() {
        $model = new LoginForm();

        $success = false;
        if ($model->load(Yii::$app->request->post())) {
            $loginDuration = $this->module->loginDuration;
            $success =  ($model->validate() && $model->login($loginDuration));
            if ($success) {
                return [
                    'success' => $success,
                    'user' => Yii::$app->user->identity,
                    'permissions' => Yii::$app->user->identity->getPermissions()
                ];
            }
        }

        return [
            'success' => $success
        ];
    }

    public function actionLogout()
    {
        Yii::$app->user->deleteUserAfterLogout();
        $success = Yii::$app->user->logout();
        return [
            'success' => $success
        ];
    }

    public function actionGetCurrentUser() {
        $result['user'] = Yii::$app->user->identity;
        $result['permissions'] = (Yii::$app->user->identity === null) ? [] : Yii::$app->user->identity->getPermissions();
        return $result;
    }
}
