<?php

namespace app\modules\user\behaviors;

use Yii;
use yii\base\ActionEvent;
use yii\base\Behavior;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use app\modules\user\models\User;

/**
 * Check, is current user is active
 * 'only' and 'except' parameters are not required, but can be selected only 1 parameter
 * Examples of using:
 *
 *  'activeUser' => [
 *      'class' => ActiveUserBehavior::className(),
 *          'only' => ['method1', 'method2'],
 *      ],
 *
 *  'activeUser' => [
 *      'class' => ActiveUserBehavior::className(),
 *          'except' => ['method1', 'method2'],
 *      ],
 */
class ActiveUserBehavior extends Behavior
{
    public $except = false;
    public $only   = false;


    /**
     * Declares event handlers for the [[owner]]'s events.
     * @return array events (array keys) and the corresponding event handler methods (array values).
     */
    public function events()
    {
        return [Controller::EVENT_BEFORE_ACTION => 'beforeAction'];
    }

    /**
     * @param ActionEvent $event
     * @return boolean
     * @throws Exception
     */
    public function beforeAction($event)
    {
        if ($this->except && $this->only) {
            // behavior can have only one of this parameters
            throw new Exception();
        }

        $methodName = $event->action->actionMethod;

        // except checking for excepted methods, if exist
        if ($this->except && in_array($methodName, $this->except)) {
            return $event->isValid;
        }

        // prevent, if method is does not need checking
        if ($this->only && !in_array($methodName, $this->only)) {
            return $event->isValid;
        }
        
        $currentUser = Yii::$app->user->getIdentity();
        
        if ($currentUser && ($currentUser->status != User::STATUS_INACTIVE)) {
            return $event->isValid;
        } else {
            throw new MethodNotAllowedHttpException('Method allowed only for active users');
        }
    }
}
