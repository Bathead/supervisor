<?php
namespace app\modules\user\models;

use yii;
use yii\db\ActiveRecord;
use ReflectionClass;
use yii\helpers\Inflector;
use app\modules\payment\models\Currency;
use app\modules\englishLanguageLevel\models\EnglishLanguageLevel;
/**
 * This is the model class for table "profile".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $phone
 * @property string $skype
 * @property string $icq
 * @property double $monthly_rate
 * @property integer $monthly_rate_currency_id
 * @property double $external_hourly_rate
 * @property double $internal_hourly_rate
 * @property string $profile_image
 * @property integer $external_hourly_rate_currency_id
 * @property integer $internal_hourly_rate_currency_id
 * @property double $rating
 * @property integer $rank_id
 * @property integer $status
 * @property string $status_description
 * @property string $status_free_start_date
 * @property string $status_free_oriented_date
 *
 * @property Currency $externalHourlyRateCurrency
 * @property Currency $internalHourlyRateCurrency
 * @property string $monthlyRateAsString
 * @property Currency $monthlyRateCurrency
 * @property EnglishLanguageLevel $englishLanguageLevel_id
 * @property string $externalHourlyRateAsString
 * @property string $internalHourlyRateAsString
 * @property string $departmentAsString
 * @property string $englishLanguageLevelAsString
 * @property Rank $rank
 * @property Department $department
 * @property User $user
 * @property string $profileImageFullPath
 * @property string $rankAsString
 * @property int | false $lastFreeDaysCount
 */
class Profile extends ActiveRecord
{
    public $profileImage;
    public static $defaultImage = 'data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABQAAD/4QMpaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjAtYzA2MCA2MS4xMzQ3NzcsIDIwMTAvMDIvMTItMTc6MzI6MDAgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgV2luZG93cyIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo4RDkyMjIzOEIyQkExMUU1ODNFNEE4NjFDNUMzNjY2NyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo4RDkyMjIzOUIyQkExMUU1ODNFNEE4NjFDNUMzNjY2NyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjhEOTIyMjM2QjJCQTExRTU4M0U0QTg2MUM1QzM2NjY3IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjhEOTIyMjM3QjJCQTExRTU4M0U0QTg2MUM1QzM2NjY3Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQAAgICAgICAgICAgMCAgIDBAMCAgMEBQQEBAQEBQYFBQUFBQUGBgcHCAcHBgkJCgoJCQwMDAwMDAwMDAwMDAwMDAEDAwMFBAUJBgYJDQsJCw0PDg4ODg8PDAwMDAwPDwwMDAwMDA8MDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/8AAEQgAQABAAwERAAIRAQMRAf/EAHkAAQEBAQEAAAAAAAAAAAAAAAAHBggJAQEBAQEBAAAAAAAAAAAAAAAABAMFAhAAAgEDAgUCBAcAAAAAAAAAAQIDAAQFESFREhMGB4EiMUFhkXHB0UKCkkQRAQACAQQCAgMAAAAAAAAAAAABAgMRIUEEMXFREmEiMv/aAAwDAQACEQMRAD8A9N67qAoFBtsN2DnssiTvGuNtX3WW51DsOKxjf76VNk7VKbeWlcUy0GY8cLi8Rc3lvNc5a9iAIhjVUUD9zcujM2g30B1rOnbm1tJ2h7ti0j8pTVrAoFAoFBUvGeCtb65u8rdxiYY9kjtI2GqiRhzFyOKjTSo+5kmIisctsNdd5XOucpKCF+TMHbWN1aZW0jEIyDPHdxqNFMijmD6cWGuv4V0epkmYms8Js1dN4S6rGJQKBQW/xS4ONy0fzW6Vj/KMD8q53c/qPSnB4lVqjbFBKfKzgY7Ex/Nrpm/rGf1q3pf1Pphn8QiFdBOUCgUF58WvC2DvEVFWeO7YTOB7mBVSup+m4Fc3uRP3j0pw+FMqRsUEz8pPCuDs0dFaeS7UQuR7lAVi2h+uwNV9OP3n0xzeEGrpJigUCg3/AI4ykePzxt5nCRZOLoAk6DqqeZPvuPWpe1T7U1+GmK2lnQ1cxWUHPPkfKR5DPC3hcPFjIuixB1HVY8z/AG2HpXT6lPrTX5S5Z1lgKqZFAoFBoe1LCfI9wYuGBOfozx3E+4HLHEwZm3rLPaK0mZe6RrLqSuOsKDlvuuwnx3cGUhnTk608k8G4PNHKxZW2rsYLRakaI7xpLPVq8FAoNTiOzc/mSrQWZtrZv9dzrGmnEAjmb0FY5OxSnOr3XHNln7V7Lte2mkujcteX80fTklICoq6gkIu53I+JNc/N2JybcKKY4q2tYNCgxXdXZdr3K0d0LlrO/hj6ccoAZGXUkB12PxPxBqjD2Jx7cM744si+X7Oz+G5nnszc2y/67bWRNOJAHMvqKvx9il+dE9sc1Zetnh0zhOzMJhFRo7Zbu8Ue6+nAZ9eKg7L6VyMme9+dllccVaysXsoFAoFAoMnnOzMJm1d5LZbS8Ye2+gAV9eLAbN61tjz2pzs8WpFn/9k=';
    public static $profileImagesPath = 'uploads/avatars/';

//    public $billingUSD;
//    public $billingUAH;

    const STATUS_PROFILE_FREE = 1;
    const STATUS_PROFILE_OUTER_PROJECT = 2;
    const STATUS_PROFILE_INNER_PROJECT = 3;
//    const STATUS_PROFILE_ESSAY = 4;

    const SCENARIO_PROFILE = 'profile';

    public static function tableName()
    {
        return 'profile';
    }

    public function rules()
    {
        return [
            [['monthly_rate', 'external_hourly_rate', 'internal_hourly_rate', 'rating'], 'number'],
            [['monthly_rate_currency_id', 'external_hourly_rate_currency_id', 'internal_hourly_rate_currency_id', 'rank_id', 'englishLanguageLevel_id'], 'integer'],
            [['first_name', 'last_name', 'middle_name', 'phone', 'skype', 'icq', 'profile_image', /*'billing_info',*/ 'email_home'], 'string', 'max' => 255],
            ['rating', 'double', 'min' => 0, 'max' => 10],
            [['profileImage'], 'file', 'skipOnEmpty' => true],
            ['status', 'integer'],
            ['status_description', 'string', 'max' => 30],
            ['birthday', 'safe'],
            ['email_home', 'email'],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_PROFILE] = [
            'first_name',
            'last_name',
            'middle_name',
            'phone',
            'skype',
            'icq',
            'englishLanguageLevel_id',
            'profile_image',
            'email_home',
            'birthday'
        ];
        return $scenarios;
    }

    public function attributeLabels()
    {
        return [
            'id'                        => 'Id',
            'first_name'                => 'First name',
            'last_name'                 => 'Last name',
            'middle_name'               => 'Middle name',
            'phone'                     => 'Phone',
            'skype'                     => 'Skype',
            'icq'                       => 'Icq',
            'monthly_rate'              => 'Monthly rate',
            'monthly_rate_currency_id'  => 'Monthly rate currency',
            'external_hourly_rate'               => 'External hourly rate',
            'external_hourly_rate_currency_id'   => 'External hourly rate currency',
            'internal_hourly_rate'               => 'Internal hourly rate',
            'internal_hourly_rate_currency_id'   => 'Internal hourly rate currency',
            'rating'                    => 'Rating',
            'rank_id'                   => 'Rank',
            'monthlyRateAsString'       => 'Monthly rate',
            'externalHourlyRateAsString'        => 'External hourly rate',
            'internalHourlyRateAsString'        => 'Internal hourly rate',
            'englishLanguageLevel_id'   => 'English language level',
            'profile_image'             => 'Profile Image',
            'profileImage'              => 'Profile Image',
            'status'                    => 'Employment status',
            'status_description'        => 'Employment status description',
            'email_home'                => 'Home email',
            'birthday'                  => 'Birthday',
            'lastFreeDaysCount'         => 'Last free days count',
        ];
    }

    public function fields()
    {
        $fields = array_merge(parent::fields(), [
            'monthlyRateAsString',
            'monthlyRateCurrency',
            'externalHourlyRateAsString',
            'internalHourlyRateAsString',
            'externalHourlyRateCurrency',
            'internalHourlyRateCurrency',
            'rank',
            'rankAsString',
            'profileImageFullPath',
//            'department',
//            'departmentAsString',
            'englishLanguageLevelAsString',
            'statusAsString',
            'lastFreeDaysCount'
        ]);
        return $fields;
    }

    public static function statusDropdown() {
        $result = [];
        $constPrefix = "STATUS_PROFILE_";

        // create a reflection class to get constants
        $reflClass = new ReflectionClass(get_called_class());
        $constants = $reflClass->getConstants();

        // check for status constants (e.g., STATUS_ACTIVE)
        foreach ($constants as $constantName => $constantValue) {

            // add prettified name to dropdown
            if (strpos($constantName, $constPrefix) === 0) {
                $prettyName = str_replace($constPrefix, "", $constantName);
                $prettyName = Inflector::humanize(strtolower($prettyName));
                $result[] = [
                    'id' => $constantValue,
                    'title' => $prettyName
                ];
            }
        }

        return $result;
    }

    public function getStatusAsString()
    {
        if ($this->status === null) {
            return null;
        } else {
            $statuses = self::statusDropdown();
            $status = array_search($this->status, array_column($statuses, 'id'));
            return $statuses[$status]['title'];
        }
    }

    public function getRank() {
        return $this->hasOne(Rank::className(), ['id' => 'rank_id']);
    }

    public function getMonthlyRateCurrency() {
        return $this->hasOne(Currency::className(), ['id' => 'monthly_rate_currency_id']);
    }

    public function getMonthlyRateAsString() {
        return $this->monthly_rate . ' ' . (($this->monthlyRateCurrency === null) ? null : $this->monthlyRateCurrency->name);
    }

    public function getExternalHourlyRateCurrency() {
        return $this->hasOne(Currency::className(), ['id' => 'external_hourly_rate_currency_id']);
    }

    public function getExternalHourlyRateAsString() {
        return $this->external_hourly_rate . ' ' . (($this->externalHourlyRateCurrency === null) ? null : $this->externalHourlyRateCurrency->name);
    }

    public function getInternalHourlyRateCurrency() {
        return $this->hasOne(Currency::className(), ['id' => 'internal_hourly_rate_currency_id']);
    }

    public function getInternalHourlyRateAsString() {
        return $this->internal_hourly_rate . ' ' . (($this->internalHourlyRateCurrency === null) ? null : $this->internalHourlyRateCurrency->name);
    }

    public function getUser(){
        return $this->hasOne(User::className(), ['profile_id' => 'id']);
    }

    public function getEnglishLanguageLevel(){
        return $this->hasOne(EnglishLanguageLevel::className(), ['id' => 'englishLanguageLevel_id']);
    }

    public function getEnglishLanguageLevelAsString(){
        return ($this->englishLanguageLevel === null) ? null : $this->englishLanguageLevel->name;
    }

    public function getProfileImageFullPath() {
        return ($this->profile_image === null) ?
            self::$defaultImage :
            Yii::$app->params['backendPublicDirPath'] . self::$profileImagesPath . $this->profile_image;
    }

//    public function getDepartment(){
//        return $this->hasOne(Department::className(), ['id' => 'department_id']);
//    }
//
//    public function getDepartmentAsString(){
//        return ($this->department === null) ? null : $this->department->name;
//    }

    public function getRankAsString() {
        return ($this->rank === null) ? null : $this->rank->name;
    }

    public function getLastFreeDaysCount() {
        if ($this->status_free_start_date !== null) {
            $d1 = strtotime($this->status_free_start_date);
            $d2 = time();
            $daysDiff = floor(($d2 - $d1) / 86400); // 86400 seconds in 1 day
            return $daysDiff;
        }
        return false;
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            if ($this->profile_image) {
                $path = Profile::$profileImagesPath . $this->profile_image;
                if (file_exists($path)) {
                    @unlink($path);
                }
            }
            return true;
        } else {
            return false;
        }
    }
}