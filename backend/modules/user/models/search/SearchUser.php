<?php

namespace app\modules\user\models\search;

use app\modules\department\models\UserDepartment;
use app\modules\user\models\Rank;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\user\models\User;
use app\modules\user\models\Profile;


/**
 * search represents the model behind the search form about `app\modules\user\models\User`.
 */
class SearchUser extends User
{
    public $employmentStatus;
    public $userOnline;
    public $fullName;
    public $rating;
    public $rank;
    public $role;
    public $monthlyRate;
    public $externalHourlyRate;
    public $internalHourlyRate;
    public $departmentsIds;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'profile_id'], 'integer'],
            [[
                'email',
                'username',
                'userOnline',
                'fullName',
                'employmentStatus',
                'rating',
                'rank',
                'role',
                'monthlyRate',
                'externalHourlyRate',
                'internalHourlyRate',
                'departmentsIds'
            ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $tn_user = User::tableName();
        $tn_profile = Profile::tableName();
        $tn_rank = Rank::tableName();
        $tn_user_department = UserDepartment::tableName();
//        $tn_department = Department::tableName();

        $query = User::find()
            ->from("$tn_user as u")
            ->leftJoin("$tn_profile as p", 'p.id = u.profile_id')
            ->leftJoin("$tn_rank as r", 'r.id = p.rank_id');
//            ->leftJoin("$tn_department as d", 'd.id = p.department_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $params['count'],
                'page' => $params['page'] - 1
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'fullName' => [
                    'asc' => ['first_name' => SORT_ASC, 'last_name' => SORT_ASC, 'middle_name' => SORT_ASC],
                    'desc' => ['first_name' => SORT_DESC, 'last_name' => SORT_DESC], 'middle_name' => SORT_DESC,
                    'label' => 'Name',
                    'default' => SORT_ASC
                ],
                'rating' => [
                    'asc' => ['rating' => SORT_ASC],
                    'desc' => ['rating' => SORT_DESC],
                ],
                'rank' => [
                    'asc' => ['r.name' => SORT_ASC],
                    'desc' => ['r.name' => SORT_DESC],
                ],
//                'department' => [
//                    'asc' => ['d.name' => SORT_ASC],
//                    'desc' => ['d.name' => SORT_DESC],
//                ],
                'employmentStatus' => [
                  'asc' => ['p.status' => SORT_ASC],
                  'desc' => ['p.status' => SORT_DESC],
                ],
            ]
        ]);


        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (!empty($this->fullName)) {
            $query->andFilterWhere(['like', 'p.first_name', $this->fullName])
                ->orFilterWhere(['like', 'p.last_name', $this->fullName])
                ->orFilterWhere(['like', 'p.middle_name', $this->fullName]);
        }

        if (!empty($this->rank)) {
            $query->andWhere(['p.rank_id' => $this->rank]);
        }

//        if (!empty($this->department)) {
//            $query->andWhere(['p.department_id' => $this->department]);
//        }

        if (isset($this->rating) && $this->rating !== '') {
            if ($c = $this->isComparable($this->rating)) {
                $query->andWhere([$c['condition'], 'p.rating', $c['value']]);
            } else {
                $query->andWhere(['p.rating' => $this->rating]);
            }
        }

        if (!empty($this->role)) {
            $roles_type = 1;
            $query->leftJoin('auth_assignment as aa', 'aa.user_id = u.id')
                ->leftJoin('auth_item as ai', 'ai.name = aa.item_name')
                ->andWhere(['ai.name' => $this->role])
                ->andWhere(['ai.type' => $roles_type]);
        }

        if (!empty($this->monthlyRate)) {
            if ($c = $this->isComparable($this->monthlyRate)) {
                $query->andWhere([$c['condition'], 'p.monthly_rate', $c['value']]);
            } else {
                $query->andWhere(['p.monthly_rate' => $this->monthlyRate]);
            }
        }

        if (!empty($this->externalHourlyRate)) {
            if ($c = $this->isComparable($this->externalHourlyRate)) {
                $query->andWhere([$c['condition'], 'p.external_hourly_rate', $c['value']]);
            } else {
                $query->andWhere(['p.external_hourly_rate' => $this->externalHourlyRate]);
            }
        }

        if (!empty($this->internalHourlyRate)) {
            if ($c = $this->isComparable($this->internalHourlyRate)) {
                $query->andWhere([$c['condition'], 'p.internal_hourly_rate', $c['value']]);
            } else {
                $query->andWhere(['p.internal_hourly_rate' => $this->internalHourlyRate]);
            }
        }

        if (!empty($this->employmentStatus)) {
            $query->andWhere(['p.status' => $this->employmentStatus]);
        }

        if (!empty($this->departmentsIds)) {
            $query
                ->leftJoin("$tn_user_department as ud", 'u.id = ud.user_id')
                ->andWhere(['in', 'ud.department_id', $this->departmentsIds]);
        }

//        if (!empty($this->userOnline)) {
//            switch($this->userOnline) {
//                case 'offline':
//                    foreach (Yii::$app->user->onlineUsers as $key => $user) {
//                        $query->andWhere(['!=', 'u.id', $key]);
//                    }
//                    break;
//                case 'online':
//                    $query->andWhere(['u.id' => array_keys(Yii::$app->user->onlineUsers)]);
//                    break;
//            }
//
//        }

        return $dataProvider;
    }

    // check, is string contain comparable symbols ('>=', '<=', '>', '<')
    // and return false, or ['condition' => '...', 'value' => '...']
    private function isComparable($string) {
        $conditions = ['>=', '<=', '>', '<'];
        foreach ($conditions as $condition) {
            if (strpos($string, $condition) === 0) {
                return [
                    'condition' => $condition,
                    'value' => str_replace($condition, '', $string)
                ];
            }
        }
        return false;
    }
}
