<?php

namespace app\modules\user\models\forms;

use Yii;
use app\modules\user\models\User;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username'   => 'Username',
            'password'   => 'Password',
            'rememberMe' => 'Remember me',
        ];
    }

    /**
     * Validates the username and password.
     * This method serves as the inline validation for password.
     */
    public function validatePassword()
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if ($user === NULL) {
                $this->addError('username', 'Wrong username');
            } elseif (!$user || !$user->validatePassword($this->password)) {
                $this->addError('password', 'Wrong password');
            } elseif ($user && $user->status == User::STATUS_INACTIVE) {
                $this->addError('username', 'This user is inactive');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login($loginDuration)
    {
        if ($this->validate()) {
            $loginSuccess = Yii::$app->user->login($this->getUser(), $this->rememberMe ? $loginDuration : 0);
            return $loginSuccess;
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->user === false) {
            $this->user = User::findByUsername($this->username);
        }
        return $this->user;
    }
}
