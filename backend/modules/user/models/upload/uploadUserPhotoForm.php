<?php
namespace app\modules\user\models\upload;

use yii\base\Model;
use yii\web\UploadedFile;
use app\modules\user\models\Profile;

class uploadUserPhotoForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function __construct() {
        parent::__construct();
        $this->imageFile = UploadedFile::getInstanceByName('file');
    }

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif, jpeg'],
        ];
    }

    public function upload()
    {
        if ($this->imageFile === null) {
            return null;
        }
        if ($this->validate()) {
            if (!file_exists(Profile::$profileImagesPath)) {
                mkdir(Profile::$profileImagesPath, 0777, true);
            }
            $fileName = time() . '.' . $this->imageFile->extension;
            $this->imageFile->saveAs(Profile::$profileImagesPath . $fileName);
            return $fileName;
        } else {
            return false;
        }
    }
}