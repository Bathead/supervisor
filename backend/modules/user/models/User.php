<?php
namespace app\modules\user\models;

use app\modules\department\models\UserDepartment;
use yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\modules\project\models\Project;
use yii\rbac\Role;
use app\modules\technology\models\UserTechnology;
use app\modules\complaint\models\Complaint;
use ReflectionClass;
use yii\helpers\Inflector;
use app\modules\dialog\models\Dialog;
use app\modules\dialog\models\DialogUser;
use app\modules\payment\models\BillingCard;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $email
 * @property string $username
 * @property string $password
 * @property string $auth_key
 * @property string $access_token
 * @property string $fullName
 * @property string $extendedName
 * @property string $rating
 * @property integer $status
 * @property integer $profile_id
 *
 * @property Project[] $projects
 * @property Profile $profile
 * @property Role[] $roles
 * @property string[] $rolesAsArray
 * @property string $rolesAsString
 * @property UserTechnology[] $userTechnologies
 * @property string $technologiesAsString
 * @property string $statusAsString
 * @property Complaint[] $complaints
 * @property Dialog[] $dialogs
 * @property BillingCard[] $billingCards
 * @property BillingCard $billingCardUSD
 * @property BillingCard $billingCardUAH
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const SCENARIO_PROFILE = 'profile';

    public static function tableName()
    {
        return 'user';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_PROFILE] = [
            'email',
            'password'
        ];
        return $scenarios;
    }

    public function rules()
    {
        return [
            [['username', 'email'], 'required'],
            [['username', 'email'], 'unique'],
            [['status', 'profile_id'], 'integer'],
            [['email', 'username', 'password', 'auth_key', 'access_token'], 'string', 'max' => 255],
            ['email', 'email'],
            ['status', 'default', 'value' => 1],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'                                => 'Id',
            'email'                             => 'Email',
            'username'                          => 'Username',
            'password'                          => 'Password',
            'auth_key'                          => 'Auth key',
            'access_token'                      => 'Access token',
            'fullName'                          => 'Name',
            'extendedName'                      => 'Name',
            'status'                            => 'Status',
            'profile_id'                        => 'Profile id',
            'rolesAsArray'                      => 'Roles',
            'rolesAsString'                     => 'Roles',
            'technologiesAsString'              => 'Technologies',
            'statusAsString'                    => 'Status',
            'monthlyRateAsString'               => 'Monthly rate',
            'rating'                            => 'Rating',
            'rank'                              => 'Rank',
            'profile.englishLanguageLevel.name' => 'English level',
            'profile.skype'                     => 'Skype',
        ];
    }

    public function fields()
    {
        $fields = array_merge(parent::fields(), [
            'fullName',
            'extendedName',
            'rolesAsString',
            'rolesAsArray',
            'technologiesAsString',
            'userTechnologies',
            'departmentsAsString',
            'userDepartments',
            'profile'
        ]);

        return $fields;
    }

    // remove important fields, which must not be returned as request result
    public function cleanImportantFields($unsetPaymentInfo = false) {
        unset($this->password);
        unset($this->auth_key);
        unset($this->access_token);
        if ($unsetPaymentInfo) {
            unset($this->profile->monthly_rate);
            unset($this->profile->monthly_rate_currency_id);
            unset($this->profile->external_hourly_rate);
            unset($this->profile->external_hourly_rate_currency_id);
            unset($this->profile->internal_hourly_rate);
            unset($this->profile->internal_hourly_rate_currency_id);
        }
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return boolean if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $date = date('Y-m-d h:i:s');;
            if ($this->isNewRecord) {
                $this->auth_key = \Yii::$app->security->generateRandomString();
                $this->access_token = \Yii::$app->security->generateRandomString();
                $this->created_at = $date;
            }
            $this->updated_at = $date;
            return true;
        }
        return false;
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            UserTechnology::deleteAll(['user_id' => $this->id]);
            Complaint::deleteAll("who_id = $this->id");
            Complaint::deleteAll("whom_id = $this->id");
            return true;
        } else {
            return false;
        }
    }

    public function afterDelete()
    {
        $this->profile->delete();
        Yii::$app->authManager->revokeAll($this->id);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public function getDisplayName()
    {
        return $this->username;
    }

    public function getFullName()
    {
        if (empty($this->profile->first_name) && empty($this->profile->last_name))
            return $this->username;
        return $this->profile->first_name . ' ' . $this->profile->last_name;
    }

    public function getExtendedName()
    {
        if (empty($this->profile->first_name) && empty($this->profile->last_name) && empty($this->profile->middle_name))
            return $this->username;
        return $this->profile->first_name . ' ' . $this->profile->last_name . ' ' . $this->profile->middle_name;
    }

    public function getFirstName()
    {
        return $this->profile->first_name;
    }

    public function getLastName()
    {
        return $this->profile->last_name;
    }

    public function getMiddleName()
    {
        return $this->profile->middle_name;
    }

    public function getUserOnline()
    {
        return (isset(Yii::$app->user->onlineUsers[$this->id])) ? 'Online' : 'Offline';
    }

    public function getEmploymentStatus()
    {
        return $this->profile->getStatusAsString();
    }

    public function getRating()
    {
        return $this->profile->rating;
    }

    public function getMonthlyRateAsString() {
        return $this->profile->monthlyRateAsString;
    }

    public function getRank()
    {
        return ($this->profile->rank === null) ? null : $this->profile->rank->name;
    }

    public static function getActiveUsersCount()
    {
        return
            User::find()
                ->where(['status' => User::STATUS_ACTIVE])
                ->count();
    }

    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['id_sales' => 'id']);
    }

    public function getProfile() {
        return $this->hasOne(Profile::className(), ['id' => 'profile_id']);
    }

    public function getRoles() {
        return Yii::$app->authManager->getRolesByUser($this->id);
    }

    public function getRolesAsArray() {
        $result = [];
        foreach($this->roles as $role) {
            $result[] = $role->name;
        }
        return $result;
    }

    public function getRolesAsString() {
        $result = '';
        foreach ($this->roles as $role) {
            $result .= empty($result) ? $role->name : ', ' . $role->name;
        }
        return $result;
    }

    public function getUserDepartments() {
        return $this->hasMany(UserDepartment::className(), ['user_id' => 'id']);
    }

    public function getDepartmentsAsString() {
        $result = '';
        foreach ($this->userDepartments as $ud) {
            $result .= empty($result) ? $ud->department->name : ', ' . $ud->department->name;
        }
        return $result;
    }

    public function getUserTechnologies() {
        return $this->hasMany(UserTechnology::className(), ['user_id' => 'id']);
    }

    public function getTechnologiesAsString() {
        $result = '';
        foreach ($this->userTechnologies as $ut) {
            if ($ut->active) {
                $result .= empty($result) ? $ut->technology->name : ', ' . $ut->technology->name;
            }
        }
        return $result;
    }

    public function getComplaints() {
        return $this->hasMany(Complaint::className(), ['whom_id' => 'id'])
            ->addOrderBy(['status' => SORT_ASC, 'date' => SORT_DESC]);
    }

    public function getCreated() {
        return $this->created_at;
    }

    public function getUpdated() {
        return $this->updated_at;
    }

    public static function getUsersWithPermission($permission) {
        return self::find()
            ->from(self::tableName() . ' as u')
            ->leftJoin('auth_assignment as aa', 'aa.user_id = u.id')
            ->leftJoin('auth_item_child as aic', 'aic.parent = aa.item_name')
            ->where("aic.child = '$permission'")
            ->with('profile')
            ->all();
    }

    public function getPermissions() {
        $permissions = Yii::$app->authManager->getPermissionsByUser($this->id);
        $result = [];
        foreach ($permissions as $name => $properties) {
            $result[] = $name;
        }
        return $result;
    }

    public static function getUserStatuses() {
        $result = [];
        $constPrefix = "STATUS_";

        // create a reflection class to get constants
        $reflClass = new ReflectionClass(get_called_class());
        $constants = $reflClass->getConstants();

        // check for status constants (e.g., STATUS_ACTIVE)
        foreach ($constants as $constantName => $constantValue) {

            // add prettified name to dropdown
            if (strpos($constantName, $constPrefix) === 0) {
                $prettyName = str_replace($constPrefix, "", $constantName);
                $prettyName = Inflector::humanize(strtolower($prettyName));
                $result[] = [
                    'id' => $constantValue,
                    'title' => $prettyName
                ];
            }
        }

        return $result;
    }

    public function getStatusAsString()
    {
        if ($this->status === null) {
            return null;
        } else {
            $statuses = self::getUserStatuses();
            $status = array_search($this->status, array_column($statuses, 'id'));
            return $statuses[$status]['title'];
        }
    }

    public function getDialogs() {
        return Dialog::find()
            ->with('lastMessage')
            ->from(Dialog::tableName() . ' as d')
            ->rightJoin(DialogUser::tableName() . ' as du', 'd.id = du.dialog_id')
            ->where(['du.user_id' => Yii::$app->user->id])
            ->orderBy(['d.last_action_date' => SORT_DESC])
            ->all();
    }

    public function getBillingCards(){
        return $this->hasMany(BillingCard::className(), ['owner_id' => 'id']);
    }

    public function getBillingCardUSD(){
        return BillingCard::findOne(['owner_id' => $this->id, 'currency_id' => '1']);
    }

    public function getBillingCardUAH(){
        return BillingCard::findOne(['owner_id' => $this->id, 'currency_id' => '2']);
    }

    public function can($permissionName, $params = [])
    {
        if (($manager = Yii::$app->authManager) === null) {
            return false;
        }
        return $manager->checkAccess($this->id, $permissionName, $params);
    }
}