<?php

namespace app\modules\department\controllers;

use Yii;
use app\modules\department\models\Department;
use app\modules\department\models\search\SearchDepartment;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;
use app\modules\user\models\User;
use app\modules\user\models\Profile;
use app\modules\department\models\UserDepartment;

/**
 * DepartmentController implements the CRUD actions for Department model.
 */
class DepartmentController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionGetDepartments() {
        if (!Yii::$app->user->can('department.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $searchModel = new SearchDepartment();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return [
            'list' => $dataProvider->getModels(),
            'totalCount' => $dataProvider->totalCount
        ];
    }

    public function actionGetAllDepartments() {
        return [
            'list' => Department::find()->all()
        ];
    }

    public function actionGetDepartment($id) {
        if (!Yii::$app->user->can('department.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        return [
            'entity' => Department::findOne($id)->toArray([], ['head'])
        ];
    }

    public function actionGetStatistic($id) {
        if (!Yii::$app->user->can('department.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $department = $this->findModel($id);
        return [
            'employees'             => $department->employees,
            'bestWorker'            => $department->bestWorker,
            'employeesPercent'   => $department->employeesPercent,
            'busyEmployeesPercent'    => $department->busyEmployeesPercent
        ];
    }

    public function actionCreate() {
        if (!Yii::$app->user->can('department.create')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $model = new Department();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['success' => true];
        }
        return ['success' => false];
    }

    public function actionUpdate($id) {
        if (!Yii::$app->user->can('department.update')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['success' => true];
        }
        return ['success' => false];
    }

    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('department.delete')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $success = $this->findModel($id)->delete();
        return ['success' => $success];
    }

    public function actionGetUsersByName() {
        if (!Yii::$app->user->can('user.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $name = Yii::$app->request->post('name');
        $departmentId = Yii::$app->request->post('departmentId');

        $users = User::find()
            ->from(User::tableName() . ' as u')
            ->rightJoin(UserDepartment::tableName() . ' as ud', 'ud.user_id = u.id')
            ->where(['ud.department_id' => $departmentId])
            ->leftJoin(Profile::tableName() . ' as p', 'p.id = u.profile_id')
            ->andFilterWhere([
                'or',
                ['like', 'p.first_name', $name],
                ['like', 'p.last_name', $name],
                ['like', 'p.middle_name', $name]
            ])
            ->andWhere(['u.status' => User::STATUS_ACTIVE])
            ->all();

        return [
            'list' => $users
        ];
    }

    protected function findModel($id)
    {
        if (($model = Department::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
