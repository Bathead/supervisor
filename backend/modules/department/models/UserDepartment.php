<?php
namespace app\modules\department\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_department".
 *
 * @property integer $user_id
 * @property integer $department_id
 * @property Department $department
 *
 */
class UserDepartment extends ActiveRecord
{
    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */

    public static function tableName()
    {
        return 'user_department';
    }

    public function fields()
    {
        $fields = array_merge(parent::fields(), [
            'department',
        ]);

        return $fields;
    }

    public function getDepartment() {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }
}