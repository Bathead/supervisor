<?php

namespace app\modules\department\models;

use Yii;
use yii\db\ActiveRecord;
use app\modules\user\models\Profile;
use app\modules\user\models\User;
use app\modules\department\models\UserDepartment;
use yii\db\Query;

/**
 * This is the model class for table "department".
 *
 * @property integer $id
 * @property string $name
 */
class Department extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'department';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            ['head_id', 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    public function extraFields(){
        $fields = array_merge(parent::extraFields(), [
            'head'
        ]);

        return $fields;
    }

    public function fields()
    {
        $fields = array_merge(parent::fields(), [
            'headInfo'
        ]);

        return $fields;
    }

    public function getHead(){
        return $this->hasOne(User::className(), ['id' => 'head_id']);
    }

    public function getHeadInfo(){
        if($this->head)
            return [
                'id' => $this->head->id,
                'fullName' => $this->head->fullName,
                'profileImageFullPath' => $this->head->profile->profileImageFullPath
            ];
        else
            return null;
    }

    public function getBestWorker(){
            return User::find()
                ->from(User::tableName() . ' as u')
                ->rightJoin(UserDepartment::tableName() . ' as ud', 'ud.user_id = u.id')
                ->where(['ud.department_id' => $this->id])
                ->leftJoin(Profile::tableName() . ' as p', 'p.id = u.profile_id')
                ->orderBy(['p.rating' => SORT_DESC])
                ->one();
    }

    public function getEmployees(){
        return $this
            ->hasMany(User::className(), ['id' => 'user_id'])
            ->viaTable(UserDepartment::tableName(), ['department_id' => 'id']);
    }

    public function getBusyEmployeesCount(){
        return User::find()
            ->from(User::tableName() . ' as u')
            ->rightJoin(UserDepartment::tableName() . ' as ud', 'ud.user_id = u.id')
            ->where(['ud.department_id' => $this->id])
            ->leftJoin(Profile::tableName() . ' as p', 'p.id = u.profile_id')
            ->andWhere(['!=', 'p.status', Profile::STATUS_PROFILE_FREE])
            ->count();
    }

    public function getBusyEmployeesPercent() {
        return round(($this->busyEmployeesCount / count($this->employees)) * 100, 2);
    }

    public function getEmployeesPercent(){
        return round((count($this->employees) / User::getActiveUsersCount()) * 100, 2);
    }
}
