<?php

namespace app\modules\department\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\department\models\Department;
use app\modules\user\models\Profile;
use app\modules\user\models\User;

/**
 * SearchDepartment represents the model behind the search form about `app\modules\department\models\Department`.
 */
class SearchDepartment extends Department
{
    public $headAsString;

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name'], 'safe'],
            [[
                'headAsString',
            ], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $tn_profile = Profile::tableName();
        $tn_department = Department::tableName();
        $tn_user = User::tableName();

        $query = Department::find()
            ->from("$tn_department as d")
            ->leftJoin("$tn_user as u", 'u.id = d.head_id')
            ->leftJoin("$tn_profile as p", 'p.id = u.profile_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $params['count'],
                'page' => $params['page'] - 1
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'headAsString' => [
                    'asc' => ['p.first_name' => SORT_ASC, 'p.last_name' => SORT_ASC, 'p.middle_name' => SORT_ASC],
                    'desc' => ['p.first_name' => SORT_DESC, 'p.last_name' => SORT_DESC, 'p.middle_name' => SORT_DESC],
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->headAsString)) {
            $query->andFilterWhere(['like', 'p.first_name', $this->headAsString])
                ->orFilterWhere(['like', 'p.last_name', $this->headAsString])
                ->orFilterWhere(['like', 'p.middle_name', $this->headAsString]);
        }

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
