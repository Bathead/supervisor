<?php

namespace app\modules\englishLanguageLevel\models;

use Yii;
use yii\db\ActiveRecord;
use app\modules\user\models\User;
use app\modules\user\models\Profile;


/**
 * This is the model class for table "english_language_level".
 *
 * @property integer $id
 * @property string $name
 */
class EnglishLanguageLevel extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'english_language_level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'English level',
        ];
    }

    public function getEmployees()
    {
        return
            User::find()
                ->from(User::tableName() . ' as u')
                ->leftJoin(Profile::tableName() . ' as p', 'p.id = u.profile_id')
                ->where(
                    [
                        'p.englishLanguageLevel_id' => $this->id,
                        'u.status' => User::STATUS_ACTIVE
                    ]
                )
                ->all();
    }

    public function getEmployeesPercent(){
        return round((count($this->employees) / User::getActiveUsersCount()) * 100, 2);
    }
}
