<?php

namespace app\modules\englishLanguageLevel\controllers;

use Yii;
use app\modules\englishLanguageLevel\models\EnglishLanguageLevel;
use app\modules\englishLanguageLevel\models\search\SearchEnglishLanguageLevel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/**
 * EnglishLanguageLevelController implements the CRUD actions for EnglishLanguageLevel model.
 */
class EnglishLanguageLevelController extends Controller
{
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        Yii::$app->response->format = Response::FORMAT_JSON;
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionGetAllEnglishLanguageLevels() {
        if (!Yii::$app->user->can('language.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        return [
            'englishLanguageLevels' => EnglishLanguageLevel::find()->all()
        ];
    }

    protected function findModel($id)
    {
        if (($model = EnglishLanguageLevel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetLevels() {
        if (!Yii::$app->user->can('language.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $searchModel = new SearchEnglishLanguageLevel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return [
            'list' => $dataProvider->getModels(),
            'totalCount' => $dataProvider->totalCount
        ];
    }

    public function actionGetAllLevels() {
        return [
            'list' => EnglishLanguageLevel::find()->all()
        ];
    }

    public function actionGetLevel($id){
        if (!Yii::$app->user->can('language.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        return [
            'entity' => EnglishLanguageLevel::findOne($id)
        ];
    }

    public function actionGetStatistic($id){
        if (!Yii::$app->user->can('language.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $englishLanguageLevel = $this->findModel($id);

        return [
            'employees' => $englishLanguageLevel->employees,
            'employeesPercent' => $englishLanguageLevel->employeesPercent
        ];
    }

    public function actionCreate() {
        if (!Yii::$app->user->can('language.create')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = new EnglishLanguageLevel();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['success' => true];
        }

        return ['success' => false];
    }

    public function actionUpdate($id) {
        if (!Yii::$app->user->can('language.update')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['success' => true];
        }

        return ['success' => false];
    }

    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('language.delete')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $success = $this->findModel($id)->delete();

        return ['success' => $success];
    }
}
