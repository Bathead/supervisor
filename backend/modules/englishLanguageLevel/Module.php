<?php

namespace app\modules\englishLanguageLevel;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\englishLanguageLevel\controllers';

    public function init()
    {
        parent::init();
    }
}
