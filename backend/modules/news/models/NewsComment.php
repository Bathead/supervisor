<?php

namespace app\modules\news\models;

use Yii;
use app\modules\user\models\User;

/**
 * This is the model class for table "{{%news_comment}}".
 *
 * @property integer $id
 * @property integer $news_id
 * @property integer $user_id
 * @property string $message
 * @property string $updated_at
 * @property string $created_at
 * @property integer $is_deleted
 * @property bool $isEditable
 * @property bool $isDeletable
 *
 * @property User $user
 * @property News $news
 */
class NewsComment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news_comment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_id', 'user_id', 'is_deleted'], 'integer'],
            [['message'], 'string'],
            [['updated_at', 'created_at'], 'safe']
        ];
    }

    public function fields()
    {
        $fields = array_merge(parent::fields(), [
            'user',
            'isEditable',
            'isDeletable',
        ]);

        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'news_id'    => 'News ID',
            'user_id'    => 'News ID',
            'message'    => 'Message',
            'parent_id'  => 'Parent Id',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
            'is_deleted' => 'Is Deleted',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->created_at = date('Y-m-d H:i:s');
                $this->user_id = Yii::$app->user->id;
            }
            return true;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'news_id']);
    }

    public  function SoftDelete() {
        $this->is_deleted = true;
        return $this->save();
    }

    public function getIsEditable() {
        return (Yii::$app->user->can('newsComments.update') || $this->user_id == Yii::$app->user->id);
    }

    public function getIsDeletable() {
        return (Yii::$app->user->can('newsComments.delete') || $this->user_id == Yii::$app->user->id);
    }
}
