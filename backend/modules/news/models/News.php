<?php

namespace app\modules\news\models;

use Yii;
use app\modules\user\models\User;
use ReflectionClass;
use yii\helpers\Inflector;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $excerpt
 * @property string $content
 * @property integer $author_id
 * @property string $created_at
 * @property integer $type
 *
 * @property User $author
 * @property NewsComment[] $newsComments
 * @property NewsUnread[] $newsUnread
 * @property User[] $users
 * @property string $typeAsString
 * @property string $excerptFromContent
 * @property bool $isUnread
 * @property integer $commentsCount
 */
class News extends \yii\db\ActiveRecord
{
    const TYPE_USUAL = 1;
    const TYPE_IMPORTANT = 2;

    private $maxContentLengthForExcerpt = 300;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'excerpt', 'content'], 'string'],
            [['author_id', 'type'], 'integer'],
            [['created_at'], 'safe']
        ];
    }

    public function fields()
    {
        $fields = array_merge(parent::fields(), [
            'author',
            'commentsCount',
            'isUnread'
        ]);

        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'title'      => 'Title',
            'excerpt'    => 'Excerpt',
            'content'    => 'Content',
            'author_id'  => 'Author',
            'created_at' => 'Created At',
            'type'       => 'Type',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->author_id = Yii::$app->user->id;
            }
            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);

        NewsUnread::deleteAll(['news_id' => $this->id]);
        if ($this->type == self::TYPE_IMPORTANT) {

            $userIds = User::find()
                ->where(['status' => User::STATUS_ACTIVE])
                ->andWhere(['!=', 'id', Yii::$app->user->id])
                ->indexBy('id')
                ->column();

            $data = [];
            foreach ($userIds as $userId) {
                $data[] = [$this->id, $userId];
            }
            Yii::$app->db->createCommand()->batchInsert(NewsUnread::tableName(), ['news_id', 'user_id'], $data)->execute();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsComments()
    {
        return $this->hasMany(NewsComment::className(), ['news_id' => 'id']);
    }

    public static function getUnreadNews($type = false) {
        $result = News::find()
            ->from(News::tableName() . ' as n')
            ->leftJoin(NewsUnread::tableName() . ' as nu', 'n.id = nu.news_id')
            ->where(['nu.user_id' => Yii::$app->user->id]);

        if ($type) {
            $result->andWhere(['n.type' => $type]);
        }
        return $result;
    }

    public function getExcerptFromContent() {
        $content = strip_tags($this->content);
        return substr($content, 0, $this->maxContentLengthForExcerpt) . ' ...';
    }

    public static function getNewsTypes() {
        $result = [];
        $constPrefix = "TYPE_";

        // create a reflection class to get constants
        $reflClass = new ReflectionClass(get_called_class());
        $constants = $reflClass->getConstants();

        foreach ($constants as $constantName => $constantValue) {

            // add prettified name to dropdown
            if (strpos($constantName, $constPrefix) === 0) {
                $prettyName = str_replace($constPrefix, "", $constantName);
                $prettyName = Inflector::humanize(strtolower($prettyName));
                $result[] = [
                    'id' => $constantValue,
                    'title' => $prettyName
                ];
            }
        }

        return $result;
    }

    public function getTypeAsString()
    {
        if ($this->type === null) {
            return null;
        } else {
            $types = self::getNewsTypes();
            $type = array_search($this->type, array_column($types, 'id'));
            return $types[$type]['title'];
        }
    }

    public function getIsUnread () {
        $unread = NewsUnread::find()
            ->where(['news_id' => $this->id, 'user_id' => Yii::$app->user->id]);
        return ($unread->count() > 0) ? true : false;
    }

    public function getCommentsCount() {
        return NewsComment::find()
            ->where(['news_id' => $this->id, 'is_deleted' => false])
            ->count();
    }
}
