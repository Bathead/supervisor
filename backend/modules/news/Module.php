<?php

namespace app\modules\news;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\news\controllers';

    public $newsPerPage = 10;
    public $commentsLimit = 10;

    public function init()
    {
        parent::init();
    }
}
