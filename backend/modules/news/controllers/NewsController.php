<?php

namespace app\modules\news\controllers;

use app\modules\news\models\NewsComment;
use app\modules\news\models\NewsUnread;
use Yii;
use app\modules\news\models\News;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;

/**
 * NewsController implements the CRUD actions for news model.
 */
class NewsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionGetNewsList() {
        if (!Yii::$app->user->can('news.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        
        $list = News::find()
            ->limit($this->module->newsPerPage)
            ->orderBy('created_at desc');
        
        $offset = Yii::$app->request->post('offset');
        if ($offset !== null) {
            $list->offset($offset);
        }

        $list = $list->all();

        $allNewsCount = News::find()->count();
        $allElementsLoaded = (($offset + count($list)) >= $allNewsCount) ? true : false;

        return [
            'list' => $list,
            'allElementsLoaded' => $allElementsLoaded
        ];
    }

    public function actionGetLatestNews() {
        if (!Yii::$app->user->can('news.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $count = Yii::$app->request->post('count');

        $list = News::find()
            ->limit($this->module->newsPerPage)
            ->orderBy('created_at desc')
            ->limit($count)
            ->all();

        return [
            'list' => $list,
        ];
    }

    public function actionGetNewsTypes() {
        return [
            'list' => News::getNewsTypes()
        ];
    }

    public function actionGetNews($id) {
        if (!Yii::$app->user->can('news.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        // remove unread record for current user, if exist
        NewsUnread::deleteAll(['news_id' => $id, 'user_id' => Yii::$app->user->id]);

        return [
            'entity' => $this->findModel($id)
        ];
    }

    public function actionGetUnreadCount(){
        if (!Yii::$app->user->can('news.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $count = News::getUnreadNews(News::TYPE_IMPORTANT)->count();

        return [
            'count' => $count,
        ];
    }

    public function actionCreate() {
        if (!Yii::$app->user->can('news.create')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $model = new News();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['success' => true];
        }
        return ['success' => false];
    }

    public function actionUpdate($id) {
        if (!Yii::$app->user->can('news.update')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['success' => true];
        }
        return ['success' => false];
    }

    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('news.delete')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $success = $this->findModel($id)->delete();
        return ['success' => $success];
    }

    public function actionGetNewsComments() {
        if (!Yii::$app->user->can('news.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $newsId = Yii::$app->request->post('newsId');
        $offset = Yii::$app->request->post('offset');

        $comments = NewsComment::find()
            ->where(['news_id' => $newsId, 'is_deleted' => false])
            ->orderBy('created_at DESC')
            ->limit($this->module->commentsLimit)
            ->offset($offset);

        if ($offset !== null) {
            $comments->offset($offset);
        }
        $comments = $comments->all();

        $allCommentsCount = NewsComment::find()
            ->where(['news_id' => $newsId])
            ->count();
        $allElementsLoaded = (($offset + count($comments)) >= $allCommentsCount) ? true : false;

        return [
            'list' => $comments,
            'allElementsLoaded' => $allElementsLoaded,
        ];
    }

    public function actionAddComment() {
        if (!Yii::$app->user->can('newsComments.create')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $model = new NewsComment();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return [
                'success' => true,
                'model' => $model
            ];
        }
        return ['success' => false];
    }

    public function actionUpdateComment($id) {
        if ($model = NewsComment::findOne($id)) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                if (!$model->isEditable) {
                    throw new ForbiddenHttpException('You are not allowed to perform this action.');
                }
                return [
                    'success' => true,
                    'model' => NewsComment::findOne($id)
                ];
            }
        }
        return ['success' => false];
    }

    public function actionDeleteComment($id) {
        $comment = NewsComment::findOne($id);
        if (!$comment->isDeletable) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $success = $comment->SoftDelete();
        return ['success' => $success];
    }

    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
