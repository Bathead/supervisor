<?php

namespace app\modules\ticket;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\ticket\controllers';
    public $commentsLimit = 10;

    public function init()
    {
        parent::init();
    }

}
