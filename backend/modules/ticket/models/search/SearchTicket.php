<?php

namespace app\modules\ticket\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\ticket\models\Ticket;
use app\modules\user\models\Profile;
use app\modules\user\models\User;

/**
 * SearchTicket represents the model behind the search form about `app\models\Ticket`.
 */
class SearchTicket extends Ticket
{
    public $authorFullName;
    public $receiverFullName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'author_id'], 'integer'],
            [['title', 'description', 'created_at', 'updated_at', 'status', 'priority', 'file_path', 'authorFullName', 'receiverFullName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function searchMy($params)
    {
        $tn_ticket = Ticket::tableName();
        $tn_user = User::tableName();
        $tn_profile = Profile::tableName();

        $currentUserId = Yii::$app->user->id;

        $query = Ticket::find()
            ->from("$tn_ticket as t")
            ->leftJoin("$tn_user as u", 't.receiver_id = u.id')
            ->leftJoin("$tn_profile as p", 'u.profile_id = p.id')
            ->andWhere(['t.author_id'=> $currentUserId]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $params['count'],
                'page' => $params['page'] - 1
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'receiverFullName' => [
                    'asc' => ['p.first_name' => SORT_ASC, 'p.last_name' => SORT_ASC, 'p.middle_name' => SORT_ASC],
                    'desc' => ['p.first_name' => SORT_DESC, 'p.last_name' => SORT_DESC, 'p.middle_name' => SORT_DESC],
                ],
                'created_at' => [
                    'asc' => ['created_at' => SORT_ASC],
                    'desc' => ['created_at' => SORT_DESC],
                ],
            ],
            'defaultOrder' => ['created_at'=>SORT_DESC]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->receiverFullName)) {
            $query->andFilterWhere([
                'or',
                ['like', 'p.first_name', $this->receiverFullName],
                ['like', 'p.last_name', $this->receiverFullName],
                ['like', 'p.middle_name', $this->receiverFullName],
            ]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['status' => $this->status])
            ->andFilterWhere(['priority' => $this->priority])
        ;

        return $dataProvider;
    }

    public function searchReceived($params)
    {
        $tn_ticket = Ticket::tableName();
        $tn_user = User::tableName();
        $tn_profile = Profile::tableName();

        $currentUserId = Yii::$app->user->id;

        $query = Ticket::find()
            ->from("$tn_ticket as t")
            ->leftJoin("$tn_user as u", 't.author_id = u.id')
            ->leftJoin("$tn_profile as p", 'u.profile_id = p.id')
            ->andWhere(['t.receiver_id' => $currentUserId]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $params['count'],
                'page' => $params['page'] - 1
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'authorFullName' => [
                    'asc' => ['p.first_name' => SORT_ASC, 'p.last_name' => SORT_ASC, 'p.middle_name' => SORT_ASC],
                    'desc' => ['p.first_name' => SORT_DESC, 'p.last_name' => SORT_DESC, 'p.middle_name' => SORT_DESC],
                ],
                'created_at' => [
                    'asc' => ['created_at' => SORT_ASC],
                    'desc' => ['created_at' => SORT_DESC],
                ],
            ],
            'defaultOrder' => ['created_at'=>SORT_DESC]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->authorFullName)) {
            $query->andFilterWhere([
                'or',
                ['like', 'p.first_name', $this->authorFullName],
                ['like', 'p.last_name', $this->authorFullName],
                ['like', 'p.middle_name', $this->authorFullName],
            ]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'priority', $this->priority])
        ;

        return $dataProvider;
    }
}
