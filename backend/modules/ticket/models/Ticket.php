<?php

namespace app\modules\ticket\models;

use Yii;
use ReflectionClass;
use yii\helpers\Inflector;
use yii\base\Object;

use app\modules\user\models\User;
use app\modules\user\models\Profile;

/**
 * This is the model class for table "ticket".
 *
 * @property integer $id
 * @property integer $author_id
 * @property string $title
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $priority
 * @property string $file_path
 *
 * @property User $author
 */
class Ticket extends \yii\db\ActiveRecord
{
    public $fileAttached;

    public static function tableName()
    {
        return 'ticket';
    }

    public static $dir = 'uploads/tickets/';

    public function rules()
    {
        return [
            [['author_id', 'receiver_id'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'status', 'priority', 'file_path'], 'string', 'max' => 255],
            [['fileAttached'], 'file', 'skipOnEmpty' => true, 'maxSize' => 5*1024*1024],
        ];
    }

    public function fields()
    {
        $fields = array_merge(parent::fields(), [
            'author',
            'receiver',
            'statusAsObject',
            'priorityAsObject'
        ]);

        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author_id' => 'Author ID',
            'title' => 'Title',
            'description' => 'Сontents',
            'created_at' =>  'Created',
            'updated_at' => 'Updated',
            'status' =>  'Status',
            'priority' => 'Priority',
            'file_path' => 'File Path',
            'fileAttached' => 'Attach File',
            'user.username' => 'Author',
            'user.profile.last_name' => 'Last name',
            'user.profile.first_name' => 'First name',
            'user.profile.middle_name' => 'Middle name',
        ];
    }

    const PRIORITY_VERY_IMPORTANT = 1;
    const PRIORITY_IMPORTANT = 2;
    const PRIORITY_NORMAL = 3;
    const PRIORITY_NO_PRIORITY = 4;

    public static function getTicketPriorities() {
        $result = [];
        $constPrefix = "PRIORITY_";

        // create a reflection class to get constants
        $reflClass = new ReflectionClass(get_called_class());
        $constants = $reflClass->getConstants();

        // check for status constants (e.g., STATUS_PAYED)
        foreach ($constants as $constantName => $constantValue) {

            // add prettified name to dropdown
            if (strpos($constantName, $constPrefix) === 0) {
                $prettyName = str_replace($constPrefix, "", $constantName);
                $prettyName = Inflector::humanize(strtolower($prettyName));
                $result[] = [
                    'id' => $constantValue,
                    'title' => $prettyName
                ];
            }
        }
        return $result;
    }

    public function getPriorityAsString() {
        $statuses = $this->getTicketPriorities();
        return $statuses[$this->priority-1]['title'];
    }

    const STATUS_ACTIVE = 1;
    const STATUS_WAITING_VERIFICATION = 2;
    const STATUS_CLOSED = 3;
    const STATUS_IN_PROGRESS = 4;

    public static function getTicketStatuses() {
        $result = [];
        $constPrefix = "STATUS_";

        // create a reflection class to get constants
        $reflClass = new ReflectionClass(get_called_class());
        $constants = $reflClass->getConstants();

        // check for status constants (e.g., STATUS_PAYED)
        foreach ($constants as $constantName => $constantValue) {

            // add prettified name to dropdown
            if (strpos($constantName, $constPrefix) === 0) {
                $prettyName = str_replace($constPrefix, "", $constantName);
                $prettyName = Inflector::humanize(strtolower($prettyName));
                $result[] = [
                    'id' => $constantValue,
                    'title' => $prettyName
                ];
            }
        }
        return $result;
    }

    public function getStatusAsString() {
        $statuses = $this->getTicketStatuses();
        return $statuses[$this->status-1]['title'];
    }

    public function getReceiver() {
        return $this->hasOne(User::className(), ['id' => 'receiver_id']);
    }

    public function getAuthor() {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    public function getCommentsCount() {
        return TicketComment::find()
            ->where(['ticket_id' => $this->id, 'is_deleted' => false])
            ->count();
    }

    public static function getNewTickets() {
        return self::find()->where(['status' => self::STATUS_ACTIVE])->andWhere(['receiver_id' => Yii::$app->user->id]);
    }

    public static function getNewSentTickets() {
        return self::find()->where(['status' => self::STATUS_WAITING_VERIFICATION])->andWhere(['author_id' => Yii::$app->user->id]);
    }

    public function getStatusAsObject() {
        $labelName = '';
        switch ($this->status){
            case Ticket::STATUS_ACTIVE:
                $labelName = 'success';
                break;
            case Ticket::STATUS_WAITING_VERIFICATION:
                $labelName = 'warning';
                break;
            case Ticket::STATUS_CLOSED:
                $labelName = 'danger';
                break;
            case Ticket::STATUS_IN_PROGRESS:
                $labelName = 'primary';
                break;
        }
        return (object) ['value' => $this->status, 'name' => $this->statusAsString, 'labelName' => $labelName];
    }

    public function getPriorityAsObject() {
        $labelName = '';
        switch ($this->priority){
            case Ticket::PRIORITY_VERY_IMPORTANT:
                $labelName = 'danger';
                break;
            case Ticket::PRIORITY_IMPORTANT:
                $labelName = 'warning';
                break;
            case Ticket::PRIORITY_NORMAL:
                $labelName = 'primary';
                break;
            case Ticket::PRIORITY_NO_PRIORITY:
                $labelName = 'default';
                break;
        }
        return (object) ['value' => $this->priority, 'name' => $this->priorityAsString, 'labelName' => $labelName];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->created_at = date('Y-m-d H:i:s');
                $this->author_id = Yii::$app->user->id;;
                $this->status = (string)Ticket::STATUS_ACTIVE;
            }
            else {
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        } else {
            return false;
        }
    }
}
