<?php
namespace app\modules\ticket\models\upload;

use yii\base\Model;
use yii\web\UploadedFile;
use app\modules\ticket\models\Ticket;

class uploadTicketFileForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $attachedFile;

    public function __construct() {
        parent::__construct();
        $this->attachedFile = UploadedFile::getInstanceByName('file');
    }

    public function rules()
    {
        return [
            [['attachedFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif, jpeg, doc, docx, xls, xlsx, 7z, zip, rar, txt, pdf, html, css, php'],
        ];
    }

    public function upload()
    {
        if ($this->attachedFile === null) {
            return null;
        }
        else{
            if ($this->validate()) {
                if (!file_exists(Ticket::$dir)) {
                    mkdir(Ticket::$dir, 0777, true);
                }
                $fileName = $this->attachedFile->name;
                $this->attachedFile->saveAs(Ticket::$dir . $fileName);
                return $fileName;
            }
            else {
                return false;
            }
        }
    }

    public function getFile()
    {
        return $this->attachedFile;
    }
}