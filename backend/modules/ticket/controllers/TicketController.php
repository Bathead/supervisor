<?php

namespace app\modules\ticket\controllers;

use Yii;
use app\modules\ticket\models\Ticket;
use app\modules\ticket\models\TicketComment;
use app\modules\ticket\models\search\SearchTicket;
use app\modules\user\models\Profile;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;

use yii\web\Response;
use yii\web\UploadedFile;
use app\modules\ticket\models\upload\uploadTicketFileForm;
use yii\filters\AccessControl;
/**
 * TicketController implements the CRUD actions for Ticket model.
 */
class TicketController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionGetSentTickets() {
        if (!Yii::$app->user->can('ticket.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $searchModel = new SearchTicket();
        $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams);
        return [
            'list' => $dataProvider->getModels(),
            'totalCount' => $dataProvider->totalCount
        ];
    }

    public function actionGetReceivedTickets() {
        if (!Yii::$app->user->can('ticket.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $searchModel = new SearchTicket();
        $dataProvider = $searchModel->searchReceived(Yii::$app->request->queryParams);
        return [
            'list' => $dataProvider->getModels(),
            'totalCount' => $dataProvider->totalCount
        ];
    }

    public function actionGetLatestReceivedTickets() {
        if (!Yii::$app->user->can('ticket.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $count = Yii::$app->request->post('count');

        $models = Ticket::find()
            ->where(['receiver_id'=> Yii::$app->user->id])
            ->orderBy([
                'created_at' => SORT_DESC
            ])
            ->limit($count)
            ->all();

        return [
            'list' => $models
        ];
    }

    public function actionGetTicket($id) {
        if (!Yii::$app->user->can('ticket.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        //show ticket only if logged user is author or receiver
        $model = $this->findModel($id);
        if (Yii::$app->user->id !== $model->author_id && Yii::$app->user->id !== $model->receiver_id){
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        return [
            'entity' => Ticket::findOne($id)
        ];
    }

    public function actionGetCountOpenedReceived(){
        if (!Yii::$app->user->can('ticket.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $count = Ticket::find()
            ->where(['receiver_id'=> Yii::$app->user->id, 'status' => Ticket::STATUS_ACTIVE])
            ->count();

        return [
            'count' => $count,
        ];
    }

    public function actionGetCountWaitingWerification(){
        if (!Yii::$app->user->can('ticket.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $count = Ticket::find()
            ->where(['author_id'=> Yii::$app->user->id, 'status' => Ticket::STATUS_WAITING_VERIFICATION])
            ->count();

        return [
            'count' => $count,
        ];
    }


    public function actionChangeStatus($id)
    {
        if ((!Yii::$app->user->can('ticket.update'))) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = $this->findModel($id);
        $data = Yii::$app->request->post();
        $status = $data['status'];
        if ($status === Ticket::STATUS_CLOSED) {
            if ((Yii::$app->user->id !== $model->author_id)) {
                throw new ForbiddenHttpException('You are not allowed to perform this action.');
            }
        } else {
            if ((Yii::$app->user->id !== $model->receiver_id)) {
                throw new ForbiddenHttpException('You are not allowed to perform this action.');
            }
        }
        $model->status = $status;
        $model->save(false);
        return ['success' => true];
    }

    public function actionCreate() {
        if (!Yii::$app->user->can('ticket.create')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $data = json_decode(Yii::$app->request->post('data'), true);
        $model = new Ticket();

        if ($model->load($data)) {
            $isValid = $model->validate();
            if ($isValid) {
                //save file
                $uploadTicketFileForm = new uploadTicketFileForm();
                $fileName = $uploadTicketFileForm->upload();
                if ($fileName === false) {
                    return [
                        'success' => false,
                        'message' => 'File upload error'
                    ];
                }

                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $model->file_path = $fileName;

                    if ($model->save(false)) {
                        //rename file path for save files in folders with name "id" of model
                        $filePath = $model->file_path;
                        $dir = Ticket::$dir;
                        if (!empty($filePath)) {
                            $folder = ($dir . $filePath);
                            mkdir($dir . $model->id, 0777, true);
                            rename($folder, $dir . $model->id . '/' . $filePath);
                        }
                    }

                    $transaction->commit();
                    return ['success' => true];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    return [
                        'success' => false,
                        'message' => 'Save data fail (transaction rollBack)'
                    ];
                }
            }
            return [
                'success' => false,
                'message' => 'Not valid'
            ];

        }
        return [
            'success' => false,
            'message' => "Can't load data from post"
        ];
    }

    public function actionUpdate($id) {
        if (!Yii::$app->user->can('ticket.manage') && !Yii::$app->user->can('ticket.consider')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $data = json_decode(Yii::$app->request->post('data'), true);

        $model = $this->findModel($id);
        $oldFileName = $model->file_path;

        if ($model->load($data)) {
            $model->save(false);
            $isValid = $model->validate();
            if ($isValid) {
                $uploadTicketFileForm = new uploadTicketFileForm();
                $fileName = $uploadTicketFileForm->upload();
                if ($fileName === false) {
                    return [
                        'success' => false,
                        'message' => 'File upload error'
                    ];
                }

                $transaction = Yii::$app->db->beginTransaction();
                try {
                    //if we do not upload new file
                    if ($fileName === null) {
                        return ['success' => true];
                    }

                    //otherwise
                    if ($fileName !== null) {
                        $model->file_path = $fileName;
                    }
                    if ($model->save(false)) {
                        //rename file path for save files in folders with name "id" of model
                        $filePath = $model->file_path;
                        $dir = Ticket::$dir;
                        if (!empty($filePath)) {
                            $folder = ($dir . $filePath);
                            if (!file_exists($dir . $model->id)) {
                                mkdir($dir . $model->id, 0777, true);
                            }
                            rename($folder, $dir . $model->id . '/' . $filePath);
                        }
                    }

                    $transaction->commit();

                    //delete old file
                    if ($fileName !== null) {
                        @unlink(Ticket::$dir . $model->id . '/' . $oldFileName);
                    }
                    return ['success' => true];
                } catch (\Exception $e){
                    $transaction->rollBack();
                    @unlink(Ticket::$dir . $model->id . '/' . $fileName);
                    rmdir(Ticket::$dir . $model->id);
                    return [
                        'success' => false,
                        'message' => 'Save data fail (transaction rollBack)'
                    ];
                }
            }

            return [
                'success' => false,
                'mwssage' => 'Not valid'
            ];
        }
        return [
            'success' => false,
            'message' => "Cant't load data from post"
        ];
    }

    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('ticket.manage') && !Yii::$app->user->can('ticket.consider')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $model = $this->findModel($id);
        //delete file
        if (@unlink(Ticket::$dir . $model->id . '/' . $model->file_path)) {
            $model->file_path = null;
            rmdir(Ticket::$dir . $model->id);
        }

        $success = $model->delete();
        return ['success' => $success];
    }

    public function actionDeleteFile($id)
    {
        $model = $this->findModel($id);
        @unlink(Ticket::$dir . $model->id . '/' . $model->file_path);
        $model->file_path = null;
        $model->save(false);
    }


    public function actionGetTicketComments() {
        if (!Yii::$app->user->can('ticket.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $ticketId = Yii::$app->request->post('ticketId');
        $offset = Yii::$app->request->post('offset');

        $comments = TicketComment::find()
            ->where(['ticket_id' => $ticketId, 'is_deleted' => false])
            ->orderBy('created_at DESC')
            ->limit($this->module->commentsLimit)
            ->offset($offset);

        if ($offset !== null) {
            $comments->offset($offset);
        }
        $comments = $comments->all();

        $allCommentsCount = TicketComment::find()
            ->where(['ticket_id' => $ticketId])
            ->count();
        $allElementsLoaded = (($offset + count($comments)) >= $allCommentsCount) ? true : false;

        return [
            'list' => $comments,
            'allElementsLoaded' => $allElementsLoaded,
        ];
    }

    public function actionAddComment() {
        if (!Yii::$app->user->can('ticketComments.create')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $model = new TicketComment();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return [
                'success' => true,
                'model' => $model
            ];
        }
        return ['success' => false];
    }

    public function actionUpdateComment($id) {
        if ($model = TicketComment::findOne($id)) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                if (!$model->isEditable) {
                    throw new ForbiddenHttpException('You are not allowed to perform this action.');
                }
                return [
                    'success' => true,
                    'model' => TicketComment::findOne($id)
                ];
            }
        }
        return ['success' => false];
    }

    public function actionDeleteComment($id) {
        $comment = TicketComment::findOne($id);
        if (!$comment->isDeletable) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $success = $comment->SoftDelete();
        return ['success' => $success];
    }

    protected function findModel($id)
    {
        if (($model = Ticket::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetTicketStatuses() {
        return [
            'list' => Ticket::getTicketStatuses()
        ];
    }

    public function actionGetTicketPriorities() {
        return [
            'list' => Ticket::getTicketPriorities()
        ];
    }
}
