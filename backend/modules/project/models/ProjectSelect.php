<?php

namespace app\modules\project\models;

use app\modules\user\models\User;
use app\modules\user\models\Profile;
use app\modules\project\models\ProjectDeveloper;
use Yii;

/**
 * This is the model class for table "project_select".
 *
 * @property integer $id
 * @property integer $id_project
 * @property integer $id_user
 * @property string $time
 * @property string $reject_reason
 * @property integer $evaluate
 *
 * @property Project $idProject
 * @property User $idUser
 * 
 * @property Project $project
 * @property User $user
 */
class ProjectSelect extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'project_select';
    }

    public function rules()
    {
        return [
            [['id_project', 'id_user'], 'required'],
            [['id_project', 'id_user', 'evaluate'], 'integer'],
            [['time', 'reject_reason'], 'string', 'max' => 255]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_project' => 'Id Project',
            'id_user' => 'Id User',
            'time' => 'Set time',
            'reject_reason' => 'Specify a reason',
            'evaluate' => 'evaluate',
        ];
    }

    public function fields()
    {
        $fields = array_merge(parent::fields(), [
            'usernameAsString',
            'user',
            'isAssigned'
        ]);
        return $fields;
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    public function getUsernameAsString() {
        return User::find()->where(['id' => $this->id_user])->one()->username;
    }

    public function getIsAssigned()
    {
        $val = $this->hasOne(ProjectDeveloper::className(), ['id_project' => 'id_project', 'id_developer' => 'id_user'])->count();
        if ($val)
            return true;
        return false;
    }
}
