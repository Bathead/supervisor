<?php

namespace app\modules\project\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\project\models\Project;
use app\modules\project\models\ProjectDeveloper;
use app\modules\user\models\User;
use app\modules\user\models\Profile;

use app\modules\technology\models\Technology;
use app\modules\technology\models\ProjectTechnology;

/**
 * ProjectSearch represents the model behind the search form about `app\modules\project\models\Project`.
 */
class SearchProject extends Project
{
    public $technologiesIds;
    public $author;
    public $usernameAsString;
    public $authorFullName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_sales'], 'integer'],
            [['title', 'description', 'payment_method', 'start_date', 'end_date', 'publish_date', 'status', 'client', 'quality', 'client_feedback'], 'safe'],
            [['internal_price', 'external_price'], 'number'],
            [['time_spent', 'payment_sum', 'hours_developers', 'hours_sales', 'hours_pm', 'hours_qa', 'technologiesIds', 'author', 'usernameAsString', 'authorFullName'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function searchByType($params, $type = null, $id = null)
    {
        $tn_project = Project::tableName();
        $tn_technology = Technology::tableName();
        $tn_project_technologies = ProjectTechnology::tableName();
        $tn_user = User::tableName();
        $tn_profile = Profile::tableName();

        $query = Project::find()
            ->from("$tn_project as pr")
            ->leftJoin("$tn_user as u", 'pr.id_sales = u.id')
            ->leftJoin("$tn_profile as p", 'u.profile_id = p.id');

        switch($type) {
            case 'all': {
                $query->andWhere(['<>', 'pr.status', Project::STATUS_IN_ARCHIVE])
                    ->andWhere(['=', 'version', '0'])
                ;
                break;
            }
            case 'allArchived': {
                $query->andWhere(['=', 'pr.status', Project::STATUS_IN_ARCHIVE])
                    ->andWhere(['=', 'version', '0'])
                ;
                break;
            }
            case 'versions': {
                $query->andWhere(['>', 'version', '0'])
                    ->andWhere(['=', 'parent', $id])
                ;
                break;
            }
            case 'my': {
                $currentUserId = Yii::$app->user->id;
                $query->andWhere(['id_sales'=> $currentUserId])
                    ->leftJoin(ProjectDeveloper::tableName() . ' as pd', 'pd.id_developer = ' . $currentUserId)
                    ->where(['pr.id_sales' => $currentUserId])->andWhere(['<>', 'pr.status', Project::STATUS_IN_ARCHIVE])->andWhere(['=', 'version', 0])
                    //->orwhere("(pr.id = pd.id_project) AND (pr.status = ". Project::STATUS_IN_WORK .") AND (version = 0)")
                    ->orwhere("(pr.id = pd.id_project) AND (version = 0)")
                    ->groupBy('pr.id');
                break;
            }
            case 'myArchived': {
                $currentUserId = Yii::$app->user->id;
                $query->andWhere(['id_sales'=> $currentUserId])
                    ->leftJoin(ProjectDeveloper::tableName() . ' as pd', 'pd.id_developer = ' . $currentUserId)
                    ->where(['pr.id_sales' => $currentUserId])->andWhere(['=', 'pr.status', Project::STATUS_IN_ARCHIVE])->andWhere(['=', 'version', 0])
                    ->orWhere('pr.id = ' . 'pd.id_project')->andWhere(['=', 'pr.status', Project::STATUS_IN_ARCHIVE])->andWhere(['=', 'version', 0])
                    ->groupBy('pr.id');
                break;
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $params['count'],
                'page' => $params['page'] - 1
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'authorFullName' => [
                    'asc' => ['p.first_name' => SORT_ASC, 'p.last_name' => SORT_ASC, 'p.middle_name' => SORT_ASC],
                    'desc' => ['p.first_name' => SORT_DESC, 'p.last_name' => SORT_DESC, 'p.middle_name' => SORT_DESC],
                ],
                'publish_date' => [
                    'asc' => ['publish_date' => SORT_ASC],
                    'desc' => ['publish_date' => SORT_DESC],
                ],
            ],
            'defaultOrder' => ['publish_date'=>SORT_DESC]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->authorFullName)) {
            $query->andFilterWhere([
                'or',
                ['like', 'p.first_name', $this->authorFullName],
                ['like', 'p.last_name', $this->authorFullName],
                ['like', 'p.middle_name', $this->authorFullName],
            ]);
        }

        $query->andFilterWhere(['like', 'pr.title', $this->title])
            ->andFilterWhere(['like', 'pr.publish_date', $this->publish_date])
            ->andFilterWhere(['pr.status' => $this->status])
            ->andFilterWhere(['like', 'pr.client', $this->client])
            ->andFilterWhere(['like', 'pr.hours_developers', $this->hours_developers])
            ->andFilterWhere(['like', 'pr.hours_sales', $this->hours_sales])
            ->andFilterWhere(['like', 'pr.hours_pm', $this->hours_pm])
            ->andFilterWhere(['like', 'pr.hours_qa', $this->hours_qa])
            ->andFilterWhere(['like', 'pr.payment_sum', $this->payment_sum])
            ->andFilterWhere(['like', 'pr.payment_method', $this->payment_method])
            ->andFilterWhere(['like', 'pr.start_date', $this->start_date])
            ->andFilterWhere(['like', 'pr.end_date', $this->end_date])
            ->andFilterWhere(['like', 'pr.time_spent', $this->time_spent])
            ->andFilterWhere(['like', 'pr.quality', $this->quality])
            ->andFilterWhere(['like', 'pr.client_feedback', $this->client_feedback])
        ;

        if (!empty($this->technologiesIds)) {
            $query
                ->leftJoin("$tn_project_technologies as pt", 'pr.id = pt.project_id')
                ->andWhere(['in', 'pt.technology_id', $this->technologiesIds]);
        }

        return $dataProvider;
    }
}
