<?php

namespace app\modules\project\models;

use Yii;
use app\modules\user\models\User;

/**
 * This is the model class for table "project_comm".
 *
 * @property integer $id
 * @property integer $project_id
 * @property integer $user_id
 * @property string $message
 * @property integer $parent_id
 * @property string $updated_at
 * @property string $created_at
 * @property integer $is_deleted
 *
 * @property User $user
 * @property Project $project
 */
class ProjectComment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_comm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'user_id', 'parent_id', 'is_deleted'], 'integer'],
            [['message'], 'string'],
            [['updated_at', 'created_at'], 'safe']
        ];
    }

    public function fields()
    {
        $fields = array_merge(parent::fields(), [
            'user',
            'isEditable',
            'isDeletable',
        ]);

        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'user_id' => 'User ID',
            'message' => 'Message',
            'parent_id' => 'Parent ID',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
            'is_deleted' => 'Is Deleted',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->created_at = date('Y-m-d H:i:s');
                $this->user_id = Yii::$app->user->id;
            }
            return true;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    public  function SoftDelete() {
        $this->is_deleted = true;
        return $this->save();
    }

    public function getIsEditable() {
        return (Yii::$app->user->can('projectComments.update') || $this->user_id == Yii::$app->user->id);
    }

    public function getIsDeletable() {
        return (Yii::$app->user->can('projectComments.delete') || $this->user_id == Yii::$app->user->id);
    }
}
