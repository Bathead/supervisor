<?php

namespace app\modules\project\models;

use Yii;
use app\modules\user\models\User;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "project_developer".
 *
 * @property integer $id
 * @property integer $id_project
 * @property integer $id_developer
 *
 * @property User $idDeveloper
 * @property Project $idProject
 */
class ProjectDeveloper extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'project_developer';
    }

    public function rules()
    {
        return [
            [['id_project', 'id_developer'], 'required'],
            [['id_project', 'id_developer', 'currency_id'], 'integer'],
            [['rate'], 'number']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_project' => 'Id Project',
            'id_developer' => 'Developers',
        ];
    }
}
