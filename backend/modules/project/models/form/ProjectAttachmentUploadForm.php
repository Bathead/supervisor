<?php

namespace app\modules\project\models\form;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use app\modules\project\models\Project;

class ProjectAttachmentUploadForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $files;

    public function rules()
    {
        return [
            [['files'], 'file', 'skipOnEmpty' => true, 'maxFiles' => 50],
        ];
    }

    public function __construct() {
        parent::__construct();
        $this->files = UploadedFile::getInstancesByName('files');
    }


    public function upload($projectId)
    {
        if ($this->validate()) {
            if (!empty($this->files)) {
                $dirPath = Project::$attachmentsPath . $projectId;
                FileHelper::removeDirectory($dirPath);
                FileHelper::createDirectory($dirPath, 0777, true);

                foreach ($this->files as $file) {
                    $file->saveAs($dirPath . '/' . $file->baseName . '.' . $file->extension);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public function uploadInExistingDirectory($projectId)
    {
        if ($this->validate()) {
            if (!empty($this->files)) {
                $dirPath = Project::$attachmentsPath . $projectId;

                foreach ($this->files as $file) {
                    $file->saveAs($dirPath . '/' . $file->baseName . '.' . $file->extension);
                }
            }
            return true;
        } else {
            return false;
        }
    }

}