<?php

namespace app\modules\project\models\form;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

class UploadForm extends Model
{
  /**
   * @var UploadedFile[]
   */
  public $files;
  public $fileNames;
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['files'], 'file', 'skipOnEmpty' => true, 'maxFiles' => 10],
    ];
  }
  /**
   * @inheritdoc
   */
  public function upload($dir)
  {
    if ($this->validate()) {
      FileHelper::createDirectory('uploads/' . $dir, 0777);
      foreach ($this->files as $file) {
        $file->saveAs('uploads/' . $dir . '/' . $file->baseName . '.' . $file->extension);
      }
      return true;
    } else {
      return false;
    }
  }
  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'files' => Yii::t('app', 'Files'),
    ];
  }
  /**
   * @inheritdoc
   */
  public function getFilesName()
  {
    $this->fileNames = [];
    foreach ($this->files as $file) {
      $this->fileNames[] = $file->baseName . '.' . $file->extension;
    }
    return $this->fileNames;
  }
}