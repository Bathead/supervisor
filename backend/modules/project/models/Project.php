<?php

namespace app\modules\project\models;

use app\modules\user\models\User;
use Yii;
use ReflectionClass;
use yii\base\Object;
use yii\db\ActiveRecord;
use yii\helpers\Inflector;
use app\modules\technology\models\Technology;
use app\modules\technology\models\ProjectTechnology;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "project".
 *
 * @property integer $id
 * @property integer $id_sales
 * @property string $title
 * @property string $description
 * @property string $payment_method
 * @property string $start_date
 * @property string $end_date
 * @property string $internal_price
 * @property string $external_price
 * @property string $hours_developers
 * @property string $hours_sales
 * @property string $hours_pm
 * @property string $hours_qa
 * @property string $publish_date
 * @property string $status
 * @property int payment_sum
 * @property int time_spent
 *
 * @property User idSales
 * @property Technology[] technologies
 * @property ProjectDeveloper[] projectDevelopers
 *
 * @property string $statusAsString
 * @property string $paymentAsString
 * @property bool|string updated_date
 * @property string $client
 * @property string $quality
 * @property int $parent
 * @property int $version
 */
class Project extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_IN_WORK = 2;
    const STATUS_WITHOUT_ANSWER = 3;
    const STATUS_IN_ARCHIVE = 4;
    const STATUS_COMPLETED = 5;
    const STATUS_READY_TO_START = 6;
    const STATUS_STOPPED = 7;

    const PAYMENT_HOURLY = 1;
    const PAYMENT_FOR_THE_PROJECT = 2;

    const EVALUATE_RATED = 1;
    const EVALUATE_REJECTED = 0;

    public $devs;

    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    public static $dir = 'uploads/projects/';
    public static $attachmentsPath = 'uploads/projects/';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[/*'id_sales',*/ 'title', 'description'/*, 'payment_method', 'status'*/], 'required'],
            [['id_sales', 'dialog_id'], 'integer'],
            [['description', 'client_feedback'], 'string'],
            [['start_date', 'end_date', 'payment_method', 'status'], 'safe'],
            [['internal_price', 'external_price', 'hours_developers', 'hours_sales', 'hours_pm', 'hours_qa', 'payment_sum', 'time_spent'], 'number'],
            [['title', 'client'], 'string', 'max' => 255],
            ['quality', 'double', 'min' => 0, 'max' => 10],
            [['client'], 'default',  'value' => null],
            ['end_date', 'compare', 'compareAttribute' => 'start_date' ? 'start_date' : 0, 'operator' => '>='],
            [['fileAttached'], 'file', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_sales' => 'Sales',
            'title' => 'Title',
            'description' => 'Description',
            'payment_method' => 'Payment method',
            'start_date' => 'Start',
            'end_date' => 'End',
            'internal_price' => 'Internal price',
            'external_price' => 'External price',
            'hours_developers' => 'Number of programmers working hours',
            'hours_sales' => 'Number of sales working hours',
            'hours_pm' => 'Number of PMs working hours',
            'hours_qa' => 'Number of QAs working hours',
            'publish_date' => 'Date',
            'status' => 'Status',
            'file' => 'Attach file',
            'statusAsString' => 'Status',
            'paymentAsString' => 'Payment method',
            'client' => 'Client',
            'payment_sum' => 'Payment sum',
            'time_spent' => 'Time spent',
            'quality' => 'Quality',
            'technologiesIds' => 'Technologies',
            'technologiesAsString' => 'Technologies',
            'user.username' => 'Author of the project',
            'lastComment.message' => 'Last comment',
            'lastComment.fullComment' => 'Last comment',
            'author' => 'Author of the project',
        ];
    }

    public function fields()
    {
        $fields = array_merge(parent::fields(), [
            'author',
            'developers',
            'technologies',
            'fullPathFileAttachments',
            'developersIds',
            'lastCommentMessage',
            'technologiesIds',
            'paymentAsString',
            'developersMarks',

            'statusAsString',

            'statusAsObject'
        ]);
        return $fields;
    }


    public function getFileAttachments() {
        $path = FileHelper::normalizePath(self::$attachmentsPath . $this->id);
        if (file_exists($path)) {
            return FileHelper::findFiles($path);
        }
        return false;
    }

    public function getFullPathFileAttachments() {
        if ($files = $this->fileAttachments) {
            $result = [];
            foreach ($files as $file) {
                $result[] = [
                    'name' => basename($file),
                    'path' => Yii::$app->params['backendPublicDirPath'] . $file
                ];
            }
            return $result;
        }
        return false;
    }

    public function isUserAuthor($userId)
    {
        return $this->id_sales === $userId;
    }

    public function isOwner()
    {
        return $this->id_sales === Yii::$app->user->id;
    }

    public function getDevelopers()
    {
        return $this->hasMany(User::className(), ['id' => 'id_developer'])
            ->viaTable(ProjectDeveloper::tableName(), ['id_project' => 'id']);
    }

    public function getDevelopersMarks()
    {
        return $this->hasMany(ProjectSelect::className(), ['id_project' => 'id']);
    }

    public static function getTitles()
    {
        //finding datas about titles for autocomplete
        $titlesData =
            Project::find()
                ->select('title')
                ->where(['version' => 0])
                ->all();

        $titles = [];
        foreach ($titlesData as $item){
            if (!is_null($item["title"]))
                array_push($titles, $item["title"]);
        }

        return $titles;
    }

    public static function getClients()
    {
        //finding datas about client for autocomplete
        $clientsData =
            Project::find()
                ->select('client')
                ->where(['version' => 0])
                ->all();

        $clients = [];
        foreach ($clientsData as $item){
            if (!is_null($item["client"]))
                array_push($clients, $item["client"]);
        }
        return $clients;
    }

    public function getDescriptions()
    {
        //finding datas about description for autocomplete
        $descriptionsData =
            Project::find()
                ->select('description')
                ->where(['version' => 0])
                ->all();

        $descriptions = [];
        foreach ($descriptionsData as $item){
            array_push($descriptions, $item["description"]);
        }
        return $descriptions;
    }

    public function isOriginalVersion()
    {
        return $this->version == 0;
    }

    public function getVersionsCount(){
        return Project::find()
            ->where(['parent' => $this->id])
            ->count();
    }

    public function getLastVersion(){
        return Project::findOne(['parent' => $this->id, 'version' => $this->versionsCount]);
    }

    public static function getProjectStatuses() {
        $result = [];
        $constPrefix = "STATUS_";

        // create a reflection class to get constants
        $reflClass = new ReflectionClass(get_called_class());
        $constants = $reflClass->getConstants();

        // check for status constants (e.g., STATUS_PAYED)
        foreach ($constants as $constantName => $constantValue) {

            // add prettified name to dropdown
            if (strpos($constantName, $constPrefix) === 0) {
                $prettyName = str_replace($constPrefix, "", $constantName);
                $prettyName = Inflector::humanize(strtolower($prettyName));
                $result[] = [
                    'id' => $constantValue,
                    'title' => $prettyName
                ];
            }
        }
        return $result;
    }

    public function getStatusAsObject() {
        $labelName = '';
        switch ($this->status){
            case Project::STATUS_ACTIVE:
                $labelName = 'success';
                break;
            case Project::STATUS_IN_WORK:
                $labelName = 'primary';
                break;
            case Project::STATUS_WITHOUT_ANSWER:
                $labelName = 'default';
                break;
            case Project::STATUS_IN_ARCHIVE:
                $labelName = 'danger';
                break;
            case Project::STATUS_COMPLETED:
                $labelName = 'brown';
                break;
            case Project::STATUS_READY_TO_START:
                $labelName = 'cyan';
                break;
            case Project::STATUS_STOPPED:
                $labelName = 'warning';
                break;
        }

        return (object) ['value' => $this->status, 'name' => $this->statusAsString, 'labelName' => $labelName];
    }

    public function getStatusAsString() {
        $statuses = $this->getProjectStatuses();
        return $statuses[$this->status-1]['title'];
    }

    public static function getPaymentMethods() {
        $result = [];
        $constPrefix = "PAYMENT_";

        // create a reflection class to get constants
        $reflClass = new ReflectionClass(get_called_class());
        $constants = $reflClass->getConstants();

        // check for status constants (e.g., STATUS_PAYED)
        foreach ($constants as $constantName => $constantValue) {

            // add prettified name to dropdown
            if (strpos($constantName, $constPrefix) === 0) {
                $prettyName = str_replace($constPrefix, "", $constantName);
                $prettyName = Inflector::humanize(strtolower($prettyName));
                $result[] = [
                    'id' => $constantValue,
                    'title' => $prettyName
                ];
            }
        }
        return $result;
    }

    public function getPaymentAsString() {
        $payments = $this->getPaymentMethods();
        return $payments[$this->payment_method-1]['title'];
    }

    public function getTechnologies() {
        return $this->hasMany(Technology::className(), ['id' => 'technology_id'])
            ->viaTable(ProjectTechnology::tableName(), ['project_id' => 'id']);
    }

    public function getTechnologiesIds() {
        $result = [];
        foreach($this->technologies as $technology) {
            $result[] = $technology->id;
        }
        return $result;
    }

    public function setTechnologiesIds($technologiesIds){
        foreach($technologiesIds as $technologyId) {
            $projectTechnology = new ProjectTechnology($this->id, $technologyId);
            $projectTechnology->save();
        }
    }

    public function getDevelopersIds() {
        $result = [];
        foreach($this->developers as $developer) {
            $result[] = $developer->id;
        }
        return $result;
    }

    public function setDevelopersIds($developersIds){
        foreach($developersIds as $developerId) {
            $projectDeveloper = new ProjectDeveloper($this->id, $developerId);
            $projectDeveloper->save();
        }
    }

    public function getAuthor() {
        return $this->hasOne(User::className(), ['id' => 'id_sales']);
    }

    public function getAuthorFullName() {
        return $this->author->fullName;
    }

    public function getLastCommentMessage() {
        $projectId = $this->id;
        if (ProjectComment::find()->where(['project_id' => $projectId])->count() > 0) {
            $comment = ProjectComment::find()
                ->where(['project_id' => $projectId])
                ->orderBy('created_at DESC')
                ->one()->message;
        }
        else {
            $comment = "";
        }
        return $comment;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->publish_date = date('Y-m-d H:i:s');
            }
            $this->updated_date = date('Y-m-d H:i:s');
            return true;
        } else {
            return false;
        }
    }

    public static function getAllUserProjects($userId) {
        return Project::find()
            ->from(Project::tableName() . ' as p')
            ->leftJoin(ProjectDeveloper::tableName() . ' as pd', 'p.id = pd.id_project')
            ->where(['p.id_sales' => $userId])
            ->orWhere(['pd.id_developer' => $userId])
            ->orderBy(['p.publish_date' => SORT_DESC])
            ->all();
    }
}
