<?php

namespace app\modules\project\controllers;

use app\modules\dialog\models\Dialog;
use app\modules\dialog\models\DialogUser;
use app\modules\project\models\ProjectComment;
use app\modules\project\models\ProjectDeveloper;
use app\modules\project\models\ProjectSelect;
use Ratchet\Wamp\Exception;
use Yii;
use app\modules\project\models\Project;
use app\modules\project\models\search\SearchProject;
use app\modules\technology\models\ProjectTechnology;
use app\modules\user\models\User;
use app\modules\user\models\Profile;
use yii\helpers\BaseFileHelper;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;

use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;

use app\modules\project\models\form\ProjectAttachmentUploadForm;

/**
 * ProjectController implements the CRUD actions for Project model.
 */
class ProjectController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'deletefile' => ['post'],
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionGetProjects() {
        if (!Yii::$app->user->can('project.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $searchModel = new SearchProject();
        $dataProvider = $searchModel->searchByType(Yii::$app->request->queryParams, 'all');

        return [
            'list' => $dataProvider->getModels(),
            'totalCount' => $dataProvider->totalCount
        ];
    }

    public function actionGetProjectsArchived() {
        if (!Yii::$app->user->can('project.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $searchModel = new SearchProject();
        $dataProvider = $searchModel->searchByType(Yii::$app->request->queryParams, 'allArchived');

        return [
            'list' => $dataProvider->getModels(),
            'totalCount' => $dataProvider->totalCount
        ];
    }

    public function actionGetProjectsMyArchived() {
        if (!Yii::$app->user->can('project.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $searchModel = new SearchProject();
        $dataProvider = $searchModel->searchByType(Yii::$app->request->queryParams, 'myArchived');

        return [
            'list' => $dataProvider->getModels(),
            'totalCount' => $dataProvider->totalCount
        ];
    }

    public function actionGetProjectsVersions($id) {
        if (!Yii::$app->user->can('project.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $searchModel = new SearchProject();
        $dataProvider = $searchModel->searchByType(Yii::$app->request->queryParams, 'versions', $id);

        return [
            'list' => $dataProvider->getModels(),
            'totalCount' => $dataProvider->totalCount
        ];
    }

    public function actionGetMyProjects() {
        if (!Yii::$app->user->can('project.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $searchModel = new SearchProject();
        $dataProvider = $searchModel->searchByType(Yii::$app->request->queryParams, 'my');

        return [
            'list' => $dataProvider->getModels(),
            'totalCount' => $dataProvider->totalCount
        ];
    }

    public function actionGetProject($id) {
        if (!Yii::$app->user->can('project.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        return [
            'entity' => Project::findOne($id)
        ];
    }

    //for filters
    public function actionGetTitles()
    {
        //finding datas about titles for autocomplete
        return [
            'list' => Project::getTitles()
        ];
    }

    public function actionGetClients()
    {
        //finding datas about client for autocomplete
        return [
            'list' => Project::getClients()
        ];
    }

    public function actionGetInvaluableCount(){
        if (!Yii::$app->user->can('project.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $count = Project::find()
            ->where(['status' => Project::STATUS_ACTIVE])
            ->count();

        return [
            'count' => $count,
        ];
    }

    public function actionGetLatestProjects(){
        if (!Yii::$app->user->can('project.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $count = Yii::$app->request->post('count');

        $models = Project::find()
            ->orderBy([
                'publish_date' => SORT_DESC
            ])
            ->limit($count)
            ->all();

        return [
            'list' => $models
        ];
    }

    public function actionCreate() {
        if (!Yii::$app->user->can('project.create')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $data = json_decode(Yii::$app->request->post('data'), true);

        $model = new Project();
        $model->id_sales = Yii::$app->user->id;
        $model->publish_date = date('Y-m-d H:i:s');
        $model->status = Project::STATUS_ACTIVE;

        $uploadModel = new ProjectAttachmentUploadForm();

        if ($model->load($data)) {
            $model->save(false);

            //save files
            $uploadModel->upload($model->id);

            //save project technologies
            if (!empty($data['Project']['technologiesIds'])) {
                foreach($data['Project']['technologiesIds'] as $technologyId) {
                    $projectTechnology = new ProjectTechnology($model->id, $technologyId);
                    $projectTechnology->save();
                }
            }

            return ['success' => true];
        }
        return ['success' => false];
    }

    //for editing with storage of versions
    public function actionUpdate($id)
    {
        if ((!Yii::$app->user->can('project.update') || !Yii::$app->user->can('project.manage')) && !Yii::$app->user->can('project.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = $this->findModel($id);
        if (!($model->isOriginalVersion())) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $data = json_decode(Yii::$app->request->post('data'), true);

        $newProject = new Project();

        $newProject->attributes = $model->attributes;
        $newProject->publish_date = $model->publish_date;

        $newProject->parent = $model->id;
        $newProject->version = $model->versionsCount + 1;

        $uploadModel = new ProjectAttachmentUploadForm();
        if ($model->load($data)) {

            $newProject->save(false);
            $model->save(false);

            //copy existing files in version
            if (file_exists(Project::$attachmentsPath . $model->id)) {
                BaseFileHelper::copyDirectory(Project::$attachmentsPath . $model->id, Project::$attachmentsPath . $newProject->id);
                //download new files in project
                $uploadModel->uploadInExistingDirectory($model->id);
            } else {
                $uploadModel->upload($model->id);
            }

            
            ProjectTechnology::saveTechnologiesInVersions($model->id, $newProject->id);
            if (!empty($data['Project']['technologiesIds'])) {
                foreach ($data['Project']['technologiesIds'] as $technologyId) {
                    $projectTechnology = new ProjectTechnology($model->id, $technologyId);
                    $projectTechnology->save();
                }
            }
            return ['success' => true];
        }
        return ['success' => false];
    }


    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('project.delete')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = $this->findModel($id);
        $success = $model->delete();

        //delete attached files
        FileHelper::removeDirectory(Project::$attachmentsPath . $id);

        return ['success' => $success];
    }

    public function actionMoveToArchive($id)
    {
        if (!Yii::$app->user->can('project.delete')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = $this->findModel($id);

        if (!($model->isOriginalVersion())) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }

        $model->status = Project::STATUS_IN_ARCHIVE;
        $model->save(false);

        //Yii::$app->db->createCommand()->update(Project::tableName(), ['status' => Project::STATUS_IN_ARCHIVE], "id = $id" )->execute();

        return ['success' => true];
    }

    public function actionRestore($id)
    {
        if (!Yii::$app->user->can('project.delete')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = $this->findModel($id);

        if (!($model->isOriginalVersion())) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }

        $model->status = Project::STATUS_ACTIVE;
        $model->save(false);

//        Yii::$app->db->createCommand()->update(Project::tableName(), ['status' => Project::STATUS_ACTIVE], "id = $id" )->execute();

        return ['success' => true];
    }

    public function actionStart($id)
    {
        if ((!Yii::$app->user->can('project.update') || !Yii::$app->user->can('project.manage')) && !Yii::$app->user->can('project.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = $this->findModel($id);
        $model->status = Project::STATUS_IN_WORK;
        $model->save(false);
        return ['success' => true];
    }


    public function actionChangeStatus($id)
    {
        if ((!Yii::$app->user->can('project.update') || !Yii::$app->user->can('project.manage')) && !Yii::$app->user->can('project.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = $this->findModel($id);
        $data = Yii::$app->request->post();
        $status = $data['status'];
        $model->status = $status;
        $model->save(false);
        return ['success' => true];
    }


    public function actionComplete($id)
    {
        if ((!Yii::$app->user->can('project.update') || !Yii::$app->user->can('project.manage')) && !Yii::$app->user->can('project.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = $this->findModel($id);
        $model->status = Project::STATUS_COMPLETED;
        $model->save(false);
        return ['success' => true];
    }

    public function actionGetDevelopers()
    {
        $tn_users = User::tableName();
        $currentUserId = Yii::$app->user->id;

        $data = User::find()
            ->from(["$tn_users AS u"])
            ->leftJoin("profile AS prof", "u.profile_id = prof.id")
            ->leftJoin("auth_assignment AS aa", "aa.user_id = u.id")
            ->leftJoin("auth_item_child AS aic", "aic.parent = aa.item_name")
            ->andWhere("aic.child = 'user.assignableToProject'")
            ->andWhere("u.id != $currentUserId")
            ->andWhere(['u.status' => User::STATUS_ACTIVE])
            ->all();

        return [
            'list' => $data
        ];
    }

    public function actionGetSales()
    {
        $tn_users = User::tableName();

        $data = (new Query())
            ->select(['u.id', 'u.username', 'prof.last_name', 'prof.first_name'])
            ->from("$tn_users AS u")
            ->leftJoin("profile AS prof", "u.profile_id = prof.id")
            ->leftJoin("auth_assignment AS aa", "aa.user_id = u.id")
            ->leftJoin("auth_item_child AS aic", "aic.parent = aa.item_name")
            ->where(['aic.child' => 'project.create'])
            ->andWhere(['u.status' => User::STATUS_ACTIVE])
            ->all();

        return ['list' => $data];
    }

    public function actionGetSalesAsObject() {
        if (!Yii::$app->user->can('user.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $name = Yii::$app->request->post('name');

        $users = User::find()
            ->from(User::tableName() . ' as u')
            ->leftJoin(Profile::tableName() . ' as p', 'p.id = u.profile_id')
            ->where(['!=' , 'u.id', Yii::$app->user->id])
            ->andFilterWhere([
                'or',
                ['like', 'p.first_name', $name],
                ['like', 'p.last_name', $name]
            ])

            ->leftJoin("auth_assignment AS aa", "aa.user_id = u.id")
            ->leftJoin("auth_item_child AS aic", "aic.parent = aa.item_name")
            ->where(['aic.child' => 'project.create'])
            ->andWhere(['u.status' => User::STATUS_ACTIVE])

            ->all();

        return [
            'list' => $users
        ];


    }

    public function actionGetDeveloperAsObject() {
        if (!Yii::$app->user->can('user.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $name = Yii::$app->request->post('name');
        $currentUserId = Yii::$app->user->id;

        $users = User::find()
            ->from(User::tableName() . ' as u')
            ->leftJoin(Profile::tableName() . ' as prof', 'prof.id = u.profile_id')
            ->leftJoin("auth_assignment AS aa", "aa.user_id = u.id")
            ->leftJoin("auth_item_child AS aic", "aic.parent = aa.item_name")
            ->andWhere("aic.child = 'user.assignableToProject'")
            ->andWhere("u.id != $currentUserId")
            ->andWhere(['u.status' => User::STATUS_ACTIVE])
            ->all();

        return [
            'list' => $users
        ];
    }

    public function actionGetProjectVersion() {

        $id = Yii::$app->request->post('id');
        $version = Yii::$app->request->post('version');

        return [
            'entity' => Project::findOne(['parent' => $id, 'version' => $version])
        ];
    }

    public function actionAddDevelopers($id)
    {
        if (!Yii::$app->user->can('project.update')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $data = json_decode(Yii::$app->request->post('data'), true);
        $model = $this->findModel($id);
        if ($model->load($data)) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                ProjectDeveloper::deleteAll(['id_project' => $model->id]);
                // save developers
                if (!empty($data['Project']['developersFullList'])) {
                    foreach($data['Project']['developersFullList'] as $item) {
                        if ($item['active']) {
                            $pd = new ProjectDeveloper();
                            $pd->id_project = $model->id;
                            $pd->id_developer = $item['id'];
                            $pd->rate = $item['rate'];
                            $pd->currency_id = $item['currency'];
                            $pd->save();
                        }
                    }
                }

                if (is_null($model->dialog_id)) { //create new dialog
                    $query = ProjectDeveloper::find()
                        ->select('id_developer')
                        ->where(['id_project' => $model->id])->all();
                    $ids = array();
                    foreach ($query as $item) {
                        if (!is_null($item["id_developer"]))
                            array_push($ids, $item["id_developer"] * 1);
                    }
                    $dialog_id = Dialog::createDialog($model->title, $ids, $model->title);
                    $model->dialog_id = $dialog_id;
                    $model->save(false);
                } else { //change existing dialog
                    $query = ProjectDeveloper::find()
                        ->select('id_developer')
                        ->where(['id_project' => $model->id])->all();
                    $ids = array();
                    foreach ($query as $item) {
                        if (!is_null($item["id_developer"]))
                            array_push($ids, $item["id_developer"] * 1);
                    }
                    Yii::$app->db->createCommand()->delete(DialogUser::tableName(), "dialog_id = $model->dialog_id")->execute();
                    $dialogUser = new DialogUser(['dialog_id' => $model->dialog_id, 'user_id' => $model->id_sales]); //store sales id in dialog
                    $dialogUser->save(false);
                    if (!empty($ids)) {
                        foreach ($ids as $item) {
                            $dialogUser = new DialogUser(['dialog_id' => $model->dialog_id, 'user_id' => $item]); //store developers ids in dialog
                            $dialogUser->save(false);
                        }
                    }
                }
                $transaction->commit();
            } catch (Exception $e) {
                $transaction->rollBack();
            }
            return ['success' => true];
        }
        return ['success' => false];
    }

    public function actionAddDeveloper()
    {
        if (!Yii::$app->user->can('project.create')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = new ProjectDeveloper();
        //$model->id_user = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['success' => true];
        }

        return ['success' => false];
    }

    public function actionRate()
    {
        if (!Yii::$app->user->can('project.select')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = new ProjectSelect();
        $model->id_user = Yii::$app->user->id;
        $model->evaluate = Project::EVALUATE_RATED;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['success' => true];
        }

        return ['success' => false];
    }

    public function actionReject()
    {
        if (!Yii::$app->user->can('project.select')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = new ProjectSelect();
        $model->id_user = Yii::$app->user->id;
        $model->evaluate = Project::EVALUATE_REJECTED;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['success' => true];
        }

        return ['success' => false];
    }

    public function actionDeleteRate($id)
    {
        if (!Yii::$app->user->can('project.select') && !Yii::$app->user->can('project.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        return ['success' => ProjectSelect::deleteAll(['id' => $id])];
    }

    public function actionAssignDeveloper($id)
    {
        if (!Yii::$app->user->can('project.create')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $valuation = ProjectSelect::findOne($id);
        $id_project = $valuation->id_project;
        $id_user = $valuation->id_user;

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = new ProjectDeveloper(['id_project' => $id_project, 'id_developer' => $id_user]);
            $success = $model->save(false);

            $modelProject = Project::findOne($id_project);

            if (is_null($modelProject->dialog_id)) {
                $query = ProjectDeveloper::find()
                    ->select('id_developer')
                    ->where(['id_project' => $modelProject->id])->all();
                $ids = array();
                foreach ($query as $item) {
                    if (!is_null($item["id_developer"]))
                        array_push($ids, $item["id_developer"] * 1);
                }
                $dialog_id = Dialog::createDialog($modelProject->title, $ids, $modelProject->title);
                $modelProject->dialog_id = $dialog_id;
                $modelProject->save(false);
            } else {
                $query = ProjectDeveloper::find()
                    ->select('id_developer')
                    ->where(['id_project' => $modelProject->id])->all();
                $ids = array();
                foreach ($query as $item) {
                    if (!is_null($item["id_developer"]))
                        array_push($ids, $item["id_developer"] * 1);
                }
                Yii::$app->db->createCommand()->delete(DialogUser::tableName(), "dialog_id = $modelProject->dialog_id")->execute();
                $dialogUser = new DialogUser(['dialog_id' => $modelProject->dialog_id, 'user_id' => $modelProject->id_sales]); //store sales id in dialog
                $dialogUser->save(false);
                if (!empty($ids)) {
                    foreach ($ids as $item) {
                        $dialogUser = new DialogUser(['dialog_id' => $modelProject->dialog_id, 'user_id' => $item]); //store developers ids in dialog
                        $dialogUser->save(false);
                    }
                }
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
        }
        return ['success' => $success];
    }

    public function actionDeassignDeveloper()
    {
        if ((!Yii::$app->user->can('project.update') || !Yii::$app->user->can('project.manage')) && !Yii::$app->user->can('project.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $data = Yii::$app->request->post();
        $id_developer = $data['id_developer'];
        $id_project = $data['id_project'];
        $model = ProjectDeveloper::findOne(['id_project' => $id_project, 'id_developer' => $id_developer]);

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $success = $model->delete();
            $modelProject = Project::findOne($id_project);
            $modelProject->save(false);
            if (is_null($modelProject->dialog_id)) {
                $query = ProjectDeveloper::find()
                    ->select('id_developer')
                    ->where(['id_project' => $modelProject->id])->all();
                $ids = array();
                foreach ($query as $item) {
                    if (!is_null($item["id_developer"]))
                        array_push($ids, $item["id_developer"] * 1);
                }
                $dialog_id = Dialog::createDialog($modelProject->title, $ids, $modelProject->title);
                $modelProject->dialog_id = $dialog_id;
                $modelProject->save(false);
            } else {
                $query = ProjectDeveloper::find()
                    ->select('id_developer')
                    ->where(['id_project' => $modelProject->id])->all();
                $ids = array();
                foreach ($query as $item) {
                    if (!is_null($item["id_developer"]))
                        array_push($ids, $item["id_developer"] * 1);
                }
                Yii::$app->db->createCommand()->delete(DialogUser::tableName(), "dialog_id = $modelProject->dialog_id")->execute();
                $dialogUser = new DialogUser(['dialog_id' => $modelProject->dialog_id, 'user_id' => $modelProject->id_sales]); //store sales id in dialog
                $dialogUser->save(false);
                if (!empty($ids)) {
                    foreach ($ids as $item) {
                        $dialogUser = new DialogUser(['dialog_id' => $modelProject->dialog_id, 'user_id' => $item]); //store developers ids in dialog
                        $dialogUser->save(false);
                    }
                }
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
        }
        return ['success' => $success];
    }

    public function actionGetShowDiscussionButton($id){
        $model = Project::findOne($id);
        $dialog_id = $model->dialog_id;
        $user_id = Yii::$app->user->id;
        $model = DialogUser::find()
            ->where(['dialog_id' => $dialog_id, 'user_id' => $user_id])
            ->all();
        if ($model)
            return ['show' => true];
        return ['show' => false];
    }

    public function actionGetIsValuatedByCurrentUser($id)
    {
        if (!Yii::$app->user->can('project.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $projectId = $id;
        $userId = Yii::$app->user->id;
        $valuations =
            ProjectSelect::find()
                ->where(['id_project' => $projectId])
                ->andWhere(['id_user' => $userId])
                ->all();
        if ($valuations)
            return ['value' => true];
        return ['value' => false];
    }

    public function actionGetProjectComments() {
        if (!Yii::$app->user->can('project.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $projectId = Yii::$app->request->post('projectId');
        $offset = Yii::$app->request->post('offset');

        $comments = ProjectComment::find()
            ->where(['project_id' => $projectId, 'is_deleted' => false])
            ->orderBy('created_at DESC')
            ->limit($this->module->commentsLimit)
            ->offset($offset);

        if ($offset !== null) {
            $comments->offset($offset);
        }
        $comments = $comments->all();

        $allCommentsCount = ProjectComment::find()
            ->where(['project_id' => $projectId])
            ->count();

        $allElementsLoaded = (($offset + count($comments)) >= $allCommentsCount) ? true : false;

        return [
            'list' => $comments,
            'allElementsLoaded' => $allElementsLoaded,
        ];
    }

    public function actionAddComment() {
        if (!Yii::$app->user->can('projectComments.create')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $model = new ProjectComment();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return [
                'success' => true,
                'model' => $model
            ];
        }
        return ['success' => false];
    }

    public function actionUpdateComment($id) {
        if ($model = ProjectComment::findOne($id)) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                if (!$model->isEditable) {
                    throw new ForbiddenHttpException('You are not allowed to perform this action.');
                }
                return [
                    'success' => true,
                    'model' => ProjectComment::findOne($id)
                ];
            }
        }
        return ['success' => false];
    }

    public function actionDeleteComment($id) {
        $comment = ProjectComment::findOne($id);
        if (!$comment->isDeletable) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        return ['success' => $comment->SoftDelete()];
    }


    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetProjectStatuses() {
        return [
            'list' => Project::getProjectStatuses()
        ];
    }

    public function actionGetPaymentMethods() {
        return [
            'list' => Project::getPaymentMethods()
        ];
    }

    public function actionDeleteAttachedFile($id)
    {
        $data = Yii::$app->request->post();
        $name = $data['name'];
        @unlink(Project::$dir . $id . '/' . $name);
    }

    public function actionGetAllUserProjects($id) {
        if (!Yii::$app->user->can('project.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        return [
            'list' => Project::getAllUserProjects($id)
        ];
    }

}