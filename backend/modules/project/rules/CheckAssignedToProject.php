<?php

namespace app\modules\project\rules;

use app\modules\project\models\Project;
use app\modules\project\models\ProjectDeveloper;
use yii\rbac\Rule;

class CheckAssignedToProject extends Rule
{
    public $name = 'isProjectDeveloper';

    public function execute($user, $item, $params)
    {
        return ProjectDeveloper::find()->where([
            'id_project' => $params['projectID'],
            'id_developer' => $user,
        ])->exists() || Project::find()->where(['id_sales' => $user])->exists();
    }
}