<?php

namespace app\modules\project\rules;
use app\modules\project\models\Project;
use Yii;
use yii\rbac\Rule;
use yii\rbac\Item;


class ProjectManageRule extends Rule
{
  public $name = 'isProjectCreator';
  /**
   * @param string|integer $user   the user ID.
   * @param Item           $item   the role or permission that this rule is associated with
   * @param array          $params parameters passed to ManagerInterface::checkAccess().
   *
   * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
   */
  public function execute($user, $item, $params)
  {
    if (isset(Yii::$app->request->queryParams['id'])) {
      $project = Project::findOne(Yii::$app->request->queryParams['id']);
      return (isset($project) and Yii::$app->user->id == $project->id_sales) ? true : false;
    }
    return false;
  }
}
?>