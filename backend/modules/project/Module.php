<?php
/**
 * Created by PhpStorm.
 * User: wp-18
 * Date: 28.12.2015
 * Time: 15:37
 */

namespace app\modules\project;

use Yii;



class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\project\controllers';

    public $projectPerPage = 15;
    public $commentsLimit = 10;

    public function init()
    {
        parent::init();
    }
}