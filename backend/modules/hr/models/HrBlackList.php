<?php

namespace app\modules\hr\models;

use Yii;
use app\modules\user\models\User;

/**
 * This is the model class for table "{{%hr_black_list}}".
 *
 * @property integer $id
 * @property string $email
 * @property string $reason
 * @property string $created_at
 * @property integer $created_by_id
 *
 * @property HrContact|null $contact
 * @property User $createdBy
 * @property string $createdByName
 * @property string $contactInfo
 */
class HrBlackList extends \yii\db\ActiveRecord
{
    const REASON_UNSUBSCRIBED = 'Unsubscribed';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%hr_black_list}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reason'], 'string', 'max' => 255],
            [['email'], 'unique'],
            ['email', 'email'],
            [['email', 'reason'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'email'         => 'Email',
            'reason'        => 'Reason',
            'created_at'    => 'Created at',
            'createdByName' => 'Created by',
            'contactInfo'   => 'Contact info',
        ];
    }

    public function fields()
    {
        $fields = array_merge(parent::fields(), [
            'createdBy'
        ]);
        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(HrContact::className(), ['email' => 'email']);
    }

    public function getContactInfo()
    {
        return ($this->contact === null) ? null : $this->contact->contactInfo;
    }

    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by_id']);
    }

    public function getCreatedByName()
    {
        return ($this->createdBy === null) ? null : $this->createdBy->fullName;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->created_by_id = Yii::$app->user->id;
            }
            return true;
        } else {
            return false;
        }
    }
}
