<?php

namespace app\modules\hr\models;

use Yii;
use app\modules\englishLanguageLevel\models\EnglishLanguageLevel;
use app\modules\technology\models\Technology;
use app\modules\technology\models\HrContactTechnology;
use app\modules\user\models\User;

/**
 * This is the model class for table "hr_contact".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $contacts
 * @property integer $main_technology_id
 * @property string $monthly_rate
 * @property string $hourly_rate
 * @property integer $english_level_id
 * @property string $city
 * @property string $years
 * @property string $attached_resume_path
 * @property string $attached_sg_resume_path
 * @property integer $creator_id
 *
 * @property HrBlackList[] $hrBlackLists
 * @property User $creator
 * @property integer $contactInfo
 * @property EnglishLanguageLevel $englishLevel
 * @property Technology $mainTechnology
 */
class HrContact extends \yii\db\ActiveRecord
{
    public $fileResume, $fileResumeSG;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hr_contact';
    }

    public static $dir = 'uploads/hr/resumes/';
    public static $dir2 = 'uploads/hr/resumessg/';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['main_technology_id', 'english_level_id', 'creator_id'], 'integer'],
            [['monthly_rate', 'hourly_rate', 'years'], 'number'],
            ['email', 'unique'],
            ['email', 'email'],
            [['name', 'contacts', 'city', 'attached_resume_path', 'attached_sg_resume_path'], 'string', 'max' => 255],
            [['fileResume', 'fileResumeSG'], 'file', 'skipOnEmpty' => true],
            ['email', 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name'                    => 'Name',
            'email'                   => 'Email',
            'contacts'                => 'Other Contacts',
            'main_technology_id'      => 'Main Technology',
            'monthly_rate'            => 'Monthly Rate',
            'hourly_rate'             => 'Hourly Rate',
            'english_level_id'        => 'English Level',
            'city'                    => 'City',
            'years'                   => 'Years Experience',
            'attached_resume_path'    => 'Resume',
            'attached_sg_resume_path' => 'Remade Resume SG CV',
            'mainTechnology.name'     => 'Main Technology',
            'mainTechnologyAsString'  => 'Main Technology',
            'englishLevel.name'       => 'English Level',
            'creator.username'        => 'Creator',
            'technologiesAsString'    => 'Additional Technologies',
            'technologiesIds'         => 'Additional Technologies',
            'fileResume'              => 'Resume',
            'fileResumeSG'            => 'Remade Resume SG CV',
            'createdByName'           => 'Created by',
        ];
    }

    public function fields()
    {
        $fields = array_merge(parent::fields(), [
            'mainTechnology',
            'technologiesAsString',
            'technologiesIds',
            'createdByName',
            'englishLevel',
            'creator'
        ]);
        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHrBlackLists()
    {
        return $this->hasOne(HrBlackList::className(), ['email' => 'email']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEnglishLevel()
    {
        return $this->hasOne(EnglishLanguageLevel::className(), ['id' => 'english_level_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainTechnology()
    {
        return $this->hasOne(Technology::className(), ['id' => 'main_technology_id']);
    }



    public function getTechnologies() {
        return $this->hasMany(Technology::className(), ['id' => 'technology_id'])
            ->viaTable(HrContactTechnology::tableName(), ['contact_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getTechnologiesAsString() {
        $result = '';
        foreach ($this->technologies as $technology) {
            $result .= empty($result) ? $technology->name : ', ' . $technology->name;
        }
        return $result;
    }

    /**
     * @return array
     */
    public function getTechnologiesIds() {
        $result = [];
        foreach($this->technologies as $technology) {
            $result[] = $technology->id;
        }
        return $result;
    }

    /**
     * @param $technologiesIds
     */
    public function setTechnologiesIds($technologiesIds){
        foreach($technologiesIds as $technologyId) {
            $hrContactTechnology = new HrContactTechnology($this->id, $technologyId);
            $hrContactTechnology->save();
        }
    }

    public function getContactInfo()
    {
        $city = empty($this->city) ? '' : ', ' . $this->city;
        return "$this->email ({$this->name}{$city})";
    }

    public function getUsernameById($id) {
        //return User::find()->where(['id' => $id])->one()->username;
        return User::find()->where(['id' => $id])->one()->fullname;
    }

    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    public function getCreatedByName()
    {
        return ($this->createdBy === null) ? null : $this->createdBy->fullName;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->creator_id = Yii::$app->user->id;
            }
            return true;
        } else {
            return false;
        }
    }
}
