<?php

namespace app\modules\hr\models;

use Yii;
use app\modules\user\models\User;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "{{%hr_mailing}}".
 *
 * @property integer $id
 * @property string $description
 * @property string $subject
 * @property string $message
 * @property string $from_name
 * @property string $from_email
 * @property string $from
 * @property string $info
 * @property string $created_at
 * @property integer $created_by_id
 *
 * @property User $createdBy
 * @property User $createdByName
 * @property HrMailingContact[] $mailingContacts
 * @property string[] $fileAttachments
 * @property string[] $fullPathFileAttachments
 */
class HrMailing extends \yii\db\ActiveRecord
{
    public static $attachmentsPath = 'uploads/mailingsAttachments/';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%hr_mailing}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'subject', 'message', 'from_email', 'from_name'], 'required'],
            [['description', 'subject', 'message', 'from_email', 'from_name'], 'string'],
            [['created_at'], 'safe'],
            [['from_email'], 'email'],
            [['created_by_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => 'ID',
            'description'     => 'Description',
            'subject'         => 'Mail subject',
            'message'         => 'Message',
            'from_name'       => 'From Name',
            'from_email'      => 'From Email',
            'from'            => 'From',
            'created_at'      => 'Created at',
            'created_by_id'   => 'Created By ID',
            'createdBy'       => 'Created by',
            'createdByName'   => 'Created by',
            'fileAttachments' => 'Attached files',
        ];
    }

    public function fields()
    {
        $fields = array_merge(parent::fields(), [
            'createdBy',
            'info',
            'mailingContacts',
            'fullPathFileAttachments'
        ]);

        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by_id']);
    }

    public function getCreatedByName()
    {
        return ($this->createdBy === null) ? null : $this->createdBy->fullName;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMailingContacts()
    {
        return $this->hasMany(HrMailingContact::className(), ['mailing_id' => 'id']);
    }

    public function getFrom() {
        return "$this->from_name ($this->from_email)";
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->created_by_id = Yii::$app->user->id;
            }
            return true;
        } else {
            return false;
        }
    }

    public function getFileAttachments() {
        $path = FileHelper::normalizePath(self::$attachmentsPath . $this->id);
        if (file_exists($path)) {
            return FileHelper::findFiles($path);
        }
        return false;
    }

    public function getFullPathFileAttachments() {
        if ($files = $this->fileAttachments) {
            $result = [];
            foreach ($files as $file) {
                $result[] = [
                    'name' => basename($file),
                    'path' => Yii::$app->params['backendPublicDirPath'] . $file
                ];
            }
            return $result;
        }
        return false;
    }

    public function sendMessages()
    {
        $emails = [];
        foreach($this->mailingContacts as $c) {
            $emails[] = $c->email;
        }

        $fromEmail = $this->from_email;
        $fromName = $this->from_name;
        $subject = $this->subject;
        $content = $this->message;

        $messages = [];
        foreach ($emails as $email) {
            $tempMessage = Yii::$app->mailer->compose('hrMail', ['content' => $content, 'email' => $email])
                ->setFrom([$fromEmail => $fromName])
                ->setTo($email)
                ->setSubject($subject);

            if ($this->fileAttachments) {
                foreach($this->fileAttachments as $file) {
                    $tempMessage->attach($file);
                }
            }

            $messages[] = $tempMessage;
        }

        return Yii::$app->mailer->sendMultiple($messages);
    }

    public function afterDelete() {
        FileHelper::removeDirectory(self::$attachmentsPath . $this->id);
    }

    public function getInfo() {
        return $this->description . ' (' . $this->created_at . ',' . $this->createdBy->fullName . ')';
    }
}
