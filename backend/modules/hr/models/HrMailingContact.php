<?php

namespace app\modules\hr\models;

use Yii;

/**
 * This is the model class for table "{{%hr_mailing_contact}}".
 *
 * @property integer $mailing_id
 * @property string $email
 *
 * @property HrContact $contact
 * @property HrMailing $mailing
 */
class HrMailingContact extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%hr_mailing_contact}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mailing_id', 'contact_id'], 'required'],
            [['mailing_id', 'contact_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mailing_id' => 'Mailing ID',
            'contact_id' => 'Contact ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(HrContact::className(), ['email' => 'email']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMailing()
    {
        return $this->hasOne(HrMailing::className(), ['id' => 'mailing_id']);
    }
}
