<?php

namespace app\modules\hr\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\hr\models\HrMailing;
use app\modules\user\models\User;
use app\modules\user\models\Profile;

/**
 * searchHrMailing represents the model behind the search form about `app\modules\hr\models\HrMailing`.
 */
class SearchHrMailing extends HrMailing
{
    public $created_by;
    public $from;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['description', 'subject', 'message', 'from', 'created_at', 'created_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $tn_hrMailing = HrMailing::tableName();
        $tn_user = User::tableName();
        $tn_profile = Profile::tableName();

        $query = HrMailing::find()
            ->from("$tn_hrMailing as m")
            ->leftJoin("$tn_user as u", 'm.created_by_id = u.id')
            ->leftJoin("$tn_profile as p", 'u.profile_id = p.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $params['count'],
                'page' => $params['page'] - 1
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'description' => [
                    'asc' => ['m.description' => SORT_ASC],
                    'desc' => ['m.description' => SORT_DESC],
                ],
                'subject' => [
                    'asc' => ['m.subject' => SORT_ASC],
                    'desc' => ['m.subject' => SORT_DESC],
                ],
                'message' => [
                    'asc' => ['m.message' => SORT_ASC],
                    'desc' => ['m.message' => SORT_DESC],
                ],
                'from' => [
                    'asc' => ['m.from_name' => SORT_ASC, 'm.from_email' => SORT_ASC],
                    'desc' => ['m.from_name' => SORT_DESC, 'm.from_email' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'created_at' => [
                    'asc' => ['m.created_at' => SORT_ASC],
                    'desc' => ['m.created_at' => SORT_DESC],
                ],
                'created_by' => [
                    'asc' => ['m.created_by_id' => SORT_ASC],
                    'desc' => ['m.created_by_id' => SORT_DESC],
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'm.description', $this->description])
            ->andFilterWhere(['like', 'm.subject', $this->subject])
            ->andFilterWhere(['like', 'm.message', $this->message])
            ->andFilterWhere(['like', 'm.created_at', $this->created_at]);
        
        if (!empty($this->created_by)) {
            $query->andWhere(['m.created_by_id' => $this->created_by]);
        }

        if (!empty($this->from)) {
            $query->andFilterWhere(['like', 'm.from_email', $this->from])
                ->orFilterWhere(['like', 'm.from_name', $this->from]);
        }

        return $dataProvider;
    }
}
