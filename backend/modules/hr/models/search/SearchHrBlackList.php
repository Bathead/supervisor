<?php

namespace app\modules\hr\models\search;

use app\modules\user\models\User;
use app\modules\user\models\Profile;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\hr\models\HrBlackList;

/**
 * searchHrBlackList represents the model behind the search form about `app\modules\hr\models\HrBlackList`.
 */
class SearchHrBlackList extends HrBlackList
{
    public $createdByName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['email', 'reason', 'created_at', 'createdByName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function search($params)
    {
        $tn_hrBlackList = HrBlackList::tableName();
        $tn_user = User::tableName();
        $tn_profile = Profile::tableName();

        $query = HrBlackList::find()
            ->from("$tn_hrBlackList as bl")
            ->leftJoin("$tn_user as u", 'bl.created_by_id = u.id')
            ->leftJoin("$tn_profile as p", 'u.profile_id = p.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $params['count'],
                'page' => $params['page'] - 1
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'email' => [
                    'asc' => ['bl.email' => SORT_ASC],
                    'desc' => ['bl.email' => SORT_DESC],
                    'label' => 'Email',
                    'default' => SORT_ASC
                ],
                'reason' => [
                    'asc' => ['bl.reason' => SORT_ASC],
                    'desc' => ['bl.reason' => SORT_DESC],
                    'label' => 'Reason',
                    'default' => SORT_ASC
                ],
                'created_at' => [
                    'asc' => ['bl.created_at' => SORT_ASC],
                    'desc' => ['bl.created_at' => SORT_DESC],
                ],
                'created_by_id' => [
                    'asc' => ['p.first_name' => SORT_ASC, 'p.last_name' => SORT_ASC, 'p.middle_name' => SORT_ASC],
                    'desc' => ['p.first_name' => SORT_DESC, 'p.last_name' => SORT_DESC, 'p.middle_name' => SORT_DESC],
                ],
                'createdByName' => [
                    'asc' => ['p.first_name' => SORT_ASC, 'p.last_name' => SORT_ASC, 'p.middle_name' => SORT_ASC],
                    'desc' => ['p.first_name' => SORT_DESC, 'p.last_name' => SORT_DESC, 'p.middle_name' => SORT_DESC],
                ],
            ],
            'defaultOrder' => ['created_at' => SORT_DESC]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'bl.id' => $this->id,
        ]);

        if (!empty($this->createdByName)) {
            $query->andWhere(['bl.created_by_id' => $this->createdByName]);
        }
        $query->andFilterWhere(['like', 'bl.created_by_id', $this->created_by_id]);

        $query->andFilterWhere(['like', 'bl.email', $this->email])
            ->andFilterWhere(['like', 'bl.reason', $this->reason])
            ->andFilterWhere(['like', 'bl.created_at', $this->created_at])
        ;

        return $dataProvider;
    }
}
