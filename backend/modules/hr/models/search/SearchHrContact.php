<?php

namespace app\modules\hr\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\hr\models\HrContact;
use app\modules\technology\models\Technology;
use app\modules\technology\models\HrContactTechnology;
use app\modules\englishLanguageLevel\models\EnglishLanguageLevel;
use app\modules\user\models\User;
use app\modules\user\models\Profile;

/**
 * SearchHrContact represents the model behind the search form about `app\models\HrContact`.
 */
class SearchHrContact extends HrContact
{
    public $technologiesAsString;
    public $mainTechnologyAsString;
    public $createdByName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'main_technology_id', 'english_level_id', 'creator_id'], 'integer'],
            [['name',
                'email',
                'contacts',
                'city',
                'attached_resume_path',
                'attached_sg_resume_path',
                'technologiesAsString',
                'mainTechnologyAsString',
                'createdByName'], 'safe'],
            [['monthly_rate', 'hourly_rate', 'years'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    public function search($params)
    {
        $tn_hr_contact = HrContact::tableName();
        $tn_technology = Technology::tableName();
        $tn_hr_contact_technology = HrContactTechnology::tableName();
        $tn_english_language_levels = EnglishLanguageLevel::tableName();
        $tn_user = User::tableName();
        $tn_profile = Profile::tableName();

        $query = HrContact::find()
            ->from("$tn_hr_contact as hrc");

        $query->leftJoin("$tn_technology as tech", 'hrc.main_technology_id = tech.id');
        $query->leftJoin("$tn_english_language_levels as ell", 'hrc.english_level_id = ell.id');


        $query->leftJoin("$tn_user as u", 'hrc.creator_id = u.id')
            ->leftJoin("$tn_profile as p", 'u.profile_id = p.id');


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $params['count'],
                'page' => $params['page'] - 1
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC],],
                'name' => [
                    'asc' => ['name' => SORT_ASC],
                    'desc' => ['name' => SORT_DESC],],
                'email' => [
                    'asc' => ['email' => SORT_ASC],
                    'desc' => ['email' => SORT_DESC],],
                'contacts' => [
                    'asc' => ['contacts' => SORT_ASC],
                    'desc' => ['contacts' => SORT_DESC],],
                'years' => [
                    'asc' => ['years' => SORT_ASC],
                    'desc' => ['years' => SORT_DESC],],
                'attached_resume_path' => [
                    'asc' => ['attached_resume_path' => SORT_ASC],
                    'desc' => ['attached_resume_path' => SORT_DESC],],
                'attached_sg_resume_path' => [
                    'asc' => ['attached_sg_resume_path' => SORT_ASC],
                    'desc' => ['attached_sg_resume_path' => SORT_DESC],],
                'mainTechnologyAsString' => [
                    'asc' => ['tech.name' => SORT_ASC],
                    'desc' => ['tech.name' => SORT_DESC],],

                'monthly_rate' => [
                    'asc' => ['monthly_rate' => SORT_ASC],
                    'desc' => ['monthly_rate' => SORT_DESC],],
                'hourly_rate' => [
                    'asc' => ['hourly_rate' => SORT_ASC],
                    'desc' => ['hourly_rate' => SORT_DESC],],
                'city' => [
                    'asc' => ['city' => SORT_ASC],
                    'desc' => ['city' => SORT_DESC],],
                'english_level_id' => [
                    'asc' => ['ell.name' => SORT_ASC],
                    'desc' => ['ell.name' => SORT_DESC],],
                'createdByName' => [
                    'asc' => ['p.first_name' => SORT_ASC, 'p.last_name' => SORT_ASC, 'p.middle_name' => SORT_ASC],
                    'desc' => ['p.first_name' => SORT_DESC, 'p.last_name' => SORT_DESC, 'p.middle_name' => SORT_DESC],],
            ],
            'defaultOrder' => ['id' => SORT_DESC]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'main_technology_id' => $this->main_technology_id,
            'monthly_rate' => $this->monthly_rate,
            'hourly_rate' => $this->hourly_rate,
            'english_level_id' => $this->english_level_id,
            'years' => $this->years,
            'creator_id' => $this->creator_id,
        ]);

        $query->andFilterWhere(['like', 'hrc.name', $this->name])
            ->andFilterWhere(['like', 'hrc.email', $this->email])
            ->andFilterWhere(['like', 'contacts', $this->contacts])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'attached_resume_path', $this->attached_resume_path])
            ->andFilterWhere(['like', 'attached_sg_resume_path', $this->attached_sg_resume_path]);

        if (!empty($this->technologiesAsString)) {
            $query
                ->leftJoin("$tn_hr_contact_technology as hrct", 'hrc.id = hrct.contact_id')
                ->leftJoin("$tn_technology as t", 't.id = hrct.technology_id')
                ->andWhere(['t.name' => $this->technologiesAsString]);
        }

        if (!empty($this->mainTechnologyAsString)) {
            $query
                ->andWhere(['hrc.main_technology_id' => $this->mainTechnologyAsString]);
        }

        if (!empty($this->createdByName)) {
            $query->andWhere(['hrc.creator_id' => $this->createdByName]);
        }

        return $dataProvider;
    }
    
}
