<?php
namespace app\modules\hr\models\upload;

use yii\base\Model;
use yii\web\UploadedFile;
use app\modules\hr\models\HrContact;

class uploadHrContactFileForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $resumeFile;
    public $resumeFileSG;

    public function __construct() {
        parent::__construct();
        $this->resumeFile = UploadedFile::getInstanceByName('file');
        $this->resumeFileSG = UploadedFile::getInstanceByName('file2');
    }


    public function rules()
    {
        return [
            [['resumeFile'], 'file', 'skipOnEmpty' => true,  'extensions' => 'png, jpg, gif, jpeg, doc, docx, xls, xlsx, 7z, zip, rar, txt, pdf, html, css, php'],
            [['resumeFileSG'], 'file', 'skipOnEmpty' => true,  'extensions' => 'png, jpg, gif, jpeg, doc, docx, xls, xlsx, 7z, zip, rar, txt, pdf, html, css, php'],
        ];
    }

    public function upload()
    {
        if ($this->resumeFile === null) {
            return null;
        }
        if ($this->validate()) {
            if (!file_exists(HrContact::$dir)) {
                mkdir(HrContact::$dir, 0777, true);
            }
            $fileName = $this->resumeFile->name;
            $this->resumeFile->saveAs(HrContact::$dir . $fileName);
            return $fileName;
        } else {
            return false;
        }
    }

    public function upload2()
    {
        if ($this->resumeFileSG === null) {
            return null;
        }
        if ($this->validate()) {
            if (!file_exists(HrContact::$dir2)) {
                mkdir(HrContact::$dir2, 0777, true);
            }
            $fileName2 = $this->resumeFileSG->name;
            $this->resumeFileSG->saveAs(HrContact::$dir2 . $fileName2);
            return $fileName2;
        } else {
            return false;
        }
    }

    public function upload3()
    {
        if ($this->resumeFile === null && $this->resumeFileSG === null) {
            return null;
        }
        if ($this->validate()) {
            $fileName = null;
            $fileName2 = null;
            if ($this->resumeFile !== null) {
                if (!file_exists(HrContact::$dir)) {
                    mkdir(HrContact::$dir, 0777, true);
                }
                $fileName = $this->resumeFile->name;
                $this->resumeFile->saveAs(HrContact::$dir . $fileName);
            }

            if ($this->resumeFileSG !== null) {
                if (!file_exists(HrContact::$dir2)) {
                    mkdir(HrContact::$dir2, 0777, true);
                }
                $fileName2 = $this->resumeFileSG->name;
                $this->resumeFileSG->saveAs(HrContact::$dir2 . $fileName2);
            }
            return ['file1' => $fileName,
                    'file2' => $fileName2];
        }
    }
}