<?php

namespace app\modules\hr\models\upload;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use app\modules\hr\models\HrMailing;

class UploadMailingAttachmentForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $files;

    public function rules()
    {
        return [
            [['files'], 'file', 'skipOnEmpty' => true, 'maxFiles' => 50],
        ];
    }

    public function attributeLabels()
    {
        return [
            'files' => 'Attached files',
        ];
    }

    public function __construct() {
        parent::__construct();
        $this->files = UploadedFile::getInstancesByName('files');
    }


    public function upload($mailingId)
    {
        if ($this->validate()) {
            if (!empty($this->files)) {
                $dirPath = HrMailing::$attachmentsPath . $mailingId;
                FileHelper::removeDirectory($dirPath);
                FileHelper::createDirectory($dirPath, 0777, true);

                foreach ($this->files as $file) {
                    $file->saveAs($dirPath . '/' . $file->baseName . '.' . $file->extension);
                }
            }
            return true;
        } else {
            return false;
        }
    }
}