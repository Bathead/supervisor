<?php

namespace app\modules\hr;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\hr\controllers';

    public function init()
    {
        parent::init();
    }
}
