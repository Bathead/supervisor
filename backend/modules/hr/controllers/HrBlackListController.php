<?php

namespace app\modules\hr\controllers;

use app\modules\user\models\User;
use Yii;
use app\modules\hr\models\HrBlackList;
use app\modules\hr\models\search\SearchHrBlackList;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;

/**
 * HrBlackListController implements the CRUD actions for HrBlackList model.
 */
class HrBlackListController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionGetHrBlackLists() {
        if (!Yii::$app->user->can('hrBlackList.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $searchModel = new SearchHrBlackList();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return [
            'list' => $dataProvider->getModels(),
            'totalCount' => $dataProvider->totalCount
        ];
    }

    public function actionGetHrBlackList($id) {
        if (!Yii::$app->user->can('hrBlackList.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        return [
            'entity' => HrBlackList::findOne($id)
        ];
    }

    //for filters
    public function actionGetAllUsersForFilter() {
        $usersForFilter = User::find()
            ->from(User::tableName() . ' as u')
            ->rightJoin(HrBlackList::tableName() . ' as bl', 'u.id = bl.created_by_id')
            ->andWhere(['is not', 'bl.created_by_id', null])
            ->andWhere(['u.status' => User::STATUS_ACTIVE])
            ->all();
        return [
            'list' => $usersForFilter//User::find()->all()
        ];
    }

    /**
     * Lists all HrBlackList models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->can('hrBlackList.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $searchModel = new searchHrBlackList();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $usersForFilter = User::find()
            ->from(User::tableName() . ' as u')
            ->rightJoin(HrBlackList::tableName() . ' as bl', 'u.id = bl.created_by_id')
            ->andWhere(['is not', 'bl.created_by_id', null])
            ->andWhere(['u.status' => User::STATUS_ACTIVE])
            ->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'usersForFilter' => $usersForFilter,
        ]);
    }

    /**
     * Displays a single HrBlackList model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!Yii::$app->user->can('hrBlackList.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionCheckHrUniqueField() {
        if (!Yii::$app->user->can('hrBlackList.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $fieldName = Yii::$app->request->post('fieldName');
        $fieldValue = Yii::$app->request->post('fieldValue');

        $count = HrBlackList::find()
            ->where([$fieldName => $fieldValue])
            ->count();
        return [
            'isUnique' => !($count > 0)
        ];
    }



    /**
     * Creates a new HrBlackList model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        if (!Yii::$app->user->can('hrBlackList.create')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = new hrBlackList();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['success' => true];
        }
        return ['success' => false];
    }

    /**
     * Updates an existing HrBlackList model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        if (!Yii::$app->user->can('hrBlackList.update')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['success' => true];
        }
        return ['success' => false];
    }

    /**
     * Deletes an existing HrBlackList model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('hrBlackList.delete')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $success = $this->findModel($id)->delete();
        return ['success' => $success];
    }

    /**
     * Finds the HrBlackList model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return HrBlackList the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HrBlackList::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
