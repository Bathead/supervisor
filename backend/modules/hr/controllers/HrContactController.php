<?php

namespace app\modules\hr\controllers;

use Yii;
use app\modules\hr\models\HrContact;
use app\modules\hr\models\search\SearchHrContact;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\technology\models\HrContactTechnology;
use app\modules\user\models\User;
use yii\web\ForbiddenHttpException;
use app\modules\hr\models\upload\uploadHrContactFileForm;
use yii\filters\AccessControl;

/**
 * HrContactController implements the CRUD actions for HrContact model.
 */
class HrContactController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionGetHrContacts() {
        if (!Yii::$app->user->can('hrContact.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $searchModel = new SearchHrContact();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return [
            'list' => $dataProvider->getModels(),
            'totalCount' => $dataProvider->totalCount
        ];
    }

    public function actionGetHrContact($id) {
        if (!Yii::$app->user->can('hrContact.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        return [
            'entity' => HrContact::findOne($id)
        ];
    }

    public function actionCheckHrUniqueField() {
        if (!Yii::$app->user->can('hrContact.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $fieldName = Yii::$app->request->post('fieldName');
        $fieldValue = Yii::$app->request->post('fieldValue');

        $count = HrContact::find()
            ->where([$fieldName => $fieldValue])
            ->count();
        return [
            'isUnique' => !($count > 0)
        ];
    }

    //for filters
    public function actionGetAllUsersForFilter() {
        $usersForFilter = User::find()
            ->from(User::tableName() . ' as u')
            ->rightJoin(HrContact::tableName() . ' as hrc', 'u.id = hrc.creator_id')
            ->andWhere(['is not', 'hrc.creator_id', null])
            ->andWhere(['u.status' => User::STATUS_ACTIVE])
            ->all();
        return [
            'list' => $usersForFilter//User::find()->all()
        ];
    }

    public function actionCreate()
    {
        if (!Yii::$app->user->can('hrContact.create')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $data = json_decode(Yii::$app->request->post('data'), true);
        $model = new HrContact();
        if ($model->load($data)) {
            //$model->save(false);
            $isValid = $model->validate();
            if ($isValid) {
                //save files
                $uploadHrContactFileForm = new uploadHrContactFileForm();

                $fileNames = $uploadHrContactFileForm->upload3();
                $file1Name = $fileNames['file1'];
                $file2Name = $fileNames['file2'];

                if ($file1Name === false) {
                    return [
                        'success' => false,
                        'message' => 'File upload error'
                    ];
                }
                if ($file2Name === false) {
                    return [
                        'success' => false,
                        'message' => 'File upload error'
                    ];
                }

                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $model->attached_resume_path = $file1Name;
                    $model->attached_sg_resume_path = $file2Name;
                    if ($model->save(false)) {
                        // save user technologies
                        if (!empty($data['HrContact']['technologiesIds'])) {
                            foreach ($data['HrContact']['technologiesIds'] as $technologyId) {
                                $hrContactTechnology = new HrContactTechnology($model->id, $technologyId);
                                $hrContactTechnology->save();
                            }
                        }

                        //rename file path for save files in folders with name "id" of model
                        $filePath = $model->attached_resume_path;
                        $dir = HrContact::$dir;
                        if (!empty($filePath)) {
                            $folder = ($dir . $filePath);
                            mkdir($dir . $model->id, 0777, true);
                            rename($folder, $dir . $model->id . '/' . $filePath);
                        }

                        $filePath2 = $model->attached_sg_resume_path;
                        $dir2 = HrContact::$dir2;
                        if (!empty($filePath2)) {
                            $folder2 = ($dir2 . $filePath2);
                            mkdir($dir2 . $model->id, 0777, true);
                            rename($folder2, $dir2 . $model->id . '/' . $filePath2);
                        }
                    }

                    $transaction->commit();
                    return ['success' => true];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    return [
                        'success' => false,
                        'message' => 'Save data fail (transaction rollBack)'
                    ];
                }
            }
            return [
                'success' => false,
                'message' => 'Not valid'
            ];
        }
        return [
            'success' => false,
            'message' => "Can't load data from post"
        ];
    }

    public function actionUpdate($id) {
        if (!Yii::$app->user->can('hrContact.update')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $data = json_decode(Yii::$app->request->post('data'), true);
        $model = $this->findModel($id);
        $oldFileName1 = $model->attached_resume_path;
        $oldFileName2 = $model->attached_sg_resume_path;

        if ($model->load($data)) {
            $model->save(false);
            HrContactTechnology::deleteAll(['contact_id' => $model->id]);
            if (!empty($data['HrContact']['technologiesIds'])) {
                foreach ($data['HrContact']['technologiesIds'] as $technologyId) {
                    $hrContactTechnology = new HrContactTechnology($model->id, $technologyId);
                    $hrContactTechnology->save();
                }
            }

            $isValid = $model->validate();
            if ($isValid) {
                $uploadHrContactFileForm = new uploadHrContactFileForm();
                $file1Name = $uploadHrContactFileForm->upload();
                $file2Name = $uploadHrContactFileForm->upload2();

                $transaction = Yii::$app->db->beginTransaction();
                try {
                    //if we do not upload both new files
                    if ($file1Name === null && $file2Name === null) {
                        return ['success' => true];
                    }
                    //if we upload first new file
                    if ($file1Name !== null) {
                        $model->attached_resume_path = $file1Name;
                        $model->save(false);
                        if ($model->save(false)) {
                            //rename file path for save files in folders with name "id" of model
                            $filePath = $model->attached_resume_path;
                            $dir = HrContact::$dir;
                            if (!empty($filePath)) {
                                $folder = ($dir . $filePath);
                                if (!file_exists($dir . $model->id)) {
                                    mkdir($dir . $model->id, 0777, true);
                                }
                                rename($folder, $dir . $model->id . '/' . $filePath);
                            }
                        }
                    }
                    //if we upload second new file
                    if ($file2Name !== null) {
                        $model->attached_sg_resume_path = $file2Name;
                        $model->save(false);
                        if ($model->save(false)) {
                            //rename file path for save files in folders with name "id" of model
                            $filePath = $model->attached_sg_resume_path;
                            $dir2 = HrContact::$dir2;
                            if (!empty($filePath)) {
                                $folder = ($dir2 . $filePath);
                                if (!file_exists($dir2 . $model->id)) {
                                    mkdir($dir2 . $model->id, 0777, true);
                                }
                                rename($folder, $dir2 . $model->id . '/' . $filePath);
                            }
                        }
                    }

                    $transaction->commit();

                    //delete old files
                    if ($file1Name !== null) {
                        @unlink(HrContact::$dir . $model->id . '/' . $oldFileName1);
                    }
                    if ($file2Name !== null) {
                        @unlink(HrContact::$dir2 . $model->id . '/' . $oldFileName2);
                    }

                } catch (\Exception $e){
                    $transaction->rollBack();
                    if (file_exists(HrContact::$dir . $model->id . '/' . $file1Name)) {
                        @unlink(HrContact::$dir . $model->id . '/' . $file1Name);
                        rmdir(HrContact::$dir . $model->id);
                    }
                    if (file_exists(HrContact::$dir2 . $model->id . '/' . $file2Name)) {
                        @unlink(HrContact::$dir2 . $model->id . '/' . $file2Name);
                        rmdir(HrContact::$dir2 . $model->id);
                    }
                    return [
                        'success' => false,
                        'message' => 'Save data fail (transaction rollBack)'
                    ];
                }
            }

            return ['success' => true];
        }
        return ['success' => false];
    }

    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('hrContact.delete')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $model = $this->findModel($id);
        //delete files
        if (@unlink(HrContact::$dir . $model->id . '/' . $model->attached_resume_path)) {
            $model->attached_resume_path = null;
            rmdir(HrContact::$dir . $model->id);
        }
        if (@unlink(HrContact::$dir2 . $model->id . '/' . $model->attached_sg_resume_path)) {
            $model->attached_sg_resume_path = null;
            rmdir(HrContact::$dir2 . $model->id);
        }

        $success = $model->delete();
        return ['success' => $success];
    }

    public function actionDeletefile($id)
    {
        $model = $this->findModel($id);
        @unlink(HrContact::$dir . $model->id . '/' . $model->attached_resume_path);
        $model->attached_resume_path = null;
        $model->save(false);
    }

    public function actionDeletefilesg($id)
    {
        $model = $this->findModel($id);
        @unlink(HrContact::$dir2 . $model->id . '/' . $model->attached_sg_resume_path);
        $model->attached_sg_resume_path = null;
        $model->save(false);
    }
    
    protected function findModel($id)
    {
        if (($model = HrContact::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
