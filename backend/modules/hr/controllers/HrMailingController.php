<?php

namespace app\modules\hr\controllers;

use app\modules\hr\models\HrBlackList;
use app\modules\hr\models\HrContact;
use app\modules\hr\models\HrMailingContact;
use app\modules\technology\models\HrContactTechnology;
use app\modules\user\models\User;
use Yii;
use app\modules\hr\models\HrMailing;
use app\modules\hr\models\search\SearchHrMailing;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\validators\EmailValidator;
use yii\web\ForbiddenHttpException;
use app\modules\hr\models\upload\UploadMailingAttachmentForm;
use yii\filters\AccessControl;

/**
 * HrMailingController implements the CRUD actions for HrMailing model.
 */
class HrMailingController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionGetHrMailings() {
        if (!Yii::$app->user->can('hrMailing.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $searchModel = new SearchHrMailing();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return [
            'list' => $dataProvider->getModels(),
            'totalCount' => $dataProvider->totalCount
        ];
    }

    public function actionGetAllMailings() {
        if (!Yii::$app->user->can('hrMailing.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        return [
            'list' => HrMailing::find()->all()
        ];
    }

    public function actionGetHrMailing($id) {
        if (!Yii::$app->user->can('hrMailing.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        return [
            'entity' => $this->findModel($id)
        ];
    }

    public function actionFiltrateContacts() {
        if (!Yii::$app->user->can('hrMailing.create')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $post = Yii::$app->request->post();

        $checkCreatedBy = isset($post['createdByIds']);
        $checkMainTechnology = !empty($post['mainTechnologyId']);
        $checkAdditionalTechnologies = isset($post['additionalTechnologiesIds']) && !empty($post['additionalTechnologiesIds']);
        $checkBlackList = isset($post['exceptBlackListContacts']) ? $post['exceptBlackListContacts'] : true;
        $checkOtherMailings = isset($post['exceptMailingsIds']) && !empty($post['exceptMailingsIds']);

        $contacts = HrContact::find()
            ->from(HrContact::tableName() . ' as c');

        // filtration by creator id
        if ($checkCreatedBy) {
            $contacts->andWhere(['c.creator_id' => $post['createdByIds']]);
        }

        // filtration by main technology
        if ($checkMainTechnology) {
            $contacts->andWhere(['c.main_technology_id' => $post['mainTechnologyId']]);
        }

        // filtration by additional technology
        if ($checkAdditionalTechnologies) {
            $contacts->rightJoin(HrContactTechnology::tableName() . ' as ct', 'c.id = ct.contact_id')
                ->andWhere(['ct.technology_id' => $post['additionalTechnologiesIds']]);
        }

        // except users from black list
        if ($checkBlackList) {
            $blackListEmails = HrBlackList::find()->all();
            $blackListEmails = ArrayHelper::getColumn($blackListEmails, 'email');

            $contacts->leftJoin(HrBlackList::tableName() . ' as bl', 'c.email = bl.email')
                ->andWhere(['bl.email' => null]);
        }

        // except users, who already received letters from other mailings
        if ($checkOtherMailings) {
            $mailingsContactsEmails = HrMailingContact::find()
                ->where(['mailing_id' => $post['exceptMailingsIds']])
                ->all();

            $mailingsContactsEmails = ArrayHelper::getColumn($mailingsContactsEmails, 'email');

            $contacts->andWhere(['NOT IN', 'c.email', $mailingsContactsEmails]);
        }

        $contacts = $contacts->all();
        $contactsEmails = ArrayHelper::getColumn($contacts, 'email');

        // checking other contacts emails (not from database)
        $validOtherEmails = [];
        if (!empty($post['otherEmails'])) {
            $otherEmails = explode("\n", $post['otherEmails']);
            $validator = new EmailValidator();

            foreach ($otherEmails as $email) {
                $email = trim($email);
                if (!empty($email)) {

                    if (!$validator->validate($email)) {
                        continue;
                    }

                    // black list checking
                    if ($checkBlackList && in_array($email, $blackListEmails)) {
                        continue;
                    }

                    // latest mailings checking
                    if ($checkOtherMailings && in_array($email, $mailingsContactsEmails)) {
                        continue;
                    }

                    // check for duplicates with contacts from database, if email passed preliminary inspections
                    if (in_array($email, $contactsEmails)) {
                        continue;
                    }

                    // check for duplicates in previous $validOtherEmails
                    if (in_array($email, $validOtherEmails)) {
                        continue;
                    }

                    $validOtherEmails[] = $email;
                }
            }
        }

        return [
            'contacts' => $contacts,
            'validOtherEmails' => $validOtherEmails,
        ];

    }

    public function actionCreate()
    {
        if (!Yii::$app->user->can('hrMailing.create')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = new HrMailing();
        $uploadModel = new UploadMailingAttachmentForm();
        $data = json_decode(Yii::$app->request->post('data'), true);

        if ($model->load($data) && $model->save()) {

            $uploadModel->upload($model->id);

            // save mailing emails
            foreach ($data['HrMailing']['contactsEmails'] as $email) {
                $mailingContactsData[] = [$model->id, $email];
            }
            Yii::$app->db->createCommand()->batchInsert(HrMailingContact::tableName(), ['mailing_id', 'email'], $mailingContactsData)->execute();

            $model->sendMessages();
            return ['success' => true];
        }
        return ['success' => false];
    }

    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('hrMailing.delete')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $success = $this->findModel($id)->delete();
        return ['success' => $success];
    }

    protected function findModel($id)
    {
        if (($model = HrMailing::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetHrMailingsCreators() {
        if (!Yii::$app->user->can('hrMailing.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $list = User::find()
            ->from(User::tableName() . ' as u')
            ->rightJoin(HrMailing::tableName() . ' as m', 'u.id = m.created_by_id')
            ->andWhere(['u.status' => User::STATUS_ACTIVE])
            ->all();

        return [
            'list' => $list
        ];
    }

    public function actionUnsubscribe($email)
    {
        $email = urldecode($email);

        $blackListModel = new HrBlackList();
        $blackListModel->email = $email;
        $blackListModel->reason = HrBlackList::REASON_UNSUBSCRIBED;
        $success = $blackListModel->save();

        return [
            'success' => $success
        ];
    }
}
