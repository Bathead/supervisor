<?php

namespace app\modules\rbac\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\user\models\Profile;

/**
 * AssignmentSearch represents the model behind the search form about Assignment.
 * 
 * @author Misbahul D Munir <misbahuldmunir@gmail.com>
 * @since 1.0
 */
class SearchAssignment extends Model
{
    public $id;
    public $username;
    public $name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'username', 'name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'name' => 'Name',
        ];
    }

    /**
     * Create data provider for Assignment model.
     * @param  array                        $params
     * @param  \yii\db\ActiveRecord         $class
     * @param  string                       $usernameField
     * @return \yii\data\ActiveDataProvider
     */
    public function search($params, $class, $usernameField)
    {
        $tn_user = $class::tableName();
        $tn_profile = Profile::tableName();

        $query = $class::find()
            ->from("$tn_user as u")
            ->leftJoin("$tn_profile as p", 'p.id = u.profile_id');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $params['count'],
                'page' => $params['page'] - 1
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'username' => [
                    'asc' => ['u.username' => SORT_ASC],
                    'desc' => ['u.username' => SORT_DESC],
                    'label' => 'Username',
                    'default' => SORT_ASC
                ],
                'name' => [
                    'asc' => ['p.first_name' => SORT_ASC, 'p.last_name' => SORT_ASC],
                    'desc' => ['p.first_name' => SORT_DESC, 'p.last_name' => SORT_DESC],
                    'label' => 'Name',
                    'default' => SORT_ASC
                ],
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'u.' . $usernameField, $this->username]);

        if ($this->name !== null) {
            $query->andWhere('p.first_name LIKE "%' . $this->name . '%" ' .
                'OR p.last_name LIKE "%' . $this->name . '%"'
            );
        }

        return $dataProvider;
    }
}
