<?php

namespace app\modules\rbac;

use Yii;

/**
 * GUI manager for RBAC.
 * Original: https://github.com/mdmsoft/yii2-admin
 */
class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();
    }
}