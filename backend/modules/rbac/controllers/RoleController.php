<?php

namespace app\modules\rbac\controllers;

use app\modules\rbac\models\AuthItem;
use app\modules\rbac\models\search\SearchAuthItem;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\rbac\Item;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;

class RoleController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionGetRoles() {
        if (!Yii::$app->user->can('rbac.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $searchModel = new SearchAuthItem(['type' => Item::TYPE_ROLE]);
        $dataProvider = $searchModel->search(Yii::$app->getRequest()->getQueryParams());

        return [
            'list' => $dataProvider->getModels(),
            'totalCount' => $dataProvider->totalCount
        ];
    }

    public function actionGetAllRoles() {
        if (!Yii::$app->user->can('user.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $result = [];
        foreach (Yii::$app->authManager->roles as $key => $value) {
            $result[] = $key;
        }
        return ['list' => $result];
    }

    public function actionGetRole($id) {
        if (!Yii::$app->user->can('rbac.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        return [
            'entity' => $this->findModel($id)
        ];
    }

    /**
     * Creates a new AuthItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return Json
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('rbac.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $model = new AuthItem(null);
        $model->type = Item::TYPE_ROLE;
        if ($model->load(Yii::$app->getRequest()->post()) && $model->save()) {
            return ['success' => true];
        }
        return ['success' => false];
    }

    /**
     * Updates an existing AuthItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param  string $id
     * @return Json
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('rbac.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->getRequest()->post()) && $model->save()) {
            return ['success' => true];
        }
        return ['success' => false];
    }

    /**
     * Deletes an existing AuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param  string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('rbac.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $model = $this->findModel($id);
        $success = Yii::$app->getAuthManager()->remove($model->item);

        return ['success' => $success];
    }

    /**
     * Search role
     * @param string $id
     * @param string $target
     * @param string $term
     * @return Json
     */
    public function actionSearch($id, $target, $term = '')
    {
        if (!Yii::$app->user->can('rbac.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $result = [
            'Roles' => [],
            'Permissions' => [],
            'Routes' => [],
        ];
        $authManager = Yii::$app->authManager;
        if ($target == 'available') {
            $children = array_keys($authManager->getChildren($id));
            $children[] = $id;
            foreach ($authManager->getRoles() as $name => $role) {
                if (in_array($name, $children)) {
                    continue;
                }
                if (empty($term) or strpos($name, $term) !== false) {
                    $result['Roles'][$name] = $name;
                }
            }
            foreach ($authManager->getPermissions() as $name => $role) {
                if (in_array($name, $children)) {
                    continue;
                }
                if (empty($term) or strpos($name, $term) !== false) {
                    $result[$name[0] === '/' ? 'Routes' : 'Permissions'][$name] = $name;
                }
            }
        } else {
            foreach ($authManager->getChildren($id) as $name => $child) {
                if (empty($term) or strpos($name, $term) !== false) {
                    if ($child->type == Item::TYPE_ROLE) {
                        $result['Roles'][$name] = $name;
                    } else {
                        $result[$name[0] === '/' ? 'Routes' : 'Permissions'][$name] = $name;
                    }
                }
            }
        }
        return ['list' => array_filter($result)];
    }

    /**
     * Assign or remove items
     * @param string $id
     * @param string $action
     * @return Json
     */
    public function actionAssign()
    {
        if (!Yii::$app->user->can('rbac.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $post = Yii::$app->getRequest()->post();
        $id = $post['id'];
        $action = $post['action'];
        $roles = $post['roles'];
        $manager = Yii::$app->getAuthManager();
        $parent = $manager->getRole($id);
        $error = [];
        if ($action == 'assign') {
            foreach ($roles as $role) {
                $child = $manager->getRole($role);
                $child = $child ? : $manager->getPermission($role);
                try {
                    $manager->addChild($parent, $child);
                } catch (\Exception $e) {
                    $error[] = $e->getMessage();
                }
            }
        } else {
            foreach ($roles as $role) {
                $child = $manager->getRole($role);
                $child = $child ? : $manager->getPermission($role);
                try {
                    $manager->removeChild($parent, $child);
                } catch (\Exception $e) {
                    $error[] = $e->getMessage();
                }
            }
        }

        return[
            'success' => (count($error) > 0) ? false : true,
        ];
    }

    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param  string        $id
     * @return AuthItem      the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $item = Yii::$app->getAuthManager()->getRole($id);
        if ($item) {
            return new AuthItem($item);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}