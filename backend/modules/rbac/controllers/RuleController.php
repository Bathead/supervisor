<?php

namespace app\modules\rbac\controllers;

use Yii;
use app\modules\rbac\models\BizRule;
use app\modules\rbac\models\AuthItem;
use yii\helpers\Json;
use yii\web\Controller;
use app\modules\rbac\models\search\SearchBizRule;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

class RuleController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionGetRules() {
        if (!Yii::$app->user->can('rbac.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $searchModel = new SearchBizRule();
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return [
            'list' => $dataProvider->getModels(),
            'totalCount' => $dataProvider->totalCount
        ];
    }

    public function actionGetRule($id) {
        if (!Yii::$app->user->can('rbac.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        return [
            'entity' => $this->findModel($id)
        ];
    }

    public function actionGetAllRules() {
        if (!Yii::$app->user->can('rbac.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $rules = Yii::$app->authManager->getRules();
        return [
            'list' => $rules
        ];
    }

    /**
     * Creates a new AuthItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return Json
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('rbac.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = new BizRule(null);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['success' => true];
        }
        return ['success' => false];
    }

    /**
     * Updates an existing AuthItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param  string $id
     * @return Json
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('rbac.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['success' => true];
        }
        return ['success' => false];
    }

    /**
     * Deletes an existing AuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param  string $id
     * @return Json
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('rbac.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $model = $this->findModel($id);
        $success = Yii::$app->authManager->remove($model->item);

        return ['success' => $success];
    }

    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param  string        $id
     * @return AuthItem      the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $item = Yii::$app->authManager->getRule($id);
        if ($item) {
            return new BizRule($item);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
