<?php

namespace app\modules\rbac\controllers;

use app\modules\rbac\models\AuthItem;
use app\modules\rbac\models\search\SearchAuthItem;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\rbac\Item;
use Yii;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

class PermissionController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionGetPermissions() {
        if (!Yii::$app->user->can('rbac.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $searchModel = new SearchAuthItem(['type' => Item::TYPE_PERMISSION]);
        $dataProvider = $searchModel->search(Yii::$app->getRequest()->getQueryParams());

        return [
            'list' => $dataProvider->getModels(),
            'totalCount' => $dataProvider->totalCount
        ];
    }

    public function actionGetPermission($id) {
        if (!Yii::$app->user->can('rbac.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        return [
            'entity' => $this->findModel($id)
        ];
    }

    public function actionCreate()
    {
        if (!Yii::$app->user->can('rbac.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $model = new AuthItem(null);
        $model->type = Item::TYPE_PERMISSION;
        if ($model->load(Yii::$app->getRequest()->post()) && $model->save()) {
            return ['success' => true];
        }
        return ['success' => false];
    }

    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('rbac.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->getRequest()->post()) && $model->save()) {
            return ['success' => true];
        }
        return ['success' => false];
    }

    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('rbac.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $model = $this->findModel($id);
        $success = Yii::$app->getAuthManager()->remove($model->item);
        return ['success' => $success];
    }

    public function actionSearch($id, $target, $term = '')
    {
        if (!Yii::$app->user->can('rbac.all')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        $result = [
            'Permission' => [],
            'Routes' => [],
        ];
        $authManager = Yii::$app->getAuthManager();
        if ($target == 'available') {
            $children = array_keys($authManager->getChildren($id));
            $children[] = $id;
            foreach ($authManager->getPermissions() as $name => $role) {
                if (in_array($name, $children)) {
                    continue;
                }
                if (empty($term) or strpos($name, $term) !== false) {
                    $result[$name[0] === '/' ? 'Routes' : 'Permissions'][$name] = $name;
                }
            }
        } else {
            foreach ($authManager->getChildren($id) as $name => $child) {
                if (empty($term) or strpos($name, $term) !== false) {
                    $result[$name[0] === '/' ? 'Routes' : 'Permissions'][$name] = $name;
                }
            }
        }
        
        return [
            'list' => array_filter($result)
        ];
    }

    public function actionAssign()
    {
        $post = Yii::$app->getRequest()->post();
        $id = $post['id'];
        $action = $post['action'];
        $roles = $post['roles'];
        $manager = Yii::$app->getAuthManager();
        $parent = $manager->getPermission($id);
        $error = [];
        if ($action == 'assign') {
            foreach ($roles as $role) {
                $child = $manager->getPermission($role);
                try {
                    $manager->addChild($parent, $child);
                } catch (\Exception $exc) {
                    $error[] = $exc->getMessage();
                }
            }
        } else {
            foreach ($roles as $role) {
                $child = $manager->getPermission($role);
                try {
                    $manager->removeChild($parent, $child);
                } catch (\Exception $exc) {
                    $error[] = $exc->getMessage();
                }
            }
        }

        return[
            'success' => (count($error) > 0) ? false : true,
        ];
    }

    protected function findModel($id)
    {
        $item = Yii::$app->getAuthManager()->getPermission($id);
        if ($item) {
            return new AuthItem($item);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
