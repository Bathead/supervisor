<?php

namespace app\modules\technology\controllers;

use Yii;
use app\modules\technology\models\Technology;
use app\modules\technology\models\search\SearchTechnology;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;

/**
 * TechnologyController implements the CRUD actions for Technology model.
 */
class TechnologyController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionGetTechnologies() {
        if (!Yii::$app->user->can('technology.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $searchModel = new SearchTechnology();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return [
            'list' => $dataProvider->getModels(),
            'totalCount' => $dataProvider->totalCount
        ];
    }

    public function actionGetAllTechnologies() {
        return [
            'list' => Technology::find()->all()
        ];
    }

    public function actionGetTechnology($id) {
        if (!Yii::$app->user->can('technology.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        return [
            'entity' => $this->findModel($id)
        ];
    }

    public function actionCreate() {
        if (!Yii::$app->user->can('technology.create')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $model = new Technology();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['success' => true];
        }
        return ['success' => false];
    }

    public function actionUpdate($id) {
        if (!Yii::$app->user->can('technology.update')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['success' => true];
        }
        return ['success' => false];
    }

    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('technology.delete')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $success = $this->findModel($id)->delete();
        return ['success' => $success];
    }

    public function actionGetStatistic($id) {
        if (!Yii::$app->user->can('technology.read')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }
        $technology = $this->findModel($id);
        return [
            'employees'                   => $technology->users,
            'technologyPopularityPercent' => $technology->technologyPopularityPercent,
            'averageKnowledgeRating'      => $technology->averageKnowledgeRating,
            'technologyProjectPercent'    => $technology->technologyProjectPercent,
        ];
    }

    protected function findModel($id)
    {
        if (($model = Technology::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
