<?php

namespace app\modules\technology;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\technology\controllers';

    public function init()
    {
        parent::init();
    }
}
