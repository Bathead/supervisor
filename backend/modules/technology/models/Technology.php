<?php
namespace app\modules\technology\models;

use Yii;
use yii\db\ActiveRecord;
use app\modules\user\models\User;
use app\modules\project\models\Project;

/**
 * This is the model class for table "technology".
 *
 * @property integer $id
 * @property string $name
 * @property User[] $users
 * @property integer $technologyPopularityPercent
 * @property integer $averageKnowledgeRating
 * @property integer $technologyProjectPercent
 *
 */
class Technology extends ActiveRecord
{
    public static function tableName()
    {
        return 'technology';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'name' => 'Name',
        ];
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            UserTechnology::deleteAll(['technology_id' => $this->id]);
            return true;
        } else {
            return false;
        }
    }

    public function getUsers() {
        return User::find()
            ->from(User::tableName() . ' as u')
            ->rightJoin(UserTechnology::tableName() . ' as ut', 'u.id = ut.user_id')
            ->where([
                'ut.technology_id' => $this->id,
                'ut.active'        => true,
                'u.status'         => User::STATUS_ACTIVE
            ])
            ->orderBy(['rating' => SORT_DESC])
            ->all();
    }

    public function getProjects() {
        return $this->hasMany(Project::className(), ['id' => 'project_id'])
            ->viaTable(ProjectTechnology::tableName(), ['technology_id' => 'id']);
    }

    public function getTechnologyPopularityPercent() {
        $usersCount = User::find()
            ->from(User::tableName() . ' as u')
            ->rightJoin(UserTechnology::tableName() . ' as ut', 'ut.user_id = u.id')
            ->where([
                'u.status' => User::STATUS_ACTIVE,
                'ut.technology_id' => $this->id,
                'ut.active'        => true
            ])
            ->count();

        $totalUserTechnologiesCount = User::find()
            ->from(User::tableName() . ' as u')
            ->rightJoin(UserTechnology::tableName() . ' as ut', 'ut.user_id = u.id')
            ->where([
                'u.status' => User::STATUS_ACTIVE,
                'ut.active'        => true
            ])
            ->count();

        return round(($usersCount / $totalUserTechnologiesCount) * 100, 2);
    }

    public function getAverageKnowledgeRating() {
        $averageRating = User::find()
            ->from(User::tableName() . ' as u')
            ->rightJoin(UserTechnology::tableName() . ' as ut', 'ut.user_id = u.id')
            ->where([
                'u.status' => User::STATUS_ACTIVE,
                'ut.technology_id' => $this->id,
                'ut.active'        => true
            ])
            ->andWhere(['not', ['ut.rating' => null]])
            ->average('ut.rating');
        return round($averageRating, 2);
    }

    public function getTechnologyProjectPercent() {
        $technologyProjectsCount = Project::find()
            ->from(Project::tableName() . ' as p')
            ->rightJoin(ProjectTechnology::tableName() . ' as pt', 'p.id = pt.project_id')
            ->where(['pt.technology_id' => $this->id])
            ->count();

        $projectTotalCount = Project::find()->count();
        return round(($technologyProjectsCount / $projectTotalCount) * 100, 2);
    }
}