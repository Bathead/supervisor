<?php
namespace app\modules\technology\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_technologies".
 *
 * @property integer $user_id
 * @property string $technology_id
 * @property int $rating
 * @property bool $active
 * @property Technology $technology
 *
 */
class UserTechnology extends ActiveRecord
{
    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */

    public static function tableName()
    {
        return 'user_technologies';
    }

    public function fields()
    {
        $fields = array_merge(parent::fields(), [
            'technology',
        ]);

        return $fields;
    }

    public function getTechnology() {
        return $this->hasOne(Technology::className(), ['id' => 'technology_id']);
    }
}