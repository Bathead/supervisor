<?php

namespace app\modules\technology\models;

use Yii;
use app\modules\hr\models\HrContact;

/**
 * This is the model class for table "hr_contact_technology".
 *
 * @property integer $id
 * @property integer $contact_id
 * @property integer $technology_id
 *
 * @property Technology $technology
 * @property HrContact $contact
 */
class HrContactTechnology extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hr_contact_technology';
    }

    public function __construct($contact_id, $technology_id)
    {
        parent::__construct();
        $this->contact_id = $contact_id;
        $this->technology_id = $technology_id;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contact_id', 'technology_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contact_id' => 'Contact ID',
            'technology_id' => 'Technology ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTechnology()
    {
        return $this->hasOne(Technology::className(), ['id' => 'technology_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(HrContact::className(), ['id' => 'contact_id']);
    }
}
