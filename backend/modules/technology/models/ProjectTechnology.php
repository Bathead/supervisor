<?php
namespace app\modules\technology\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_technologies".
 *
 * @property integer $user_id
 * @property string $technology_id
 *
 */
class ProjectTechnology extends ActiveRecord
{
    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */

    public static function tableName()
    {
        return 'project_technologies';
    }

    /**
     * @param int $project_id
     * @param $technology_id
     */
    public function __construct($project_id, $technology_id)
    {
        parent::__construct();
        $this->project_id = $project_id;
        $this->technology_id = $technology_id;
    }
    public static function saveTechnologiesInVersions($idpr1, $idpr2)
    {
        ProjectTechnology::updateAll(['project_id' => $idpr2], ['like', 'project_id', $idpr1]);
    }
}