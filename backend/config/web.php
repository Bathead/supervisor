<?php

$params = require(__DIR__ . '/params.php');
$config = [
    'id' => 'basic',
    'language' => 'uk-UA',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
    ],
    'timeZone' => 'UTC+2',
    'as access' => [
        'class' => 'app\modules\rbac\components\AccessControl',
        'allowActions' => [
            'user/*',
            'site/*',
            'rank/*',
            'gii/*',
            'debug/*',
            'project/*',
            'technology/*',
            'currency/*',
            'complaint/*',
            'news/*',
            'englishLanguageLevel/*',
            'ticket/*',
            'dialog/*',
            'payment/*',
            'department/*',
            'hr/*',
            'billingCard/*',
            'rbac/*'
        ]
    ],
    'components' => [
        'request' => [
            'enableCsrfValidation'=>false,
            'cookieValidationKey' => 'Bd8hiAJQf02x',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'response' => [
            'format' => yii\web\Response::FORMAT_JSON,
            'charset' => 'UTF-8',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [

                // technology
                'technology/<action:[-\w]+>' => 'technology/technology/<action>',
                'technology/<action:[-\w]+>/<id:\w+>' => 'technology/technology/<action>',
                
                // englishLanguageLevel
                'englishLanguageLevel/<action:[-\w]+>' => 'englishLanguageLevel/english-language-level/<action>',
                'englishLanguageLevel/<action:[-\w]+>/<id:\w+>' => 'englishLanguageLevel/english-language-level/<action>',

                // department
                'department/<action:[-\w]+>' => 'department/department/<action>',
                'department/<action:[-\w]+>/<id:\w+>' => 'department/department/<action>',

                // ticket
                'ticket/<action:[-\w]+>' => 'ticket/ticket/<action>',
                'ticket/<action:[-\w]+>/<id:\w+>' => 'ticket/ticket/<action>',

                // hr
                'hrContact/<action:[-\w]+>' => 'hr/hr-contact/<action>',
                'hrContact/<action:[-\w]+>/<id:\w+>' => 'hr/hr-contact/<action>',

                'hrBlackList/<action:[-\w]+>' => 'hr/hr-black-list/<action>',
                'hrBlackList/<action:[-\w]+>/<id:\w+>' => 'hr/hr-black-list/<action>',

                'hrMailing/<action:[-\w]+>' => 'hr/hr-mailing/<action>',
                'hrMailing/<action:[-\w]+>/<id:\w+>' => 'hr/hr-mailing/<action>',
                'unsubscribe/<email:.{1,}+>' => 'hr/hr-mailing/unsubscribe',

                // project
                'project/<action:[-\w]+>' => 'project/project/<action>',
                'project/<action:[-\w]+>/<id:\w+>' => 'project/project/<action>',

                // currency
                'currency/<action:[-\w]+>' => 'payment/currency/<action>',
                'currency/<action:[-\w]+>/<id:\w+>' => 'payment/currency/<action>',

                // billing card
                'billingCard/<action:[-\w]+>' => 'payment/billing-card/<action>',
                'billingCard/<action:[-\w]+>/<id:\w+>' => 'payment/billing-card/<action>',

                // payment
                'payment/<action:[-\w]+>' => 'payment/payment/<action>',
                'payment/<action:[-\w]+>/<id:\w+>' => 'payment/payment/<action>',

                // user
                'auth/<action:[-\w]+>' => 'user/auth/<action>',
                'user/<action:[-\w]+>' => 'user/user/<action>',
                'user/<action:[-\w]+>/<id:\w+>' => 'user/user/<action>',
                'rank/<action:[-\w]+>' => 'user/rank/<action>',
                'rank/<action:[-\w]+>/<id:\w+>' => 'user/rank/<action>',

                // rbac
                'role/<action:[-\w]+>' => 'rbac/role/<action>',
                'role/<action:[-\w]+>/<id:\w+>' => 'rbac/role/<action>',

                // complaint
                'complaint/<action:[-\w]+>' => 'complaint/complaint/<action>',
                'complaint/<action:[-\w]+>/<id:\w+>' => 'complaint/complaint/<action>',

                // news
                'news/<action:[-\w]+>' => 'news/news/<action>',
                'news/<action:[-\w]+>/<id:\w+>' => 'news/news/<action>',

                // dialog
                'dialog/<id:\d+>' => 'dialog/dialog/index',
                'dialog/<action:[-\w]+>' => 'dialog/dialog/<action>',
                'dialog/<action:[-\w]+>/<id:\w+>' => 'dialog/dialog/<action>',

                // rbac
                'rbac/<controller>/<action:[-\w]+>' => 'rbac/<controller>/<action>',
                'rbac/<controller>/<action:[-\w]+>/<id:[\.\w]+>' => 'rbac/<controller>/<action>',
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'app\modules\user\components\User',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'sourceLanguage' => 'en-US',
                ],
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'username',
                'password' => 'password',
                'port' => '587',
                'encryption' => 'tls',
            ],
            'useFileTransport' => true,
            'messageConfig' => [
                'from' => ['admin@supervisor.com' => 'Admin'],
                'charset' => 'UTF-8',
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'forceCopy' => true,
        ],
    ],
    'modules' => [
        'user' => [
            'class' => 'app\modules\user\Module',
        ],
        'project' => [
            'class' => 'app\modules\project\Module',
        ],
        'rbac' => [
            'class' => 'app\modules\rbac\Module',
        ],
        'technology' => [
            'class' => 'app\modules\technology\Module',
        ],
        'englishLanguageLevel' => [
            'class' => 'app\modules\englishLanguageLevel\Module',
        ],
        'complaint' => [
            'class' => 'app\modules\complaint\Module',
        ],
        'news' => [
            'class' => 'app\modules\news\Module',
        ],
        'ticket' => [
            'class' => 'app\modules\ticket\Module',
        ],
        'dialog' => [
            'class' => 'app\modules\dialog\Module',
        ],
        'payment' => [
            'class' => 'app\modules\payment\Module',
        ],
        'department' => [
            'class' => 'app\modules\department\Module',
        ],
        'hr' => [
            'class' => 'app\modules\hr\Module',
        ],
    ],
    'params' => $params,
];

$config = \yii\helpers\ArrayHelper::merge($config, require(__DIR__ . '/db.php'));

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['*.*.*.*'],
    ]; 
}

return $config;
