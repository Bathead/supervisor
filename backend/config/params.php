<?php

return [
    'adminEmail' => 'admin@example.com',
    'comment.editPeriod' => 60 * 15,
    'imageTypes' => ['png', 'jpg', 'jpeg', 'gif'],
    'discussion.editPeriod' => 60 * 15,
    'discussion.attachmentSavePath' => '@webroot/uploads/discussion/',
    'discussion.attachmentWebPath' => '@web/uploads/discussion/',
    'online.User' => 60, // 1 minute
    'backendPublicDirPath' => 'backend/web/'
];
