Supervisor project
============================

Supervisor - accounting system for projects, personnel, payments and other with additional possibilities.

  - [Requirements](#markdown-header-requirements)
  - [Used technologies](#markdown-header-used-technologies)
  - [Installation](#markdown-header-installation)
  - [Installation on production](#markdown-header-installation-on-production)
  - [Directory structure](#markdown-header-directory-structure)
  - [Test accounts](#markdown-header-test-accounts)

### Requirements

  - PHP 5.4.0
  - MySQL
  - Redis

### Used technologies

  - Yii2
  - AngularJs 1.5.0
  - Html, Css (Bootstrap 4)
  - Mysql, Redis

### Installation

Clone project to your web server directory.

Install NPM/Bower Dependency Manager for Composer (if not installed).
~~~
php composer.phar global require "fxp/composer-asset-plugin:~1.1.1"
~~~

Update dependencies.
~~~
php composer.phar update
~~~

Set database connection and redis settings in `backend/config/db.php`

Apply migrations.
~~~
php yii migrate
~~~

Update MySQL config file `my.cnf`. Set `wait_timeout` as more as possible (need for websocket server).

Run ratchet websocket server for dialogs.
~~~
php yii wsserver/run
~~~

Also you can run sever in background (but there is not posibility to stop server, without web server restart).
~~~
php yii wsserver/runbg
~~~

Set mail server settings in `backend/config/web.php`.
```php
'mailer' => [
           'class' => 'yii\swiftmailer\Mailer',
           'transport' => [
               'class' => 'Swift_SmtpTransport',
               'host' => 'smtp.gmail.com',
               'username' => 'username',
               'password' => 'password',
               'port' => '587',
               'encryption' => 'tls',
           ],
           'useFileTransport' => true,
           'messageConfig' => [
               'from' => ['admin@supervisor.com' => 'Admin'],
               'charset' => 'UTF-8',
           ]
        ],
```

Set 755 permissions for `backend/web/uploads` directory.

### Installation on production

Change next variables in `backend/web/index.php` and `yii` files :

```php
defined('YII_DEBUG') or define('YII_DEBUG', false);
defined('YII_ENV') or define('YII_ENV', 'prod');
```

### Directory structure

      backend/            yii2 based backend
      frontend/           angular based frontend (js, css, views)
      docs/               contains project documentation
      
### Test accounts

      Login:              Password:
      sup                 asdf
      sysadmin            asdf
      sales               asdf
      dev                 asdf
      pm                  asdf
      hr                  asdf
      qa                  asdf

