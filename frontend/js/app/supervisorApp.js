// create supervisor application
var svApp = angular.module('svApp', [
    'ngRoute',
    'ngAnimate',
    'ngTable',
    'ngCookies',
    'ui.bootstrap',
    'checklist-model',
    'pascalprecht.translate',
    'ckeditor',
    'ngSanitize',
    'angular-loading-bar',
    'rt.debounce',
    'jkAngularRatingStars'
]);

svApp.config(function($routeProvider, $locationProvider, $httpProvider, $translateProvider, cfpLoadingBarProvider, CONFIG) {
    cfpLoadingBarProvider.includeSpinner = true;
    cfpLoadingBarProvider.includeBar = true;

    $routeProvider

        .when('/', {
            templateUrl : 'views/modules/user/user/home.html',
            controller  : 'homeController as home'
        })

        // technology module url settings

        .when('/technologies', {
            templateUrl : 'views/modules/technology/technology/list.html',
            controller  : 'technologyListController as technology'
        })

        .when('/technology/create', {
            templateUrl : 'views/modules/technology/technology/form.html',
            controller  : 'technologyCreateController as technology'
        })

        .when('/technology/:id', {
            templateUrl : 'views/modules/technology/technology/view.html',
            controller  : 'technologyViewController as technology'
        })

        .when('/technology/:id/update', {
            templateUrl : 'views/modules/technology/technology/form.html',
            controller  : 'technologyUpdateController as technology'
        })

        // englishLanguageLevel module url settings

        .when('/englishLanguageLevels', {
            templateUrl : 'views/modules/englishLanguageLevel/englishLanguageLevel/list.html',
            controller  : 'englishLanguageLevelListController as englishLanguageLevel'
        })

        .when('/englishLanguageLevel/create', {
            templateUrl : 'views/modules/englishLanguageLevel/englishLanguageLevel/form.html',
            controller  : 'englishLanguageLevelCreateController as englishLanguageLevel'
        })

        .when('/englishLanguageLevel/:id', {
            templateUrl : 'views/modules/englishLanguageLevel/englishLanguageLevel/view.html',
            controller  : 'englishLanguageLevelViewController as englishLanguageLevel'
        })

        .when('/englishLanguageLevel/:id/update', {
            templateUrl : 'views/modules/englishLanguageLevel/englishLanguageLevel/form.html',
            controller  : 'englishLanguageLevelUpdateController as englishLanguageLevel'
        })

        // department module url settings

        .when('/departments', {
            templateUrl : 'views/modules/department/department/list.html',
            controller  : 'departmentListController as department'
        })

        .when('/department/create', {
            templateUrl : 'views/modules/department/department/form.html',
            controller  : 'departmentCreateController as department'
        })

        .when('/department/:id', {
            templateUrl : 'views/modules/department/department/view.html',
            controller  : 'departmentViewController as department'
        })

        .when('/department/:id/update', {
            templateUrl : 'views/modules/department/department/form.html',
            controller  : 'departmentUpdateController as department'
        })

        // ticket module url settings
        .when('/sentTickets', {
            templateUrl : 'views/modules/ticket/ticket/sentList.html',
            controller  : 'ticketSentListController as ticket'
        })

        .when('/receivedTickets', {
            templateUrl : 'views/modules/ticket/ticket/receivedList.html',
            controller  : 'ticketReceivedListController as ticket'
        })

        .when('/ticket/create', {
            templateUrl : 'views/modules/ticket/ticket/form.html',
            controller  : 'ticketCreateController as ticket'
        })

        .when('/ticket/:id', {
            templateUrl : 'views/modules/ticket/ticket/view.html',
            controller  : 'ticketViewController as ticket'
        })

        .when('/ticket/:id/update', {
            templateUrl : 'views/modules/ticket/ticket/form.html',
            controller  : 'ticketUpdateController as ticket'
        })

        // hr module url settings

        .when('/hrContacts', {
            templateUrl : 'views/modules/hr/hrContact/list.html',
            controller  : 'hrContactListController as hrContact'
        })

        .when('/hrContact/create', {
            templateUrl : 'views/modules/hr/hrContact/form.html',
            controller  : 'hrContactCreateController as hrContact'
        })

        .when('/hrContact/:id', {
            templateUrl : 'views/modules/hr/hrContact/view.html',
            controller  : 'hrContactViewController as hrContact'
        })

        .when('/hrContact/:id/update', {
            templateUrl : 'views/modules/hr/hrContact/form.html',
            controller  : 'hrContactUpdateController as hrContact'
        })

        .when('/hrBlackLists', {
            templateUrl : 'views/modules/hr/hrBlackList/list.html',
            controller  : 'hrBlackListController as hrBlackList'
        })

        .when('/hrBlackList/create', {
            templateUrl : 'views/modules/hr/hrBlackList/form.html',
            controller  : 'hrBlackListCreateController as hrBlackList'
        })

        .when('/hrBlackList/:id', {
            templateUrl : 'views/modules/hr/hrBlackList/view.html',
            controller  : 'hrBlackListViewController as hrBlackList'
        })

        .when('/hrBlackList/:id/update', {
            templateUrl : 'views/modules/hr/hrBlackList/form.html',
            controller  : 'hrBlackListUpdateController as hrBlackList'
        })

        .when('/hrMailings', {
            templateUrl : 'views/modules/hr/hrMailing/list.html',
            controller  : 'hrMailingListController as hrMailing'
        })

        .when('/hrMailing/create', {
            templateUrl : 'views/modules/hr/hrMailing/form.html',
            controller  : 'hrMailingCreateController as hrMailing'
        })

        .when('/hrMailing/:id', {
            templateUrl : 'views/modules/hr/hrMailing/view.html',
            controller  : 'hrMailingViewController as hrMailing'
        })

        // project module url settings

        .when('/projects', {
            templateUrl : 'views/modules/project/project/list.html',
            controller  : 'projectListController as project'
        })

        .when('/myProjects', {
            templateUrl : 'views/modules/project/project/mylist.html',
            controller  : 'projectMyListController as project'
        })

        .when('/myProjects/archived', {
            templateUrl : 'views/modules/project/project/archived.html',
            controller  : 'projectMyArchivedController as project'
        })

        .when('/project/create', {
            templateUrl : 'views/modules/project/project/form.html',
            controller  : 'projectCreateController as project'
        })

        .when('/project/:id', {
            templateUrl : 'views/modules/project/project/view.html',
            controller  : 'projectViewController as project'
        })

        .when('/project/:id/update', {
            templateUrl : 'views/modules/project/project/form.html',
            controller  : 'projectUpdateController as project'
        })

        .when('/project/:id/versions', {
            templateUrl : 'views/modules/project/project/versions.html',
            controller  : 'projectVersionsController as project'
        })

        .when('/projects/archived', {
            templateUrl : 'views/modules/project/project/archived.html',
            controller  : 'projectArchivedController as project'
        })

        .when('/project/:id/version/:version', {
            templateUrl : 'views/modules/project/project/viewVersion.html',
            controller  : 'projectViewVersionController as project'
        })

        // billing card submodule url settings

        .when('/billingCards', {
            templateUrl : 'views/modules/payment/billingCard/list.html',
            controller  : 'billingCardListController as billingCard'
        })

        .when('/billingCard/create', {
            templateUrl : 'views/modules/payment/billingCard/form.html',
            controller  : 'billingCardCreateController as billingCard'
        })

        .when('/billingCard/:id', {
            templateUrl : 'views/modules/payment/billingCard/view.html',
            controller  : 'billingCardViewController as billingCard'
        })

        .when('/billingCard/:id/update', {
            templateUrl : 'views/modules/payment/billingCard/form.html',
            controller  : 'billingCardUpdateController as billingCard'
        })

        // currency submodule url settings

        .when('/currencies', {
            templateUrl : 'views/modules/payment/currency/list.html',
            controller  : 'currencyListController as currency'
        })

        .when('/currency/create', {
            templateUrl : 'views/modules/payment/currency/form.html',
            controller  : 'currencyCreateController as currency'
        })

        .when('/currency/:id', {
            templateUrl : 'views/modules/payment/currency/view.html',
            controller  : 'currencyViewController as currency'
        })

        .when('/currency/:id/update', {
            templateUrl : 'views/modules/payment/currency/form.html',
            controller  : 'currencyUpdateController as currency'
        })

        // payment submodule url settings

        .when('/payments', {
            templateUrl : 'views/modules/payment/payment/list.html',
            controller  : 'paymentListController as payment'
        })

        .when('/payment/create', {
            templateUrl : 'views/modules/payment/payment/form.html',
            controller  : 'paymentCreateController as payment'
        })

        .when('/payment/createList', {
            templateUrl : 'views/modules/payment/payment/formTable.html',
            controller  : 'paymentCreateListController as payment'
        })

        .when('/payment/updateList', {
            templateUrl : 'views/modules/payment/payment/formUpdateList.html',
            controller  : 'paymentUpdateListController as payment'
        })

        .when('/payment/:id/versions', {
            templateUrl : 'views/modules/payment/payment/versions.html',
            controller  : 'paymentVersionsController as payment'
        })

        .when('/payment/:id', {
            templateUrl : 'views/modules/payment/payment/view.html',
            controller  : 'paymentViewController as payment'
        })

        .when('/payment/:id/version/:version', {
            templateUrl : 'views/modules/payment/payment/viewVersion.html',
            controller  : 'paymentViewVersionController as payment'
        })

        //.when('/payment/:id/edit', {
        //    templateUrl : 'views/modules/payment/payment/form.html',
        //    controller  : 'paymentEditController as payment'
        //})

        .when('/payment/:id/update', {
            templateUrl : 'views/modules/payment/payment/form.html',
            controller  : 'paymentUpdateController as payment'
        })

        // user module url settings
        .when('/login', {
            templateUrl : 'views/modules/user/auth/login.html',
            controller  : 'loginController as login'
        })

        .when('/users', {
            templateUrl : 'views/modules/user/user/list.html',
            controller  : 'userListController as user'
        })

        .when('/user/create', {
            templateUrl : 'views/modules/user/user/form.html',
            controller  : 'userCreateController as user'
        })

        .when('/user/:id', {
            templateUrl : 'views/modules/user/user/profile.html',
            controller  : 'userViewController as user'
        })

        .when('/user/:id/update', {
            templateUrl : 'views/modules/user/user/form.html',
            controller  : 'userUpdateController as user'
        })

        .when('/user/:id/update-profile', {
            templateUrl : 'views/modules/user/user/profileUpdate.html',
            controller  : 'userUpdateController as user'
        })

        .when('/ranks', {
            templateUrl : 'views/modules/user/rank/list.html',
            controller  : 'rankListController as rank'
        })

        .when('/rank/create', {
            templateUrl : 'views/modules/user/rank/form.html',
            controller  : 'rankCreateController as rank'
        })

        .when('/rank/:id', {
            templateUrl : 'views/modules/user/rank/view.html',
            controller  : 'rankViewController as rank'
        })

        .when('/rank/:id/update', {
            templateUrl : 'views/modules/user/rank/form.html',
            controller  : 'rankUpdateController as rank'
        })

        // complaint module url settings

        .when('/complaints', {
            templateUrl : 'views/modules/complaint/complaint/list.html',
            controller  : 'complaintListController as complaint'
        })

        .when('/complaint/create', {
            templateUrl : 'views/modules/complaint/complaint/form.html',
            controller  : 'complaintCreateController as complaint'
        })

        .when('/complaint/:id', {
            templateUrl : 'views/modules/complaint/complaint/view.html',
            controller  : 'complaintViewController as complaint'
        })

        .when('/complaint/:id/update', {
            templateUrl : 'views/modules/complaint/complaint/form.html',
            controller  : 'complaintUpdateController as complaint'
        })

        // news module url settings

        .when('/news-list', {
            templateUrl : 'views/modules/news/news/list.html',
            controller  : 'newsListController as news'
        })

        .when('/news/create', {
            templateUrl : 'views/modules/news/news/form.html',
            controller  : 'newsCreateController as news'
        })

        .when('/news/:id', {
            templateUrl : 'views/modules/news/news/view.html',
            controller  : 'newsViewController as news'
        })

        .when('/news/:id/update', {
            templateUrl : 'views/modules/news/news/form.html',
            controller  : 'newsUpdateController as news'
        })

        // dialogs module url settings

        .when('/dialogs', {
            templateUrl : 'views/modules/dialog/dialog/list.html',
            controller  : 'dialogsController as dialog'
        })

        .when('/dialogs/:id', {
            templateUrl : 'views/modules/dialog/dialog/list.html',
            controller  : 'dialogsController as dialog'
        })

        // rbac module url settings

        .when('/rbac/permissions', {
            templateUrl : 'views/modules/rbac/permission/list.html',
            controller  : 'permissionListController as permission'
        })

        .when('/rbac/permission/create', {
            templateUrl : 'views/modules/rbac/permission/form.html',
            controller  : 'permissionCreateController as permission'
        })

        .when('/rbac/permission/:id', {
            templateUrl : 'views/modules/rbac/permission/view.html',
            controller  : 'permissionViewController as permission'
        })

        .when('/rbac/permission/:id/update', {
            templateUrl : 'views/modules/rbac/permission/form.html',
            controller  : 'permissionUpdateController as permission'
        })

        .when('/rbac/rules', {
            templateUrl : 'views/modules/rbac/rule/list.html',
            controller  : 'ruleListController as rule'
        })

        .when('/rbac/rule/create', {
            templateUrl : 'views/modules/rbac/rule/form.html',
            controller  : 'ruleCreateController as rule'
        })

        .when('/rbac/rule/:id', {
            templateUrl : 'views/modules/rbac/rule/form.html',
            controller  : 'ruleViewController as rule'
        })

        .when('/rbac/rule/:id/update', {
            templateUrl : 'views/modules/rbac/rule/form.html',
            controller  : 'ruleUpdateController as rule'
        })

        .when('/rbac/routes', {
            templateUrl : 'views/modules/rbac/route/list.html',
            controller  : 'routeListController as route'
        })

        .when('/rbac/route/create', {
            templateUrl : 'views/modules/rbac/route/form.html',
            controller  : 'routeCreateController as route'
        })

        .when('/rbac/roles', {
            templateUrl : 'views/modules/rbac/role/list.html',
            controller  : 'roleListController as role'
        })

        .when('/rbac/role/create', {
            templateUrl : 'views/modules/rbac/role/form.html',
            controller  : 'roleCreateController as role'
        })

        .when('/rbac/role/:id', {
            templateUrl : 'views/modules/rbac/role/view.html',
            controller  : 'roleViewController as role'
        })

        .when('/rbac/role/:id/update', {
            templateUrl : 'views/modules/rbac/role/form.html',
            controller  : 'roleUpdateController as role'
        })

        .when('/rbac/assignments', {
            templateUrl : 'views/modules/rbac/assignment/list.html',
            controller  : 'assignmentListController as assignment'
        })

        .when('/rbac/assignment/:id', {
            templateUrl : 'views/modules/rbac/assignment/view.html',
            controller  : 'assignmentViewController as assignment'
        })

        .when('/404', {
            templateUrl : 'views/common/404.html'
            //controller  : 'loginController as login'
        })

        .otherwise({
            redirectTo: '/404'
        });

    $locationProvider.html5Mode(true);

    // ------------ localization -----------

    var langKeys = CONFIG.langs.map(function(lang) {
        return lang.key;
    });

    var langOptions = {};
    CONFIG.langs.forEach(function(lang, i, languages) {
        langOptions[lang.code] = lang.key;
    });

    $translateProvider.registerAvailableLanguageKeys(langKeys, langOptions);

    $translateProvider.useStaticFilesLoader({
        files: [{
            prefix: '../translation/lang-',
            suffix: '.json'
        }]
    });

    $translateProvider.useSanitizeValueStrategy('escape');

    var currentLanguage = (localStorage.getItem('lang')) ? localStorage.getItem('lang') : CONFIG.defaultLang;
    $translateProvider.preferredLanguage(currentLanguage);

    // ------------ end localization -----------

    // ------------ http provider configurations -----------

    // change 'api/...' to real api path in all api requests
    $httpProvider.interceptors.push(function ($q) {
        return {
            'request': function (config) {
                var apiIndex = config.url.indexOf('api');
                if (apiIndex === 0 || apiIndex === 1) {
                    config.url = config.url.replace('api', 'backend/web');
                }
                return config || $q.when(config);
            }
        }
    });

    // catch all responses with with some codes
    $httpProvider.interceptors.push(function ($q, notification) {
        return {
            'responseError': function(errorResponse) {
                switch (errorResponse.status) {
                    case 403:
                        notification.alert('You are not allowed to perform this action', false);
                        break;
                }
                return $q.reject(errorResponse);
            }
        };
    });

    // send data as 'form-urlencoded' (by default angular send post requests with content-type: 'application/json'
    // uncomment, if needed

    //$httpProvider.defaults.transformRequest = function(data) {
    //    if (data === undefined) { return data; }
    //    return $.param(data);
    //};
    //$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

    // ------------ end http provider configurations -----------

});