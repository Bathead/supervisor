svApp.controller('mainController', [
    '$translate',
    '$location',
    '$scope',
    '$rootScope',
    '$timeout',
    'menuBuilder',
    'themeSwitcher',
    'localizationHelper',
    'auth',
    'CONFIG',
    function(
        $translate,
        $location,
        $scope,
        $rootScope,
        $timeout,
        menuBuilder,
        themeSwitcher,
        localizationHelper,
        auth,
        CONFIG
) {
        var controller = this;

        // localization init
        controller.currentLang = $translate.proposedLanguage();
        controller.langs = CONFIG.langs;
        $scope.$watch('mainController.user.permissions', function() {
            controller.menu = menuBuilder.init();
        });

        // theme init
        controller.themes = CONFIG.themes;
        controller.currentTheme = themeSwitcher.initTheme();
        controller.changeTheme = function(theme) {
            themeSwitcher.changeTheme(theme);
            controller.currentTheme = themeSwitcher.initTheme();
        };

        // update browser page title
        $scope.$watch('mainController.title', function(newValue, oldValue) {
            $rootScope.title = newValue;
        });

        // user and menu init
        auth.initUser(function() {
            controller.user = auth.user;
            controller.menu = menuBuilder.init();
            if (controller.user === null && !auth.isUrlAvailableForGuest($location.url())) {
                $location.path('/login');
            }
        });

        var available = auth.isUrlAvailableForGuest($location.url());

        // update menu active item after route change
        $rootScope.$on("$routeChangeSuccess",
            function (event, current, previous, rejection) {
                $(document).unbind('scroll');
                menuBuilder.updateActive();
                menuBuilder.updateCounters();
                $timeout(function () {
                    menuBuilder.bindDropDowns();
                }, 0);
            });

        // change app lang
        controller.changeLanguage = function(langKey){
            controller.currentLang = langKey;
            $translate.use(controller.currentLang);
            localStorage.setItem('lang', langKey);
        };

        // get current lang flag
        controller.getCurrentLanguageFlag = localizationHelper.getCurrentLanguageFlag;

        // user logout
        controller.logout = function() {
            auth.logout(function() {
                controller.user = null;
                controller.menu = null;
                $location.path('/login');
            });
        };
    }
]);