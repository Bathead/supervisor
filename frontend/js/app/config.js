svApp.constant('CONFIG', {
    defaultLang: 'uk',
    langs: [
        {
            key:    'en',
            title:  'English(US)',
            code:   'en_*',
            flag:   'us'
        },
        {
            key:    'ru',
            title:  'Russian',
            code:   'ru_*',
            flag:   'ru'
        },
        {
            key:    'uk',
            title:  'Ukrainian',
            code:   'uk_*',
            flag:   'ua'
        }
    ],

    defaultTheme: 'litmusBlue',
    themes: {
        white: {
            title: 'White skin',
            class: 'white',
            logo: 'img/logo/sg-logo-black-1.png',
            logoMobile: 'img/logo/sg-logo-black-1.png'
        },
        ebonyClay: {
            title: 'Ebony clay',
            class: 'theme-side-ebony-clay',
            logo: 'img/logo/sg-logo-white.png',
            logoMobile: 'img/logo/sg-logo-white.png'
        },
        madisonCaribbean: {
            title: 'Madison Caribbean',
            class: 'theme-side-madison-caribbean',
            logo: 'img/logo/sg-logo-white.png',
            logoMobile: 'img/logo/sg-logo-white.png'
        },
        caesiumDarkCaribbean: {
            title: 'Caesium dark caribbean',
            class: 'theme-side-caesium-dark-caribbean',
            logo: 'img/logo/sg-logo-white.png',
            logoMobile: 'img/logo/sg-logo-white.png'
        },
        tin: {
            title: 'Tin',
            class: 'theme-side-tin',
            logo: 'img/logo/sg-logo-white.png',
            logoMobile: 'img/logo/sg-logo-white.png'
        },
        litmusBlue: {
            title: 'Litmus Blue',
            class: 'theme-side-litmus-blue',
            logo: 'img/logo/sg-logo-white.png',
            logoMobile: 'img/logo/sg-logo-white.png'
        },
        rebeccaPurple: {
            title: 'Rebecca purple',
            class: 'theme-rebecca-purple',
            logo: 'img/logo/sg-logo-white.png',
            logoMobile: 'img/logo/sg-logo-white.png'
        },
        pictonBlue: {
            title: 'Picton blue',
            class: 'theme-picton-blue',
            logo: 'img/logo/sg-logo-white.png',
            logoMobile: 'img/logo/sg-logo-white.png'
        },
        pictonBlueWhiteEbony: {
            title: 'Picton blue white ebony',
            class: 'theme-picton-blue-white-ebony',
            logo: 'img/logo/sg-logo-white.png',
            logoMobile: 'img/logo/sg-logo-white.png'
        }
    },

    defaultCkEditorOptions: {
        allowedContent: true,
        entities: false
    },

    webSocketsOptions: {
        dialogPort: 8080
    }
});
