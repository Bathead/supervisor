svApp.controller('assignmentViewController', [
    '$routeParams',
    '$scope',
    'assignment',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        assignment,
        ACTIONS
    ) {

        var controller = this;
        controller.action = ACTIONS.VIEW;
        controller.model = {};
        controller.available = [];
        controller.assigned = [];
        controller.availableSelected = [];
        controller.assignedSelected = [];
        controller.availableFilter = '';
        controller.assignedFilter = '';

        assignment.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.initBreadcrumbs();
            controller.initPermissions();
        });

        controller.initBreadcrumbs = function() {
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: 'Assignments',
                    url: '/rbac/assignments',
                    active: false
                },
                {
                    title: controller.model.fullName,
                    active: true
                }
            ];
            $scope.mainController.title = controller.model.fullName;
        };

        controller.initPermissions = function() {
            updateAvailable();
            updateAssign();
        };

        controller.availableFilterChange = function() {
            updateAvailable();
        };

        controller.assignedFilterChange = function() {
            updateAssign();
        };

        function updateAvailable() {
            assignment.search(controller.model.id, 'available', controller.availableFilter, function(response) {
                controller.available = response.list;
            });
        }

        function updateAssign() {
            assignment.search(controller.model.id, 'assigned', controller.assignedFilter, function(response) {
                controller.assigned = response.list;
            });
        }

        controller.assign = function() {
            assignment.assign(controller.model.id, 'assign', controller.availableSelected, function(response) {
                if (response.success) {
                    controller.initPermissions();
                }
            });
        };

        controller.unbind = function() {
            assignment.assign(controller.model.id, 'remove', controller.assignedSelected, function(response) {
                if (response.success) {
                    controller.initPermissions();
                }
            });
        };
    }

]);