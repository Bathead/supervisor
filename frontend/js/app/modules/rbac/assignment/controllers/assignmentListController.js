svApp.controller('assignmentListController', [
    '$http',
    '$scope',
    'assignment',
    'crudHelper',
    function(
        $http,
        $scope,
        assignment,
        crudHelper
    ) {

        var controller = this;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Assignments',
                active: true
            }
        ];
        $scope.mainController.title = 'Assignments';

        controller.tableParams = crudHelper.generateTable({}, {}, assignment, controller);
    }
]);