svApp.factory('assignment', [
    '$http',
    'requestHelper',
    function(
        $http,
        requestHelper
    ) {
        var assignment = {

            getList : function(page, count, filter, sorting, successCallback) {
                var searchQuery = requestHelper.formatSearchRequest('Assignment', page, count, filter, sorting);
                return $http.get('/api/rbac/assignment/get-assignments?' + searchQuery).success(function(response) {
                    var formattedResult = [];
                    for (var key in response.list) {
                        formattedResult.push(response.list[key]);
                    }
                    response.list = formattedResult;
                    successCallback(response);
                });
            },

            getEntity : function(id, successCallback) {
                return $http.get('/api/rbac/assignment/get-assignment/' + id).success(successCallback);
            },

            search : function(id, target, term, successCallback) {
                var data = {
                    id: id,
                    target: target,
                    term: term
                };

                return $http.get('/api/rbac/assignment/search?' + $.param(data)).success(successCallback);
            },

            assign : function(id, action, roles, successCallback) {
                var data = {
                    id: id,
                    action: action,
                    roles: roles
                };
                return $http.post('/api/rbac/assignment/assign', data).success(successCallback);
            }
        };
        return assignment;
    }
]);