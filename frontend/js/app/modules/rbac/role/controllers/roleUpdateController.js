svApp.controller('roleUpdateController', [
    '$routeParams',
    '$scope',
    'role',
    'rule',
    'crudHelper',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        role,
        rule,
        crudHelper,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.UPDATE;
        controller.isNewRecord = false;
        controller.model = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Roles',
                url: '/rbac/roles',
                active: false
            },
            {
                title: 'Update',
                active: true
            }
        ];

        $scope.mainController.title = 'Update role';

        role.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.model.oldName = controller.model.name;
        });

        rule.getAll(function(response) {
            controller.rules = response.list;
        });

        controller.update = function() {
            crudHelper.update(
                controller.model,
                role,
                '/rbac/roles'
            )
        };

    }
]);