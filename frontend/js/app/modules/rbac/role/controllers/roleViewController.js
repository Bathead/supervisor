svApp.controller('roleViewController', [
    '$routeParams',
    '$scope',
    'role',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        role,
        ACTIONS
    ) {

        var controller = this;
        controller.action = ACTIONS.VIEW;
        controller.model = {};
        controller.available = [];
        controller.assigned = [];
        controller.availableSelected = [];
        controller.assignedSelected = [];
        controller.availableFilter = '';
        controller.assignedFilter = '';

        role.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.initBreadcrumbs();
            controller.initPermissions();
        });

        controller.initBreadcrumbs = function() {
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: 'Roles',
                    url: '/rbac/roles',
                    active: false
                },
                {
                    title: controller.model.name,
                    active: true
                }
            ];
            $scope.mainController.title = controller.model.name;
        };

        controller.initPermissions = function() {
            updateAvailable();
            updateAssign();
        };

        controller.availableFilterChange = function() {
            updateAvailable();
        };

        controller.assignedFilterChange = function() {
            updateAssign();
        };

        function updateAvailable() {
            role.search(controller.model.name, 'available', controller.availableFilter, function(response) {
                controller.available = response.list;
            });
        }

        function updateAssign() {
            role.search(controller.model.name, 'assigned', controller.assignedFilter, function(response) {
                controller.assigned = response.list;
            });
        }

        controller.assign = function() {
            role.assign(controller.model.name, 'assign', controller.availableSelected, function(response) {
                if (response.success) {
                    controller.initPermissions();
                }
            });
        };

        controller.unbind = function() {
            role.assign(controller.model.name, 'remove', controller.assignedSelected, function(response) {
                if (response.success) {
                    controller.initPermissions();
                }
            });
        };
    }

]);