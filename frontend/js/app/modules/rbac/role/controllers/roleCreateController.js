svApp.controller('roleCreateController', [
    '$scope',
    'role',
    'rule',
    'crudHelper',
    'ACTIONS',
    function(
        $scope,
        role,
        rule,
        crudHelper,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.CREATE;
        controller.isNewRecord = true;
        controller.model = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Roles',
                url: '/rbac/roles',
                active: false
            },
            {
                title: 'Create',
                active: true
            }
        ];
        $scope.mainController.title = 'Create new role';

        rule.getAll(function(response) {
            controller.rules = response.list;
        });

        controller.create = function() {
            crudHelper.create(
                controller.model,
                role,
                '/rbac/roles'
            );
        };
    }
]);