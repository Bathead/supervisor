svApp.controller('roleListController', [
    '$http',
    '$scope',
    'role',
    'crudHelper',
    function(
        $http,
        $scope,
        role,
        crudHelper
    ) {

        var controller = this;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Roles',
                active: true
            }
        ];
        $scope.mainController.title = 'Roles';

        controller.tableParams = crudHelper.generateTable({}, {}, role, controller);

        controller.delete = function(id) {
            crudHelper.delete(id, role, controller);
        };
    }
]);