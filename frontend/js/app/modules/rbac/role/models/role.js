// for CrudHelper support model must contain next methods:
// - getList
// - getEntity
// - create
// - update
// - delete

svApp.factory('role', [
    '$http',
    'requestHelper',
    '$q',
    function(
        $http,
        requestHelper,
        $q
    ) {
        var role = {

            getList : function(page, count, filter, sorting, successCallback) {
                var searchQuery = requestHelper.formatSearchRequest('AuthItem', page, count, filter, sorting);
                return $http.get('/api/rbac/role/get-roles?' + searchQuery).success(function(response) {
                    var formattedResult = [];
                    for (var key in response.list) {
                        formattedResult.push(response.list[key]);
                    }
                    response.list = formattedResult;
                    successCallback(response);
                });
            },

            // get all roles (without pagination, filter, sorting)
            getAll : function(successCallback) {
                return $http.get('/api/rbac/role/get-all-roles' ).success(successCallback);
            },

            // return roles for ng-table select filter
            getFilter : function() {
                var def = $q.defer(),
                    result = [];
                role.getAll(function(response) {
                    for (var index in response.list) {
                        result.push({
                            id : response.list[index],
                            title : response.list[index]
                        })
                    }
                });
                def.resolve(result);
                return def;
            },

            getEntity : function(id, successCallback) {
                return $http.get('/api/rbac/role/get-role/' + id).success(successCallback);
            },

            create : function(entity, successCallback) {
                var data = {
                    AuthItem: entity
                };
                return $http.post('/api/rbac/role/create', data).success(successCallback);
            },

            update : function(entity, successCallback) {
                var data = {
                    AuthItem: entity
                };
                return $http.post('/api/rbac/role/update/' + entity.oldName, data).success(successCallback);
            },

            delete : function(id, successCallback) {
                return $http.post('/api/rbac/role/delete/' + id).success(successCallback);
            },

            search : function(id, target, term, successCallback) {
                var data = {
                    id: id,
                    target: target,
                    term: term
                };

                return $http.get('/api/rbac/role/search?' + $.param(data)).success(successCallback);
            },

            assign : function(id, action, roles, successCallback) {
                var data = {
                    id: id,
                    action: action,
                    roles: roles
                };
                return $http.post('/api/rbac/role/assign', data).success(successCallback);
            }
        };
        return role;
    }
]);