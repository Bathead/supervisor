svApp.controller('ruleListController', [
    '$http',
    '$scope',
    'rule',
    'crudHelper',
    function(
        $http,
        $scope,
        rule,
        crudHelper
    ) {

        var controller = this;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Rules',
                active: true
            }
        ];
        $scope.mainController.title = 'Rules';

        controller.tableParams = crudHelper.generateTable({}, {}, rule, controller);

        controller.delete = function(id) {
            crudHelper.delete(id, rule, controller);
        };
    }
]);