svApp.controller('ruleUpdateController', [
    '$routeParams',
    '$scope',
    'rule',
    'crudHelper',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        rule,
        crudHelper,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.UPDATE;
        controller.model = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Rules',
                url: '/rbac/rules',
                active: false
            },
            {
                title: 'Update',
                active: true
            }
        ];
        $scope.mainController.title = 'Update rule';

        rule.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.model.oldName = controller.model.name;
        });

        controller.update = function() {
            crudHelper.update(
                controller.model,
                rule,
                '/rbac/rules'
            )
        };

    }
]);