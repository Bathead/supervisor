svApp.controller('ruleViewController', [
    '$routeParams',
    '$scope',
    'rule',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        rule,
        ACTIONS
    ) {

        var controller = this;
        controller.action = ACTIONS.VIEW;
        controller.model = {};

        rule.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.initBreadcrumbs();
        });

        controller.initBreadcrumbs = function() {
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: 'Rules',
                    url: '/rbac/rules',
                    active: false
                },
                {
                    title: controller.model.name,
                    active: true
                }
            ];
            $scope.mainController.title = controller.model.name;
        };


    }
]);