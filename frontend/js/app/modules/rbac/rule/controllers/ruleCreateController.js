svApp.controller('ruleCreateController', [
    '$scope',
    'rule',
    'crudHelper',
    'ACTIONS',
    function(
        $scope,
        rule,
        crudHelper,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.CREATE;
        controller.isNewRecord = true;
        controller.model = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Rules',
                url: '/rbac/rules',
                active: false
            },
            {
                title: 'Create',
                active: true
            }
        ];
        $scope.mainController.title = 'Create new rule';

        controller.create = function() {
            crudHelper.create(
                controller.model,
                rule,
                '/rbac/rules'
            )
        };
    }
]);