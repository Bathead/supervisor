// for CrudHelper support model must contain next methods:
// - getList
// - getEntity
// - create
// - update
// - delete

svApp.factory('rule', [
    '$http',
    'requestHelper',
    function(
        $http,
        requestHelper
    ) {
        var rule = {

            getAll : function(successCallback) {
                return $http.get('/api/rbac/rule/get-all-rules' ).success(function(response) {
                    var formattedResult = [];
                    for (var key in response.list) {
                        formattedResult.push(response.list[key]);
                    }
                    response.list = formattedResult;
                    successCallback(response);
                });
            },

            getList : function(page, count, filter, sorting, successCallback) {
                var searchQuery = requestHelper.formatSearchRequest('BizRule', page, count, filter, sorting);
                return $http.get('/api/rbac/rule/get-rules?' + searchQuery).success(function(response) {
                    var formattedResult = [];
                    for (var key in response.list) {
                        formattedResult.push(response.list[key]);
                    }
                    response.list = formattedResult;
                    successCallback(response);
                });
            },

            getEntity : function(id, successCallback) {
                return $http.get('/api/rbac/rule/get-rule/' + id).success(successCallback);
            },

            create : function(entity, successCallback) {
                var data = {
                    BizRule: entity
                };
                return $http.post('/api/rbac/rule/create', data).success(successCallback);
            },

            update : function(entity, successCallback) {
                var data = {
                    BizRule: entity
                };
                return $http.post('/api/rbac/rule/update/' + entity.oldName, data).success(successCallback);
            },

            delete : function(id, successCallback) {
                return $http.post('/api/rbac/rule/delete/' + id).success(successCallback);
            }
        };
        return rule;
    }
]);