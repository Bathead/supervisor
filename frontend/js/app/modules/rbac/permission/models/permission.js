// for CrudHelper support model must contain next methods:
// - getList
// - getEntity
// - create
// - update
// - delete

svApp.factory('permission', [
    '$http',
    'requestHelper',
    function(
        $http,
        requestHelper
    ) {
        var permission = {

            getList : function(page, count, filter, sorting, successCallback) {
                var searchQuery = requestHelper.formatSearchRequest('AuthItem', page, count, filter, sorting);
                return $http.get('/api/rbac/permission/get-permissions?' + searchQuery).success(function(response) {
                    var formattedResult = [];
                    for (var key in response.list) {
                        formattedResult.push(response.list[key]);
                    }
                    response.list = formattedResult;
                    successCallback(response);
                });
            },

            getEntity : function(id, successCallback) {
                return $http.get('/api/rbac/permission/get-permission/' + id).success(successCallback);
            },

            create : function(entity, successCallback) {
                var data = {
                    AuthItem: entity
                };
                return $http.post('/api/rbac/permission/create', data).success(successCallback);
            },

            update : function(entity, successCallback) {
                var data = {
                    AuthItem: entity
                };
                return $http.post('/api/rbac/permission/update/' + entity.oldName, data).success(successCallback);
            },

            delete : function(id, successCallback) {
                return $http.post('/api/rbac/permission/delete/' + id).success(successCallback);
            },

            search : function(id, target, term, successCallback) {
                var data = {
                    id: id,
                    target: target,
                    term: term
                };

                return $http.get('/api/rbac/permission/search?' + $.param(data)).success(successCallback);
            },

            assign : function(id, action, roles, successCallback) {
                var data = {
                    id: id,
                    action: action,
                    roles: roles
                };
                return $http.post('/api/rbac/permission/assign', data).success(successCallback);
            }
        };
        return permission;
    }
]);