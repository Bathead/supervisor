svApp.controller('permissionCreateController', [
    '$scope',
    'permission',
    'crudHelper',
    'rule',
    'ACTIONS',
    function(
        $scope,
        permission,
        crudHelper,
        rule,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.CREATE;
        controller.isNewRecord = true;
        controller.model = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Permissions',
                url: '/rbac/permissions',
                active: false
            },
            {
                title: 'Create',
                active: true
            }
        ];
        $scope.mainController.title = 'Create new permission';

        rule.getAll(function(response) {
            controller.rules = response.list;
        });

        controller.create = function() {
            crudHelper.create(
                controller.model,
                permission,
                '/rbac/permissions'
            );
        };
    }
]);