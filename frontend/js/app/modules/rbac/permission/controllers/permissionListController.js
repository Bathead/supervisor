svApp.controller('permissionListController', [
    '$http',
    '$scope',
    'permission',
    'crudHelper',
    function(
        $http,
        $scope,
        permission,
        crudHelper
    ) {

        var controller = this;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Permissions',
                active: true
            }
        ];
        $scope.mainController.title = 'Permissions';

        controller.tableParams = crudHelper.generateTable({}, {}, permission, controller);

        controller.delete = function(id) {
            crudHelper.delete(id, permission, controller);
        };
    }
]);