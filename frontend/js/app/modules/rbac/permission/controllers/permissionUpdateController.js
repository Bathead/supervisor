svApp.controller('permissionUpdateController', [
    '$routeParams',
    '$scope',
    'permission',
    'rule',
    'crudHelper',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        permission,
        rule,
        crudHelper,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.UPDATE;
        controller.isNewRecord = false;
        controller.model = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Permissions',
                url: '/rbac/permissions',
                active: false
            },
            {
                title: 'Update',
                active: true
            }
        ];

        $scope.mainController.title = 'Update permission';

        permission.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.model.oldName = controller.model.name;
        });

        rule.getAll(function(response) {
            controller.rules = response.list;
        });

        controller.update = function() {
            crudHelper.update(
                controller.model,
                permission,
                '/rbac/permissions'
            )
        };

    }
]);