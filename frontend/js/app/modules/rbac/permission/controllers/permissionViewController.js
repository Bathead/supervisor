svApp.controller('permissionViewController', [
    '$routeParams',
    '$scope',
    'permission',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        permission,
        ACTIONS
    ) {

        var controller = this;
        controller.action = ACTIONS.VIEW;
        controller.model = {};
        controller.available = [];
        controller.assigned = [];
        controller.availableSelected = [];
        controller.assignedSelected = [];
        controller.availableFilter = '';
        controller.assignedFilter = '';

        permission.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.initBreadcrumbs();
            controller.initPermissions();
        });

        controller.initBreadcrumbs = function() {
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: 'Permissions',
                    url: '/rbac/permissions',
                    active: false
                },
                {
                    title: controller.model.name,
                    active: true
                }
            ];
            $scope.mainController.title = controller.model.name;
        };

        controller.initPermissions = function() {
            updateAvailable();
            updateAssign();
        };
        
        controller.availableFilterChange = function() {
            updateAvailable();
        };

        controller.assignedFilterChange = function() {
            updateAssign();
        };

        function updateAvailable() {
            permission.search(controller.model.name, 'available', controller.availableFilter, function(response) {
                controller.available = response.list;
            });
        }

        function updateAssign() {
            permission.search(controller.model.name, 'assigned', controller.assignedFilter, function(response) {
                controller.assigned = response.list;
            });
        }

        controller.assign = function() {
            permission.assign(controller.model.name, 'assign', controller.availableSelected, function(response) {
                if (response.success) {
                    controller.initPermissions();
                }
            });
        };

        controller.unbind = function() {
            permission.assign(controller.model.name, 'remove', controller.assignedSelected, function(response) {
                if (response.success) {
                    controller.initPermissions();
                }
            });
        };
    }

]);