svApp.factory('route', [
    '$http',
    'requestHelper',
    '$q',
    function(
        $http,
        requestHelper,
        $q
    ) {
        var route = {

            search : function(target, term, refresh, successCallback) {
                var data = {
                    target: target,
                    term: term,
                    refresh: refresh
                };

                return $http.get('/api/rbac/route/search?' + $.param(data)).success(successCallback);
            },

            assign : function(action, routes, successCallback) {
                var data = {
                    action: action,
                    routes: routes
                };
                return $http.post('/api/rbac/route/assign', data).success(successCallback);
            },

            create : function(entity, successCallback) {
                var data = {
                    Route: entity
                };
                return $http.post('/api/rbac/route/create', data).success(successCallback);
            }

        };
        return route;
    }
]);