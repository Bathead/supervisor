svApp.controller('routeCreateController', [
    '$scope',
    'route',
    'crudHelper',
    'ACTIONS',
    function(
        $scope,
        route,
        crudHelper,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.CREATE;
        controller.isNewRecord = true;
        controller.model = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Routes',
                url: '/rbac/routes',
                active: false
            },
            {
                title: 'Create',
                active: true
            }
        ];
        $scope.mainController.title = 'Create new route';

        controller.create = function() {
            crudHelper.create(
                controller.model,
                route,
                '/rbac/routes'
            )
        };
    }
]);