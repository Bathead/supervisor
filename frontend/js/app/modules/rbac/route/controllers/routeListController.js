svApp.controller('routeListController', [
    '$http',
    '$scope',
    'route',
    function(
        $http,
        $scope,
        route
    ) {

        var controller = this;
        controller.available = [];
        controller.assigned = [];
        controller.availableSelected = [];
        controller.assignedSelected = [];
        controller.availableFilter = '';
        controller.assignedFilter = '';

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Routes',
                active: true
            }
        ];
        $scope.mainController.title = 'Routes';

        controller.initPermissions = function() {
            updateAvailable(0);
            updateAssign(0);
        };

        controller.initPermissions();

        controller.availableFilterChange = function() {
            updateAvailable(0);
        };

        controller.assignedFilterChange = function() {
            updateAssign(0);
        };

        function updateAvailable(refresh) {
            route.search('available', controller.availableFilter, refresh, function(response) {
                controller.available = response.list;
            });
        }

        function updateAssign(refresh) {
            route.search('assigned', controller.assignedFilter, refresh, function(response) {
                controller.assigned = response.list;
            });
        }

        controller.assign = function() {
            route.assign('assign', controller.availableSelected, function(response) {
                if (response.success) {
                    controller.initPermissions();
                }
            });
        };

        controller.unbind = function() {
            route.assign('remove', controller.assignedSelected, function(response) {
                if (response.success) {
                    controller.initPermissions();
                }
            });
        };
        
        controller.refresh = function() {
            updateAvailable(1);
        };
    }
]);