svApp.factory('news', [
    '$http',
    'requestHelper',
    function(
        $http,
        requestHelper
    ) {
        var news = {

            getList : function(offset, successCallback) {
                var data = {
                    offset: offset
                };
                return $http.post('/api/news/get-news-list', data).success(successCallback);
            },

            getLatest: function(count, successCallback){
                var data = {
                    count: count
                };
                return $http.post('/api/news/get-latest-news', data).success(successCallback);
            },

            getUnreadCount: function (successCallback) {
                return $http.get('/api/news/get-unread-count').success(successCallback);
            },

            getNewsTypes : function(successCallback) {
                return $http.get('/api/news/get-news-types').success(successCallback);
            },

            getEntity : function(id, successCallback) {
                return $http.get('/api/news/get-news/' + id).success(successCallback);
            },

            create : function(entity, successCallback) {
                var data = {
                    News: entity
                };
                return $http.post('/api/news/create', data).success(successCallback);
            },

            update : function(entity, successCallback) {
                var data = {
                    News: entity
                };
                return $http.post('/api/news/update/' + entity.id, data).success(successCallback);
            },

            delete : function(id, successCallback) {
                return $http.post('/api/news/delete/' + id).success(successCallback);
            },

            getComments : function(newsId, offset, successCallback) {
                var data = {
                    newsId: newsId,
                    offset: offset
                };
                return $http.post('/api/news/get-news-comments', data).success(successCallback);
            },

            addComment : function(entity, successCallback) {
                var data = {
                    NewsComment: entity
                };
                return $http.post('/api/news/add-comment', data).success(successCallback);
            },

            updateComment : function(entity, successCallback) {
                var data = {
                    NewsComment: entity
                };
                return $http.post('/api/news/update-comment/' + entity.id, data).success(successCallback);
            },

            deleteComment : function(id, successCallback) {
                return $http.post('/api/news/delete-comment/' + id).success(successCallback);
            }
        };
        return news;
    }
]);