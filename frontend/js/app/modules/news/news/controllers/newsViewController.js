svApp.controller('newsViewController', [
    '$routeParams',
    '$scope',
    '$location',
    'news',
    'notification',
    'menuBuilder',
    'localizationHelper',
    'CONFIG',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        $location,
        news,
        notification,
        menuBuilder,
        localizationHelper,
        CONFIG,
        ACTIONS
    ) {

        var controller = this;
        controller.action = ACTIONS.VIEW;
        controller.model = {};

        controller.comments = [];
        controller.allCommentsLoaded = false;

        controller.newCommentModel = {
            news_id : $routeParams.id
        };
        controller.editableCommentModel = {};
        controller.editableCommentObject = {}; // link to editable commment

        controller.initBreadcrumbs = function() {
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: 'News',
                    url: '/news-list',
                    active: false
                },
                {
                    title: controller.model.title,
                    active: true
                }
            ];
            $scope.mainController.title = 'News';
        };

        controller.ckEditorOptions = angular.copy(CONFIG.defaultCkEditorOptions);
        controller.ckEditorOptions.language = localizationHelper.getCurrentLanguage();

        news.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.initBreadcrumbs();
            menuBuilder.updateCounters();

            getMoreComments();
        });

        news.getNewsTypes(function(response) {
            var result = {};
            for (var i = 0; i < response.list.length; i++) {
                var type = response.list[i];
                result[type.title] = type.id;
            }
            controller.newsTypes = result;
        });

        // load more news, when page scrolled to bottom
        $(document).on('scroll', function () {
            if($(window).scrollTop() + $(window).height() == $(document).height()) {
                getMoreComments();
            }
        });

        function getMoreComments() {
            if (!controller.allCommentsLoaded) {
                var alreadyLoadedCommentsCount = controller.comments.length;
                news.getComments(controller.model.id, alreadyLoadedCommentsCount, function(response) {
                    for (var i = 0; i < response.list.length; i++) {
                        controller.comments.push(response.list[i]);
                    }
                    controller.allCommentsLoaded = response.allElementsLoaded;
                });
            }
        }

        // delete news
        controller.delete = function(id) {
            notification.confirm(function() {
                news.delete(id, function(response) {
                    if (response.success) {
                        notification.alert('Deleted successfully', true);
                        $location.path('/news-list');
                    } else {
                        notification.alert('Error occurred while deleting', false);
                    }
                });
            });
        };

        controller.openCreateCommentModal = function() {
            controller.modalScenario = 'create';
            controller.newCommentModel.message = '';
            $('#commentModal').modal('show');
        };

        controller.openUpdateCommentModal = function(comment) {
            controller.editableCommentModel.id = comment.id;
            controller.editableCommentModel.message = comment.message;
            controller.editableCommentObject = comment;
            controller.modalScenario = 'update';
            $('#commentModal').modal('show');
        };

        controller.createNewComment = function() {
            news.addComment(controller.newCommentModel, function(response) {
                if (response.success) {
                    response.model.updated_at = response.model.created_at;
                    controller.comments.unshift(response.model);
                } else {
                    notification.alert('Creating error', false);
                }
                $('#commentModal').modal('hide');
                controller.model.commentsCount++;
            });
        };

        controller.updateOldComment = function() {
            news.updateComment(controller.editableCommentModel, function(response) {
                if (response.success) {
                    controller.editableCommentObject.message = response.model.message;
                    controller.editableCommentObject.updated_at = response.model.updated_at;
                } else {
                    notification.alert('Updating error', false);
                }
                $('#commentModal').modal('hide');
            });
        };

        controller.deleteComment = function(id) {
            notification.confirm(function() {
                news.deleteComment(id, function(response) {
                    if (response.success) {
                        // delete comment from list
                        for (var i = 0; i < controller.comments.length; i++) {
                            var comment = controller.comments[i];
                            if (comment.id == id) {
                                controller.comments.splice(i, 1);
                            }
                        }
                        controller.model.commentsCount--;
                    } else {
                        notification.alert('Error occurred while deleting', false);
                    }
                });
            }, {
                closeOnConfirm: true
            });
        }
    }
]);