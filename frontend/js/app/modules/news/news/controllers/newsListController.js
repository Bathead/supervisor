svApp.controller('newsListController', [
    '$http',
    '$scope',
    'news',
    function(
        $http,
        $scope,
        news
    ) {

        var controller = this;
        controller.list = [];
        controller.allElementsLoaded = false;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'News',
                active: true
            }
        ];

        news.getNewsTypes(function(response) {
            var result = {};
            for (var i = 0; i < response.list.length; i++) {
                var type = response.list[i];
                result[type.title] = type.id;
            }
            controller.types = result;
        });

        $scope.mainController.title = 'News';

        if (controller.list == 0) {
            getMoreNews();
        }

        // load more news, when page scrolled to bottom
        $(document).on('scroll', function () {
            if($(window).scrollTop() + $(window).height() == $(document).height()) {
                // except page loading scroll event
                if (controller.list.length > 0) {
                    getMoreNews();
                }
            }
        });

        function getMoreNews() {
            if (!controller.allElementsLoaded) {
                var alreadyLoaded = controller.list.length;
                news.getList(alreadyLoaded, function(response) {
                    for (var i = 0; i < response.list.length; i++) {
                        controller.list.push(response.list[i]);
                    }
                    controller.allElementsLoaded = response.allElementsLoaded;
                });
            }
        }
    }
]);