svApp.controller('newsUpdateController', [
    '$routeParams',
    '$scope',
    'news',
    'crudHelper',
    'localizationHelper',
    'CONFIG',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        news,
        crudHelper,
        localizationHelper,
        CONFIG,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.UPDATE;
        controller.model = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'News',
                url: '/news-list',
                active: false
            },
            {
                title: 'Update',
                active: true
            }
        ];
        $scope.mainController.title = 'Update news';

        controller.ckEditorOptions = angular.copy(CONFIG.defaultCkEditorOptions);
        controller.ckEditorOptions.language = localizationHelper.getCurrentLanguage();

        news.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
        });

        news.getNewsTypes(function(response) {
            controller.newsTypes = response.list;
        });

        controller.update = function() {
            crudHelper.update(
                controller.model,
                news,
                '/news-list'
            )
        };

    }
]);