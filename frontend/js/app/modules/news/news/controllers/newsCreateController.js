svApp.controller('newsCreateController', [
    '$scope',
    'news',
    'crudHelper',
    'localizationHelper',
    'CONFIG',
    'ACTIONS',
    function(
        $scope,
        news,
        crudHelper,
        localizationHelper,
        CONFIG,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.CREATE;
        controller.model = {};
        controller.model.type = 1;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'News',
                url: '/news-list',
                active: false
            },
            {
                title: 'Create',
                active: true
            }
        ];
        $scope.mainController.title = 'Create news';

        controller.ckEditorOptions = angular.copy(CONFIG.defaultCkEditorOptions);
        controller.ckEditorOptions.language = localizationHelper.getCurrentLanguage();
        
        news.getNewsTypes(function(response) {
            controller.newsTypes = response.list;
        });

        controller.create = function() {
            crudHelper.create(
                controller.model,
                news,
                '/news-list'
            )
        };
    }
]);