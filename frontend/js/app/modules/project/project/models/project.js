// for CrudHelper support model must contain next methods:
// - getList
// - getEntity
// - create
// - update
// - delete

svApp.factory('project', [
    '$http',
    'requestHelper',
    '$q',
    'localizationHelper',
    function(
        $http,
        requestHelper,
        $q,
        localizationHelper
    ) {
        var project = {
            getList : function(page, count, filter, sorting, successCallback) {
                var searchQuery = requestHelper.formatSearchRequest('Project', page, count, filter, sorting);
                return $http.get('/api/project/get-projects?' + searchQuery).success(successCallback);
            },

            getLatest: function(count, successCallback){
                var data = {
                    count: count
                };
                return $http.post('/api/project/get-latest-projects', data).success(successCallback);
            },

            getArchived : function(page, count, filter, sorting, successCallback) {
                var searchQuery = requestHelper.formatSearchRequest('Project', page, count, filter, sorting);
                return $http.get('/api/project/get-projects-archived?' + searchQuery).success(successCallback);
            },

            getMyArchived : function(page, count, filter, sorting, successCallback) {
                var searchQuery = requestHelper.formatSearchRequest('Project', page, count, filter, sorting);
                return $http.get('/api/project/get-projects-my-archived?' + searchQuery).success(successCallback);
            },

            getVersion: function(id, version, successCallback){
                var data = {
                    id: id,
                    version: version
                };
                return $http.post('/api/project/get-project-version', data).success(successCallback);
            },

            getVersions : function(page, count, filter, sorting, id, successCallback) {
                var searchQuery = requestHelper.formatSearchRequest('Project', page, count, filter, sorting);
                return $http.get('/api/project/get-projects-versions/' + id + '?' + searchQuery).success(successCallback);
            },

            getMyProjects : function(page, count, filter, sorting, successCallback) {
                var searchQuery = requestHelper.formatSearchRequest('Project', page, count, filter, sorting);
                return $http.get('/api/project/get-my-projects?' + searchQuery).success(successCallback);
            },

            getEntity : function(id, successCallback) {
                return $http.get('/api/project/get-project/' + id).success(successCallback);
            },

            getTitles : function(successCallback){
                return $http.get('/api/project/get-titles').success(successCallback);
            },

            getClients: function(successCallback){
                return $http.get('/api/project/get-clients').success(successCallback);
            },

            getInvaluableCount: function(successCallback){
                return $http.get('/api/project/get-invaluable-count').success(successCallback);
            },

            create : function(entity, successCallback) {
                var data = {
                    Project: {}
                };
                angular.copy(entity, data.Project);
                delete data.Project.files;

                var formData = new FormData();
                formData.append('data', JSON.stringify(data));

                requestHelper.addMultipleFilesToFormData(formData, entity.files);
                delete entity.files;

                return $http.post(
                    '/api/project/create',
                    formData,
                    {
                        headers: {
                            'Content-Type': undefined
                        }
                    }).success(successCallback);
            },

            changeStatus: function(id, data, successCallback){
                return $http.post('/api/project/change-status/' + id, data).success(successCallback);
            },


            moveToArchive : function(id, successCallback) {
                return $http.post('/api/project/move-to-archive/' + id).success(successCallback);
            },

            delete : function(id, successCallback) {
                return $http.post('/api/project/delete/' + id).success(successCallback);
            },

            update : function(entity, successCallback) {
                var data = {
                    Project: {}
                };
                angular.copy(entity, data.Project);

                var formData = new FormData();
                formData.append('data', JSON.stringify(data));

                requestHelper.addMultipleFilesToFormData(formData, entity.files);
                delete entity.files;

                return $http.post(
                    '/api/project/update/' + entity.id,
                    formData,
                    {
                        headers: {
                            'Content-Type': undefined
                        }
                    }).success(successCallback);
            },

            rate : function(entity, successCallback) {
                var data = {
                    ProjectSelect: entity
                };
                return $http.post('/api/project/rate', data).success(successCallback);
            },

            reject : function(entity, successCallback) {
                var data = {
                    ProjectSelect: entity
                };
                return $http.post('/api/project/reject', data).success(successCallback);
            },


            getDevelopers : function(successCallback){
                return $http.get('/api/project/get-developers').success(successCallback);
            },

            getSales : function(successCallback){
                return $http.get('/api/project/get-sales').success(successCallback);
            },

            getShowDiscussionButton : function(id, successCallback) {
                return $http.get('/api/project/get-show-discussion-button/' + id).success(successCallback);
            },

            getIsValuatedByCurrentUser : function(id, successCallback) {
                return $http.get('/api/project/get-is-valuated-by-current-user/' + id).success(successCallback);
            },

            addDevelopers : function(entity, successCallback) {
                var data = {
                    Project: {}
                };
                angular.copy(entity, data.Project);

                var formData = new FormData();
                formData.append('data', JSON.stringify(data));

                return $http.post(
                    '/api/project/add-developers/' + entity.id,
                    formData,
                    {
                        headers: {
                            'Content-Type': undefined
                        }
                    }).success(successCallback);
            },

            deleteRate : function(id, successCallback) {
                return $http.post('/api/project/delete-rate/' + id).success(successCallback);
            },

            assignDeveloper: function(id, successCallback) {
                return $http.post('/api/project/assign-developer/' + id).success(successCallback);
            },

            deassignDeveloper: function(data, successCallback) {
                return $http.post('/api/project/deassign-developer', data).success(successCallback);
            },

            getComments : function(projectId, offset, successCallback) {
                var data = {
                    projectId: projectId,
                    offset: offset
                };
                return $http.post('/api/project/get-project-comments', data).success(successCallback);
            },

            addComment : function(entity, successCallback) {
                var data = {
                    ProjectComment: entity
                };
                return $http.post('/api/project/add-comment', data).success(successCallback);
            },

            updateComment : function(entity, successCallback) {
                var data = {
                    ProjectComment: entity
                };
                return $http.post('/api/project/update-comment/' + entity.id, data).success(successCallback);
            },

            deleteComment : function(id, successCallback) {
                return $http.post('/api/project/delete-comment/' + id).success(successCallback);
            },

            getProjectStatuses : function(successCallback) {
                return $http.get('/api/project/get-project-statuses').success(successCallback);
            },

            getProjectStatusesAsObject : function(successCallback) {
                return project.getProjectStatuses(function(response) {
                    var result = {};
                    for (var i = 0; i < response.list.length; i++) {
                        var status = response.list[i];
                        result[status.title.replace(/ /g,"_")] = status.id;
                    }
                    response.list = result;
                    successCallback(response);
                });
            },

            getProjectStatusesFilter : function() {
                var def = $q.defer(),
                    result = [];
                project.getProjectStatuses(function(response){
                    for (var index in response.list) {
                        result.push({
                            id : response.list[index].id,
                            title : localizationHelper.translate(response.list[index].title)
                        })
                    }
                });
                def.resolve(result);
                return def;
            },

            getPaymentMethods : function(successCallback) {
                return $http.get('/api/project/get-payment-methods').success(successCallback);
            },

            getPaymentMethodsFilter : function() {
                var def = $q.defer(),
                    result = [];
                project.getPaymentMethods(function(response){
                    for (var index in response.list) {
                        result.push({
                            id : response.list[index].id,
                            title : localizationHelper.translate(response.list[index].title)
                        })
                    }
                });
                def.resolve(result);
                return def;
            },

            getAllUserProjects : function(userId, successCallback) {
                return $http.get('/api/project/get-all-user-projects/' + userId).success(successCallback);
            },

            getSalesAsObject : function(name, successCallback) {
                var data = {
                    name : name
                };
                return $http.post('/api/project/get-sales-as-object', data).then(successCallback);
            },

            getDeveloperAsObject : function(name, successCallback) {
                var data = {
                    name : name
                };
                return $http.post('/api/project/get-developer-as-object', data).then(successCallback);
            },

            addDeveloper : function(entity, successCallback) {
                var data = {
                    ProjectDeveloper: entity
                };
                return $http.post('/api/project/add-developer', data).success(successCallback);
            }
        };

        return project;
    }
]);