svApp.controller('projectUpdateController', [
    '$http',
    '$routeParams',
    '$scope',
    'project',
    'technology',
    'localizationHelper',
    'crudHelper',
    'componentsInit',
    'notification',
    'ACTIONS',
    function(
        $http,
        $routeParams,
        $scope,
        project,
        technology,
        localizationHelper,
        crudHelper,
        componentsInit,
        notification,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.UPDATE;
        controller.model = {};
        controller.oldModel = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Projects',
                url: '/projects',
                active: false
            },
            {
                title: 'Update',
                active: true
            }
        ];
        $scope.mainController.title = 'Update project';

        technology.getAll(function(response) {
            controller.technologiesList = response.list;
        });

        project.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.oldModel = angular.copy(controller.model);
            initComponents();
        });

        controller.update = function() {
            controller.model.files = $scope.files;

            controller.deletedFiles.forEach(function(item) {
                var data = {
                    name: item
                };
                $http.post('/api/project/delete-attached-file/' + $routeParams.id, data);
            });


            /*foreach(controller.deletedFiles as item){
               $http.post('/api/project/delete-attached-file/' + id, data);
            }*/

            crudHelper.update(
                controller.model,
                project,
                '/projects'
            )
         };

        project.getPaymentMethods(function(response) {
            controller.paymentMethods = response.list;
        });

        project.getProjectStatuses(function(response) {
            controller.statuses = response.list;
        });

        controller.deletedFiles = [];
        controller.deleteAttachedFile = function(id, name) {
            var data = {
                name: name
            };

            controller.deletedFiles.push(name);

            /*notification.confirm(function () {
                notification.alert("Deleted successfully", true);
                return $http.post('/api/project/delete-attached-file/' + id, data);
            })*/
        };

        function initComponents() {
            componentsInit.initSelect();
            $(document).ready(function() {
                $("#files").filestyle({
                    placeholder: localizationHelper.translate('Attach files'),
                    buttonText: localizationHelper.translate('Select')
                });
            });
        }

    }
]);