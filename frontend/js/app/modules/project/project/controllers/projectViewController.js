svApp.controller('projectViewController', [
    '$routeParams',
    '$scope',
    'project',
    'ACTIONS',
    'localizationHelper',
    'notification',
    '$http',
    '$location',
    'auth',
    '$route',
    'crudHelper',
    'user',
    'currency',
    function(
        $routeParams,
        $scope,
        project,
        ACTIONS,
        localizationHelper,
        notification,
        $http,
        $location,
        auth,
        $route,
        crudHelper,
        user,
        currency
    ) {

        var controller = this;

        controller.action = ACTIONS.VIEW;
        controller.model = {};

        controller.comments = [];
        controller.isAllCommentsLoaded = false;

        controller.newCommentModel = {
            project_id : $routeParams.id
        };

        controller.editableCommentModel = {};
        controller.editableCommentObject = {}; // link to editable commment

        controller.selectModel = {
            id_project : $routeParams.id
        };

        controller.developerModel = {
            id_project : $routeParams.id
        };

        controller.salesObject = null;
        controller.developerObject = null;

        auth.makeForCurrentUser(function(user) {
            controller.loggedUserId = user.id;
            controller.authPermissions = user.permissions;
        });

        currency.getAll(function(response) {
            controller.currencies = response.list;
        });

        project.getProjectStatuses(function(response){
            var result = {};
            for (var i = 0; i < response.list.length; i++) {
                var status = response.list[i];
                result[status.title.replace(/ /g, "_")] = status.id;
            }
            controller.statuses = result;
        });

        project.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.isCurrentUserAssigned = (controller.model.developersIds.indexOf(controller.loggedUserId) != -1);
            controller.initBreadcrumbs();
            getMoreComments();

            project.getDevelopers(function(response) {
                controller.developers = response.list;

                controller.model.developersFullList = [];
                for (var i = 0; i < controller.developers.length; i++) {
                    controller.model.developersFullList.push({
                        id:     controller.developers[i].id,
                        fullName:   controller.developers[i].fullName,
                        rate:   controller.developers[i].profile.external_hourly_rate,
                        currency:   controller.developers[i].profile.external_hourly_rate_currency_id,
                        active: false
                    });
                }
            });
        });

        controller.initBreadcrumbs = function() {
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: 'Projects',
                    url: '/projects',
                    active: false
                },
                {
                    title: controller.model.title,
                    active: true
                }
            ];
            $scope.mainController.title = localizationHelper.translate("Project") + " - " + controller.model.title;
        };

        var startOptions = {
            title: localizationHelper.translate("Start project"),
            text: localizationHelper.translate("Are you sure that you want to start the project?"),
            confirmButtonText: localizationHelper.translate("Start")
        };

        var completeOptions = {
            title: localizationHelper.translate("Complete project"),
            text: localizationHelper.translate("Are you sure that you want to complete the project?"),
            confirmButtonText: localizationHelper.translate("Complete")
        };

        var restoreOptions = {
            title: localizationHelper.translate("Activate project"),
            text: localizationHelper.translate("Are you sure that you want to restore the project?"),
            confirmButtonText: localizationHelper.translate("Activate")
        };

        var moveToArchiveOptions = {
            title: localizationHelper.translate("Are you sure?"),
            text: localizationHelper.translate("You can restore that item later"),
            confirmButtonText: localizationHelper.translate("To archive")
        };

        controller.changeStatus = function(status) {
            var options = null;
            switch (status) {
                case controller.statuses.In_work:
                    options = startOptions;
                    break;
                case controller.statuses.Completed:
                    options = completeOptions;
                    break;
                case controller.statuses.Active:
                    options = restoreOptions;
                    break;
                case controller.statuses.In_archive:
                    options = moveToArchiveOptions;
                    break;
            }

            notification.confirm(function () {
                var data = {
                    status: status
                };
                project.changeStatus(controller.model.id, data, function (response) {
                    notification.alert('', response.success);
                });
                $route.reload();
            }, options)
        };

        controller.moveToArchive = function() {
            notification.confirm(function () {
                $location.path('/projects');
                project.moveToArchive(controller.model.id, function (response) {
                    var message = '';
                    notification.alert(message, response.success);
                });
            }, moveToArchiveOptions)
        };

        controller.delete = function(id) {
            notification.confirm(function () {
                $location.path('/projects');
                project.delete(id, function (response) {
                    var message = '';
                    notification.alert(message, response.success);
                });
            })
        };

        controller.rate = function() {
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

            project.rate(controller.selectModel, function(response) {});
            $route.reload();
        };

        controller.reject = function() {
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

            project.reject(controller.selectModel, function(response) {});
            $route.reload();
        };



        project.getSales(function(response) {
            controller.sales = response.list;
        });

        project.getShowDiscussionButton($routeParams.id, function(response) {
            controller.ShowDiscussionButton = response.show;
        });

        project.getIsValuatedByCurrentUser($routeParams.id, function(response) {
            controller.isValuatedByCurrentUser = response.value;
        });

        controller.addDevelopers = function() {
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

            project.addDevelopers(controller.model, function(response) {});
            $route.reload();
        };

        controller.deleteRate = function(id) {
            notification.confirm(function () {
                //$location.path('/projects');
                project.deleteRate(id, function (response) {
                    var message = '';
                    notification.alert(message, response.success);
                    $route.reload();
                });
            });
        };

        controller.assignDeveloper = function(id) {
            project.assignDeveloper(id, function (response) {
                var message = '';
                $route.reload();
            });
        };

        controller.deassignDeveloper = function(id_developer) {
            var data = {
                id_developer: id_developer,
                id_project: $routeParams.id
            };

            project.deassignDeveloper(data, function (response) {
                var message = '';
                $route.reload();
            });
        };

        controller.refresh = function () {
            $route.reload();
        };

        controller.getEndDateColor = function() {
            var startDate = new Date(this.model.start_date);
            var currentDate = new Date();
            var endDate = new Date(this.model.end_date);
            var currentDifference = (currentDate.getTime() - startDate.getTime()) / (1000 * 3600 *24);
            var difference = (endDate.getTime() - startDate.getTime()) / (1000 * 3600 * 24);
            var division = currentDifference / difference;

            if (division <= 0){
                return 'success';
            } else if (division <= 0.5 && division > 0) {
                return 'info';
            } else if (division <= 1 && division > 0.5) {
                return 'warning';
            } else {
                return 'danger';
            }
        };

        controller.getDaysAgo = function() {
            var currentDate = new Date();
            var publishDate = new Date(this.model.publish_date);
            var days = Math.floor((currentDate.getTime() - publishDate.getTime()) / (1000 * 3600 * 24));
            return days;
        };

        // get user for typeahead (in pass project modal)
        controller.getSalesAsObject = function(name) {
            return project.getSalesAsObject(name, function(response){
                return response.data.list;
            })
        };

        controller.getDeveloperAsObject = function(name) {
            return project.getDeveloperAsObject(name, function(response){
                return response.data.list;
            });

        };

        // load more project comments, when page scrolled to bottom
        $(document).on('scroll', function () {
            if($(window).scrollTop() + $(window).height() == $(document).height()) {
                getMoreComments();
            }
        });

        function getMoreComments() {

            if (!controller.isAllCommentsLoaded) {
                var alreadyLoadedCommentsCount = controller.comments.length;
                project.getComments(controller.model.id, alreadyLoadedCommentsCount, function(response) {
                    for (var i = 0; i < response.list.length; i++) {
                        controller.comments.push(response.list[i]);
                    }
                    controller.isAllCommentsLoaded = response.allElementsLoaded;
                });
            }
        }

        controller.update = function() {
            crudHelper.update(
                controller.model,
                project,
                '/project/' + $routeParams.id
            );
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $route.reload();
        };

        controller.pass = function() {
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

            //if (!controller.model.id_sales) {
                // user id is not set, get user id from autocomplete selected object (if object selected)
                //if ((typeof controller.sales == 'object') && (controller.sales !== null)) {
                    controller.model.id_sales = controller.salesObject.id;
                //}
            //}

            crudHelper.update(
                controller.model,
                project,
                '/projects/'
            )
        };

        controller.addDeveloper = function() {
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

            controller.developerModel.id_developer = controller.developerObject.id;
            controller.developerModel.rate = controller.developerObject.profile.external_hourly_rate;
            controller.developerModel.currency_id = controller.developerObject.profile.external_hourly_rate_currency_id;
            project.addDeveloper(controller.developerModel, function(response) {});
            controller.developerObject = null;
            $route.reload();
        };

        controller.openCreateCommentModal = function() {
            controller.commentModalScenario = 'create';
            controller.newCommentModel.message = '';
            $('#commentModal').modal('show');
        };

        controller.openUpdateCommentModal = function(comment) {
            controller.editableCommentModel.id = comment.id;
            controller.editableCommentModel.message = comment.message;
            controller.editableCommentObject = comment;
            controller.commentModalScenario = 'update';
            $('#commentModal').modal('show');
        };

        controller.createComment = function() {
            project.addComment(controller.newCommentModel, function(response) {
                if (response.success) {
                    response.model.updated_at = response.model.created_at;
                    controller.comments.unshift(response.model);
                } else {
                    notification.alert('Creating error', false);
                }
                $('#commentModal').modal('hide');
            });
        };

        controller.updateComment = function() {
            project.updateComment(controller.editableCommentModel, function(response) {
                if (response.success) {
                    controller.editableCommentObject.message = response.model.message;
                    controller.editableCommentObject.updated_at = response.model.updated_at;
                } else {
                    notification.alert('Updating error', false);
                }
                $('#commentModal').modal('hide');
            });
        };

        controller.deleteComment = function(id) {
            notification.confirm(function() {
                project.deleteComment(id, function(response) {
                    if (response.success) {
                        // delete comment from list
                        for (var i = 0; i < controller.comments.length; i++) {
                            var comment = controller.comments[i];
                            if (comment.id == id) {
                                controller.comments.splice(i, 1);
                            }
                        }
                    } else {
                        notification.alert('Error occurred while deleting', false);
                    }
                });
            }, {
                closeOnConfirm: true
            });
        };

        function getKey(object, value){
            for(var key in object){
                if(object[key] == value){
                    return key;
                }
            }
            return null;
        }
    }
]);