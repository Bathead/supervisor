svApp.controller('projectArchivedController', [
    '$http',
    '$scope',
    'project',
    'technology',
    'auth',
    'crudHelper',
    'localizationHelper',
    'notification',
    function(
        $http,
        $scope,
        project,
        technology,
        auth,
        crudHelper,
        localizationHelper,
        notification
    ) {

        var controller = this;

        auth.makeForCurrentUser(function(user) {
            controller.loggedUserId = user.id;
            controller.authPermissions = user.permissions;
        });

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Projects',
                url: '/projects',
                active: false
            },
            {
                title: 'In archive',
                active: true
            }
        ];
        $scope.mainController.title = 'Archived projects';

        controller.tableParams = crudHelper.generateCustomTable({}, {}, project, controller, project.getArchived);

        controller.getProjectStatusesFilter = project.getProjectStatusesFilter;
        controller.getPaymentMethodsFilter = project.getPaymentMethodsFilter;

        controller.getTechnologiesFilter = technology.getFilter;

        var restoreOptions = {
            title: localizationHelper.translate("Activate project"),
            text: localizationHelper.translate("Are you sure that you want to restore the project?"),
            confirmButtonText: localizationHelper.translate("Activate")
        };

        controller.restore = function(id) {
            notification.confirm(function () {
                project.restore(id, function (response) {
                    var message = '';
                    notification.alert(message, response.success);
                    controller.tableParams.reload();
                });
            }, restoreOptions)
        };


        controller.delete = function(id) {
            crudHelper.delete(id, project, controller);
        };
    }
]);