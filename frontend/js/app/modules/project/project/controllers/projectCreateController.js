svApp.controller('projectCreateController', [
    '$http',
    '$scope',
    'project',
    'technology',
    'localizationHelper',
    'crudHelper',
    'componentsInit',
    'ACTIONS',
    function(
        $http,
        $scope,
        project,
        technology,
        localizationHelper,
        crudHelper,
        componentsInit,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.CREATE;
        controller.model = {};
        controller.model.payment_method = 1; //default value
        controller.model.status = 1; //default value

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Projects',
                url: '/projects',
                active: false
            },
            {
                title: 'Create',
                active: true
            }
        ];
        $scope.mainController.title = 'Create new project';

        technology.getAll(function(response) {
            controller.technologiesList = response.list;
        });

        initComponents();

        project.getTitles(function(response) {
            controller.titles = response.list;
            componentsInit.initTypeahead(
                "title",
                controller.titles
            )
        });

        project.getClients(function(response) {
            controller.clients = response.list;
            componentsInit.initTypeahead(
                "client",
                controller.clients
            )
        });

        controller.create = function() {
            controller.model.files = $scope.files;

            crudHelper.create(
                controller.model,
                project,
                '/projects'
            )
        };

        project.getPaymentMethods(function(response) {
            controller.paymentMethods = response.list;
        });

        project.getProjectStatuses(function(response) {
            controller.statuses = response.list;
        });

        function initComponents() {
            componentsInit.initSelect();
            $(document).ready(function() {
                $("#files").filestyle({
                    placeholder: localizationHelper.translate('Attach files'),
                    buttonText: localizationHelper.translate('Select')
                });
            });
        };
    }
]);