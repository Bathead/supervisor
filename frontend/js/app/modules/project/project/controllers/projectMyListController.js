svApp.controller('projectMyListController', [
    '$http',
    '$scope',
    'project',
    'technology',
    'crudHelper',
    'localizationHelper',
    'notification',
    '$location',
    '$route',
    function(
        $http,
        $scope,
        project,
        technology,
        crudHelper,
        localizationHelper,
        notification,
        $location,
        $route
    ) {

        var controller = this;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Projects',
                url: '/projects',
                active: false
            },
            {
                title: 'My projects',
                active: true
            }
        ];
        $scope.mainController.title = 'My projects';

        controller.tableParams = crudHelper.generateCustomTable({}, {}, project, controller, project.getMyProjects);

        controller.getProjectStatusesFilter = project.getProjectStatusesFilter;
        controller.getPaymentMethodsFilter = project.getPaymentMethodsFilter;

        controller.getTechnologiesFilter = technology.getFilter;

        var moveToArchiveOptions = {
            title: localizationHelper.translate("Are you sure?"),
            text: localizationHelper.translate("You can restore that item later"),
            confirmButtonText: localizationHelper.translate("To archive")
        };

        controller.moveToArchive = function(id) {
            notification.confirm(function () {
                //$location.path('/projects');
                project.moveToArchive(id, function (response) {
                    var message = '';
                    notification.alert(message, response.success);
                });
                $route.reload();
            }, moveToArchiveOptions)
        };
    }
]);