svApp.controller('projectListController', [
    '$http',
    '$scope',
    'project',
    'technology',
    'crudHelper',
    'auth',
    'localizationHelper',
    'notification',
    '$location',
    '$route',
    function(
        $http,
        $scope,
        project,
        technology,
        crudHelper,
        auth,
        localizationHelper,
        notification,
        $location,
        $route
    ) {

        var controller = this;

        auth.makeForCurrentUser(function(user) {
            controller.loggedUserId = user.id;
            controller.authPermissions = user.permissions;
        });

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Projects',
                active: true
            }
        ];
        $scope.mainController.title = 'Projects';

        controller.tableParams = crudHelper.generateTable({}, {}, project, controller);

        controller.getProjectStatusesFilter = project.getProjectStatusesFilter;
        controller.getPaymentMethodsFilter = project.getPaymentMethodsFilter;

        controller.getTechnologiesFilter = technology.getFilter;

        var moveToArchiveOptions = {
            title: localizationHelper.translate("Are you sure?"),
            text: localizationHelper.translate("You can restore that item later"),
            confirmButtonText: localizationHelper.translate("To archive")
        };

        controller.moveToArchive = function(id) {
            notification.confirm(function () {
                //$location.path('/projects');
                project.moveToArchive(id, function (response) {
                    var message = '';
                    notification.alert(message, response.success);
                });
                $route.reload();
            }, moveToArchiveOptions)
        };
    }
]);