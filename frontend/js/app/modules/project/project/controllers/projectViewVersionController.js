svApp.controller('projectViewVersionController', [
    '$routeParams',
    '$scope',
    'project',
    function(
        $routeParams,
        $scope,
        project
    ) {

        var controller = this;

        controller.oldModel = {};
        controller.originalModel = {};

        project.getEntity($routeParams.id, function(data){
            controller.originalModel = data.entity;
            controller.originalModel.technologiesAsString = getTechnologiesAsString(controller.originalModel.technologies);
            project.getVersion($routeParams.id, $routeParams.version, function(data) {
                controller.oldModel = data.entity;
                controller.oldModel.technologiesAsString = getTechnologiesAsString(controller.oldModel.technologies);
                controller.initBreadcrumbs();
            });
        });

        function getTechnologiesAsString(technologiesArray){
            var result = "";
            technologiesArray.forEach(function(technology, i){
                result += (result.length == 0) ? technology.name : "," + technology.name;
            });
            return result;
        }

        project.getProjectStatuses(function(response){
            var result = {};
            for (var i = 0; i < response.list.length; i++) {
                var status = response.list[i];
                result[status.title.replace(/ /g,"_")] = status.id;
            }
            controller.statuses = result;
        });

        controller.initBreadcrumbs = function() {
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: 'Projects',
                    url: '/projects',
                    active: false
                },
                {
                    title: controller.originalModel.title + ' (№ ' + controller.originalModel.id + ')',
                    url: '/project/' + controller.originalModel.id,
                    active: false
                },
                {
                    title: 'Versions',
                    url: '/project/' + controller.originalModel.id +  '/versions',
                    active: false
                },
                {
                    title: controller.oldModel.version,
                    active: true
                }
            ];
            $scope.mainController.title = controller.originalModel.title;
        };
    }
]);