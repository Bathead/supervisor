svApp.controller('projectVersionsController', [
    '$http',
    '$scope',
    'project',
    'technology',
    'crudHelper',
    'localizationHelper',
    '$routeParams',
    function(
        $http,
        $scope,
        project,
        technology,
        crudHelper,
        localizationHelper,
        $routeParams
    ) {

        var controller = this;

        project.getEntity($routeParams.id, function(response){
            controller.model = response.entity;
            initBreadcrumbs();
        });

        function initBreadcrumbs(){
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: 'Projects',
                    url: '/projects',
                    active: false
                },
                {
                    title: controller.model.title + ' (№ ' + $routeParams.id + ')',
                    url: '/project/' + $routeParams.id,
                    active: false
                },
                {
                    title: 'Versions',
                    active: true
                }
            ];
            $scope.mainController.title = localizationHelper.translate("Versions of the project") + " - " + controller.model.title;
        }

        controller.tableParams = crudHelper.generateCustomTable({}, {}, project, controller, project.getVersions, $routeParams.id);

        controller.getProjectStatusesFilter = project.getProjectStatusesFilter;
        controller.getPaymentMethodsFilter = project.getPaymentMethodsFilter;

        controller.getTechnologiesFilter = technology.getFilter;
    }
]);