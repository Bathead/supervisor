svApp.controller('ticketCreateController', [
    '$scope',
    'ticket',
    'user',
    'localizationHelper',
    'crudHelper',
    'componentsInit',
    'ACTIONS',
    function(
        $scope,
        ticket,
        user,
        localizationHelper,
        crudHelper,
        componentsInit,
        ACTIONS
    ) {
        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.CREATE;
        controller.model = {};
        controller.receiver = null;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Tickets',
                url: '/sentTickets',
                active: false
            },
            {
                title: 'Create',
                active: true
            }
        ];
        $scope.mainController.title = 'Create new ticket';

        initComponents();

        controller.create = function() {
            controller.model.file = $scope.file;

            if (!controller.model.receiver_id) {
                // user id is not set, get user id from autocomplete selected object (if object selected)
                if ((typeof controller.receiver == 'object') && (controller.receiver !== null)) {
                    controller.model.receiver_id = controller.receiver.id;
                }
            }

            crudHelper.create(
                controller.model,
                ticket,
                '/sentTickets'
            )
        };


        // get user for typeahead (in add new user modal)
        controller.getOtherUsersByName = function(name) {
            return user.getOtherUsersByName(name, function(response){
                return response.data.list;
            })
        };

        ticket.getTicketPriorities(function(response) {
            controller.priorities = response.list;
        });

        function initComponents() {
            $(document).ready(function() {
                $("#file").filestyle({
                    placeholder: localizationHelper.translate('Attach File'),
                    buttonText: localizationHelper.translate('Select')
                });
            });
        }
    }
]);