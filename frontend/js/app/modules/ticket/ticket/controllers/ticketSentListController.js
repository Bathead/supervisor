svApp.controller('ticketSentListController', [
    '$http',
    '$scope',
    'ticket',
    'crudHelper',
    function(
        $http,
        $scope,
        ticket,
        crudHelper
    ) {

        var controller = this;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Sent tickets',
                active: true
            }
        ];
        $scope.mainController.title = 'Sent tickets';

        controller.tableParams = crudHelper.generateCustomTable({}, {}, ticket, controller, ticket.getSent);

        controller.getTicketStatusesFilter = ticket.getTicketStatusesFilter;
        controller.getTicketPrioritiesFilter = ticket.getTicketPrioritiesFilter;

        controller.delete = function(id) {
            crudHelper.delete(id, ticket, controller);
        };
    }
]);