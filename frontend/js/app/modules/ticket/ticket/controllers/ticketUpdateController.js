svApp.controller('ticketUpdateController', [
    '$routeParams',
    '$scope',
    '$http',
    'ticket',
    'user',
    'localizationHelper',
    'crudHelper',
    'notification',
    'componentsInit',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        $http,
        ticket,
        user,
        localizationHelper,
        crudHelper,
        notification,
        componentsInit,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.UPDATE;
        controller.model = {};
        controller.receiver = null;
        controller.deletedFile = null;

        controller.initBreadcrumbs = function() {
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: 'Sent tickets',
                    url: '/sentTickets',
                    active: false
                },
                {
                    title: controller.model.title,
                    url: '/ticket/' + controller.model.id,
                    active: false
                },
                {
                    title: 'Update',
                    active: true
                }
            ];
            $scope.mainController.title = 'Update ticket';
        };

        ticket.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.initBreadcrumbs();
            //get attached file path
            if (controller.model.file_path) {
                controller.downloadLink = "/backend/web/uploads/tickets/" + controller.model.id + "/" + controller.model.file_path;
            }

        });

        initComponents();

        controller.update = function() {
            controller.model.file = $scope.file;

            // user id is not set, get user id from autocomplete selected object (if object selected)
            if ((typeof controller.receiver == 'object') && (controller.receiver !== null)) {
                controller.model.receiver_id = controller.receiver.id;
            }

            if (controller.deletedFile  == true) {
                $http.post('/api/ticket/delete-file/' + $routeParams.id);
            }

            crudHelper.update(
                controller.model,
                ticket,
                '/sentTickets'
            )
        };

        // get user for typeahead (in add new user modal)
        controller.getOtherUsersByName = function(name) {
            return user.getOtherUsersByName(name, function(response){
                return response.data.list;
            })
        };

        controller.deleteFile = function(id) {
            //notification.confirm(function () {
                //controller.downloadLink = null;
                //notification.alert("Deleted successfully", true);
                //return $http.post('/api/ticket/delete-file/' + id);
            //})
            controller.downloadLink = null;
            controller.deletedFile = true;
        };

        ticket.getTicketPriorities(function(response) {
            controller.priorities = response.list;
        });

        function initComponents() {
            $(document).ready(function() {
                $("#file").filestyle({
                    placeholder: localizationHelper.translate('Attach File'),
                    buttonText: localizationHelper.translate('Select')
                });
            });
        }

    }
]);