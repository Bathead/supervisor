svApp.controller('ticketViewController', [
    '$routeParams',
    '$scope',
    'ticket',
    'crudHelper',
    'notification',
    'menuBuilder',
    'localizationHelper',
    'CONFIG',
    'auth',
    '$location',
    '$route',
    function(
        $routeParams,
        $scope,
        ticket,
        crudHelper,
        notification,
        menuBuilder,
        localizationHelper,
        CONFIG,
        auth,
        $location,
        $route
    ) {

        var controller = this;
        controller.action = 'View';
        controller.model = {};

        controller.comments = [];
        controller.allCommentsLoaded = false;

        controller.newCommentModel = {
            ticket_id : $routeParams.id
        };

        controller.editableCommentModel = {};
        controller.editableCommentObject = {}; // link to editable commment

        auth.makeForCurrentUser(function(user) {
            controller.loggedUserId = user.id;
            controller.authPermissions = user.permissions;
        });

        controller.initBreadcrumbs = function() {
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: (controller.loggedUserId == controller.model.author_id) ? 'Sent tickets' : 'Received tickets',
                    url: (controller.loggedUserId == controller.model.author_id) ? '/sentTickets' : '/receivedTickets',
                    active: false
                },
                {
                    title: controller.model.title,
                    active: true
                }
            ];
            $scope.mainController.title = localizationHelper.translate("Ticket") + " - " + controller.model.title;
        };

        ticket.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.initBreadcrumbs();

            //get attached file path
            if (controller.model.file_path) {
                controller.downloadLink = "/backend/web/uploads/tickets/" + controller.model.id + "/" + controller.model.file_path;
            }

            getMoreComments();
        });

        // load more tickets comments, when page scrolled to bottom
        $(document).on('scroll', function () {
            if($(window).scrollTop() + $(window).height() == $(document).height()) {
                getMoreComments();
            }
        });

        function getMoreComments() {
            if (!controller.allCommentsLoaded) {
                var alreadyLoadedCommentsCount = controller.comments.length;
                ticket.getComments(controller.model.id, alreadyLoadedCommentsCount, function(response) {
                    for (var i = 0; i < response.list.length; i++) {
                        controller.comments.push(response.list[i]);
                    }
                    controller.allCommentsLoaded = response.allElementsLoaded;
                });
            }
        }

        controller.update = function() {
            crudHelper.update(
                controller.model,
                ticket,
                '/ticket/' + $routeParams.id
            );
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $route.reload();
        };

        ticket.getTicketStatuses(function(response) {
            var result = {};
            for (var i = 0; i < response.list.length; i++) {
                var status = response.list[i];
                result[status.title.replace(/ /g, "_")] = status.id;
            }
            controller.statuses = result;
        });

        ticket.getTicketPriorities(function(response) {
            var result = {};
            for (var i = 0; i < response.list.length; i++) {
                var priority = response.list[i];
                result[priority.title.replace(/ /g, "_")] = status.id;
            }
            controller.priorities = result;
        });

        var startOptions = {
            title: localizationHelper.translate("Start ticket"),
            text: localizationHelper.translate("Are you sure that you want to sign ticket as In progress?"),
            confirmButtonText: localizationHelper.translate("Start")
        };

        var markAsDoneOptions = {
            title: localizationHelper.translate("Mark as done"),
            text: localizationHelper.translate("Are you sure that you want to sign ticket as Waiting verification?"),
            confirmButtonText: localizationHelper.translate("OK")
        };

        var confirmExecutionOptions = {
            title: localizationHelper.translate("Confirm execution"),
            text: localizationHelper.translate("Are you sure that you want to sign ticket as Closed?"),
            confirmButtonText: localizationHelper.translate("OK")
        };

        controller.changeStatus = function(status) {
            var options = null;
            switch (status) {
                case controller.statuses.In_progress:
                    options = startOptions;
                    break;
                case controller.statuses.Waiting_verification:
                    options = markAsDoneOptions;
                    break;
                case controller.statuses.Closed:
                    options = confirmExecutionOptions;
                    break;
            }

            notification.confirm(function () {
                var data = {
                    status: status
                };
                ticket.changeStatus(controller.model.id, data, function (response) {
                    notification.alert('', response.success);
                    menuBuilder.updateCounters();
                });
                $route.reload();
            }, options)
        };

        controller.openCreateCommentModal = function() {
            controller.modalScenario = 'create';
            controller.newCommentModel.message = '';
            $('#commentModal').modal('show');
        };

        controller.openUpdateCommentModal = function(comment) {
            controller.editableCommentModel.id = comment.id;
            controller.editableCommentModel.message = comment.message;
            controller.editableCommentObject = comment;
            controller.modalScenario = 'update';
            $('#commentModal').modal('show');
        };

        controller.createNewComment = function() {
            ticket.addComment(controller.newCommentModel, function(response) {
                if (response.success) {
                    response.model.updated_at = response.model.created_at;
                    controller.comments.unshift(response.model);
                } else {
                    notification.alert('Creating error', false);
                }
                $('#commentModal').modal('hide');
            });
        };

        controller.updateOldComment = function() {
            ticket.updateComment(controller.editableCommentModel, function(response) {
                if (response.success) {
                    controller.editableCommentObject.message = response.model.message;
                    controller.editableCommentObject.updated_at = response.model.updated_at;
                } else {
                    notification.alert('Updating error', false);
                }
                $('#commentModal').modal('hide');
            });
        };

        controller.deleteComment = function(id) {
            notification.confirm(function() {
                ticket.deleteComment(id, function(response) {
                    if (response.success) {
                        // delete comment from list
                        for (var i = 0; i < controller.comments.length; i++) {
                            var comment = controller.comments[i];
                            if (comment.id == id) {
                                controller.comments.splice(i, 1);
                            }
                        }
                    } else {
                        notification.alert('Error occurred while deleting', false);
                    }
                });
            }, {
                closeOnConfirm: true
            });
        };

        controller.delete = function() {
            notification.confirm(function () {
                $location.path('/sentTickets');
                ticket.delete(controller.model.id, function (response) {
                    var message = '';
                    notification.alert(message, response.success);
                });
            })
        };
    }
]);