svApp.controller('ticketReceivedListController', [
    '$http',
    '$scope',
    'ticket',
    'crudHelper',
    function(
        $http,
        $scope,
        ticket,
        crudHelper
    ) {

        var controller = this;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Received tickets',
                active: true
            }
        ];
        $scope.mainController.title = 'Received tickets';

        controller.tableParams = crudHelper.generateCustomTable({}, {}, ticket, controller, ticket.getReceived);

        controller.getTicketStatusesFilter = ticket.getTicketStatusesFilter;
        controller.getTicketPrioritiesFilter = ticket.getTicketPrioritiesFilter;

        controller.delete = function(id) {
            crudHelper.delete(id, ticket, controller);
        };
    }
]);