// for CrudHelper support model must contain next methods:
// - getList
// - getEntity
// - create
// - update
// - delete

svApp.factory('ticket', [
    '$http',
    'requestHelper',
    '$q',
    'localizationHelper',
    function(
        $http,
        requestHelper,
        $q,
        localizationHelper
    ) {
        var ticket = {
            getSent : function(page, count, filter, sorting, successCallback) {
                var searchQuery = requestHelper.formatSearchRequest('Ticket', page, count, filter, sorting);
                return $http.get('/api/ticket/get-sent-tickets?' + searchQuery).success(successCallback);
            },

            getReceived : function(page, count, filter, sorting, successCallback) {
                var searchQuery = requestHelper.formatSearchRequest('Ticket', page, count, filter, sorting);
                return $http.get('/api/ticket/get-received-tickets?' + searchQuery).success(successCallback);
            },

            getLatestReceived: function(count, successCallback){
                var data = {
                    count: count
                };
                return $http.post('/api/ticket/get-latest-received-tickets', data).success(successCallback);
            },

            getEntity : function(id, successCallback) {
                return $http.get('/api/ticket/get-ticket/' + id).success(successCallback);
            },

            getCountOpenedReceived : function(successCallback){
                return $http.get('/api/ticket/get-count-opened-received').success(successCallback);
            },

            create : function(entity, successCallback) {
                var data = {
                    Ticket: {}
                };

                angular.copy(entity, data.Ticket);
                delete data.Ticket.file;

                var formData = new FormData();
                formData.append('data', JSON.stringify(data));
                requestHelper.addSingleFileToFormData(formData, entity.file);

                return $http.post(
                    '/api/ticket/create',
                    formData,
                    {
                        headers: {
                            'Content-Type': undefined
                        }
                    }).success(successCallback);
            },

            update : function(entity, successCallback) {
                var data = {
                    Ticket: {}
                };

                angular.copy(entity, data.Ticket);
                delete data.Ticket.file;

                var formData = new FormData();
                formData.append('data', JSON.stringify(data));
                requestHelper.addSingleFileToFormData(formData, entity.file);

                return $http.post(
                    '/api/ticket/update/' + entity.id,
                    formData,
                    {
                        headers: {
                            'Content-Type': undefined
                        }
                    }).success(successCallback);
            },

            delete : function(id, successCallback) {
                return $http.post('/api/ticket/delete/' + id).success(successCallback);
            },

            getComments : function(ticketId, offset, successCallback) {
                var data = {
                    ticketId: ticketId,
                    offset: offset
                };
                return $http.post('/api/ticket/get-ticket-comments', data).success(successCallback);
            },

            addComment : function(entity, successCallback) {
                var data = {
                    TicketComment: entity
                };
                return $http.post('/api/ticket/add-comment', data).success(successCallback);
            },

            updateComment : function(entity, successCallback) {
                var data = {
                    TicketComment: entity
                };
                return $http.post('/api/ticket/update-comment/' + entity.id, data).success(successCallback);
            },

            deleteComment : function(id, successCallback) {
                return $http.post('/api/ticket/delete-comment/' + id).success(successCallback);
            },

            getTicketStatuses : function(successCallback) {
                return $http.get('/api/ticket/get-ticket-statuses').success(successCallback);
            },

            getTicketStatusesFilter : function() {
                var def = $q.defer(),
                    result = [];
                ticket.getTicketStatuses(function(response){
                    for (var index in response.list) {
                        result.push({
                            id : response.list[index].id,
                            title : localizationHelper.translate(response.list[index].title)
                        })
                    }
                });
                def.resolve(result);
                return def;
            },

            getTicketPriorities : function(successCallback) {
                return $http.get('/api/ticket/get-ticket-priorities').success(successCallback);
            },

            getTicketPrioritiesFilter : function() {
                var def = $q.defer(),
                    result = [];
                ticket.getTicketPriorities(function(response){
                    for (var index in response.list) {
                        result.push({
                            id : response.list[index].id,
                            title : localizationHelper.translate(response.list[index].title)
                        })
                    }
                });
                def.resolve(result);
                return def;
            },

            changeStatus: function(id, data, successCallback){
                return $http.post('/api/ticket/change-status/' + id, data).success(successCallback);
            }
        };

        return ticket;
    }
]);