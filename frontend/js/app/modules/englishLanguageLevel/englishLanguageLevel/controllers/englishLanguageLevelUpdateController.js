svApp.controller('englishLanguageLevelUpdateController', [
    '$routeParams',
    '$scope',
    'englishLanguageLevel',
    'crudHelper',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        englishLanguageLevel,
        crudHelper,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.UPDATE;
        controller.model = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'English Language Levels',
                url: '/englishLanguageLevels',
                active: false
            },
            {
                title: 'Update',
                active: true
            }
        ];
        $scope.mainController.title = 'Update english language level';

        englishLanguageLevel.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
        });

        controller.update = function() {
            crudHelper.update(
                controller.model,
                englishLanguageLevel,
                '/englishLanguageLevels'
            );
        };

    }
]);
