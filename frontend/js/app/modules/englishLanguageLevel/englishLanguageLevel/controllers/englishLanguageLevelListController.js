svApp.controller('englishLanguageLevelListController', [
    '$http',
    '$scope',
    'englishLanguageLevel',
    'crudHelper',
    function(
        $http,
        $scope,
        englishLanguageLevel,
        crudHelper
    ) {

        var controller = this;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'English Language Levels',
                active: true
            }
        ];
        $scope.mainController.title = 'English Language Levels';

        controller.tableParams = crudHelper.generateTable({}, {}, englishLanguageLevel, controller);

        controller.delete = function(id) {
            crudHelper.delete(id, englishLanguageLevel, controller);
        };
    }
]);
