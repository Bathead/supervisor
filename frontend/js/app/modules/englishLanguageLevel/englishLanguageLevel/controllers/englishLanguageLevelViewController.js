svApp.controller('englishLanguageLevelViewController', [
    '$routeParams',
    '$scope',
    'englishLanguageLevel',
    'localizationHelper',
    function(
        $routeParams,
        $scope,
        englishLanguageLevel,
        localizationHelper
    ) {

        var controller = this;
        controller.model = {};

        englishLanguageLevel.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.initBreadcrumbs();
        });

        englishLanguageLevel.getStatistic($routeParams.id, function (response) {
            controller.statistic = response;
        });

        controller.initBreadcrumbs = function() {
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: 'English Language Levels',
                    url: '/englishLanguageLevels',
                    active: false
                },
                {
                    title: controller.model.name,
                    active: true
                }
            ];
            $scope.mainController.title = localizationHelper.translate("English language level") + " - " + controller.model.name;
        };
    }
]);
