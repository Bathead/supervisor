svApp.controller('englishLanguageLevelCreateController', [
    '$scope',
    'englishLanguageLevel',
    'crudHelper',
    'ACTIONS',
    function(
        $scope,
        englishLanguageLevel,
        crudHelper,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.CREATE;
        controller.model = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'English Language Levels',
                url: '/englishLanguageLevels',
                active: false
            },
            {
                title: 'Create',
                active: true
            }
        ];
        $scope.mainController.title = 'Create new english language level';

        controller.create = function() {
            crudHelper.create(
                controller.model,
                englishLanguageLevel,
                '/englishLanguageLevels'
            );
        };
    }
]);