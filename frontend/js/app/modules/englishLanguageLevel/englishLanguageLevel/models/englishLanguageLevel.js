svApp.factory('englishLanguageLevel', [
    '$http',
    'requestHelper',
    function(
        $http,
        requestHelper
    ) {
        return {
            getList : function(page, count, filter, sorting, successCallback) {
                var searchQuery = requestHelper.formatSearchRequest('EnglishLanguageLevel', page, count, filter, sorting);
                return $http.get('/api/englishLanguageLevel/get-levels?' + searchQuery).success(successCallback);
            },

            getAll : function(successCallback) {
                return $http.get('/api/englishLanguageLevel/get-all-levels' ).success(successCallback);
            },

            getEntity : function(id, successCallback) {
                return $http.get('/api/englishLanguageLevel/get-level/' + id).success(successCallback);
            },

            create : function(model, successCallback) {
                var data = {
                    EnglishLanguageLevel: model
                };
                return $http.post('/api/englishLanguageLevel/create', data).success(successCallback);
            },

            update : function(model, successCallback) {
                var data = {
                    EnglishLanguageLevel: model
                };
                return $http.post('/api/englishLanguageLevel/update/' + model.id, data).success(successCallback);
            },

            delete : function(id, successCallback) {
                return $http.post('/api/englishLanguageLevel/delete/' + id).success(successCallback);
            },

            getStatistic : function(id, successCallback){
                return $http.post('/api/englishLanguageLevel/get-statistic/' + id).success(successCallback);
            }
        }
    }
]);
