svApp.controller('departmentCreateController', [
    '$scope',
    'department',
    'user',
    'crudHelper',
    'componentsInit',
    'ACTIONS',
    function(
        $scope,
        department,
        user,
        crudHelper,
        componentsInit,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.CREATE;
        controller.model = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Departments',
                url: '/departments',
                active: false
            },
            {
                title: 'Create',
                active: true
            }
        ];
        $scope.mainController.title = 'Create new department';

        controller.create = function() {
            crudHelper.create(
                controller.model,
                department,
                '/departments'
            );
        };
    }
]);
