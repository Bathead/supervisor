svApp.controller('departmentUpdateController', [
    '$routeParams',
    '$scope',
    'department',
    'user',
    'crudHelper',
    'componentsInit',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        department,
        user,
        crudHelper,
        componentsInit,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.UPDATE;
        controller.model = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Departments',
                url: '/departments',
                active: false
            },
            {
                title: 'Update',
                active: true
            }
        ];
        $scope.mainController.title = 'Update department';

        department.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.headAsObject = controller.model.head;
        });

        // get users for typeahead
        controller.getUsersByName = function(name) {
            return department.getUsersByName(controller.model.id, name, function(response){
                return response.data.list;
            });
        };

        controller.update = function() {
            if(typeof controller.headAsObject == "object" && controller.headAsObject != null)
                controller.model.head_id = controller.headAsObject.id;

            crudHelper.update(
                controller.model,
                department,
                '/departments'
            );
        };

    }
]);