svApp.controller('departmentViewController', [
    '$routeParams',
    '$scope',
    'department',
    'user',
    'localizationHelper',
    function(
        $routeParams,
        $scope,
        department,
        user,
        localizationHelper
    ) {

        var controller = this;

        controller.model = {};

        department.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.initBreadcrumbs();
        });

        department.getStatistic($routeParams.id, function(response) {
            controller.statistic = response;
        });

        user.getEmploymentStatuses(function(response){
            var result = {};
            for (var i = 0; i < response.list.length; i++) {
                var status = response.list[i];
                result[status.title] = status.id;
            }
            controller.employmentStatuses = result;
        });

        controller.initBreadcrumbs = function() {
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: 'Departments',
                    url: '/departments',
                    active: false
                },
                {
                    title: controller.model.name,
                    active: true
                }
            ];
            $scope.mainController.title = localizationHelper.translate("Department") + " - " + controller.model.name;
        };


    }
]);