svApp.controller('departmentListController', [
    '$http',
    '$scope',
    'department',
    'crudHelper',
    function(
        $http,
        $scope,
        department,
        crudHelper
    ) {

        var controller = this;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Departments',
                active: true
            }
        ];
        $scope.mainController.title = 'Departments';

        controller.tableParams = crudHelper.generateTable({}, {}, department, controller);

        controller.delete = function(id) {
            crudHelper.delete(id, department, controller);
        };
    }
]);