// for CrudHelper support model must contain next methods:
// - getList
// - getEntity
// - create
// - update
// - delete

svApp.factory('department', [
    '$http',
    'requestHelper',
    '$q',
    function(
        $http,
        requestHelper,
        $q
    ) {
        var department = {
            // get departments with pagination, filtration, sotring
            getList : function(page, count, filter, sorting, successCallback) {
                var searchQuery = requestHelper.formatSearchRequest('Department', page, count, filter, sorting);
                return $http.get('/api/department/get-departments?' + searchQuery).success(successCallback);
            },

            // get all departments (without pagination, filter, sorting)
            getAll : function(successCallback) {
                return $http.get('/api/department/get-all-departments' ).success(successCallback);
            },

            getUsersByName : function(departmentId, name, successCallback) {
                var data = {
                    departmentId: departmentId,
                    name : name
                };
                return $http.post('/api/department/get-users-by-name', data).then(successCallback);
            },

            // return departments for ng-table select filter
            getFilter : function() {
                var def = $q.defer(),
                    result = [];
                department.getAll(function(response) {
                    for (var index in response.list) {
                        result.push({
                            id : response.list[index].id,
                            title : response.list[index].name
                        })
                    }
                });
                def.resolve(result);
                return def;
            },

            getEntity : function(id, successCallback) {
                return $http.get('/api/department/get-department/' + id).success(successCallback);
            },

            create : function(entity, successCallback) {
                var data = {
                    Department: entity
                };
                return $http.post('/api/department/create', data).success(successCallback);
            },

            update : function(entity, successCallback) {
                var data = {
                    Department: entity
                };
                return $http.post('/api/department/update/' + entity.id, data).success(successCallback);
            },

            delete : function(id, successCallback) {
                return $http.post('/api/department/delete/' + id).success(successCallback);
            },

            getStatistic : function(id, successCallback) {
                return $http.get('/api/department/get-statistic/' + id).success(successCallback);
            }
        };
        return department;
    }
]);