svApp.controller('technologyCreateController', [
    '$scope',
    'technology',
    'crudHelper',
    'ACTIONS',
    function(
        $scope,
        technology,
        crudHelper,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.CREATE;
        controller.model = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Technologies',
                url: '/technologies',
                active: false
            },
            {
                title: 'Create',
                active: true
            }
        ];
        $scope.mainController.title = 'Create new technology';

        controller.create = function() {
            crudHelper.create(
                controller.model,
                technology,
                '/technologies'
            )
        };
    }
]);