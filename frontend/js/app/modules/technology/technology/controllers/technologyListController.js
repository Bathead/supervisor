svApp.controller('technologyListController', [
    '$http',
    '$scope',
    'technology',
    'crudHelper',
    function(
        $http,
        $scope,
        technology,
        crudHelper
    ) {

        var controller = this;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Technologies',
                active: true
            }
        ];
        $scope.mainController.title = 'Technologies';

        controller.tableParams = crudHelper.generateTable({}, {}, technology, controller);

        controller.delete = function(id) {
            crudHelper.delete(id, technology, controller);
        };
    }
]);