svApp.controller('technologyUpdateController', [
    '$routeParams',
    '$scope',
    'technology',
    'crudHelper',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        technology,
        crudHelper,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.UPDATE;
        controller.model = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Technologies',
                url: '/technologies',
                active: false
            },
            {
                title: 'Update',
                active: true
            }
        ];
        $scope.mainController.title = 'Update technology';

        technology.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
        });

        controller.update = function() {
            crudHelper.update(
                controller.model,
                technology,
                '/technologies'
            )
        };

    }
]);