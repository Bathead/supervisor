svApp.controller('technologyViewController', [
    '$routeParams',
    '$scope',
    'technology',
    'user',
    'localizationHelper',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        technology,
        user,
        localizationHelper,
        ACTIONS
    ) {

        var controller = this;
        controller.action = ACTIONS.VIEW;
        controller.model = {};

        technology.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.initBreadcrumbs();
        });

        technology.getStatistic($routeParams.id, function(response) {
            controller.statistic = response;
        });

        user.getEmploymentStatusesAsObject(function(response){
            var result = {};
            for (var i = 0; i < response.list.length; i++) {
                var status = response.list[i];
                result[status.title] = status.id;
            }
            controller.employmentStatuses = result;
        });

        controller.initBreadcrumbs = function() {
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: 'Technologies',
                    url: '/technologies',
                    active: false
                },
                {
                    title: controller.model.name,
                    active: true
                }
            ];
            $scope.mainController.title = localizationHelper.translate("Technology") + " - " + controller.model.name;
        };


    }
]);