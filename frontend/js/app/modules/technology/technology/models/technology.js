// for CrudHelper support model must contain next methods:
// - getList
// - getEntity
// - create
// - update
// - delete

svApp.factory('technology', [
    '$http',
    'requestHelper',
    '$q',
    function(
        $http,
        requestHelper,
        $q
    ) {
        var technology = {
            getList : function(page, count, filter, sorting, successCallback) {
                var searchQuery = requestHelper.formatSearchRequest('Technology', page, count, filter, sorting);
                return $http.get('/api/technology/get-technologies?' + searchQuery).success(successCallback);
            },

            // get all technologies (without pagination, filter, sorting)
            getAll : function(successCallback) {
                return $http.get('/api/technology/get-all-technologies' ).success(successCallback);
            },

            // return technologies for ng-table select filter
            getFilter : function() {
                var def = $q.defer(),
                    result = [];
                technology.getAll(function(response) {
                    for (var index in response.list) {
                        result.push({
                            id : response.list[index].id,
                            title : response.list[index].name
                        })
                    }
                });
                def.resolve(result);
                return def;
            },

            getEntity : function(id, successCallback) {
                return $http.get('/api/technology/get-technology/' + id).success(successCallback);
            },

            create : function(entity, successCallback) {
                var data = {
                    Technology: entity
                };
                return $http.post('/api/technology/create', data).success(successCallback);
            },

            update : function(entity, successCallback) {
                var data = {
                    Technology: entity
                };
                return $http.post('/api/technology/update/' + entity.id, data).success(successCallback);
            },

            delete : function(id, successCallback) {
                return $http.post('/api/technology/delete/' + id).success(successCallback);
            },

            getStatistic : function(id, successCallback) {
                return $http.get('/api/technology/get-statistic/' + id).success(successCallback);
            }
        };
        return technology;
    }
]);