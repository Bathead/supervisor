svApp.controller('rankViewController', [
    '$routeParams',
    '$scope',
    'rank',
    'localizationHelper',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        rank,
        localizationHelper,
        ACTIONS
    ) {

        var controller = this;
        controller.action = ACTIONS.VIEW;
        controller.model = {};

        rank.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.initBreadcrumbs();
        });

        controller.initBreadcrumbs = function() {
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: 'Ranks',
                    url: '/ranks',
                    active: false
                },
                {
                    title: controller.model.name,
                    active: true
                }
            ];
            $scope.mainController.title = localizationHelper.translate("Rank") + " - " + controller.model.name;
        };
    }
]);