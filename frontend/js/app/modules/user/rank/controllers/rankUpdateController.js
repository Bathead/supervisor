svApp.controller('rankUpdateController', [
    '$routeParams',
    '$scope',
    'rank',
    'crudHelper',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        rank,
        crudHelper,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.UPDATE;
        controller.model = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Ranks',
                url: '/ranks',
                active: false
            },
            {
                title: 'Update',
                active: true
            }
        ];
        $scope.mainController.title = 'Update rank';

        rank.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
        });

        controller.update = function() {
            crudHelper.update(
                controller.model,
                rank,
                '/ranks'
            )
        };

    }
]);