svApp.controller('rankCreateController', [
    '$scope',
    'rank',
    'crudHelper',
    'ACTIONS',
    function(
        $scope,
        rank,
        crudHelper,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.CREATE;
        controller.isNewRecord = true;
        controller.model = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Ranks',
                url: '/ranks',
                active: false
            },
            {
                title: 'Create',
                active: true
            }
        ];
        $scope.mainController.title = 'Create new rank';

        controller.create = function() {
            crudHelper.create(
                controller.model,
                rank,
                '/ranks'
            )
        };
    }
]);