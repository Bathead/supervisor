svApp.controller('rankListController', [
    '$http',
    '$scope',
    'rank',
    'crudHelper',
    function(
        $http,
        $scope,
        rank,
        crudHelper
    ) {

        var controller = this;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Ranks',
                active: true
            }
        ];
        $scope.mainController.title = 'Ranks';

        controller.tableParams = crudHelper.generateTable({}, {}, rank, controller);

        controller.delete = function(id) {
            crudHelper.delete(id, rank, controller);
        };
    }
]);