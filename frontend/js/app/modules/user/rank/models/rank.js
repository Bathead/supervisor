// for CrudHelper support model must contain next methods:
// - getList
// - getEntity
// - create
// - update
// - delete

svApp.factory('rank', [
    '$http',
    'requestHelper',
    '$q',
    function(
        $http,
        requestHelper,
        $q
    ) {
        var rank = {
            getList : function(page, count, filter, sorting, successCallback) {
                var searchQuery = requestHelper.formatSearchRequest('Rank', page, count, filter, sorting);
                return $http.get('/api/rank/get-ranks?' + searchQuery).success(successCallback);
            },

            // get all ranks (without pagination, filter, sorting)
            getAll : function(successCallback) {
                return $http.get('/api/rank/get-all-ranks' ).success(successCallback);
            },

            // return ranks for ng-table select filter
            getFilter : function() {
                var def = $q.defer(),
                    result = [];
                rank.getAll(function(response) {
                    for (var index in response.list) {
                        result.push({
                            id : response.list[index].id,
                            title : response.list[index].name
                        })
                    }
                });
                def.resolve(result);
                return def;
            },

            getEntity : function(id, successCallback) {
                return $http.get('/api/rank/get-rank/' + id).success(successCallback);
            },

            create : function(entity, successCallback) {
                var data = {
                    Rank: entity
                };
                return $http.post('/api/rank/create', data).success(successCallback);
            },

            update : function(entity, successCallback) {
                var data = {
                    Rank: entity
                };
                return $http.post('/api/rank/update/' + entity.id, data).success(successCallback);
            },

            delete : function(id, successCallback) {
                return $http.post('/api/rank/delete/' + id).success(successCallback);
            }
        };

        return rank;
    }
]);