// for CrudHelper support model must contain next methods:
// - getList
// - getEntity
// - create
// - update
// - delete

svApp.factory('user', [
    '$http',
    '$q',
    'localizationHelper',
    'requestHelper',
    function(
        $http,
        $q,
        localizationHelper,
        requestHelper
    ) {
        var user = {
            getList : function(page, count, filter, sorting, successCallback) {
                var searchQuery = requestHelper.formatSearchRequest('User', page, count, filter, sorting);
                return $http.get('/api/user/get-users?' + searchQuery).success(successCallback);
            },

            getAll : function(successCallback){
                return $http.get('/api/user/get-all-users').success(successCallback);
            },

            // get users, where name like %name%
            getOtherUsersByName : function(name, successCallback) {
                var data = {
                    name : name
                };
                return $http.post('/api/user/get-other-users-by-name', data).then(successCallback);
            },

            getAllOther : function(successCallback){
                return $http.get('/api/user/get-all-other-users').success(successCallback);
            },

            getEntity : function(id, successCallback) {
                return $http.get('/api/user/get-user/' + id).success(successCallback);
            },

            getUserStatuses : function(successCallback) {
                return $http.get('/api/user/get-user-statuses').success(successCallback);
            },

            getEmploymentStatuses : function(successCallback) {
                return $http.get('/api/user/get-employment-statuses').success(successCallback);
            },

            getEmploymentStatusesAsObject : function(successCallback) {
                return user.getEmploymentStatuses(function(response) {
                    var result = {};
                    for (var i = 0; i < response.list.length; i++) {
                        var status = response.list[i];
                        result[status.title.replace(/ /g,"_")] = status.id;
                    }
                    response.list = result;
                    successCallback(response);
                });
            },

            // return employment statuses list for ng-table select filter
            getEmploymentStatusesFilter : function() {
                var def = $q.defer(),
                    result = [];
                user.getEmploymentStatuses(function(response) {
                    for (var index in response.list) {
                        result.push({
                            id : response.list[index].id,
                            title : localizationHelper.translate(response.list[index].title)
                        })
                    }
                });
                def.resolve(result);
                return def;
            },

            getUserBillingCards : function(id, successCallback) {
                return $http.get('/api/user/get-user-billing-cards/' + id).success(successCallback);
            },

            create : function(entity, successCallback) {
                var data = {
                    User: {},
                    Profile: {}
                };

                //Object.assign(data.Profile, entity.profile);
                angular.copy(entity, data.User);
                angular.copy(entity.profile, data.Profile);
                delete data.User.photo;
                delete data.User.profile;

                var formData = new FormData();
                formData.append('data', JSON.stringify(data));
                requestHelper.addSingleFileToFormData(formData, entity.photo);

                return $http.post(
                    '/api/user/create',
                    formData,
                    {
                        headers: {
                            'Content-Type': undefined
                        }
                    }).success(successCallback);
            },

            update : function(entity, successCallback) {
                var data = {
                    User: {},
                    Profile: {}
                };

                //Object.assign(data.Profile, entity.profile);
                angular.copy(entity, data.User);
                angular.copy(entity.profile, data.Profile);

                delete data.User.photo;
                delete data.User.profile;

                var formData = new FormData();
                formData.append('data', JSON.stringify(data));
                requestHelper.addSingleFileToFormData(formData, entity.photo);

                return $http.post(
                    '/api/user/update/' + entity.id,
                    formData,
                    {
                        headers: {
                            'Content-Type': undefined
                        }
                    }).success(successCallback);
            },

            delete : function(id, successCallback) {
                return $http.post('/api/user/delete/' + id).success(successCallback);
            },

            updateEmploymentStatus : function(id, model, successCallback) {
                return $http.post('/api/user/update-employment-status/' + id, model).success(successCallback);
            },

            updateCounters : function(successCallback) {
                return $http.get('/api/user/update-counters').success(successCallback);
            }
        };
        return user;
    }
]);