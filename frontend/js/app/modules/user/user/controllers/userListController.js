svApp.controller('userListController', [
    '$http',
    '$scope',
    'auth',
    'user',
    'rank',
    'complaint',
    'department',
    'role',
    'notification',
    'newDialogCreator',
    'crudHelper',
    function(
        $http,
        $scope,
        auth,
        user,
        rank,
        complaint,
        department,
        role,
        notification,
        newDialogCreator,
        crudHelper
    ) {

        var controller = this;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Users',
                active: true
            }
        ];
        $scope.mainController.title = 'Users';

        controller.tableParams = crudHelper.generateTable({}, {}, user, controller);
        controller.newComplaint = {};

        controller.getRanksFilter = rank.getFilter;
        controller.getDepartmensFilter = department.getFilter;
        controller.getRolesFilter = role.getFilter;
        controller.getEmploymentStatusFilter = user.getEmploymentStatusesFilter;

        auth.makeForCurrentUser(function(user) {
            controller.currentUser = user;
        });

        controller.can = auth.can;

        controller.delete = function(id) {
            crudHelper.delete(id, user, controller);
        };
        
        controller.openComplainModal = function(id) {
            $('#createComplaintModal').modal('show');
            controller.newComplaint.whom_id = id;
        };
        
        controller.complain = function() {
            complaint.create(controller.newComplaint, function(response) {
                $('#createComplaintModal').modal('hide');
                controller.newComplaint.reason = "";
                if (response.success) {
                    notification.alert('Complaint was successfully saved', true);
                } else {
                    notification.alert('Error occurred, while complaint saving', false);
                }
            });
        };

        controller.writeMessage = function(userId) {
            newDialogCreator.openModal(userId);
        };

        user.getEmploymentStatusesAsObject(function(response) {
            controller.employmentStatuses = response.list;
        })
    }
]);