svApp.controller('userViewController', [
    '$routeParams',
    '$scope',
    'newDialogCreator',
    'user',
    'complaint',
    'project',
    'auth',
    'notification',
    'localizationHelper',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        newDialogCreator,
        user,
        complaint,
        project,
        auth,
        notification,
        localizationHelper,
        ACTIONS
    ) {

        var controller = this;
        controller.action = ACTIONS.VIEW;
        controller.actions = ACTIONS;
        controller.model = {};
        controller.newComplaint = {};

        initPermissions();

        user.getEntity($routeParams.id, function(response) {
            controller.model = response.entity;
            controller.initBreadcrumbs();
        });

        user.getUserBillingCards($routeParams.id, function(response) {
           controller.billingCards = response.list;
        });

        user.getEmploymentStatusesAsObject(function(response) {
            controller.employmentStatuses = response.list;
        });

        project.getProjectStatusesAsObject(function(response) {
            controller.projectStatuses = response.list;
        });

        function initPermissions() {
            controller.isOwnProfile = false;
            controller.isOwnProfile = false;
            controller.displayBillingCards = false;
            controller.displaySalaryRate   = false;

            // check, is user watch himself profile, or other
            auth.makeForCurrentUser(function(user) {
                controller.isOwnProfile = (user.id == $routeParams.id);
                controller.displayBillingCards = (auth.can('payment.read') || auth.can('billingCard.read') || controller.isOwnProfile);
                controller.displaySalaryRate   = (auth.can('payment.read') || auth.can('user.canReadSalaryRate') || controller.isOwnProfile);

                // get complaints to user, if current user have required permission
                if (auth.user.can('complaint.read')) {
                    complaint.getComplaintsToUser($routeParams.id, function(response) {
                        controller.complaintsToUser = response.list;
                    });
                }

                // get user projects (projects, where user is creator, or user assigned to project)
                if (auth.user.can('project.read')) {
                    project.getAllUserProjects($routeParams.id, function(response) {
                        controller.userProjects = response.list;
                        controller.displayProjectsTab = (controller.userProjects.length > 0);
                    });
                }
            });
        }

        controller.initBreadcrumbs = function() {
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: 'Users',
                    url: '/users',
                    active: false
                },
                {
                    title: controller.model.fullName,
                    active: true
                }
            ];
            $scope.mainController.title = controller.model.fullName;
        };

        controller.openComplainModal = function(id) {
            $('#createComplaintModal').modal('show');
            controller.newComplaint.whom_id = id;
        };

        controller.complain = function() {
            complaint.create(controller.newComplaint, function(response) {
                $('#createComplaintModal').modal('hide');
                controller.newComplaint.reason = "";
                if (response.success) {
                    notification.alert('Complaint was successfully saved', true);
                    controller.complaintsToUser.unshift(response.newRecord);
                } else {
                    notification.alert('Error occurred, while complaint saving', false);
                }
            });
        };

        controller.writeMessage = function(userId) {
            newDialogCreator.openModal(userId);
        }
    }
]);