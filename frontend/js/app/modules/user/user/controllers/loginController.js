svApp.controller('loginController', [
    '$http',
    '$scope',
    '$location',
    'menuBuilder',
    'auth',
    function(
        $http,
        $scope,
        $location,
        menuBuilder,
        auth
    ) {

        $scope.mainController.breadcrumbs = false;
        $scope.mainController.title = 'Login';

        var controller = this;
        controller.model = {
            rememberMe: true
        };

        auth.makeForCurrentUser(function(user) {
            $location.path('/');
        });

        controller.login = function() {
            auth.login(controller.model, function() {
                $scope.mainController.user = auth.user;
                $scope.mainController.user = auth.user;
                $scope.mainController.menu = menuBuilder.init();
            });
        }
    }
]);