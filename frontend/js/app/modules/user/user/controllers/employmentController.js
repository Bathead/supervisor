svApp.controller('employmentController', [
    '$http',
    '$scope',
    'auth',
    'user',
    'componentsInit',
    'notification',
    function(
        $http,
        $scope,
        auth,
        user,
        componentsInit,
        notification
    ) {
        var controller = this;
        controller.model = {};
        controller.model.status = 1;

        auth.makeForCurrentUser(function(user) {
            controller.model.status = user.profile.status;
            controller.model.status_description = user.profile.status_description;
            controller.model.status_free_oriented_date = user.profile.status_free_oriented_date;

            if (controller.model.status === null) {
                controller.model.status = 1;
            }
        });

        user.getEmploymentStatuses(function(response) {
            controller.statusesList = response.list;
        });

        user.getEmploymentStatusesAsObject(function(response) {
            controller.statuses = response.list;
        });

        controller.save = function() {
            user.updateEmploymentStatus(auth.user.id, controller.model, function(response) {
                if (response.success) {
                    auth.user.profile.status = controller.model.status;
                    auth.user.profile.status_description = controller.model.status_description;
                    auth.user.profile.status_free_oriented_date = controller.model.status_free_oriented_date;
                    $('#changeEmploymentStatusModal').modal('hide');
                    notification.alert('Status was changed successfully', true);
                } else {
                    notification.alert('Error occurred while saving', false);
                }
            });
        };
    }
]);