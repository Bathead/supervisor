svApp.controller('userUpdateController', [
    '$routeParams',
    '$scope',
    '$location',
    'user',
    'crudHelper',
    'localizationHelper',
    'ACTIONS',
    'department',
    'rank',
    'technology',
    'role',
    'currency',
    'auth',
    'notification',
    'englishLanguageLevel',
    function(
        $routeParams,
        $scope,
        $location,
        user,
        crudHelper,
        localizationHelper,
        ACTIONS,
        department,
        rank,
        technology,
        role,
        currency,
        auth,
        notification,
        englishLanguageLevel
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.UPDATE;
        controller.model = {};
        controller.oldModel = {};
        controller.canRatingManage = false;

        controller.newBillingCard = {};
        controller.model.billingCards = [];

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Users',
                url: '/users',
                active: false
            },
            {
                title: 'Update',
                active: true
            }
        ];
        $scope.mainController.title = 'Update user';

        auth.makeForCurrentUser(function(user) {
            controller.canRatingManage = user.can('technology.ratingManage');
        });

        rank.getAll(function(response) {
            controller.ranks = response.list;
        });

        user.getUserStatuses(function(response) {
            controller.statuses = response.list;
        });

        role.getAll(function(response){
            controller.roles = response.list;
        });

        englishLanguageLevel.getAll(function(response) {
            controller.englishLanguageLevels = response.list;
        });

        currency.getAll(function(response) {
            controller.currencies = response.list;
        });

        user.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.oldModel = angular.copy(controller.model);

            user.getUserBillingCards($routeParams.id, function(response) {
                controller.model.billingCards = response.list;
                updateBillingCardCurrencies();
            });

            technology.getAll(function(response) {
                controller.technologies = response.list;

                controller.model.userTechnologiesFullList = [];
                for (var i = 0; i < controller.technologies.length; i++) {
                    var t = controller.technologies[i];

                    for (var j = 0; j < controller.model.userTechnologies.length; j++) {
                        var ut = controller.model.userTechnologies[j];
                        var active = false;
                        var rating = null;

                        if (ut.technology_id == t.id) {
                            active = ut.active;
                            rating = ut.rating;
                            break;
                        }
                    }

                    controller.model.userTechnologiesFullList.push({
                        id:     t.id,
                        name:   t.name,
                        active: Boolean(active),
                        rating: rating
                    })
                }
            });

            department.getAll(function(response) {
                controller.departments = response.list;

                controller.model.userDepartmentsFullList = [];
                for (var i = 0; i < controller.departments.length; i++) {
                    var d = controller.departments[i];

                    for (var j = 0; j < controller.model.userDepartments.length; j++) {
                        var ud = controller.model.userDepartments[j];
                        var chosen = false;

                        if (ud.department_id == d.id) {
                            chosen = true;
                            break;
                        }
                    }

                    controller.model.userDepartmentsFullList.push({
                        id:     d.id,
                        name:   d.name,
                        chosen: Boolean(chosen)
                    })
                }
            });

            initComponents();
        });

        controller.getCurrencyNameById = currency.getNameById;

        controller.update = function() {
            controller.model.photo = $scope.photo;

            user.update(controller.model, function(response) {
                if (response.success) {
                    notification.alert('Updated successfully', true);

                    if (auth.user.id == $routeParams.id) {
                        auth.initUser(function() {
                            $scope.mainController.user = auth.user;
                        });
                    }

                    $location.path('/users');
                } else {
                    notification.alert('Error occurred while saving', false);
                }
            });
        };

        // --- billing cards ---

        controller.addNewBillingCard = function() {
            // check is new billing card number already exist in new cards list
            for (var i = 0; i <  controller.model.billingCards.length; i++) {
                if (controller.model.billingCards[i].number == controller.newBillingCard.number) {
                    // card already exist in new cards list
                    return;
                }
            }

            controller.model.billingCards.push(angular.copy(controller.newBillingCard));
            controller.newBillingCard.expiration_date = null;
            controller.newBillingCard.number = null;
            $scope.newCardForm.$setPristine();
            $('#newBillingCardModal').modal('hide');
            updateBillingCardCurrencies();
        };

        controller.removeBillingCard = function(index) {
            controller.model.billingCards.splice(index, 1);
            updateBillingCardCurrencies();
        };

        // build new card currencies list (can create only 1 card for each currency)
        function updateBillingCardCurrencies() {
            controller.newCardCurrencies = angular.copy(controller.currencies);
            // remove already user currencies from list
            for (var i = 0; i < controller.model.billingCards.length; i++) {
                var card = controller.model.billingCards[i];
                for (var j = 0; j < controller.newCardCurrencies.length; j++) {
                    var currency = controller.newCardCurrencies[j];
                    if (card.currency_id == currency.id) {
                        controller.newCardCurrencies.splice(j, 1);
                    }
                }
            }

            if (controller.newCardCurrencies.length > 0) {
                // set default currency
                controller.newBillingCard.currency_id = controller.newCardCurrencies[0].id;
                controller.canCreateNewCard = true;
            } else {
                controller.newBillingCard.currency_id = null;
                controller.canCreateNewCard = false;
            }
        }

        // --- end billing cards ---

        function initComponents() {
            $(document).ready(function(){
                $('#phone').mask('(000) 000-00-00', {placeholder: "(___) ___-__-__"});
                $("#photo").filestyle({
                    placeholder: localizationHelper.translate('New photo'),
                    buttonText: localizationHelper.translate('Select')
                });

                $('#new_billing_card_number').mask('0000-0000-0000-0000', {placeholder: "____-____-____-____"});
                $('#new_billing_card_expiration_date').mask("0000-00-00", {placeholder: localizationHelper.translate("YYYY-MM-DD")});
            });
        }
    }
]);