svApp.controller('homeController', [
    '$scope',
    '$timeout',
    '$interval',
    'ticket',
    'news',
    'complaint',
    'project',
    'user',
    'auth',
    'componentsInit',
    function(
        $scope,
        $timeout,
        $interval,
        ticket,
        news,
        complaint,
        project,
        user,
        auth,
        componentsInit
    ) {
        var controller = this;

        $scope.mainController.breadcrumbs = null;
        $scope.mainController.title = 'Home';

        controller.tickets = [];
        controller.news = [];

        controller.greetingMessage = getGreetingMessage();

        auth.makeForCurrentUser(function(user) {
            controller.user = user;
            if (auth.can('ticket.read')) {
                getTicketsInfo();
            }
            if (auth.can('news.read')) {
                getNewsInfo();
            }
            if (auth.can('project.read')) {
                getProjectsInfo();
            }
            if (auth.can('complaint.read')) {
                getComplaintsInfo();
            }
        });

        function getGreetingMessage(){
            const MORNING_HOURS = 6;
            const DAY_HOURS = 12;
            const EVENING_HOURS = 18;
            const NIGHT_HOURS = 23;

            var message = "Good";
            var currentHour = parseFloat(moment().format("HH"));

            if((currentHour >= MORNING_HOURS) && (currentHour < DAY_HOURS)){
                message += " morning";
            }
            else if((currentHour >= DAY_HOURS) && (currentHour < EVENING_HOURS)){
                message += " day";
            }
            else if((currentHour >= EVENING_HOURS) && (currentHour < NIGHT_HOURS)){
                message += " evening";
            }
            else{
                message += " night";
            }

            return message;
        }

        function getProjectsInfo(){
            project.getLatest(5, function(response){
                controller.projects = response.list;
            });

            project.getInvaluableCount(function(response){
                controller.invaluableProjectsCount = response.count;
            });
        }

        function getComplaintsInfo(){
            complaint.getLatest(5, function(response){
                controller.complaints = response.list;
            });

            complaint.getComplaintStatusesAsObject(function(response) {
                controller.complaintStatuses = response.list;
            });

            complaint.getCountOpened(function(response){
                controller.openedComplaintsCount = response.count;
            });
        }

        function getNewsInfo(){
            news.getLatest(5, function (response) {
                controller.news = response.list;
            });

            news.getNewsTypes(function(response) {
                var result = {};
                for (var i = 0; i < response.list.length; i++) {
                    var type = response.list[i];
                    result[type.title] = type.id;
                }
                controller.types = result;
            });

            news.getUnreadCount(function(response){
                controller.unreadNewsCount = response.count;
            });
        }

        function getTicketsInfo(){
            ticket.getLatestReceived(5, function(response) {
                controller.tickets = response.list;
                $timeout(function() {
                    componentsInit.initScrollBars();
                    $(document).resize();
                }, 0);
            });

            ticket.getCountOpenedReceived(function(response){
                controller.openedReceivedTicketsCount = response.count;
            });
        }

        function splitTime(){
            var timeArray = controller.theTime.split(':');
            controller.theSecs = timeArray.pop();
            controller.theMins = timeArray.pop();
            controller.theHours = timeArray.pop();
        }

        if (!auth.can('complaint.read')) {
            controller.theTime = new Date().toLocaleTimeString();
            $interval(function () {
                controller.theTime = new Date().toLocaleTimeString();
                splitTime();
            }, 1000);
        }

    }
]);