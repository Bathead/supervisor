svApp.controller('userCreateController', [
    '$scope',
    '$timeout',
    'user',
    'department',
    'rank',
    'technology',
    'role',
    'currency',
    'englishLanguageLevel',
    'localizationHelper',
    'crudHelper',
    'auth',
    'ACTIONS',
    function(
        $scope,
        $timeout,
        user,
        department,
        rank,
        technology,
        role,
        currency,
        englishLanguageLevel,
        localizationHelper,
        crudHelper,
        auth,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.CREATE;
        controller.isNewRecord = true;
        controller.model = {};
        controller.model.profile = {};
        controller.model.status = 1;
        controller.canRatingManage = false;

        controller.newBillingCard = {};
        controller.model.billingCards = [];

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Users',
                url: '/users',
                active: false
            },
            {
                title: 'Create',
                active: true
            }
        ];
        $scope.mainController.title = 'Create new user';

        auth.makeForCurrentUser(function(user) {
            controller.canRatingManage = user.can('technology.ratingManage');
        });

        //department.getAll(function(response) {
        //    controller.departments = response.list;
        //});

        rank.getAll(function(response) {
            controller.ranks = response.list;
        });

        user.getUserStatuses(function(response) {
            controller.statuses = response.list;
        });
        
        role.getAll(function(response){
            controller.roles = response.list;
        });

        englishLanguageLevel.getAll(function(response) {
            controller.englishLanguageLevels = response.list;
        });

        currency.getAll(function(response) {
            controller.currencies = response.list;
            updateBillingCardCurrencies();
        });

        technology.getAll(function(response) {
            controller.technologies = response.list;

            controller.model.userTechnologiesFullList = [];
            for (var i = 0; i < controller.technologies.length; i++) {
                controller.model.userTechnologiesFullList.push({
                    id:     controller.technologies[i].id,
                    name:   controller.technologies[i].name,
                    active: false,
                    rating: null
                })
            }
        });

        department.getAll(function(response) {
            controller.departments = response.list;

            controller.model.userDepartmentsFullList = [];
            for (var i = 0; i < controller.departments.length; i++) {
                controller.model.userDepartmentsFullList.push({
                    id: controller.departments[i].id,
                    name: controller.departments[i].name,
                    chosen: false
                })
            }
        });

        $timeout(function () {
            initComponents();
        }, 0);

        controller.getCurrencyNameById = currency.getNameById;

        controller.create = function() {
            controller.model.photo = $scope.photo;

            crudHelper.create(
                controller.model,
                user,
                '/users'
            )
        };

        // --- billing cards ---

        controller.addNewBillingCard = function() {
            // check is new billing card number already exist in new cards list
            for (var i = 0; i <  controller.model.billingCards.length; i++) {
                if (controller.model.billingCards[i].number == controller.newBillingCard.number) {
                    // card already exist in new cards list
                    return;
                }
            }

            controller.model.billingCards.push(angular.copy(controller.newBillingCard));
            controller.newBillingCard.expiration_date = null;
            controller.newBillingCard.number = null;
            $scope.newCardForm.$setPristine();
            $('#newBillingCardModal').modal('hide');
            updateBillingCardCurrencies();
        };

        controller.removeBillingCard = function(index) {
            controller.model.billingCards.splice(index, 1);
            updateBillingCardCurrencies();
        };

        // build new card currencies list (can create only 1 card for each currency)
        function updateBillingCardCurrencies() {
            controller.newCardCurrencies = angular.copy(controller.currencies);
            // remove already user currencies from list
            for (var i = 0; i < controller.model.billingCards.length; i++) {
                var card = controller.model.billingCards[i];
                for (var j = 0; j < controller.newCardCurrencies.length; j++) {
                    var currency = controller.newCardCurrencies[j];
                    if (card.currency_id == currency.id) {
                        controller.newCardCurrencies.splice(j, 1);
                    }
                }
            }

            if (controller.newCardCurrencies.length > 0) {
                // set default currency
                controller.newBillingCard.currency_id = controller.newCardCurrencies[0].id;
                controller.canCreateNewCard = true;
            } else {
                controller.newBillingCard.currency_id = null;
                controller.canCreateNewCard = false;
            }
        }

        // --- end billing cards ---

        function initComponents() {
            $(document).ready(function() {
                $('#phone').mask('(000) 000-00-00', {placeholder: "(___) ___-__-__"});
                $("#photo").filestyle({
                    placeholder: localizationHelper.translate('Photo'),
                    buttonText: localizationHelper.translate('Select')
                });

                $('#new_billing_card_number').mask('0000-0000-0000-0000', {placeholder: "____-____-____-____"});
                $('#new_billing_card_expiration_date').mask("0000-00-00", {placeholder: localizationHelper.translate("YYYY-MM-DD")});
            });
        }
    }
]);