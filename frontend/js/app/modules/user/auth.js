svApp.factory('auth', [
    '$http',
    '$location',
    'notification',
    function(
        $http,
        $location,
        notification
    ) {

        var auth = {};
        auth.user = null;

        // url rules, which are available for guest users
        // ulr rules example:
        // - 'exampleurl*' - url must start with 'exampleurl'
        // - 'exampleurl' - url mast be equal to 'exampleurl
        auth.availableForGuestUrlRules = [
            '/unsubscribe*'
        ];

        var onUserInitCallbacks = [];

        auth.isUrlAvailableForGuest = function(url) {
            var available = false;
            for (var i = 0; i < auth.availableForGuestUrlRules.length; i++) {
                var rule = auth.availableForGuestUrlRules[i];
                if (rule.indexOf('*') > 0) {
                    var availableUrlStart = rule.substring(0, rule.indexOf('*'));
                    if (url.indexOf(availableUrlStart) == 0) {
                        available = true;
                        break;
                    }

                } else {
                    // url is available, if url == rule
                    if (url == rule) {
                        available = true;
                        break;
                    }
                }
            }
            return available;
        };

        auth.login = function(loginModel, successCallback) {
            var data = {
                LoginForm: loginModel
            };

            $http.post('/api/auth/login', data).success(function(response) {
                if (response.success) {
                    auth.user = response.user;
                    if (response.user !== null) {
                        auth.user.permissions = response.permissions;
                        auth.user.can = auth.can;
                    }
                    $location.path('/');
                    if (typeof successCallback !== 'undefined') {
                        successCallback();
                    }
                } else {
                    notification.pNotify.alert('Wrong username or password', false);
                }
            });
        };

        auth.logout = function(successCallback) {
            $http.get('/api/auth/logout').success(function(response) {
                if (response.success) {
                    auth.user = null;
                    if (typeof successCallback !== 'undefined') {
                        successCallback();
                    }
                }
            });
        };

        auth.initUser = function(successCallback) {
            $http.get('/api/auth/get-current-user').success(function(response) {
                auth.user = response.user;
                if (response.user !== null) {
                    auth.user.permissions = response.permissions;
                    auth.user.can = auth.can;
                    // call onUserInitCallbacks
                    for (var i = 0; i < onUserInitCallbacks.length; i++) {
                        onUserInitCallbacks[i](auth.user);
                    }
                    // clean callbacks list
                    onUserInitCallbacks = [];
                }
                if (typeof successCallback !== 'undefined') {
                    successCallback();
                }
            });
        };

        // check is current user have permission
        auth.can = function(permission) {
            if (auth.user !== null && auth.user.permissions !== null) {
                var check = $.inArray(permission, auth.user.permissions);
                return (check >= 0);
            }
            return false;
        };

        // call callback immediately, if user initialized,
        // or wait user initialization
        auth.makeForCurrentUser = function(callback) {
            if (auth.user !== null) {
                // user is initialized
                callback(auth.user);
            } else {
                // user not initialized already
                // callback will be called after user initialization
                auth.addOnUserInitCallback(callback);
            }
        };

        auth.addOnUserInitCallback = function(callback) {
            onUserInitCallbacks.push(callback);
        };

        return auth;
}]);