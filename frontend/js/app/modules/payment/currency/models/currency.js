svApp.factory('currency', [
    '$http',
    'requestHelper',
    '$q',
    function(
        $http,
        requestHelper,
        $q
    ) {
        var list = null;

        var currency = {
            getList : function(page, count, filter, sorting, successCallback){
                var searchQuery = requestHelper.formatSearchRequest('Currency', page, count, filter, sorting);
                return $http.get('/api/currency/get-currencies?' + searchQuery).success(successCallback);
            },

            // get all currencies (without pagination, filter, sorting)
            getAll : function(successCallback) {
                return $http.get('/api/currency/get-all-currencies' ).success(function(response) {
                    list = response.list;
                    successCallback(response);
                });
            },

            getFilter : function() {
                var def = $q.defer(),
                    result = [];
                currency.getAll(function(response) {
                    for (var index in response.list) {
                        result.push({
                            id : response.list[index].id,
                            title : response.list[index].name
                        })
                    }
                });
                def.resolve(result);
                return def;
            },

            getEntity : function(id, successCallback){
                return $http.get('/api/currency/get-currency/' + id).success(successCallback);
            },

            create : function(entity, successCallback){
                var data = {
                    Currency: entity
                };
                return $http.post('/api/currency/create', data).success(successCallback);
            },

            update : function(entity, successCallback){
                var data = {
                    Currency: entity
                };
                return $http.post('/api/currency/update/' + entity.id, data).success(successCallback);
            },

            delete: function(id, successCallback){
                return $http.post('/api/currency/delete/' + id).success(successCallback);
            },

            getNameById: function(id) {
                if (list === null) {
                    currency.getAll(function(response) {
                        list = response.list;
                        currency.getNameById(id);
                    });
                } else {
                    for (var i = 0; i < list.length; i++) {
                        if (list[i].id == id) {
                            return list[i].name;
                        }
                    }
                    return null;
                }
            }
        };

        return currency;
    }
]);