svApp.controller('currencyViewController', [
    '$routeParams',
    '$scope',
    'currency',
    'localizationHelper',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        currency,
        localizationHelper,
        ACTIONS
    ) {

        var controller = this;

        controller.model = {};

        currency.getEntity($routeParams.id, function(data){
            controller.model = data.entity;
            controller.initBreadcrumbs();
        });

        controller.initBreadcrumbs = function(){
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: 'Currencies',
                    url: '/currencies',
                    active: false
                },
                {
                    title: controller.model.name,
                    active: true
                }
            ];
            $scope.mainController.title = localizationHelper.translate("Currency") + " - " + controller.model.name;
        };
    }
]);
