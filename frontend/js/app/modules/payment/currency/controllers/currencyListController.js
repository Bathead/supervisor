svApp.controller('currencyListController',[
    '$http',
    '$scope',
    'currency',
    'crudHelper',
    function(
        $http,
        $scope,
        currency,
        crudHelper
    ) {

        var controller = this;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Currencies',
                active: true
            }
        ];
        $scope.mainController.title = 'Currencies';

        controller.tableParams = crudHelper.generateTable({}, {}, currency, controller);

        controller.delete = function(id){
            crudHelper.delete(id, currency, controller);
        };
    }
]);
