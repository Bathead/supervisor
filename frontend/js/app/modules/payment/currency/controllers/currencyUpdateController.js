svApp.controller('currencyUpdateController', [
    '$routeParams',
    '$scope',
    'currency',
    'crudHelper',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        currency,
        crudHelper,
        ACTIONS
    ) {

        var controller = this;

        controller.actions = ACTIONS;
        controller.action = ACTIONS.UPDATE;

        controller.model = {};
        controller.oldModel = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Currencies',
                url: '/currencies',
                active: false
            },
            {
                title: 'Update',
                active: true
            }
        ];
        $scope.mainController.title = 'Update currency';

        currency.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.oldModel = angular.copy(controller.model);
        });

        controller.update = function(){
            crudHelper.update(
                controller.model,
                currency,
                '/currencies'
            );
        };
    }
]);
