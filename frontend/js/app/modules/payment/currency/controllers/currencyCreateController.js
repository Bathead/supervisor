svApp.controller('currencyCreateController', [
    '$scope',
    'currency',
    'crudHelper',
    'ACTIONS',
    function(
        $scope,
        currency,
        crudHelper,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.CREATE;
        controller.model = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Currencies',
                url: '/currencies',
                active: false
            },
            {
                title: 'Create',
                active: true
            }
        ];
        $scope.mainController.title = 'Create new currency';

        controller.create = function(){
            crudHelper.create(
                controller.model,
                currency,
                '/currencies'
            );
        };
    }
]);
