svApp.factory('payment', [
    '$http',
    '$q',
    'localizationHelper',
    'requestHelper',
    function(
        $http,
        $q,
        localizationHelper,
        requestHelper
    ) {
        var payment = {
            getList : function(page, count, filter, sorting, successCallback){
                var searchQuery = requestHelper.formatSearchRequest('Payment', page, count, filter, sorting);
                return $http.get('/api/payment/get-payments?' + searchQuery).success(successCallback);
            },

            getFilteredList: function(page, count, filter, sorting, successCallback){
                var searchQuery = requestHelper.formatSearchRequest('Payment', page, count, filter, sorting);
                return $http.get('/api/payment/get-filtered-payments?' + searchQuery).success(successCallback);
            },

            getEntity : function(id, successCallback){
                return $http.get('/api/payment/get-payment/' + id).success(successCallback);
            },

            getVersion: function(id, version, successCallback){
                var data = {
                    id: id,
                    version: version
                };
                return $http.post('/api/payment/get-payment-version', data).success(successCallback);
            },

            getVersions: function (page, count, filter, sorting, id, successCallback) {
                var searchQuery = requestHelper.formatSearchRequest('Payment', page, count, filter, sorting);
                return $http.get('/api/payment/get-payment-versions/' + id + '?' + searchQuery).success(successCallback);
            },

            getPaymentStatuses : function(successCallback) {
                return $http.get('/api/payment/get-payment-statuses').success(successCallback);
            },

            getPaymentStatusesFilter : function() {
                var def = $q.defer(),
                    result = [];
                payment.getPaymentStatuses(function(response){
                    for (var index in response.list) {
                        result.push({
                            id : response.list[index].id,
                            title : localizationHelper.translate(response.list[index].title)
                        })
                    }
                });
                def.resolve(result);
                return def;
            },

            getUsersByName : function(name, successCallback) {
                var data = {
                    name : name
                };
                return $http.post('/api/payment/get-users-by-name', data).then(successCallback);
            },

            create : function(entity, successCallback){
                var data = {
                    Payment: entity
                };
                return $http.post('/api/payment/create', data).success(successCallback);
            },

            update : function(entity, successCallback){
                var data = {
                    Payment: entity
                };
                return $http.post('/api/payment/update/' + entity.id, data).success(successCallback);
            },

            updateList : function(ids, status, successCallback){
                var data = {
                    ids: ids,
                    status: status
                };
                return $http.post('/api/payment/update-list', data).success(successCallback);
            },

            delete : function(id, successCallback){
                return $http.post('/api/payment/delete/' + id).success(successCallback);
            },

            deleteList : function(ids, successCallback){
                return $http.post('/api/payment/delete-list', ids).success(successCallback);
            }
        };

        return payment;
    }
]);
