svApp.controller('paymentCreateListController', [
    '$http',
    '$scope',
    '$location',
    'payment',
    'department',
    'user',
    'crudHelper',
    'componentsInit',
    'NgTableParams',
    function(
        $http,
        $scope,
        $location,
        payment,
        department,
        user,
        crudHelper,
        componentsInit,
        NgTableParams
    ){
        var controller = this;

        controller.users = [];
        controller.data = [];
        controller.period = null;

        controller.getDepartmensFilter = department.getFilter;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Payrolls',
                url: '/payments',
                active: false
            },
            {
                title: 'Create',
                active: true
            }
        ];
        $scope.mainController.title = 'Create payrolls';

        generateList();

        function generateList(){
            $http.get('/api/user/get-all-users').success(
                function(response){
                    controller.users = response.list;
                    controller.users.forEach(function(item, i, arr) {
                        var newPayment = {
                            receiver: item,
                            receiverFullName: item['fullName'],
                            department: item['departmentsAsString'],
                            payment_sum_uah: 0,
                            payment_sum_usd: 0,
                            bonuses_sum_usd: 0,
                            other_sum_usd: 0,
                            sum_uah: 0,
                            sum_usd: 0,
                            comment: null
                        };
                        controller.data.push(newPayment);
                    });
                    generateTable();
                }
            );
        }

        function generateTable() {
            controller.tableParams = new NgTableParams(
                {
                    sorting: {
                        department: "asc"
                    }
                }, {
                    dataset: angular.copy(controller.data)
                }
            );
        }

        controller.saveList = function (){
            if(controller.period.length > 0){
                controller.period += "-01";

                var dataToSave = controller.tableParams.settings().dataset;
                dataToSave.forEach(function(item){
                    var newPayment = {
                        receiver_id: item['receiver']['id'],
                        payment_sum_uah: item['payment_sum_uah'],
                        payment_sum_usd: item['payment_sum_usd'],
                        bonuses_sum_usd: item['bonuses_sum_usd'],
                        other_sum_usd: item['other_sum_usd'],
                        comment: item['comment'],
                        status: 1,
                        period: controller.period
                    };
                    payment.create(newPayment, function(response){});
                });

                $location.path('/payments');
            }
        };
    }
]);