svApp.controller('paymentUpdateListController', [
    '$http',
    '$scope',
    '$location',
    '$timeout',
    'payment',
    'department',
    'crudHelper',
    'componentsInit',
    'localizationHelper',
    'NgTableParams',
    function(
        $http,
        $scope,
        $location,
        $timeout,
        payment,
        department,
        crudHelper,
        componentsInit,
        localizationHelper,
        NgTableParams
    ) {

        var controller = this;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Payrolls',
                url: '/payments',
                active: false
            },
            {
                title: 'Update',
                active: true
            }
        ];

        $scope.mainController.title = 'Payrolls';

        controller.getDepartmensFilter = department.getFilter;
        controller.getPaymentStatusesFilter = payment.getPaymentStatusesFilter;

        controller.canModifyChecked = false;
        controller.oldFilter = {};
        controller.oldSorting = {};

        controller.loadedData = {
            allChecked: false,

            allOriginalItemsIds: [],
            allOriginalItems: [],

            allModifiedItemsIds: [],
            allModifiedItems: [],

            currentPageItemsIds: [],
            currentPageItems: [],

            checkedItems: {}
        };

        controller.tableParams = generateTable({}, {});

        var defaultInitialParams = {
            page:   1,
            count:  10
        };

        var defaultInitialSettings = {
            counts: [10, 25, 50, 100, 200]
        };

        function generateTable(
            initialParams,
            initialSettings
        ) {
            // assign default options to final
            var finalInitialParams = $.extend({}, defaultInitialParams);
            var finalInitialSettings = $.extend({}, defaultInitialSettings);

            // overwrite options, declared manually
            for (var ipKey in initialParams) {
                finalInitialParams[ipKey] = initialParams[ipKey];
            }
            for (var isKey in initialSettings) {
                finalInitialSettings[isKey] = initialSettings[isKey];
            }

            // set default getData function
            if (typeof finalInitialSettings.getData == 'undefined') {
                finalInitialSettings.getData = function ($defer, params) {
                    var filter = params.filter();
                    var sorting = params.sorting();
                    var count = params.count();
                    var page = params.page();

                    if(JSON.stringify(controller.oldFilter) !== JSON.stringify(filter)){
                        controller.oldFilter = angular.copy(filter);
                        clearAllDataChanges();
                    }

                    if(JSON.stringify(controller.oldSorting) !== JSON.stringify(sorting)){
                        controller.oldSorting = angular.copy(sorting);
                        clearAllDataChanges();
                    }

                    payment.getList(page, count, filter, sorting, function(response) {
                        controller.tableParams.total(response.totalCount);

                        angular.forEach(controller.loadedData.currentPageItems, function(item) {
                            if(
                                (controller.loadedData.allModifiedItemsIds.indexOf(item.id) == -1) &&
                                (controller.loadedData.allOriginalItems[controller.loadedData.allOriginalItemsIds.indexOf(item.id)] != null)
                            ){
                                if(!paymentEquals(item, controller.loadedData.allOriginalItems[controller.loadedData.allOriginalItemsIds.indexOf(item.id)])){
                                    controller.loadedData.allModifiedItemsIds.push(item.id);
                                    controller.loadedData.allModifiedItems.push(item);
                                }
                            }
                        });

                        controller.loadedData.currentPageItemsIds = [];
                        controller.loadedData.currentPageItems = [];

                        angular.forEach(response.list, function(item){
                            if(controller.loadedData.allOriginalItemsIds.indexOf(item.id) == -1){
                                controller.loadedData.allOriginalItemsIds.push(item.id);
                                controller.loadedData.allOriginalItems.push(angular.copy(item));
                            }

                            controller.loadedData.currentPageItemsIds.push(item.id);

                            var itemIndex = controller.loadedData.allModifiedItemsIds.indexOf(item.id);
                            if(itemIndex != -1) {
                                controller.loadedData.currentPageItems.push(controller.loadedData.allModifiedItems[itemIndex]);
                            } else {
                                controller.loadedData.currentPageItems.push(item);
                            }
                        });

                        $defer.resolve(controller.loadedData.currentPageItems);
                    });
                }
            }
            return new NgTableParams(finalInitialParams, finalInitialSettings);
        }

        payment.getPaymentStatuses(function(response) {
            var result = {};
            for (var i = 0; i < response.list.length; i++) {
                var status = response.list[i];
                result[status.title] = status.id;
            }
            controller.statuses = result;
        });

        controller.changePaymentStatusForPaymentsList = function(status){
            var counter = 0;
            var statusAsString = getKey(controller.statuses, status);

            getModifiedItemsOnCurrentPage();

            angular.forEach(controller.loadedData.allOriginalItems, function(item){
                if(
                    (item.id in controller.loadedData.checkedItems) &&
                    (controller.loadedData.checkedItems[item.id] == true)
                ) {
                    var modifiedItemIndex = controller.loadedData.allModifiedItemsIds.indexOf(item.id);
                    if((modifiedItemIndex != -1) && (controller.loadedData.allModifiedItems[modifiedItemIndex].status != status)){
                        var modifiedItemInformation = controller.loadedData.allModifiedItems[modifiedItemIndex];
                        modifiedItemInformation.status = status;
                        modifiedItemInformation.statusAsString = statusAsString;
                        controller.loadedData.allModifiedItems[modifiedItemIndex] = modifiedItemInformation;
                        counter++;
                    }
                    else{
                        if(item.status != status){
                            var newModifiedItem = angular.copy(item);
                            newModifiedItem.status = status;
                            newModifiedItem.statusAsString = statusAsString;
                            controller.loadedData.allModifiedItems.push(newModifiedItem);
                            controller.loadedData.allModifiedItemsIds.push(newModifiedItem.id);
                            counter++;
                        }
                    }
                }
            });

            if(counter) {
                controller.tableParams.reload();
            }
        };

        controller.changeDateFilter = function(){
            var date_from = controller.date_from ? controller.date_from : "";
            if(date_from.length > 0)
                date_from += "-01";

            var date_to = controller.date_to ? controller.date_to : "";
            if(date_to.length > 0)
                date_to += "-01";

            if((date_from != null) || (date_to != null)){
                var filter = {};
                filter['date_from'] = date_from ;
                filter['date_to'] = date_to;
                clearLoadedData();
                angular.extend(controller.tableParams.filter(), filter);
            }
        };

        controller.cancelChanges = function(){
            clearAllDataChanges();
            controller.tableParams.page(1);
        };

        controller.saveChanges = function(){
            getModifiedItemsOnCurrentPage();

            var dataToSave = controller.loadedData.allModifiedItems;
            dataToSave.forEach(function (item) {
                payment.update(item, function (response) {});
            });
            $location.path('/payments');
        };

        $scope.$watch('payment.loadedData.allChecked', function(value) {
            if(value && (controller.loadedData.allOriginalItemsIds.length < controller.tableParams.total())){
                payment.getFilteredList(
                    controller.tableParams.page,
                    controller.tableParams.count,
                    controller.tableParams.filter,
                    controller.tableParams.sorting,
                    function(response){
                        angular.forEach(response.list, function(item){
                            if(controller.loadedData.allOriginalItemsIds.indexOf(item.id) == -1){
                                controller.loadedData.allOriginalItemsIds.push(item.id);
                                controller.loadedData.allOriginalItems.push(item);
                            }
                        });
                        changeEachCheckboxValue(value);
                    }
                );
            } else {
                changeEachCheckboxValue(value);
            }
        });

        $scope.$watch(function() {
            return controller.loadedData.checkedItems;
        }, function(values) {
            var checked = 0, unchecked = 0;
            var total = controller.tableParams.total();

            angular.forEach(controller.loadedData.allOriginalItems, function(item) {
                checked   +=  (controller.loadedData.checkedItems[item.id]) || 0;
                unchecked += (!controller.loadedData.checkedItems[item.id]) || 0;
            });

            controller.canModifyChecked = (checked > 0);

            if((checked == 0) || (checked == 0 && unchecked == 0)){
                controller.loadedData.allChecked = false;
            }
            else if (unchecked == 0 && checked == total) {
                controller.loadedData.allChecked = true;
            }

            // grayed checkbox
            angular.element(document.getElementsByClassName("select-all")).prop("indeterminate", (checked != 0 && (unchecked != 0 || checked != total)));
        }, true);

        function getModifiedItemsOnCurrentPage(){
            angular.forEach(controller.loadedData.currentPageItems, function(item) {
                if(controller.loadedData.allModifiedItemsIds.indexOf(item.id) == -1){
                    if(!paymentEquals(item, controller.loadedData.allOriginalItems[controller.loadedData.allOriginalItemsIds.indexOf(item.id)])){
                        controller.loadedData.allModifiedItemsIds.push(item.id);
                        controller.loadedData.allModifiedItems.push(item);
                    }
                }
            });
        }

        function paymentEquals(x, y){
            return (
                x.payment_sum_uah == y.payment_sum_uah &&
                x.payment_sum_usd == y.payment_sum_usd &&
                x.bonuses_sum_usd == y.bonuses_sum_usd &&
                x.other_sum_usd == y.other_sum_usd &&
                x.status == y.status &&
                x.comment == y.comment
            );
        }

        function getKey(object, value){
            for(var key in object){
                if(object[key] == value){
                    return key;
                }
            }
            return null;
        }

        function clearAllDataChanges(){
            clearCheckedData();
            clearModifiedData();
            clearLoadedData();
        }

        function changeEachCheckboxValue(value){
            angular.forEach(controller.loadedData.allOriginalItemsIds, function (item, i) {
                controller.loadedData.checkedItems[item] = value;
                controller.loadedData.allOriginalItems[i].selected = value;
            });
        }

        function clearCheckedData(){
            controller.loadedData.allChecked = false;
            controller.loadedData.allChecked = [];
        }

        function clearModifiedData(){
            controller.loadedData.allModifiedItemsIds = [];
            controller.loadedData.allModifiedItems = [];
        }

        function clearLoadedData() {
            controller.loadedData.allOriginalItemsIds = [];
            controller.loadedData.allOriginalItems = [];
        }
    }
]);