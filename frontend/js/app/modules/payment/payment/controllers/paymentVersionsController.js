svApp.controller('paymentVersionsController', [
    '$http',
    '$scope',
    '$routeParams',
    'payment',
    'department',
    'crudHelper',
    'localizationHelper',
    function(
        $http,
        $scope,
        $routeParams,
        payment,
        department,
        crudHelper,
        localizationHelper
    ){

        var controller = this;

        controller.modelId = $routeParams.id;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false

            },
            {
                title: 'Payrolls',
                url: '/payments',
                active: false
            },
            {
                title: "№ " + controller.modelId,
                url: '/payment/' + controller.modelId,
                active: false
            },
            {
                title: 'Versions',
                active: true
            }
        ];

        $scope.mainController.title = localizationHelper.translate("Versions of payment") + " - № " + controller.modelId;

        payment.getPaymentStatuses(function(response) {
            var result = {};
            for (var i = 0; i < response.list.length; i++) {
                var status = response.list[i];
                result[status.title] = status.id;
            }
            controller.statuses = result;
        });

        controller.tableParams = crudHelper.generateCustomTable({}, {}, payment, controller, payment.getVersions, controller.modelId);

        controller.getDepartmensFilter = department.getFilter;

        controller.getPaymentStatusesFilter = payment.getPaymentStatusesFilter;
    }

]);
