svApp.controller('paymentListController', [
    '$http',
    '$scope',
    '$location',
    '$timeout',
    'payment',
    'department',
    'crudHelper',
    'componentsInit',
    'localizationHelper',
    'NgTableParams',
    function(
        $http,
        $scope,
        $location,
        $timeout,
        payment,
        department,
        crudHelper,
        componentsInit,
        localizationHelper,
        NgTableParams
    ) {

        var controller = this;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Payrolls',
                active: true
            }
        ];
        $scope.mainController.title = 'Payrolls';

        controller.canModifyChecked = false;
        controller.oldFilter = {};

        controller.loadedData = {
            allChecked: false,

            allItemsIds: [],
            allItems: [],

            checkedItems: {}
        };

        controller.getDepartmensFilter = department.getFilter;
        controller.getPaymentStatusesFilter = payment.getPaymentStatusesFilter;

        controller.tableParams = generateTable({},{});

        var defaultInitialParams = {
            page:   1,
            count:  10
        };

        var defaultInitialSettings = {
            counts: [10, 25, 50, 100, 200]
        };

        function generateTable(
            initialParams,
            initialSettings
        ) {
            // assign default options to final
            var finalInitialParams = $.extend({}, defaultInitialParams);
            var finalInitialSettings = $.extend({}, defaultInitialSettings);

            // overwrite options, declared manually
            for (var ipKey in initialParams) {
                finalInitialParams[ipKey] = initialParams[ipKey];
            }
            for (var isKey in initialSettings) {
                finalInitialSettings[isKey] = initialSettings[isKey];
            }

            // set default getData function
            if (typeof finalInitialSettings.getData == 'undefined') {
                finalInitialSettings.getData = function ($defer, params) {
                    var filter = params.filter();
                    var sorting = params.sorting();
                    var count = params.count();
                    var page = params.page();

                    if(JSON.stringify(controller.oldFilter) !== JSON.stringify(filter)){
                        controller.oldFilter = angular.copy(filter);
                        clearLoadedData();
                    }

                    payment.getList(page, count, filter, sorting, function(response) {
                        controller.tableParams.total(response.totalCount);
                        $defer.resolve(response.list);

                        angular.forEach(response.list, function(item){
                            if(controller.loadedData.allItemsIds.indexOf(item.id) == -1){
                                controller.loadedData.allItemsIds.push(item.id);
                                controller.loadedData.allItems.push(item);
                            }
                        });
                    });
                }
            }
            return new NgTableParams(finalInitialParams, finalInitialSettings);
        }

        payment.getPaymentStatuses(function(response) {
            var result = {};
            for (var i = 0; i < response.list.length; i++) {
                var status = response.list[i];
                result[status.title] = status.id;
            }
            controller.statuses = result;
        });

        controller.changePaymentStatusForPaymentsList = function(status){
            var ids = [];
            angular.forEach(controller.loadedData.allItems, function(item){
                if((item.id in controller.loadedData.checkedItems) && (controller.loadedData.checkedItems[item.id] == true) && (item.status != status)) {
                    ids.push(item.id);
                }
            });

            if(ids.length > 0) {
                payment.updateList(ids, status, function (response) {
                    if(response.success)
                        controller.tableParams.reload();
                });
            }
        };

        controller.deleteCheckedPayments = function(){
            var ids = [];
            angular.forEach(controller.loadedData.allItems, function(item){
                if((item.id in controller.loadedData.checkedItems) && (controller.loadedData.checkedItems[item.id] == true)) {
                    ids.push(item.id);
                    delete controller.loadedData.checkedItems[item.id];
                }
            });

            var i = controller.loadedData.allItemsIds.length;
            while(i--){
                if(ids.indexOf(controller.loadedData.allItems[i].id) !== -1){
                    controller.loadedData.allItems.splice(i, 1);
                    controller.loadedData.allItemsIds.splice(i, 1);
                }
            }

            payment.deleteList(ids, function(response){
                if(response.success){
                    controller.tableParams.reload().then(function(data) {
                        if (data.length === 0 && controller.tableParams.total() > 0) {
                            controller.tableParams.page(controller.tableParams.page() - 1);
                            controller.tableParams.reload();
                        }
                    });
                }
            });
        };

        controller.delete = function(id){
            crudHelper.delete(id, payment, controller);
        };

        controller.exportList = function(){
            if(controller.canModifyChecked){
                var blob = new Blob([document.getElementById('exportable').innerHTML], {
                    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                });
                var filename = "Payments_" + new Date().toJSON().slice(0,10) + ".xls";
                saveAs(blob, filename);
            }
        };

        controller.changeDateFilter = function(){
            var date_from = controller.date_from ? controller.date_from : "";
            if(date_from.length > 0)
                date_from += "-01";

            var date_to = controller.date_to ? controller.date_to : "";
            if(date_to.length > 0)
                date_to += "-01";

            if((date_from != null) || (date_to != null)){
                var filter = {};
                filter['date_from'] = date_from ;
                filter['date_to'] = date_to;
                clearLoadedData();
                angular.extend(controller.tableParams.filter(), filter);
            }
        };

        controller.redirectOnUpdate = function(){
            $location.path('/payment/updateList');
        };

        $scope.$watch('payment.loadedData.allChecked', function(value) {
            if(value && (controller.loadedData.allItemsIds.length < controller.tableParams.total())){
                payment.getFilteredList(
                    controller.tableParams.page,
                    controller.tableParams.count,
                    controller.tableParams.filter,
                    controller.tableParams.sorting,
                    function(response){
                        angular.forEach(response.list, function(item){
                            if(controller.loadedData.allItemsIds.indexOf(item.id) == -1){
                                controller.loadedData.allItemsIds.push(item.id);
                                controller.loadedData.allItems.push(item);
                            }
                        });
                        changeEachCheckboxValue(value);
                    }
                );
            } else {
                changeEachCheckboxValue(value);
            }
        });

        $scope.$watch(function() {
            return controller.loadedData.checkedItems;
        }, function(values) {
            var checked = 0, unchecked = 0;
            var total = controller.tableParams.total();

            angular.forEach(controller.loadedData.allItems, function(item) {
                checked   +=  (controller.loadedData.checkedItems[item.id]) || 0;
                unchecked += (!controller.loadedData.checkedItems[item.id]) || 0;
            });

            controller.canModifyChecked = (checked > 0);

            if((checked == 0) || (checked == 0 && unchecked == 0)){
                controller.loadedData.allChecked = false;
            }
            else if (unchecked == 0 && checked == total) {
                controller.loadedData.allChecked = true;
            }

            // grayed checkbox
            angular.element(document.getElementsByClassName("select-all")).prop("indeterminate", (checked != 0 && (unchecked != 0 || checked != total)));
        }, true);

        function clearLoadedData(){
            controller.loadedData.allChecked = false;
            controller.loadedData.checkedItems = {};
            controller.loadedData.allItemsIds = [];
            controller.loadedData.allItems = [];
        }

        function changeEachCheckboxValue(value){
            angular.forEach(controller.loadedData.allItemsIds, function (item, i) {
                controller.loadedData.checkedItems[item] = value;
                controller.loadedData.allItems[i].selected = value;
            });
        }
    }
]);