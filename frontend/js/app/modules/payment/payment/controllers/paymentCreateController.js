svApp.controller('paymentCreateController', [
    '$scope',
    'payment',
    'user',
    'componentsInit',
    'localizationHelper',
    'crudHelper',
    'ACTIONS',
    function(
        $scope,
        payment,
        user,
        componentsInit,
        localizationHelper,
        crudHelper,
        ACTIONS
    ) {

        var controller = this;

        controller.actions = ACTIONS;
        controller.action = ACTIONS.CREATE;

        controller.model = {};
        controller.model.status = 1;

        controller.receiverAsObject = null;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Payrolls',
                url: '/payments',
                active: false
            },
            {
                title: 'Create',
                active: true
            }
        ];

        $scope.mainController.title = 'Create new payment';

        payment.getPaymentStatuses(function(response) {
            controller.statuses = response.list;
        });

        // get users for typeahead
        controller.getUsersByName = function(name) {
            return payment.getUsersByName(name, function(response){
                return response.data.list;
            });
        };

        controller.create = function(){
            if(typeof controller.receiverAsObject == "object" && controller.receiverAsObject != null) {
                controller.model.receiver_id = controller.receiverAsObject.id;
                controller.model.period = controller.model.periodAsString + "-01";
                crudHelper.create(
                    controller.model,
                    payment,
                    '/payments'
                );
            }
        };
    }
]);

