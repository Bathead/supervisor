svApp.controller('paymentViewVersionController', [
    '$routeParams',
    '$scope',
    'payment',
    'localizationHelper',
    function(
        $routeParams,
        $scope,
        payment,
        localizationHelper
    ){
        var controller = this;

        controller.oldModel = {};
        controller.originalModel = {};

        payment.getEntity($routeParams.id, function(data){
            controller.originalModel = data.entity;
            payment.getVersion($routeParams.id, $routeParams.version, function(data) {
                controller.oldModel = data.entity;
                controller.initBreadcrumbs();
            });
        });

        payment.getPaymentStatuses(function(response) {
            var result = {};
            for (var i = 0; i < response.list.length; i++) {
                var status = response.list[i];
                result[status.title] = status.id;
            }
            controller.statuses = result;
        });

        controller.initBreadcrumbs = function(){
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: 'Payrolls',
                    url: '/payments',
                    active: false
                },
                {
                    title: "№ " + controller.originalModel.id,
                    url: '/payment/' + controller.originalModel.id,
                    active: false
                },
                {
                    title: 'Versions',
                    url: '/payment/' + controller.originalModel.id +  '/versions',
                    active: false
                },
                {
                    title: controller.oldModel.version,
                    active: true
                }
            ];
            $scope.mainController.title = localizationHelper.translate("Payrolls") + " - № " + controller.originalModel.id;
        };
    }
]);

