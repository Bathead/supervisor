svApp.controller('paymentViewController', [
    '$routeParams',
    '$scope',
    'payment',
    'localizationHelper',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        payment,
        localizationHelper,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.VIEW;
        controller.model = {};

        payment.getEntity($routeParams.id, function(data){
            controller.model = data.entity;
            controller.initBreadcrumbs();
        });

        payment.getPaymentStatuses(function(response) {
            var result = {};
            for (var i = 0; i < response.list.length; i++) {
                var status = response.list[i];
                result[status.title] = status.id;
            }
            controller.statuses = result;
        });

        controller.pay = function(){
            controller.model.status = 2;
            payment.update(controller.model, function(response){
                payment.getEntity($routeParams.id, function(data){
                    controller.model = data.entity;
                });
            });
        };

        controller.delete = function(id){
            payment.delete(id, function(response){
                if(response.success)
                    $location.path('/payments');
            });
        };

        controller.initBreadcrumbs = function(){
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: 'Payrolls',
                    url: '/payments'
                },
                {
                    title: "№ " + controller.model.id,
                    active: true
                }
            ];
            $scope.mainController.title = localizationHelper.translate("Payrolls") + " - № " + controller.model.id;
        };
    }
]);
