svApp.controller('paymentUpdateController', [
    '$routeParams',
    '$scope',
    '$location',
    'payment',
    'user',
    'componentsInit',
    'localizationHelper',
    'crudHelper',
    'notification',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        $location,
        payment,
        user,
        componentsInit,
        localizationHelper,
        crudHelper,
        notification,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.UPDATE;
        controller.model = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Payrolls',
                url: '/payments',
                active: false
            },
            {
                title: 'Update',
                active: true
            }
        ];
        $scope.mainController.title = 'Update payment';

        payment.getPaymentStatuses(function(response) {
            controller.statuses = response.list;
        });

        // get users for typeahead
        controller.getUsersByName = function(name) {
            return payment.getUsersByName(name, function(response){
                return response.data.list;
            });
        };

        payment.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.receiverAsObject = controller.model.receiver;
        });

        controller.update = function(){
            if(typeof controller.receiverAsObject == "object" && controller.receiverAsObject != null) {
                controller.model.receiver_id = controller.receiverAsObject.id;
                controller.model.period = controller.model.periodAsString + "-01";
                payment.update(controller.model, function (response) {
                    var message = '';
                    if (response.success) {
                        $location.path('/payments');
                        message = "Updated successfully";
                    } else {
                        message = "Don't updated";
                    }
                    notification.alert(message, response.success);
                });
            }
        };
    }
]);

