svApp.factory('billingCard', [
    '$http',
    'requestHelper',
    function(
        $http,
        requestHelper
    ) {
        return {
            getList : function (page, count, filter, sorting, successCallback){
                var searchQuery = requestHelper.formatSearchRequest('BillingCard', page, count, filter, sorting);
                return $http.get('/api/billingCard/get-billing-cards?' + searchQuery).success(successCallback);
            },
            getEntity : function(id, successCallback){
                return $http.get('/api/billingCard/get-billing-card/' + id).success(successCallback);
            },
            getStates : function(successCallback) {
                return $http.get('/api/billingCard/get-states').success(successCallback);
            },
            getUsersByName : function(name, successCallback) {
                var data = {
                    name : name
                };
                return $http.post('/api/billingCard/get-users-by-name', data).then(successCallback);
            },
            create : function(entity, successCallback){
                var data = {
                    BillingCard: entity
                };
                return $http.post('/api/billingCard/create', data).success(successCallback);
            },
            update : function(entity, successCallback){
                var data = {
                    BillingCard: entity
                };
                return $http.post('/api/billingCard/update/' + entity.id, data).success(successCallback);
            },
            delete : function(id, successCallback){
                return $http.post('/api/billingCard/delete/' + id).success(successCallback);
            }
        };
    }
]);
