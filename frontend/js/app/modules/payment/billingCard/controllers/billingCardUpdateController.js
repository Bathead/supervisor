svApp.controller('billingCardUpdateController', [
    '$routeParams',
    '$scope',
    'billingCard',
    'currency',
    'user',
    'crudHelper',
    'localizationHelper',
    'componentsInit',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        billingCard,
        currency,
        user,
        crudHelper,
        localizationHelper,
        componentsInit,
        ACTIONS
    ) {

        var controller = this;

        controller.actions = ACTIONS;
        controller.action = ACTIONS.UPDATE;

        controller.model = {};
        controller.oldModel = {};

        controller.userBillingCards = [];
        controller.newCardCurrencies = [];

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                ulr: '/',
                active: false
            },
            {
                title: 'Billing cards',
                url: '/billingCards',
                active: false
            },
            {
                title: 'Update',
                active: true
            }
        ];
        $scope.mainController.title = 'Update billing card';

        billingCard.getEntity($routeParams.id, function(data){
            controller.model = data.entity;
            controller.oldModel = angular.copy(controller.model);
            controller.ownerAsObject = controller.model.owner;

            currency.getAll(function(response){
                controller.currencies = response.list;
                controller.updateBillingCardCurrencies();
            });
        });

        initComponents();

        // get users for typeahead
        controller.getUsersByName = function(name) {
            return billingCard.getUsersByName(name, function(response){
                return response.data.list;
            });
        };

        controller.update = function(){
            if(typeof controller.ownerAsObject == "object" && controller.ownerAsObject != null) {
                controller.model.owner_id = controller.ownerAsObject.id;

                crudHelper.update(
                    controller.model,
                    billingCard,
                    '/billingCards'
                );
            }
        };

        controller.updateBillingCardCurrencies = function(){
            controller.newCardCurrencies = angular.copy(controller.currencies);

            user.getUserBillingCards(controller.ownerAsObject.id, function(response) {
                controller.userBillingCards = response.list;
                for (var i = 0; i < controller.userBillingCards.length; i++) {
                    var card = controller.userBillingCards[i];
                    for (var j = 0; j < controller.newCardCurrencies.length; j++) {
                        var currency = controller.newCardCurrencies[j];
                        if (card.currency_id == currency.id && card.currency_id != controller.model.currency_id) {
                            controller.newCardCurrencies.splice(j, 1);
                        }
                    }
                }

                if (controller.newCardCurrencies.length > 0 && !controller.model.currency_id) {
                    controller.model.currency_id = controller.newCardCurrencies[0].id;
                }
                else if (controller.model.owner_id != controller.oldModel.owner_id){
                    controller.model.currency_id = controller.newCardCurrencies[0].id;
                }
            });
        };

        function initComponents() {
            componentsInit.initSelect();
            $(document).ready(function(){
                $('#number').mask('0000-0000-0000-0000', {placeholder: "____-____-____-____"});
            });
        }

    }
]);
