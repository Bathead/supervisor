svApp.controller('billingCardListController',[
    '$http',
    '$scope',
    'billingCard',
    'currency',
    'crudHelper',
    function(
        $http,
        $scope,
        billingCard,
        currency,
        crudHelper
    ) {

        var controller = this;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Billing cards',
                active: true
            }
        ];
        $scope.mainController.title = 'Billing cards';

        controller.tableParams = crudHelper.generateTable({}, {}, billingCard, controller);

        billingCard.getStates(function(response){
            controller.states = response.list;
        });

        controller.getCurrenciesFilter = currency.getFilter;

        controller.todayDateTime = new Date();
        controller.monthPeriod = new Date('1970-01-30');

        controller.parseToDateTime = function(stringParam){
            return new Date(stringParam);
        };

        controller.delete = function(id){
            crudHelper.delete(id, billingCard, controller);
        };
    }
]);

