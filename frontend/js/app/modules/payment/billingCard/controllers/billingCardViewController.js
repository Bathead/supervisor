svApp.controller('billingCardViewController', [
    '$routeParams',
    '$scope',
    'billingCard',
    'currency',
    'user',
    'componentsInit',
    'localizationHelper',
    function(
        $routeParams,
        $scope,
        billingCard,
        currency,
        user,
        componentsInit,
        localizationHelper
    ) {

        var controller = this;

        controller.model = {};

        billingCard.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.initBreadcrumbs();
        });

        billingCard.getStates(function(response){
            controller.states = response.list;
        });

        controller.initBreadcrumbs = function() {
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: 'Billing cards',
                    url: '/billingCards',
                    active: false
                },
                {
                    title: controller.model.number,
                    active: true
                }
            ];
            $scope.mainController.title = localizationHelper.translate("Billing card") + " - " + controller.model.number;
        };
    }
]);
