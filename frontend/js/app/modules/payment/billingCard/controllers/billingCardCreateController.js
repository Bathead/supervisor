svApp.controller('billingCardCreateController', [
    '$scope',
    'billingCard',
    'currency',
    'user',
    'componentsInit',
    'crudHelper',
    'localizationHelper',
    'ACTIONS',
    function(
        $scope,
        billingCard,
        currency,
        user,
        componentsInit,
        crudHelper,
        localizationHelper,
        ACTIONS
    ) {

        var controller = this;

        controller.actions = ACTIONS;
        controller.action = ACTIONS.CREATE;

        controller.model = {};
        controller.newCardCurrencies = [];
        controller.userBillingCards = [];
        controller.ownerAsObject = null;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Billing cards',
                url: '/billingCards',
                active: false
            },
            {
                title: 'Create',
                active: true
            }
        ];

        $scope.mainController.title = 'Create new billing card';

        currency.getAll(function(response){
            controller.currencies = response.list;
            controller.model.currency_id = controller.currencies[0].id;
            controller.newCardCurrencies = angular.copy(controller.currencies);
        });

        initComponents();

        // get users for typeahead
        controller.getUsersByName = function(name) {
            return billingCard.getUsersByName(name, function(response){
                return response.data.list;
            });
        };

        controller.create = function(){
            //controller.model.owner_id = $('#owner_id').val();

            if(typeof controller.ownerAsObject == "object" && controller.ownerAsObject != null) {
                controller.model.owner_id = controller.ownerAsObject.id;

                crudHelper.create(
                    controller.model,
                    billingCard,
                    '/billingCards'
                );
            }
        };

        controller.updateBillingCardCurrencies = function(){
            controller.newCardCurrencies = angular.copy(controller.currencies);

            user.getUserBillingCards(controller.ownerAsObject.id, function(response) {
                controller.userBillingCards = response.list;

                for (var i = 0; i < controller.userBillingCards.length; i++) {
                    var card = controller.userBillingCards[i];
                    for (var j = 0; j < controller.newCardCurrencies.length; j++) {
                        var currency = controller.newCardCurrencies[j];
                        if (card.currency_id == currency.id) {
                            controller.newCardCurrencies.splice(j, 1);
                        }
                    }
                }

                if (controller.newCardCurrencies.length > 0) {
                    controller.model.currency_id = controller.newCardCurrencies[0].id;

                } else {
                    controller.model.currency_id = null;
                }
            });
        };

        function initComponents() {
            componentsInit.initSelect();
            $(document).ready(function(){
                $('#number').mask('0000-0000-0000-0000', {placeholder: "____-____-____-____"});
            });
        }
    }
]);
