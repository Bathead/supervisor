// for CrudHelper support model must contain next methods:
// - getList
// - getEntity
// - create
// - update
// - delete

svApp.factory('hrContact', [
    '$http',
    'requestHelper',
    '$q',
    function(
        $http,
        requestHelper,
        $q
    ) {
        var hrContact = {
            getList : function(page, count, filter, sorting, successCallback) {
                var searchQuery = requestHelper.formatSearchRequest('HrContact', page, count, filter, sorting);
                return $http.get('/api/hrContact/get-hr-contacts?' + searchQuery).success(successCallback);
            },

            getEntity : function(id, successCallback) {
                return $http.get('/api/hrContact/get-hr-contact/' + id).success(successCallback);
            },

            create : function(entity, successCallback) {
                var data = {
                    HrContact: {}
                };

                angular.copy(entity, data.HrContact);
                delete data.HrContact.file1;
                delete data.HrContact.file2;

                var formData = new FormData();
                formData.append('data', JSON.stringify(data));
                requestHelper.addSingleFileToFormData(formData, entity.file1);
                requestHelper.addSingleFileToFormData(formData, entity.file2, 'file2');

                return $http.post(
                    '/api/hrContact/create',
                    formData,
                    {
                        headers: {
                            'Content-Type': undefined
                        }
                    }).success(successCallback);
            },

            update : function(entity, successCallback) {
                var data = {
                    HrContact: {}
                };

                angular.copy(entity, data.HrContact);
                delete data.HrContact.file1;
                delete data.HrContact.file2;

                var formData = new FormData();
                formData.append('data', JSON.stringify(data));
                requestHelper.addSingleFileToFormData(formData, entity.file1);
                requestHelper.addSingleFileToFormData(formData, entity.file2, 'file2');

                return $http.post(
                    '/api/hrContact/update/' + entity.id,
                    formData,
                    {
                        headers: {
                            'Content-Type': undefined
                        }
                    }).success(successCallback);
            },

            delete : function(id, successCallback) {
                return $http.post('/api/hrContact/delete/' + id).success(successCallback);
            },

            // get all users (creators) for filter
            getAllUsersForFilter : function(successCallback) {
                return $http.get('/api/hrContact/get-all-users-for-filter' ).success(successCallback);
            },

            getUsersFilter : function() {
                var def = $q.defer(),
                    result = [];
                hrContact.getAllUsersForFilter(function(response) {
                    for (var index in response.list) {
                        result.push({
                            id : response.list[index].id,
                            title : response.list[index].fullName
                        })
                    }
                });
                def.resolve(result);
                return def;
            }
        };
        return hrContact;
    }
]);