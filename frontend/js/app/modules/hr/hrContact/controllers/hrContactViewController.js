svApp.controller('hrContactViewController', [
    '$http',
    '$routeParams',
    '$scope',
    'hrContact',
    'technology',
    'englishLanguageLevel',
    'componentsInit',
    function(
        $http,
        $routeParams,
        $scope,
        hrContact,
        technology,
        englishLanguageLevel,
        componentsInit
    ) {

        var controller = this;
        controller.model = {};

        hrContact.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.initBreadcrumbs();

            //get attached files pathes
            if (controller.model.attached_resume_path) {
                controller.downloadLink1 = "/backend/web/uploads/hr/resumes/" + controller.model.id + "/" + controller.model.attached_resume_path;
            }
            if (controller.model.attached_sg_resume_path) {
                controller.downloadLink2 = "/backend/web/uploads/hr/resumessg/" + controller.model.id + "/" + controller.model.attached_sg_resume_path;
            }
        });

        controller.initBreadcrumbs = function() {
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: 'Contacts',
                    url: '/hrContacts',
                    active: false
                },
                {
                    title: controller.model.name,
                    active: true
                }
            ];
            $scope.mainController.title = controller.model.name;

            technology.getAll(function(response) {
                controller.technologiesList = response.list;
            });

            englishLanguageLevel.getAll(function(response) {
                controller.englishLanguageLevels = response.list;
            });

            componentsInit.initSelect();

        };
        
    }
]);