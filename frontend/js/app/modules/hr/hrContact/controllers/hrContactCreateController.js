svApp.controller('hrContactCreateController', [
    '$http',
    '$scope',
    'hrContact',
    'technology',
    'englishLanguageLevel',
    'localizationHelper',
    'crudHelper',
    'componentsInit',
    'ACTIONS',
    function(
        $http,
        $scope,
        hrContact,
        technology,
        englishLanguageLevel,
        localizationHelper,
        crudHelper,
        componentsInit,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.CREATE;
        controller.model = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Contacts',
                url: '/hrContacts',
                active: false
            },
            {
                title: 'Create',
                active: true
            }
        ];
        $scope.mainController.title = 'Create new contact';

        technology.getAll(function(response) {
            controller.technologiesList = response.list;
        });

        englishLanguageLevel.getAll(function(response) {
            controller.englishLanguageLevels = response.list;
        });
        componentsInit.initSelect();

        initComponents();

        controller.create = function() {
            controller.model.file1 = $scope.file1;
            controller.model.file2 = $scope.file2;

            crudHelper.create(
                controller.model,
                hrContact,
                '/hrContacts'
            );
        };


        function initComponents() {
            $(document).ready(function() {
                $("#file1").filestyle({
                    placeholder: localizationHelper.translate('Resume'),
                    buttonText: localizationHelper.translate('Select')
                });
            });

            $(document).ready(function() {
                $("#file2").filestyle({
                    placeholder: localizationHelper.translate('Remade Resume SG CV'),
                    buttonText: localizationHelper.translate('Select')
                });
            });
        }
    }
]);