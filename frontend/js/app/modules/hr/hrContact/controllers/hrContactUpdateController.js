svApp.controller('hrContactUpdateController', [
    '$http',
    '$routeParams',
    '$scope',
    'hrContact',
    'technology',
    'englishLanguageLevel',
    'localizationHelper',
    'crudHelper',
    'componentsInit',
    'notification',
    'ACTIONS',
    function(
        $http,
        $routeParams,
        $scope,
        hrContact,
        technology,
        englishLanguageLevel,
        localizationHelper,
        crudHelper,
        componentsInit,
        notification,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.UPDATE;
        controller.model = {};
        controller.oldModel = {};
        controller.deletedFile1 = null;
        controller.deletedFile2 = null;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Contacts',
                url: '/hrContacts',
                active: false
            },
            {
                title: 'Update',
                active: true
            }
        ];
        $scope.mainController.title = 'Update contact';

        technology.getAll(function(response) {
            controller.technologiesList = response.list;
        });

        englishLanguageLevel.getAll(function(response) {
            controller.englishLanguageLevels = response.list;
        });

        hrContact.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.oldModel = angular.copy(controller.model);

            //get attached files pathes
            if (controller.model.attached_resume_path) {
                controller.downloadLink1 = "/backend/web/uploads/hr/resumes/" + controller.model.id + "/" + controller.model.attached_resume_path;
            }
            if (controller.model.attached_sg_resume_path) {
                controller.downloadLink2 = "/backend/web/uploads/hr/resumessg/" + controller.model.id + "/" + controller.model.attached_sg_resume_path;
            }
        });

        componentsInit.initSelect();

        initComponents();

        controller.update = function() {
            controller.model.file1 = $scope.file1;
            controller.model.file2 = $scope.file2;

            if (controller.deletedFile1  == true) {
                $http.post('/api/hrContact/deletefile/' + $routeParams.id);
            }

            if (controller.deletedFile2  == true) {
                $http.post('/api/hrContact/deletefilesg/' + $routeParams.id);
            }

            crudHelper.update(
                controller.model,
                hrContact,
                '/hrContacts'
            );
        };

        controller.deleteFile1 = function(id) {
            controller.downloadLink1 = null;
            controller.deletedFile1 = true;
        };

        controller.deleteFile2 = function(id) {
            controller.downloadLink2 = null;
            controller.deletedFile2 = true;
        };

        function initComponents() {
            $(document).ready(function() {
                $("#file1").filestyle({
                    placeholder: localizationHelper.translate('Resume'),
                    buttonText: localizationHelper.translate('Select')
                });
            });

            $(document).ready(function() {
                $("#file2").filestyle({
                    placeholder: localizationHelper.translate('Remade Resume SG CV'),
                    buttonText: localizationHelper.translate('Select')
                });
            });
        }

    }
]);