svApp.controller('hrContactListController', [
    '$http',
    '$scope',
    'hrContact',
    'technology',
    'crudHelper',
    function(
        $http,
        $scope,
        hrContact,
        technology,
        crudHelper
    ) {

        var controller = this;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Contacts',
                active: true
            }
        ];
        $scope.mainController.title = 'Contacts';

        controller.tableParams = crudHelper.generateTable({}, {}, hrContact, controller);

        controller.getTechnologiesFilter = technology.getFilter;

        controller.getTechnologiesTextFilter = technology.getTextFilter;

        controller.getUsersFilter = hrContact.getUsersFilter;

        controller.delete = function(id) {
            crudHelper.delete(id, hrContact, controller);
        };
    }
]);