svApp.controller('hrMailingViewController', [
    '$http',
    '$routeParams',
    '$scope',
    'hrMailing',
    'localizationHelper',
    function(
        $http,
        $routeParams,
        $scope,
        hrMailing,
        localizationHelper
    ) {
        var controller = this;
        controller.action = 'View';
        controller.model = {};

        hrMailing.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.initBreadcrumbs();
        });

        controller.initBreadcrumbs = function() {
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: 'Mailings',
                    url: '/hrMailings',
                    active: false
                },
                {
                    title: controller.model.subject,
                    active: true
                }
            ];
            $scope.mainController.title = localizationHelper.translate("Mailing") + " - " + controller.model.subject;
        };
    }
]);