svApp.controller('hrMailingListController', [
    '$http',
    '$scope',
    'hrMailing',
    'crudHelper',
    function(
        $http,
        $scope,
        hrMailing,
        crudHelper
    ) {

        var controller = this;
        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Mailing',
                active: true
            }
        ];
        $scope.mainController.title = 'Mailing';

        controller.tableParams = crudHelper.generateTable({
            sorting: { created_at: "desc" }
        }, {}, hrMailing, controller);

        controller.delete = function(id) {
            crudHelper.delete(id, hrMailing, controller);
        };

        controller.getMailingsCreatorsFilter = hrMailing.getMailingsCreatorsFilter;
    }
]);