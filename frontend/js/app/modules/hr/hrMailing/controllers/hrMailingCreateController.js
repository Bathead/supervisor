svApp.controller('hrMailingCreateController', [
    '$http',
    '$scope',
    'hrMailing',
    'technology',
    'localizationHelper',
    'crudHelper',
    'CONFIG',
    'ACTIONS',
    function(
        $http,
        $scope,
        hrMailing,
        technology,
        localizationHelper,
        crudHelper,
        CONFIG,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.CREATE;
        controller.model = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Mailings',
                url: '/hrMailings',
                active: false
            },
            {
                title: 'Create',
                active: true
            }
        ];
        $scope.mainController.title = 'Create Mailing';

        controller.newMailingContacts = [];
        controller.filter = {
            exceptBlackListContacts : true,
            otherEmails : ''
        };

        controller.ckEditorOptions = angular.copy(CONFIG.defaultCkEditorOptions);
        controller.ckEditorOptions.language = localizationHelper.getCurrentLanguage();

        technology.getAll(function(response) {
            controller.technologies = response.list;
        });

        hrMailing.getAll(function(response) {
            controller.mailings = response.list;
        });

        controller.filtrateContacts = function() {
            hrMailing.getFiltratedContacts(controller.filter, function(response) {
                controller.newMailingContacts = [];

                for (var i = 0; i < response.contacts.length; i++) {
                    var contact = response.contacts[i];
                    controller.newMailingContacts.push({
                        email : contact.email,
                        info : '(' + contact.name + ', ' + contact.city + ')',
                        selected : true
                    });
                }

                for (var i = 0; i < response.validOtherEmails.length; i++) {
                    var otherEmail = response.validOtherEmails[i];
                    controller.newMailingContacts.push({
                        email : otherEmail,
                        selected : true
                    });
                }
            });
        };

        controller.filtrateContacts();

        controller.create = function() {

            controller.model.contactsEmails = [];
            for (var i = 0; i < controller.newMailingContacts.length; i++) {
                var contact = controller.newMailingContacts[i];
                if (contact.selected) {
                    controller.model.contactsEmails.push(contact.email);
                }
            }
            
            controller.model.attachedFiles = $scope.attachedFiles;

            crudHelper.create(
                controller.model,
                hrMailing,
                '/hrMailings'
            );
        };

        initComponents();

        function initComponents() {
            $(document).ready(function() {
                $("#attachedFiles").filestyle({
                    placeholder: localizationHelper.translate('Attached files'),
                    buttonText: localizationHelper.translate('Select')
                });
            });
        }
    }
]);