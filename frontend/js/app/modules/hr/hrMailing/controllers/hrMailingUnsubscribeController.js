svApp.controller('hrMailingUnsubscribeController', [
    '$http',
    '$scope',
    '$routeParams',
    'hrMailing',
    function(
        $http,
        $scope,
        $routeParams,
        hrMailing
    ) {

        var controller = this;

        $scope.mainController.title = 'Unsubscribe';

        controller.email = $routeParams.email;
        controller.success = true;

        hrMailing.unsubscribe(controller.email, function(response) {
            controller.success = response.success;
        });
    }
]);