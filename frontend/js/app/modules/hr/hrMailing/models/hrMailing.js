// for CrudHelper support model must contain next methods:
// - getList
// - getEntity
// - create
// - update
// - delete

svApp.factory('hrMailing', [
    '$http',
    '$q',
    'requestHelper',
    function(
        $http,
        $q,
        requestHelper
    ) {
        var hrMailing = {
            getList : function(page, count, filter, sorting, successCallback) {
                var searchQuery = requestHelper.formatSearchRequest('HrMailing', page, count, filter, sorting);
                return $http.get('/api/hrMailing/get-hr-mailings?' + searchQuery).success(successCallback);
            },

            getAll : function(successCallback) {
                return $http.get('/api/hrMailing/get-all-mailings').success(successCallback);
            },

            getEntity : function(id, successCallback) {
                return $http.get('/api/hrMailing/get-hr-mailing/' + id).success(successCallback);
            },

            create : function(entity, successCallback) {
                var data = {
                    HrMailing: entity
                };

                var formData = new FormData();
                formData.append('data', JSON.stringify(data));

                requestHelper.addMultipleFilesToFormData(formData, entity.attachedFiles);
                delete entity.attachedFiles;

                return $http.post(
                    '/api/hrMailing/create',
                    formData,
                    {
                        headers: {
                            'Content-Type': undefined
                        }
                    }).success(successCallback);
            },

            delete : function(id, successCallback) {
                return $http.post('/api/hrMailing/delete/' + id).success(successCallback);
            },

            getMailingsCreators : function(successCallback) {
                return $http.get('/api/hrMailing/get-hr-mailings-creators').success(successCallback);
            },

            getMailingsCreatorsFilter : function() {
                var def = $q.defer(),
                    result = [];
                hrMailing.getMailingsCreators(function(response) {
                    for (var index in response.list) {
                        result.push({
                            id : response.list[index].id,
                            title : response.list[index].name
                        })
                    }
                });
                def.resolve(result);
                return def;
            },


            // parameters is object with next possible keys:
            // - createdByIds (array)
            // - mainTechnologyId (int)
            // - additionalTechnologiesIds (array)
            // - exceptMailingsIds (array)
            // - exceptBlackListContacts (bool)
            // - otherEmails (string)
            getFiltratedContacts : function(parameters, successCallback) {
                return $http.post('/api/hrMailing/filtrate-contacts', parameters).success(successCallback)
            },

            unsubscribe : function(email, successCallback) {
                return $http.get('/api/unsubscribe/' + email).success(successCallback);
            }
        };
        return hrMailing;
    }
]);