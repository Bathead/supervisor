// for CrudHelper support model must contain next methods:
// - getList
// - getEntity
// - create
// - update
// - delete

svApp.factory('hrBlackList', [
    '$http',
    'requestHelper',
    '$q',
    function(
        $http,
        requestHelper,
        $q
    ) {
        var hrBlackList = {
            getList : function(page, count, filter, sorting, successCallback) {
                var searchQuery = requestHelper.formatSearchRequest('HrBlackList', page, count, filter, sorting);
                return $http.get('/api/hrBlackList/get-hr-black-lists?' + searchQuery).success(successCallback);
            },

            getEntity : function(id, successCallback) {
                return $http.get('/api/hrBlackList/get-hr-black-list/' + id).success(successCallback);
            },

            create : function(entity, successCallback) {
                var data = {
                    HrBlackList: entity
                };
                return $http.post('/api/hrBlackList/create', data).success(successCallback);
            },

            update : function(entity, successCallback) {
                var data = {
                    HrBlackList: entity
                };
                return $http.post('/api/hrBlackList/update/' + entity.id, data).success(successCallback);
            },

            delete : function(id, successCallback) {
                return $http.post('/api/hrBlackList/delete/' + id).success(successCallback);
            },

            // get all users (creators) for filter
            getAllUsersForFilter : function(successCallback) {
                return $http.get('/api/hrBlackList/get-all-users-for-filter' ).success(successCallback);
            },

            getUsersFilter : function() {
                var def = $q.defer(),
                    result = [];
                hrBlackList.getAllUsersForFilter(function(response) {
                    for (var index in response.list) {
                        result.push({
                            id : response.list[index].id,
                            title : response.list[index].fullName
                        })
                    }
                });
                def.resolve(result);
                return def;
            }
        };
        return hrBlackList;
    }
]);