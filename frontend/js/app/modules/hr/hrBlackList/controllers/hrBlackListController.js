svApp.controller('hrBlackListController', [
    '$http',
    '$scope',
    'hrBlackList',
    'crudHelper',
    function(
        $http,
        $scope,
        hrBlackList,
        crudHelper
    ) {

        var controller = this;
        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Black List',
                active: true
            }
        ];
        $scope.mainController.title = 'Black List';

        controller.tableParams = crudHelper.generateTable({}, {}, hrBlackList, controller);

        controller.getUsersFilter = hrBlackList.getUsersFilter;

        controller.delete = function(id) {
            crudHelper.delete(id, hrBlackList, controller);
        };
    }
]);