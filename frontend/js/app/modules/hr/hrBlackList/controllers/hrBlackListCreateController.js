svApp.controller('hrBlackListCreateController', [
    '$scope',
    'hrBlackList',
    'crudHelper',
    'ACTIONS',
    function(
        $scope,
        hrBlackList,
        crudHelper,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.CREATE;
        controller.isNewRecord = true;
        controller.model = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Black List',
                url: '/hrBlackLists',
                active: false
            },
            {
                title: 'Create',
                active: true
            }
        ];
        $scope.mainController.title = 'Add contact to black list';

        controller.create = function() {
            crudHelper.create(
                controller.model,
                hrBlackList,
                '/hrBlackLists'
            );
        };
    }
]);