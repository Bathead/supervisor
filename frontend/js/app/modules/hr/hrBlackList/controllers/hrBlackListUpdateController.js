svApp.controller('hrBlackListUpdateController', [
    '$routeParams',
    '$scope',
    'hrBlackList',
    'crudHelper',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        hrBlackList,
        crudHelper,
        ACTIONS
    ) {

        var controller = this;

        controller.actions = ACTIONS;
        controller.action = ACTIONS.UPDATE;

        controller.model = {};
        controller.oldModel = {};

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Black List',
                url: '/hrBlackLists',
                active: false
            },
            {
                title: 'Update',
                active: true
            }
        ];
        $scope.mainController.title = 'Update black list record';

        hrBlackList.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.oldModel = angular.copy(controller.model);
        });

        controller.update = function() {
            crudHelper.update(
                controller.model,
                hrBlackList,
                '/hrBlackLists'
            );
        };

    }
]);