svApp.controller('hrBlackListViewController', [
    '$routeParams',
    '$scope',
    'hrBlackList',
    'localizationHelper',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        hrBlackList,
        localizationHelper,
        ACTIONS
    ) {

        var controller = this;

        controller.model = {};

        hrBlackList.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.initBreadcrumbs();
        });

        controller.initBreadcrumbs = function() {
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: 'Black List',
                    url: '/hrBlackLists',
                    active: false
                },
                {
                    title: controller.model.email,
                    active: true
                }
            ];
            $scope.mainController.title = localizationHelper.translate("Black list") + " - " + controller.model.email;
        };
    }
]);