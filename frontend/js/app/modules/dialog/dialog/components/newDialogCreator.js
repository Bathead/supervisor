svApp.service('newDialogCreator', [
    'dialog',
    '$route',
    '$location',
    'notification',
    function(
        dialog,
        $route,
        $location,
        notification
    ) {

        this.model = {};

        this.openModal = function(userId) {
            if (typeof userId == 'undefined') {
                userId = false;
            }
            this.model.userId = userId;
            this.model.message = '';
            this.model.subject = '';
            $('#createDialogModal').modal('show');
        };

        this.create = function() {
            if (!this.model.userId ||
                (this.model.subject.length == 0) ||
                (this.model.message.length == 0)) {
                // fields not valid
            } else {
                // close modal
                $('#createDialogModal').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();

                // create dialog
                dialog.create(this.model, function(response) {
                    if (response.success) {
                        notification.pNotify.alert('Dialog created successfully', true);
                        $location.path('/dialogs/' + response.dialogId);
                    } else {
                        notification.pNotify.alert('Error occurred while dialog creating', false);
                    }
                })
            }
        };
}]);