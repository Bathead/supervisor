svApp.factory('dialog', [
    '$http',
    'requestHelper',
    function(
        $http,
        requestHelper
    ) {
        var dialog = {
            getList : function(successCallback) {
                return $http.get('/api/dialog/get-dialogs').success(successCallback);
            },

            getEntity : function(id, successCallback) {
                return $http.get('/api/dialog/get-dialog/' + id).success(successCallback);
            },

            create : function(entity, successCallback) {
                return $http.post('/api/dialog/create', entity).success(successCallback);
            },

            delete : function(id, successCallback) {
                //return $http.post('/api/complaint/delete/' + id).success(successCallback);
            },

            getMoreMessages : function(dialogId, loadedMessagesCount, successCallback) {
                var data = {
                    dialogId: dialogId,
                    loadedMessagesCount: loadedMessagesCount
                };
                return $http.post('/api/dialog/get-more-messages', data).success(successCallback);
            },

            markMessagesAsRead : function(dialogId, unreadMessagesIds, successCallback) {
                var data = {
                    dialogId: dialogId,
                    unreadMessagesIds: unreadMessagesIds
                };
                return $http.post('/api/dialog/mark-messages-as-read', data).success(successCallback);
            },

            // get users, which not assigned to dialog already
            getOtherUsersByName : function(dialogId, name, successCallback) {
                var data = {
                    dialogId : dialogId,
                    name : name
                };
                return $http.post('/api/dialog/get-other-users-by-name', data).then(successCallback);
            },

            leaveDialog : function(dialogId, successCallback) {
                return $http.post('/api/dialog/leave/' + dialogId).success(successCallback);
            },

            attachFilesToMessage : function(messageId, files, successCallback) {
                var formData = new FormData();
                requestHelper.addMultipleFilesToFormData(formData, files);

                return $http.post(
                    '/api/dialog/attach-files-to-message/' + messageId,
                    formData,
                    {
                        headers: {
                            'Content-Type': undefined
                        }
                    }).success(successCallback);
            }
        };
        return dialog;
    }
]);