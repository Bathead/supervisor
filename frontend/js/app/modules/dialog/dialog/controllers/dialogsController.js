svApp.controller('dialogsController', [
    '$http',
    '$scope',
    '$rootScope',
    '$timeout',
    '$route',
    '$location',
    '$routeParams',
    'menuBuilder',
    'debounce',
    'notification',
    'user',
    'auth',
    'localizationHelper',
    'componentsInit',
    'dialog',
    'newDialogCreator',
    'CONFIG',
    function(
        $http,
        $scope,
        $rootScope,
        $timeout,
        $route,
        $location,
        $routeParams,
        menuBuilder,
        debounce,
        notification,
        user,
        auth,
        localizationHelper,
        componentsInit,
        dialog,
        newDialogCreator,
        CONFIG
    ) {

        var controller = this;
        var messagesList = $('#dialog-chat');
        var currentUserWriteMessage = false;
        controller.usersWriteMessages = false;
        controller.currentDialog = {};
        controller.messages = [];
        controller.allMessagesLoaded = false;
        controller.wscon = null;
        controller.accessToken = '';
        controller.isDialogEditable = false;
        controller.dialogNewUser = null;
        controller.newMessage = '';
        $scope.newMessageAttachedFiles = [];

        initComponents();
        
        auth.makeForCurrentUser(function(user) {
            controller.currentUser = user;
        });

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Dialogs',
                active: true
            }
        ];
        $scope.mainController.title = 'Dialogs';

        // close current web socket connection, when user leave the page
        $scope.$on("$destroy", function(){
            if (controller.wscon) {
                controller.wscon.close();
            }
        });

        controller.createDialog = function() {
            newDialogCreator.openModal();
        };

        // dialogs lists initialization
        dialog.getList(function(response) {
            controller.list = response.list;

            if (controller.list.length > 0) {
                if ($routeParams.id) {
                    openDialog($routeParams.id);
                } else {
                    openDialog(controller.list[0].id);
                }
            }

            // scroll init
            $timeout(function() {
                componentsInit.initScrollBars();
                $(document).resize();
            }, 0);
        });

        controller.openDialog = function(id) {
            openDialog(id);
        };

        // select dialog from list, and load dialog messages
        function openDialog(id) {
            dialog.getEntity(id, function(response) {
                controller.currentDialog = response.entity;
                controller.messages = response.messages;
                controller.isDialogEditable = response.isDialogEditable;
                for (var i = 0; i < controller.messages.length; i++) {
                    controller.messages[i].message = nl2br(controller.messages[i].message);
                }
                controller.allMessagesLoaded = response.allMessagesLoaded;
                controller.accessToken = response.accessToken;
                $timeout(function() {
                    scrollToBottom(messagesList);
                }, 0);

                webSocketOpenConnection();
            });
        }

        controller.leaveDialog = function() {
            controller.removeUserFromDialog(auth.user.id);
        };

        // load more messages, if user scroll to top and user have unloaded messages
        messagesList.on("scroll", function () {
            if ($(this).scrollTop() === 0 && !controller.allMessagesLoaded) {
                dialog.getMoreMessages(controller.currentDialog.id, controller.messages.length, function(response) {
                    for (var i = (response.messages.length - 1); i >= 0; i--) {
                        var message = response.messages[i];
                        controller.messages.unshift(message);
                    }
                    controller.allMessagesLoaded = response.allMessagesLoaded;
                    //messagesList.scrollTop(10);
                    messagesList.animate({ scrollTop: 10 }, 50);
                });
            }
        });

        // mark loaded messages as read (after click to new message field)
        controller.markMassagesAsRead = function() {
            var unreadMessagesIds = [];
            for (var i = 0; i < controller.messages.length; i++) {
                var message = controller.messages[i];
                if (message.isUnread) {
                    unreadMessagesIds.push(message.id);
                }
            }

            if (unreadMessagesIds.length > 0) {
                dialog.markMessagesAsRead(controller.currentDialog.id, unreadMessagesIds, function(response) {
                    if (response.success) {
                        for (var i = 0; i < controller.messages.length; i++) {
                            controller.messages[i].isUnread = false;
                        }
                    }
                });
                // update unread messages counters
                updateAllNewMessagesCounters(-unreadMessagesIds.length);
            }
        };

        // get user for typeahead (in add new user modal)
        controller.getUsersByName = function(name) {
            return dialog.getOtherUsersByName(controller.currentDialog.id, name, function(response){
                return response.data.list;
            })
        };

        // ------ web sockets managing -------

        function webSocketOpenConnection() {
            // close previous dialog web socket connection, and open new
            if (controller.wscon !== null) {
                controller.wscon.close();
            }

            var port = CONFIG.webSocketsOptions.dialogPort;
            var url = 'ws://' + window.location.host + ':'
                + port + '?accessToken=' + controller.accessToken
                + '&dialogId=' + controller.currentDialog.id;
            controller.wscon = new WebSocket(url);
            controller.wscon.onopen = onWebSocketConnectionOpen;
            controller.wscon.onmessage = onWebSocketMessage;
        }

        function onWebSocketConnectionOpen(e) {
            //console.log("Connection established!");
        }

        function onWebSocketMessage(e) {
            var response = JSON.parse(e.data);
            if (response.eventType != 'onClose') {
                var func = controller[response.eventType];
                func(response);
            }
        }

        // ------ end web sockets managing -------

        // ------ web sockets events -------

        controller.event_new_message = function(response) {
            // message successfully created on server

            var isUnread = false;
            if (!response.isMyRequest) {
                isUnread = true;
                playNewMessageSound();
                // update unread messages counters
                updateAllNewMessagesCounters(1);
            } else {
                // attach files for new, successfully added message (if exist)
                if ($scope.newMessageAttachedFiles.length > 0) {
                    dialog.attachFilesToMessage(response.message.id, $scope.newMessageAttachedFiles, function(ajaxResponse) {
                       if (ajaxResponse.success) {

                           // update message for all users, if files added successfully
                           var data = {
                               eventType: 'event_update_message',
                               updatedMessage: ajaxResponse.updatedRecord
                           };
                           controller.wscon.send(JSON.stringify(data));
                           
                           cleanAttachedFiles();
                       } 
                    });
                }
            }

            response.message.isUnread = isUnread;
            response.message.message = nl2br(response.message.message);
            controller.messages.push(response.message);

            $scope.$apply();
            scrollToBottom(messagesList);
        };

        controller.event_update_message = function(response) {
            if (!response.isMyRequest) {
                response.updatedMessage.isUnread = true;
            }
            for(var i = 0; i < controller.messages.length; i++) {
                if (controller.messages[i].id == response.updatedMessage.id) {
                    controller.messages[i] = response.updatedMessage;
                    break;
                }
            }
            $scope.$apply();
            scrollToBottom(messagesList)
        };

        controller.event_add_user = function (response) {
            for (var i = 0; i < response['newUsers'].length; i++) {
                var user = response['newUsers'][i];
                controller.currentDialog.dialogUsers.push(user);
            }
            $scope.$apply();
        };

        controller.event_remove_user = function(response) {
            var removedUser = response.removedUser;

            for (var i = 0; i < controller.currentDialog.dialogUsers.length; i++) {
                var user = controller.currentDialog.dialogUsers[i];
                if (user.id == removedUser.id) {
                    controller.currentDialog.dialogUsers.splice(i, 1);
                }
            }
            $scope.$apply();

            if (removedUser.id == auth.user.id) {
                if ($location.url() == '/dialogs') {
                    $route.reload();
                } else {
                    $location.path('/dialogs');
                }
            }
        };

        controller.event_user_write_message = function(response) {
            var usersWriteMessage = response.usersWriteMessage;
            var usersList = [];
            for(var i = 0; i < usersWriteMessage.length; i++) {
                if (usersWriteMessage[i].id != auth.user.id) {
                    usersList.push(usersWriteMessage[i].fullName);
                }
            }
            var usersCount = usersList.length;
            usersList = usersList.join(', ');

            if (usersCount == 0) {
                controller.usersWriteMessages = false;
            } else if (usersCount == 1) {
                controller.usersWriteMessages = usersList + ' ' + localizationHelper.translate('dialog_user_write_message');
            } else {
                controller.usersWriteMessages = usersList + ' ' + localizationHelper.translate('dialog_users_write_message');
            }

            $scope.$apply();
        };

        controller.event_update_other_dialogs_counters = function(response) {
            updateAllNewMessagesCounters(response.counterInc, response.dialogId);
            playNewMessageSound();
        };

        // ------ end web sockets events -------

        // ------ dialog users -----

        controller.addUserToDialog = function() {
            $('#addUserModal').modal('hide');
            if ((typeof controller.dialogNewUser == 'object') && (controller.dialogNewUser !== null)) {
                var data = {
                    eventType: 'event_add_user',
                    userIds: [controller.dialogNewUser.id]
                };

                controller.wscon.send(JSON.stringify(data));
                controller.dialogNewUser = null;
            } else {
                notification.pNotify.alert('Select user from dropdown list', false);
            }
        };
        
        controller.removeUserFromDialog = function(id) {
            var data = {
                eventType: 'event_remove_user',
                userId: id
            };

            controller.wscon.send(JSON.stringify(data));
        };

        // ------ end dialog users -----

        // ---- dialog message writing ----

        controller.newMessageKeyUp = function(e) {
            if (e.keyCode == 13 && !e.shiftKey) {
                // enter pressed -> send message

                if (controller.newMessage.length === 0) {
                    return false;
                }

                controller.sendMessage();
                stopWriteMessage();
            } else {
                if (!currentUserWriteMessage) {
                    startWriteMessage();
                }
            }
        };

        controller.sendMessage = function() {
            if (controller.newMessage.length !== 0) {
                var data = {
                    eventType: 'event_new_message',
                    message: controller.newMessage
                };
                controller.wscon.send(JSON.stringify(data));
                controller.markMassagesAsRead();
                controller.newMessage = '';
            }
        };

        controller.writeMessageDebounce = debounce(2000, function () {
            stopWriteMessage();
        });

        function startWriteMessage() {
            if (!currentUserWriteMessage) {
                currentUserWriteMessage = true;
                var data = {
                    eventType: 'event_user_write_message',
                    type: 'start'
                };
                controller.wscon.send(JSON.stringify(data));
            }
        }

        function stopWriteMessage() {
            if (currentUserWriteMessage) {
                currentUserWriteMessage = false;
                var data = {
                    eventType: 'event_user_write_message',
                    type: 'stop'
                };
                controller.wscon.send(JSON.stringify(data));
            }
        }

        // ---- end dialog message writing ----

        // ---- dialog subject ----
        
        controller.onSubjectBlur = function() {
            changeSubject();
        };
        
        controller.onSubjectChange = function(e) {
            if (e.keyCode == 13) {
                changeSubject();
                e.preventDefault();
            }
        };

        function changeSubject() {
            var data = {
                eventType: 'event_change_dialog_subject',
                subject: controller.currentDialog.subject
            };
            controller.wscon.send(JSON.stringify(data));
        }

        controller.event_change_dialog_subject = function(response) {
            controller.currentDialog.subject = response.newSubject;

            // update subject in dialogs list
            for (var i = 0; i < controller.list.length; i++) {
                if (controller.list[i].id == controller.currentDialog.id) {
                    controller.list[i].subject = response.newSubject;
                }
            }

            $scope.$apply();
        };

        // ---- end dialog subject ----

        // ------ new messages counter ------

        function updateAllNewMessagesCounters(value, dialogId) {
            $timeout(function() {
                updateDialogCounter(value, dialogId);
                updateUnreadMessagesMainCounter(value);
                menuBuilder.updateCounterByKey('unreadMessagesCounter', value);
            }, 0);
        }
        
        function updateDialogCounter(value, dialogId) {
            if (typeof dialogId == 'undefined') {
                dialogId = controller.currentDialog.id;
            }
            for (var i = 0; i < controller.list.length; i++) {
                if (controller.list[i].id == dialogId) {
                    controller.list[i].unreadMessagesCount += value;
                }
            }
        }

        function updateUnreadMessagesMainCounter(value) {
            auth.user.counters.unreadMessagesCounter += value;
        }
        
        // ---- end new messages counter ----

        // --- additional functions ---

        // change new lines to <br/>
        function nl2br(str) {
            if (str === null) {
                return '';
            }
            return str.replace(/([^>])\n/g, '$1<br/>');
        }

        // scroll selected element to bottom, if scroll is exist
        function scrollToBottom(element) {
            var scrollHeight = element.prop('scrollHeight');
            element.scrollTop(scrollHeight - getScrollThumbHeight(element));

            //element.scrollTop(scrollHeight - height);
        }

        // calculate current scroll thumb height
        function getScrollThumbHeight(element) {
            var height = element.height();
            var scrollHeight = element.prop('scrollHeight');
            return (height * height) / scrollHeight;
        }

        function playNewMessageSound() {
            // todo: select audio
            //var snd = new Audio("sounds/onmessage/typewriter-key-1.mp3");
            //var snd = new Audio("sounds/onmessage/cell-phone-flip-1.mp3");
            //var snd = new Audio("sounds/onmessage/tape-recorder-close-1.mp3");
            var snd = new Audio("sounds/onmessage/tape-recorder-eject-1.mp3");
            //var snd = new Audio("sounds/onmessage/message2.mp3");

            snd.play();
        }

        function initComponents() {
            $(document).ready(function() {
                $(".filestyle").filestyle({
                    placeholder: localizationHelper.translate('Attached files'),
                    buttonText: localizationHelper.translate('Attach_file')
                });
            });
        }

        function cleanAttachedFiles() {
            $scope.newMessageAttachedFiles = [];
            $('.attached-files input[type="text"]').val('')
        }

        // --- end additional functions ---

    }
]);