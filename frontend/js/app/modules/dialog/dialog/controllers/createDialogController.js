svApp.controller('createDialogController', [
    'newDialogCreator',
    'user',
    function(
        newDialogCreator,
        user
    ) {
        var controller = this;
        controller.model = newDialogCreator.model;
        controller.newDialogFirstParticipant = null;

        controller.create = function() {
            if (!newDialogCreator.model.userId) {
                // user id is not set, get user id from autocomplete selected object (if object selected)
                if ((typeof controller.newDialogFirstParticipant == 'object') && (controller.newDialogFirstParticipant !== null)) {
                    newDialogCreator.model.userId = controller.newDialogFirstParticipant.id;
                }
            }
            controller.newDialogFirstParticipant = null;
            newDialogCreator.create();
        };

        // get user for typeahead (in add new user modal)
        controller.getOtherUsersByName = function(name) {
            return user.getOtherUsersByName(name, function(response){
                return response.data.list;
            })
        };
    }
]);