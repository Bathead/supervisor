// for CrudHelper support model must contain next methods:
// - getList
// - getEntity
// - create
// - update
// - delete

svApp.factory('complaint', [
    '$http',
    '$q',
    'requestHelper',
    'localizationHelper',
    function(
        $http,
        $q,
        requestHelper,
        localizationHelper
    ) {
        var complaint = {
            getList : function(page, count, filter, sorting, successCallback) {
                var searchQuery = requestHelper.formatSearchRequest('Complaint', page, count, filter, sorting);
                return $http.get('/api/complaint/get-complaints?' + searchQuery).success(successCallback);
            },

            getLatest: function(count, successCallback){
                var data = {
                    count: count
                };
                return $http.post('/api/complaint/get-latest-complaints', data).success(successCallback);
            },

            getEntity : function(id, successCallback) {
                return $http.get('/api/complaint/get-complaint/' + id).success(successCallback);
            },

            getCountOpened : function(successCallback){
                return $http.get('/api/complaint/get-count-opened').success(successCallback);
            },

            // return list of objects with next format:
            // [ {id: '...', title: '...'}, {...} ]
            getComplaintStatuses : function(successCallback) {
                return $http.get('/api/complaint/get-complaint-statuses').success(successCallback);
            },

            // return complaint statuses as object in the next format:
            // {
            //     status1Id: status1Title,
            //     status2Id: status2Title
            //     ...
            // }
            getComplaintStatusesAsObject : function(successCallback) {
                return complaint.getComplaintStatuses(function(response) {
                    var result = {};
                    for (var i = 0; i < response.list.length; i++) {
                        var status = response.list[i];
                        result[status.title.replace(/ /g,"_")] = status.id;
                    }
                    response.list = result;
                    successCallback(response);
                });
            },

            // return complaint statuses list for ng-table select filter
            getComplaintStatusesFilter : function() {
                var def = $q.defer(),
                    result = [];
                complaint.getComplaintStatuses(function(response) {
                    for (var index in response.list) {
                        result.push({
                            id : response.list[index].id,
                            title : localizationHelper.translate(response.list[index].title + '_complaint')
                        });
                    }
                });
                def.resolve(result);
                return def;
            },

            create : function(entity, successCallback) {
                var data = {
                    Complaint: entity
                };
                return $http.post('/api/complaint/create', data).success(successCallback);
            },

            update : function(entity, successCallback) {
                var data = {
                    Complaint: entity
                };
                return $http.post('/api/complaint/update/' + entity.id, data).success(successCallback);
            },

            delete : function(id, successCallback) {
                return $http.post('/api/complaint/delete/' + id).success(successCallback);
            },

            markAsReviewed : function(id, successCallback) {
                return $http.post('/api/complaint/mark-as-reviewed/' + id).success(successCallback);
            },

            getNewComplaintsCount : function(successCallback) {
                return $http.get('/api/complaint/get-new-complaints-count').success(successCallback);
            },

            getComplaintsToUser : function(id, successCallback) {
                return $http.get('/api/user/get-complaints-to-user/' + id).success(successCallback);
            }
        };
        return complaint;
    }
]);