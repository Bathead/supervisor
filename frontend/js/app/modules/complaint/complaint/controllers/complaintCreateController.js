svApp.controller('complaintCreateController', [
    '$scope',
    'complaint',
    'user',
    'crudHelper',
    'ACTIONS',
    function(
        $scope,
        complaint,
        user,
        crudHelper,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.CREATE;
        controller.isNewRecord = true;
        controller.model = {};
        controller.complaintUser = null;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Complaints',
                url: '/complaints',
                active: false
            },
            {
                title: 'Create',
                active: true
            }
        ];
        $scope.mainController.title = 'Create new complaint';

        complaint.getComplaintStatuses(function(response) {
           controller.statuses = response.list;
        });

        controller.getOtherUsersByName = function(name) {
            return user.getOtherUsersByName(name, function (response) {
                return response.data.list;
            });
        };

        controller.create = function() {
            if ((typeof controller.complaintUser == 'object') && (controller.complaintUser !== null)) {
                controller.model.whom_id = controller.complaintUser.id;

                crudHelper.create(
                    controller.model,
                    complaint,
                    '/complaints'
                );
            }
        };
    }
]);