svApp.controller('complaintUpdateController', [
    '$routeParams',
    '$scope',
    'complaint',
    'user',
    'crudHelper',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        complaint,
        user,
        crudHelper,
        ACTIONS
    ) {

        var controller = this;
        controller.actions = ACTIONS;
        controller.action = ACTIONS.UPDATE;
        controller.model = {};
        controller.complaintUser = null;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Complaints',
                url: '/complaints',
                active: false
            },
            {
                title: 'Update',
                active: true
            }
        ];
        $scope.mainController.title = 'Update complaint';

        complaint.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.complaintUser = controller.model.whom;
        });

        complaint.getComplaintStatuses(function(response) {
            controller.statuses = response.list;
        });

        controller.getOtherUsersByName = function(name) {
            return user.getOtherUsersByName(name, function (response) {
                return response.data.list;
            });
        };

        controller.update = function() {
            if ((typeof controller.complaintUser == 'object') && (controller.complaintUser !== null)) {
                controller.model.whom_id = controller.complaintUser.id;

                crudHelper.update(
                    controller.model,
                    complaint,
                    '/complaints'
                );
            }
        };

    }
]);