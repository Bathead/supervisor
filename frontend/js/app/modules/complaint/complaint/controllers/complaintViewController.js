svApp.controller('complaintViewController', [
    '$routeParams',
    '$scope',
    'complaint',
    'user',
    'menuBuilder',
    'notification',
    'localizationHelper',
    'ACTIONS',
    function(
        $routeParams,
        $scope,
        complaint,
        user,
        menuBuilder,
        notification,
        localizationHelper,
        ACTIONS
    ) {

        var controller = this;
        controller.action = ACTIONS.VIEW;
        controller.model = {};

        complaint.getEntity($routeParams.id, function(data) {
            controller.model = data.entity;
            controller.initBreadcrumbs();
        });

        controller.initBreadcrumbs = function() {
            $scope.mainController.breadcrumbs = [
                {
                    title: 'Home',
                    url: '/',
                    active: false
                },
                {
                    title: 'Complaints',
                    url: '/complaints',
                    active: false
                },
                {
                    title: '№ ' + controller.model.id,
                    active: true
                }
            ];
            $scope.mainController.title = localizationHelper.translate('Complaint') + " - № " + controller.model.id;
        };

        complaint.getComplaintStatusesAsObject(function(response) {
            controller.statuses = response.list;
        });

        controller.markAsReviewed = function() {
            complaint.markAsReviewed(controller.model.id, function(response) {
                if (response.success) {
                    controller.model.status = controller.statuses.Reviewed;
                    controller.model.statusAsString = getKey(controller.statuses, controller.model.status);
                    menuBuilder.updateCounters();
                    notification.alert('Status was changed successfully', true);
                } else {
                    notification.alert('Error occurred while changing status', false);
                }
            })
        };

        function getKey(object, value){
            for(var key in object){
                if(object[key] == value){
                    return key;
                }
            }
            return null;
        }
    }
]);