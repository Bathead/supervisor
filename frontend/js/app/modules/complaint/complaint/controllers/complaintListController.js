svApp.controller('complaintListController', [
    '$http',
    '$scope',
    'notification',
    'menuBuilder',
    'complaint',
    'crudHelper',
    function(
        $http,
        $scope,
        notification,
        menuBuilder,
        complaint,
        crudHelper
    ) {

        var controller = this;

        $scope.mainController.breadcrumbs = [
            {
                title: 'Home',
                url: '/',
                active: false
            },
            {
                title: 'Complaints',
                active: true
            }
        ];
        $scope.mainController.title = 'Complaints';

        complaint.getComplaintStatusesAsObject(function(response) {
            controller.statuses = response.list;
        });

        controller.tableParams = crudHelper.generateTable({
            sorting: { date: "desc" }
        }, {}, complaint, controller);
        controller.getComplaintStatusesFilter = complaint.getComplaintStatusesFilter;

        controller.delete = function(id) {
            crudHelper.delete(id, complaint, controller);
        };

        controller.markAsReviewed = function(id) {
            complaint.markAsReviewed(id, function (response) {
                if (response.success) {
                    controller.tableParams.reload();
                    menuBuilder.updateCounters();
                    notification.alert('Status was changed successfully', true);
                } else {
                    notification.alert('Error occurred while changing status', false);
                }
            });
        }
    }
]);