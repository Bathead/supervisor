svApp.factory('notification', [
    'localizationHelper',
    function(localizationHelper) {
        var notification = {};

        var defaultMessages = {
            successTitle:       'Success',
            errorTitle:         'Oops...',
            confirmationTitle:  'Are you sure?',
            confirmationText:   'You will not be able to recover this item!',
            confirmBtnText:     'Yes, delete it!',
            cancelBtnText:      'Cancel'
        };

        // sweet alert
        notification.alert = function(message, success) {
            message = localizationHelper.translate(message);
            if (success) {
                sweetAlert(localizationHelper.translate(defaultMessages.successTitle), message, 'success');
            } else {
                sweetAlert(localizationHelper.translate(defaultMessages.errorTitle), message, 'error');
            }
        };

        // pNotify alert
        notification.pNotify = {
            alert: function(message, success) {
                $.notify({
                    icon: (success) ? 'glyphicon glyphicon-ok' : 'glyphicon glyphicon-warning-sign',
                    title: (success) ?
                        localizationHelper.translate(defaultMessages.successTitle) :
                        localizationHelper.translate(defaultMessages.errorTitle),
                    message: localizationHelper.translate(message)
                },{
                    type: (success) ? 'success' : 'danger'
                });
            }
        };

        notification.confirm = function(callback, options) {

            var defaultOptions = {
                title: localizationHelper.translate(defaultMessages.confirmationTitle),
                text: localizationHelper.translate(defaultMessages.confirmationText),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: localizationHelper.translate(defaultMessages.confirmBtnText),
                cancelButtonText: localizationHelper.translate(defaultMessages.cancelBtnText),
                closeOnConfirm: false
            };

            var finalOptions = $.extend({}, defaultOptions);

            // overwrite options, declared manually
            for (var optKey in options) {
                finalOptions[optKey] = options[optKey];
            }

            swal(finalOptions, callback);
        };

        return notification;
    }
]);