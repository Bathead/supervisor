svApp.factory('menuBuilder', [
    'auth',
    'user',
    '$location',
    function(
        auth,
        user,
        $location
    ) {
        var menuBuilder = {};

        // menu item properties:
        // - url
        // - title
        // - icon
        // - items - array of sub menu items
        // - permissions (array, not required) - user must have any of seted permissions
        // - activeRule - array of urls ('/<action>') or urls templates ('/<action>/*'), which set, when menu item will be active
        //                if active rule is empty, item will be active when item url == current url
        // - callback - function will be called for current menu item
        // - counter {key, value}

        // sub menu items properties
        // - url
        // - title
        // - permissions
        // - callback
        // - counter

        menuBuilder.menuItems = [
            {
                url: '/',
                title: 'Home',
                icon: 'glyphicon glyphicon-home',
                color: 'green'
            },
            {
                url: '/news-list',
                title: 'News',
                icon: 'glyphicon glyphicon-blackboard',
                permissions:  ['news.read'],
                activeRule: ['/news*', '/news-list'],
                color: 'gold',
                counter: {
                    key: 'importantUnreadNewsCounter',
                    title: 'You have unread important news'
                }
            },
            {
                url: '/dialogs',
                title: 'Dialogs',
                icon: 'font-icon font-icon-comments',
                activeRule: ['/dialogs*'],
                color: 'purple',
                counter: {
                    key: 'unreadMessagesCounter'
                }
            },
            {
                url: '/myProjects',
                title: 'My projects',
                icon: 'glyphicon glyphicon-list',
                permissions:  ['project.read'],
                activeRule: ['/myProjects'],
                color: 'blue'
            },
            {
                url: '/projects',
                title: 'Projects',
                icon: 'glyphicon glyphicon-list-alt',
                permissions:  ['project.read'],
                activeRule: ['/project*', '/projects'],
                color: 'blue'
            },
            {
                url: '/users',
                title: 'Users',
                icon: 'glyphicon glyphicon-user',
                permissions:  ['user.read'],
                activeRule: ['/user*', '/users'],
                color: 'blue'
            },
            {
                title: 'RBAC',
                icon: 'glyphicon glyphicon-sunglasses',
                permissions:  ['rbac.all'],
                activeRule: ['/rbac*'],
                color: 'aquamarine',
                items: [
                    {
                        url: '/rbac/assignments',
                        title: 'Assignments',
                        permissions:  ['rbac.all']
                    },
                    {
                        url: '/rbac/roles',
                        title: 'Roles',
                        permissions:  ['rbac.all']
                    },
                    {
                        url: '/rbac/permissions',
                        title: 'Permissions',
                        permissions:  ['rbac.all']
                    },
                    {
                        url: '/rbac/routes',
                        title: 'Routes',
                        permissions:  ['rbac.all']
                    },
                    {
                        url: '/rbac/rules',
                        title: 'Rules',
                        permissions:  ['rbac.all']
                    }
                ]
            },
            {
                url: '/departments',
                title: 'Departments',
                icon: 'glyphicon glyphicon-th',
                permissions:  ['department.read'],
                activeRule: ['/department*', '/departments'],
                color: 'purple'
            },
            {
                title: 'Tickets',
                icon: 'glyphicon glyphicon-tags',
                permissions:  ['ticket.read'],
                activeRule: ['/ticket*', '/tickets'],
                color: 'blue-dirty',
                items: [
                    {
                        url: '/sentTickets',
                        title: 'Sent tickets',
                        permissions:  ['ticket.read'],
                        counter: {
                            key: 'newSentTicketsCounter'
                        }
                    },
                    {
                        url: '/receivedTickets',
                        title: 'Received tickets',
                        permissions:  ['ticket.read'],
                        counter: {
                            key: 'newTicketsCounter'
                        }
                    }
                ]
            },
            {
                url: '/complaints',
                title: 'Complaints',
                icon: 'glyphicon glyphicon-exclamation-sign',
                permissions:  ['complaint.read'],
                activeRule: ['/complaint*', '/complaints'],
                color: 'red',
                counter: {
                    key: 'newComplaintsCounter'
                }
            },
            {
                url: '/ranks',
                title: 'Ranks',
                icon: 'glyphicon glyphicon-bishop',
                permissions:  ['rank.read'],
                activeRule: ['/rank*', '/ranks'],
                color: 'brown'
            },
            {
                url: '/technologies',
                title: 'Technologies',
                icon: 'glyphicon glyphicon-wrench',
                permissions:  ['technology.read'],
                activeRule: ['/technology*', '/technologies'],
                color: 'brown'
            },
            {
                url: '/englishLanguageLevels',
                title: 'English Language Levels',
                icon: 'glyphicon glyphicon-globe',
                permissions:  ['language.read'],
                activeRule: ['/englishLanguageLevel*', '/englishLanguageLevels'],
                color: 'blue'
            },
            {
                title: 'HR',
                icon: 'glyphicon glyphicon-equalizer',
                permissions:  ['hrContact.read'],
                activeRule: ['/hrContact*', '/hrContacts', '/hrMailing*', '/hrMailings', '/hrBlackList*', '/hrBlackLists'],
                color: 'purple',
                items: [
                    {
                        url: '/hrContacts',
                        title: 'Contacts',
                        permissions:  ['hrContact.read']
                    },
                    {
                        url: '/hrMailings',
                        title: 'Mailing',
                        permissions:  ['hrMailing.read']
                    },
                    {
                        url: '/hrBlackLists',
                        title: 'Black list',
                        permissions:  ['hrBlackList.read']
                    }
                ]
            },
            {
                title: 'Payments',
                icon: 'font-icon font-icon-wallet',
                permissions:  ['payment.read', 'currency.read', 'billingCard.read'],
                activeRule: ['/payment*', '/payments', '/billingCard*', '/billingCards', '/currency*', '/currencies'],
                color: 'green',
                items: [
                    {
                        url: '/payments',
                        title: 'Payrolls',
                        permissions:  ['payment.read']
                    },
                    {
                        url: '/billingCards',
                        title: 'Billing cards',
                        permissions:  ['billingCard.read']
                    },
                    {
                        url: '/currencies',
                        title: 'Currencies',
                        permissions:  ['currency.read']
                    }
                ]
            }
        ];

        // ----------------- menu items callback functions -----------------
        var menuItemsCallbacks = {

            //exampleCallback: function(item) {
            //    item.counter.value = 1;
            //}

        };

        menuBuilder.init = function() {
            checkMenuItemsPermissions(menuBuilder.menuItems);
            return menuBuilder.menuItems;
        };

        // recursive function for check menu items (and sub menu items) permissions
        // if user don't have required permissions, menu item property 'visible' = false
        function checkMenuItemsPermissions(menuItems) {
            for (var i = 0; i < menuItems.length; i++){
                var item = menuItems[i];

                // check permissions, if menu item has required permissions
                if ('permissions' in item) {
                    var visible = false;
                    for (var permIndex = 0; permIndex < item.permissions.length; permIndex++) {
                        if (auth.can(item.permissions[permIndex])) {
                            visible = true;
                            break;
                        }
                    }
                    item.visible = visible;
                } else {
                    item.visible = true;
                }

                // call callback function, if exist
                if ('callback' in item) {
                    var fn = item['callback'];
                    menuItemsCallbacks[fn](item);
                }

                // check submenu items, if exists
                if ('items' in  item) {
                    checkMenuItemsPermissions(item.items);
                }
            }
        }

        menuBuilder.updateActive = function() {
            cleanActiveItems(menuBuilder.menuItems);
            markActiveItem(menuBuilder.menuItems);
        };

        // select active tab from menu items rules after page load (route change)
        function markActiveItem(menuItems) {
            
            var currentUrl = $location.path();
            var active = false;
            
            for (var i = 0; i < menuItems.length; i++){
                var item = menuItems[i];

                if ('activeRule' in item) {
                    // active rule is array of urls ('/<action>') or urls templates ('/<action>/*')
                    for (var ruleIndex = 0; ruleIndex < item.activeRule.length; ruleIndex++) {
                        var rule = item.activeRule[ruleIndex];
                        if (rule.indexOf('*') >= 0) {
                            // active rule format is: '/<action>/*'
                            rule = rule.substring(0, rule.length - 1);
                            // check is current url start with rule string
                            active = (currentUrl.indexOf(rule) === 0);
                            if (active) {
                                item.active = true;
                                break;
                            }
                        } else {
                            active = (rule == currentUrl);
                        }
                    }
                } else {
                    // if active rule is empty, item will be selected
                    // when current url and item url is equal
                    active = (item.url == currentUrl);
                }
                if (active) {
                    item.active = true;
                    break;
                }

                // check submenu items, if exists
                if ('items' in  item) {
                    markActiveItem(item.items);
                }
            }
        }

        // set all items as not active
        function cleanActiveItems(items) {
            for (var i = 0; i < items.length; i++){
                items[i].active = false;
                if ('items' in items[i]) {
                    cleanActiveItems(items[i]);
                }
            }
        }

        // changing active menu item by changing DOM element properties
        // rebind drop down buttons actions in menu
        menuBuilder.bindDropDowns = function() {
            $('.side-menu-list li.with-sub').each(function(){
                var parent = $(this),
                    clickLink = parent.find('>span'),
                    subMenu = parent.find('ul');

                clickLink.unbind('click');
                clickLink.click(function(){
                    cleanActiveItems(menuBuilder.menuItems);
                    $('.side-menu-list li.with-sub .d-block').removeClass('d-block');

                    if (parent.hasClass('opened')) {
                        parent.removeClass('opened');
                        subMenu.removeClass('d-block');
                        subMenu.slideUp();
                    } else {
                        $('.side-menu-list li.with-sub').not(this).removeClass('opened').find('ul').slideUp();
                        parent.addClass('opened');
                        subMenu.addClass('d-block');
                        subMenu.slideDown();
                    }
                });
            });
        };

        menuBuilder.updateCounters = function() {
            user.updateCounters(function(response) {
                if (auth.user !== null) {
                    auth.user.counters = response;
                }
                updateMenuItemsCounters(menuBuilder.menuItems, response);
            });
        };

        function updateMenuItemsCounters(menuItems, response) {
            for (var i = 0; i < menuItems.length; i++){
                var item = menuItems[i];

                if ('counter' in item) {
                    var counter = item['counter'];
                    if (('key' in counter) && (counter['key'] in response)) {
                        var key = counter['key'];
                        item.counter.value = response[key];
                    }
                }

                // check submenu items, if exists
                if ('items' in  item) {
                    updateMenuItemsCounters(item.items, response);
                }
            }
        }

        menuBuilder.updateCounterByKey = function(counterKey, inc) {
            incCounterByKey(menuBuilder.menuItems, counterKey, inc);
        };

        function incCounterByKey(menuItems, counterKey, inc) {
            for (var i = 0; i < menuItems.length; i++){
                var item = menuItems[i];

                if ('counter' in item) {
                    var counter = item['counter'];
                    if (('key' in counter) && (counter['key'] == counterKey)) {
                        var key = counter['key'];
                        item.counter.value += inc;
                    }
                }

                // check submenu items, if exists
                if ('items' in  item) {
                    incCounterByKey(item.items, counterKey, inc);
                }
            }
        }

        return menuBuilder;
    }
]);