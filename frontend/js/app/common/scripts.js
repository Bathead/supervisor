// init tabs
$(document).on('click', '.tabs-section .tabs-section-nav a.nav-link', function() {
    var currentTab = $(this).parents('section.tabs-section').find('.nav-link.active');
    currentTab.removeClass('active');

    var newTabContentId = $(this).data('tab-id');
    var tabsContents = $(this).parents('section.tabs-section').find('.tab-pane');
    tabsContents.removeClass('active in');

    var newActiveTabContent = $(this).parents('section.tabs-section').find('#' + newTabContentId);
    newActiveTabContent.addClass('active in');
    $(this).addClass('active');
});

// table columns select dropdown
// prevent dropdown close after dropdown item click
$(document).on('click', '.dropdown-menu', function(e) {
    if ($(this).hasClass('keep-open-on-click')) { e.stopPropagation(); }
});




//for table with projects actions on click or select items
$(document).on( "mousedown", "table.clicked-table-projects tr td", function(event) {
    if(event.which == 1)
    {
        if ($(this).parent().hasClass('active-row')) {
            $('.active-row').removeClass('active-row');
        }
        else {
            $('.active-row').removeClass('active-row');
            $(this).parent().addClass('active-row');
        }
    }
    if(event.which == 2)
    {
        $('.active-row').removeClass('active-row');
        $(this).parent().addClass('active-row');
        var key = $(this).parent().data("key");
        window.open('project/' + key,'_blank');
    }
});

$(document).on('click', 'body', function(e){
    if (!$(e.target).closest('.active-row').length) {
        $('.active-row').removeClass('active-row');
    }
});

$(document).on( "dblclick", "table.clicked-table-projects tr td", function(event) {
    var key = $(this).parent().data("key");
    $("#project-view-"+key).trigger('click');
});


$(document).on( "mousedown", "table.clicked-table-projects-versions tr td", function(event) {
    if(event.which == 1)
    {
        if ($(this).parent().hasClass('active-row')) {
            $('.active-row').removeClass('active-row');
        }
        else {
            $('.active-row').removeClass('active-row');
            $(this).parent().addClass('active-row');
        }
    }
    if(event.which == 2)
    {
        $('.active-row').removeClass('active-row');
        $(this).parent().addClass('active-row');
        var id = $(this).parent().data("id");
        var key = $(this).parent().data("key");
        window.open('project/' + id + '/version/' + key, '_blank');
    }
});

$(document).on( "dblclick", "table.clicked-table-projects-versions tr td", function(event) {
    var key = $(this).parent().data("key");
    $("#project-view-"+key).trigger('click');
});



//for table with tickets actions on click or select items
$(document).on( "mousedown", "table.clicked-table-tickets tr td", function(event) {
    if(event.which == 1)
    {
        if ($(this).parent().hasClass('active-row')) {
            $('.active-row').removeClass('active-row');
        }
        else {
            $('.active-row').removeClass('active-row');
            $(this).parent().addClass('active-row');
        }
    }
    if(event.which == 2)
    {
        $('.active-row').removeClass('active-row');
        $(this).parent().addClass('active-row');
        var key = $(this).parent().data("key");
        window.open('ticket/' + key,'_blank');
    }
});

$(document).on( "dblclick", "table.clicked-table-tickets tr td", function(event) {
    var key = $(this).parent().data("key");
    $("#ticket-view-"+key).trigger('click');
});