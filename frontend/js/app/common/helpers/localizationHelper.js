svApp.factory('localizationHelper', [
    '$filter',
    '$translate',
    'CONFIG',
    function(
        $filter,
        $translate,
        CONFIG
    ) {
        var helper = {};
        var $translateFilter = $filter('translate');

        // translate in js files (controllers, factories, ...)
        helper.translate = function(text) {
            return $translateFilter(text);
        };

        helper.getCurrentLanguage = function() {
            return (localStorage.getItem('lang')) ? localStorage.getItem('lang') : CONFIG.defaultLang;
        };

        helper.getCurrentLanguageFlag = function() {
            var currentLang = $translate.use();
            for(var i = 0; i < CONFIG.langs.length; i++){
                if(CONFIG.langs[i]['key'] == currentLang){
                    return CONFIG.langs[i]['flag'];
                }
            }
        };

        return helper;
}]);