svApp.factory('requestHelper', function() {
    var requestHelper = {};

    // format search query, compatible with yii2 activeDataProvider
    requestHelper.formatSearchRequest = function(entity, page, count, filter, sorting) {
        var result = {};

        // filtration
        if (!$.isEmptyObject(filter)) {
            var searchName = 'Search' + entity;
            result[searchName] = {};
            for (var filterKey in filter) {
                result[searchName][filterKey] = filter[filterKey];
            }
        }

        // sorting
        var sortKey = Object.keys(sorting)[0];
        if (typeof sortKey != 'undefined') {
            var sortType = sorting[sortKey];
            result.sort = (sortType == 'asc') ? sortKey : '-' + sortKey;
        }

        result.page = page;
        result.count = count;

        return $.param(result);
    };

    requestHelper.addSingleFileToFormData = function(formData, file, name) {
        if (typeof file !== 'undefined') {
            if (typeof name == 'undefined') {
                name = 'file';
            }
            formData.append(name, file);
        }
    };

    requestHelper.addMultipleFilesToFormData = function(formData, files, name) {
        if (typeof files !== 'undefined') {
            if (typeof name == 'undefined') {
                name = 'files[]';
            } else {
                name += '[]';
            }
            for (var i = 0; i < files.length; i++) {
                //add each file to the form data and iteratively name them
                formData.append(name, files[i]);
            }
        }
    };

    return requestHelper;
});