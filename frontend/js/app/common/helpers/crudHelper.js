svApp.factory('crudHelper', [
    '$location',
    'NgTableParams',
    'notification',
    function(
        $location,
        NgTableParams,
        notification
    ) {

        var helper = {};

        // default initial params for ng-table
        var defaultInitialParams = {
            page:   1,                      // start page
            count:  10                      // per page items count
        };

        // default initial settings for ng-table
        var defaultInitialSettings = {
            counts: [10, 25, 50, 100, 200]  // per page items counts list
        };

        var defaultMessages = {
            deleteSuccess:  "Deleted successfully",
            deleteError:    "Don't deleted",
            createSuccess:  "Added successfully",
            createError:    "Don't created",
            updateSuccess:  "Updated successfully",
            updateError:    "Don't updated"
        };

        // generate ng-table
        helper.generateTable = function(
            initialParams,   // page, count (items per page), ...
            initialSettings, // counts (per page counts list), getData, ...
            model,
            controller
        ) {
            // assign default options to final
            var finalInitialParams = $.extend({}, defaultInitialParams);
            var finalInitialSettings = $.extend({}, defaultInitialSettings);

            // overwrite options, declared manually
            for (var ipKey in initialParams) {
                finalInitialParams[ipKey] = initialParams[ipKey];
            }
            for (var isKey in initialSettings) {
                finalInitialSettings[isKey] = initialSettings[isKey];
            }

            // set default getData function
            if (typeof finalInitialSettings.getData == 'undefined') {
                finalInitialSettings.getData = function ($defer, params) {
                    var filter = params.filter();
                    var sorting = params.sorting();
                    var count = params.count();
                    var page = params.page();
                    model.getList(page, count, filter, sorting, function(response) {
                        controller.tableParams.total(response.totalCount);
                        $defer.resolve(response.list);
                    });
                }
            }

            return new NgTableParams(finalInitialParams, finalInitialSettings);
        };

        helper.generateCustomTable = function(
            initialParams,   // page, count (items per page), ...
            initialSettings, // counts (per page counts list), getData, ...
            model,
            controller,
            method,
            id
        ) {
            // assign default options to final
            var finalInitialParams = $.extend({}, defaultInitialParams);
            var finalInitialSettings = $.extend({}, defaultInitialSettings);

            // overwrite options, declared manually
            for (var ipKey in initialParams) {
                finalInitialParams[ipKey] = initialParams[ipKey];
            }
            for (var isKey in initialSettings) {
                finalInitialSettings[isKey] = initialSettings[isKey];
            }

            // set default getData function
            if (typeof finalInitialSettings.getData == 'undefined') {
                finalInitialSettings.getData = function ($defer, params) {
                    var filter = params.filter();
                    var sorting = params.sorting();
                    var count = params.count();
                    var page = params.page();
                    if (id) {
                        method(page, count, filter, sorting, id, function (response) {
                            controller.tableParams.total(response.totalCount);
                            $defer.resolve(response.list);
                        });
                    } else {
                        method(page, count, filter, sorting, function (response) {
                            controller.tableParams.total(response.totalCount);
                            $defer.resolve(response.list);
                        });
                    }
                }
            }

            return new NgTableParams(finalInitialParams, finalInitialSettings);
        };

        helper.generateTableWithCustomData = function(
            initialParams,   // page, count (items per page), ...
            initialSettings, // counts (per page counts list), getData, ...
            controller
        ){
            // assign default options to final
            var finalInitialParams = $.extend({}, defaultInitialParams);
            var finalInitialSettings = $.extend({}, defaultInitialSettings);

            // overwrite options, declared manually
            for (var ipKey in initialParams) {
                finalInitialParams[ipKey] = initialParams[ipKey];
            }
            for (var isKey in initialSettings) {
                finalInitialSettings[isKey] = initialSettings[isKey];
            }

            return new NgTableParams(finalInitialParams, finalInitialSettings);
        };

        // create wrapper (with notification)
        helper.create = function(
            entity,
            model,
            redirectUrl,
            successMessage,
            errorMessage
        ) {
            model.create(entity, function(response) {
                var message = '';
                if (response.success) {
                    $location.path(redirectUrl);
                    message = (typeof successMessage == 'undefined' || successMessage === null) ?
                        defaultMessages.createSuccess : successMessage;
                } else {
                    message = (typeof errorMessage == 'undefined' || errorMessage === null) ?
                        defaultMessages.createError : errorMessage;
                }
                notification.alert(message, response.success);
            });
        };

        // update wrapper (with notification)
        helper.update = function(
            entity,
            model,
            redirectUrl,
            successMessage,
            errorMessage
        ) {
            model.update(entity, function(response) {
                var message = '';
                if (response.success) {
                    $location.path(redirectUrl);
                    message = (typeof successMessage == 'undefined' || successMessage === null) ?
                        defaultMessages.updateSuccess : successMessage;

                } else {
                    message = (typeof errorMessage == 'undefined' || errorMessage === null) ?
                        defaultMessages.updateError : errorMessage;
                }
                notification.alert(message, response.success);
            });
        };

        // delete wrapper (with confirmation window and notification)
        helper.delete = function (
            entityId,
            model,
            controller,
            successMessage,
            errorMessage
        ) {
            notification.confirm(function () {
                model.delete(entityId, function(response) {
                    var message = '';
                    if (response.success) {
                        // go to previous page, if deleted element
                        // was last element on current page (except first page)
                        var currentPage = controller.tableParams.page();
                        if (controller.tableParams.data.length == 1 && currentPage != 1) {
                            controller.tableParams.page(currentPage - 1);
                        } else {
                            // reload current page items, if deleted element wasn't
                            // last element on current page (except first page)
                            controller.tableParams.reload();
                        }

                        message = (typeof successMessage == 'undefined' || successMessage === null) ?
                            defaultMessages.deleteSuccess : successMessage;
                    } else {
                        message = (typeof errorMessage == 'undefined' || errorMessage === null) ?
                            defaultMessages.deleteError : errorMessage;
                    }
                    notification.alert(message, response.success);
                });
            });
        };

        return helper;
    }
]);