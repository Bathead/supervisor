svApp.factory('themeSwitcher', [
    'CONFIG',
    function(
        CONFIG
    ) {
        var themeSwitcher = {};

        themeSwitcher.currentTheme = {};
        
        themeSwitcher.initTheme = function() {
            var currentThemeName = (localStorage.getItem('theme')) ? localStorage.getItem('theme') : CONFIG.defaultTheme;
            themeSwitcher.currentTheme = CONFIG.themes[currentThemeName];
            return themeSwitcher.currentTheme;
        };

        themeSwitcher.changeTheme = function(theme) {
            localStorage.setItem('theme', theme);
            themeSwitcher.currentTheme = CONFIG.themes[theme];
        };

        return themeSwitcher;
    }
]);