// directive for checking uniqueness of field

// required attributes
// - unique-field - name of unique field
// - unique-field-url-check - url of method for checking field uniqueness
//   server method input parameters: fieldName, fieldValue
//   server method output parameters: isUnique

// not required attributes:
// - unique-field-except - for this field value checking will be prevent

svApp.directive('uniqueField', function($q, $timeout, $http) {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {

            ctrl.$asyncValidators.uniqueField = function(modelValue, viewValue) {
                if (ctrl.$isEmpty(modelValue)) {
                    // consider empty model valid
                    return $q.when();
                }

                var def = $q.defer();

                $timeout(function() {
                    if ((typeof attrs.uniqueFieldExcept !== 'undefined') && (modelValue == attrs.uniqueFieldExcept)) {
                        def.resolve();
                    } else {
                        // Mock a delayed response
                        var data = {
                            fieldName: attrs.uniqueField,
                            fieldValue: modelValue
                        };
                        return $http.post(attrs.uniqueFieldUrlCheck, data).success(function(response) {
                            if (response.isUnique) {
                                // field is valid
                                def.resolve();
                            } else {
                                def.reject();
                            }
                        });
                    }
                }, 800);

                return def.promise;
            };
        }
    };
});