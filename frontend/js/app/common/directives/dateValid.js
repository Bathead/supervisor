svApp.directive('dateValid', function($q, $timeout, $http) {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {

            ctrl.$asyncValidators.uniqueField = function(modelValue, viewValue) {
                if (ctrl.$isEmpty(modelValue)) {
                    // consider empty model valid
                    return $q.when();
                }

                var def = $q.defer();

                $timeout(function() {
                    var regexp = /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/i;
                    if (regexp.test(modelValue)) {
                        // value have valid format, check year, month and day
                        var properties = modelValue.split('-');
                        var year = properties[0];
                        var month = properties[1];
                        var day = properties[2];

                        if ((year < 1900 || year > 2300) || (month < 1 || month > 12) || (day < 1 || day > 31)) {
                            def.reject(); // field is not valid
                        }
                        def.resolve(); // field is valid
                    } else {
                        def.reject(); // field is not valid
                    }
                }, 800);

                return def.promise;
            };
        }
    };
});