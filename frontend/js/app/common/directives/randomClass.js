// add random class to element
// example of using:
// <button random-class='["success", "warning"]'>Test</button>

svApp.directive('randomClass', function () {
    return {
        restrict: 'A',
        link: function($scope, elem, attr) {
            var classesList = JSON.parse(attr.randomClass);
            var randomIndex = Math.floor((Math.random() * classesList.length));
            elem.addClass(classesList[randomIndex]);
        }
    };
});