// directive add style class to progress component, which depends on progress value
//
// example of using:
//  <progress class="progress"
//      progress-indicator="progressValueVariable"
//      value="{{progressValueVariable | number:0}}"
//      max="100">
//  </progress>
//
// progressValueVariable - angular variable, or static value
// max - required value, which set maximum possible value of progress bar

svApp.directive('progressIndicator', function () {
    return {
        restrict: 'A',
        scope: {
            progressIndicator: '=progressIndicator'
        },
        link: function(scope, el, attrs) {

            var styleClasses = [
                'progress-danger',
                'progress-warning',
                'progress-success',
                'progress-aquamarine',
                'progress-info'
            ];

            var max = (typeof attrs.max == 'undefined') ? 100 : attrs.max;
            var coefficient = max / 100;
            scope.$watch("progressIndicator", function(newVal) {

                if(newVal){

                    // remove old style class
                    styleClasses.forEach(function(item, i, arr) {
                        if (el.hasClass(item)) {
                            el.removeClass(item);
                        }
                    });

                    // add new style class
                    var progress = newVal / coefficient;
                    if (progress > 0 && progress <= 20) {
                        el.addClass('progress-danger');
                    } else if (progress > 20 && progress <= 40) {
                        el.addClass('progress-warning')
                    } else if (progress > 40 && progress <= 60) {
                        //el.addClass('');
                    } else if (progress > 60 && progress <= 80) {
                        el.addClass('progress-success');
                    } else if (progress > 80 && progress <= 90) {
                        el.addClass('progress-aquamarine');
                    } else if (progress > 90 && progress <= 100) {
                        el.addClass('progress-info');
                    }
                }
            });
        }
    };
});