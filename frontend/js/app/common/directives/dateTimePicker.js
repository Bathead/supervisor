// directive for bootstrap 3 datetimepicker
// example of using:
// <div class='input-group date'>
//      <input
//          date-time-picker - init dateTimePicker (required)
//          ng-model="ngModelName" - set full name of ngModel like "project.model.start_date" (required)
//          dtp-date-format="YYYY-MM-DD " - set date format (optional)
//          dtp-locale="{{mainController.currentLang}}" - (required)
//          dtp-default-date="10-10-2001" - set the default date, not required to have the same format as date-format,
//                                  like "2001-10-05" / "09,10,2010" etc...  (optional)
//          dtp-default-date-today="true" - set today date as default date, like "true/false",
//                                  be attention, if default-date parameter is set, this parameter will be ignored(optional)
//          dtp-min-date="10-10-2001" - set min date, not required to have the same format as date-format,
//                                  like "2001-10-05" / "09,10,2010" etc...  (optional)
//          dtp-max-date="10-10-2020" - set max date, not required to have the same format as date-format,
//                                  like "2020-10-05" / "09,10,2020" etc...  (optional)
//          class="form-control" - (required)
//          type="text" - (required)
//          pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" - (optional)
//          name="name" - (optional)
//          id="id" - (optional)
//      />
//      </input>
//      <span class="input-group-addon">
//          <i class="font-icon font-icon-calend"></i>
//      </span>
//  </div>

svApp.directive('dateTimePicker', ['localizationHelper', '$timeout', function(localizationHelper, $timeout) {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function(scope, element, attrs, ngModel) {
            if (!ngModel) return;

            $timeout(
                function(){

                    var id = null;
                    var dateFormat = null;
                    var initialDefaultDate = null;
                    var defaultDateToday = null;
                    var minDate = null;
                    var maxDate = null;
                    var locale = null;

                    angular.forEach(attrs, function(value, key){
                        switch(key){
                            case 'id':
                                id = value;
                                break;
                            case 'dtpDateFormat':
                                dateFormat = value;
                                break;
                            case 'dtpDefaultDate':
                                initialDefaultDate = value;
                                break;
                            case 'dtpDefaultDateToday':
                                defaultDateToday = Boolean(value);
                                break;
                            case 'dtpLocale':
                                locale = value;
                                break;
                            case 'dtpMinDate':
                                minDate = value;
                                break;
                            case 'dtpMaxDate':
                                maxDate = value;
                                break;
                        }
                    });

                    if(!dateFormat){
                        dateFormat = "YYYY-MM-DD"
                    }

                    var mask = dateFormat.replace(new RegExp('[^-]', 'g'), '0');

                    var parent = $(element).parent();
                    $("#" + id).mask(mask, {placeholder: localizationHelper.translate(dateFormat)});

                    var defaultDate = getDefaultDate();

                    function getDefaultDate(){
                        if(ngModel.$modelValue === undefined || ngModel.$modelValue === null){
                            if(initialDefaultDate !== null){
                                return moment(initialDefaultDate).format(dateFormat);
                            }
                            else if(defaultDateToday !== null && defaultDateToday == true){
                                return moment().format(dateFormat);
                            }
                            else{
                                return null;
                            }
                        }
                        else{
                            return moment(ngModel.$modelValue).format(dateFormat);
                        }
                    }

                    var dtp = parent.datetimepicker({
                        defaultDate: defaultDate,
                        minDate: minDate ? minDate : false,
                        maxDate: maxDate ? maxDate : false,
                        widgetPositioning: {
                            horizontal: 'right'
                        },
                        format: dateFormat,
                        debug: false,
                        locale: locale,
                        showTodayButton: true,
                        showClear: true,
                        showClose: true
                    });

                    dtp.on("dp.change", function (e) {
                        ngModel.$setViewValue(moment(e.date).format(dateFormat));
                        scope.$apply();
                    });
                },
                0
            );
        }
    };
}]);
