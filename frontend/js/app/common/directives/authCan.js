// check, is current user have required permission
// if user don't have required permission, element will be hidden
// example of using:
// <button auth-can="permission">Test</button>

svApp.directive('authCan', function (auth) {
    return {
        restrict: 'A',
        link: function($scope, el, attrs) {

            $scope.$watch('mainController.user.permissions', function(newValue, oldValue) {
                if (auth.can(attrs.authCan)) {
                    el.show();
                } else {
                    el.hide();
                }
            });

        }
    };
});

//
// element will be shown, if user don't have permission
// example of using:
// <button auth-not-can="permission">Test</button>

svApp.directive('authNotCan', function (auth) {
    return {
        restrict: 'A',
        link: function($scope, el, attrs) {

            $scope.$watch('mainController.user.permissions', function(newValue, oldValue) {
                if (auth.can(attrs.authNotCan)) {
                    el.hide();
                } else {
                    el.show();
                }
            });

        }
    };
});

// check, is current user have any of required permissions
// if user don't have any of required permissions, element will be hidden
// example of using:
// <button auth-can-any-of='["permission1", "permission2"]'>Test</button>

svApp.directive('authCanAnyOf', function (auth) {
    return {
        restrict: 'A',
        link: function($scope, el, attrs) {

            $scope.$watch('mainController.user.permissions', function(newValue, oldValue) {
                var requiredPermissions = JSON.parse(attrs.authCanAnyOf);
                var visible = false;

                for (var i = 0; i < requiredPermissions.length; i++) {
                    var permission = requiredPermissions[i];
                    if (auth.can(permission)) {
                        visible = true;
                        break;
                    }
                }

                if (visible) {
                    el.show();
                } else {
                    el.hide();
                }
            });

        }
    };
});