svApp.filter('replace', function() {
    return function(input, from, to, replaceAllInstances) {
        var regExp = replaceAllInstances ? new RegExp(from, "g") : new RegExp(from);
        return input.replace(regExp, to);
    };
});
