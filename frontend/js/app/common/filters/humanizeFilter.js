svApp.filter('humanize', function() {
    return function(text, separator) {
        if(text) {
            separator = typeof separator !== 'undefined' ? separator : " ";
            text = text.split(separator);

            // go through each word in the text and capitalize the first letter
            angular.forEach(text, function (word, i) {
                word = word.toLowerCase();
                word = word.charAt(0).toUpperCase() + word.slice(1);
                text[i] = word;
            });

            return text.join(" ");
        }
    };
});
