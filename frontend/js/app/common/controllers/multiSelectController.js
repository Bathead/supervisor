svApp.controller('multiSelectController', [
    '$scope',
    function(
        $scope
    ) {

        var controller = this;
        controller.modalId        = 'multiselect-modal-' + getRandomId();
        controller.filterName     = $scope.name;
        controller.selectedParams = [];
        controller.searchTitle    = '';

        controller.isDataGrouped = !angular.isArray($scope.$column.data);
        
        controller.openModal = function() {
            $('#' + controller.modalId).modal('show');
        };

        controller.filtrate = function() {
            var selectedParamsIds    = [];
            var selectedParamsTitles = [];
            for (var i = 0; i < controller.selectedParams.length; i++) {
                selectedParamsIds.push(controller.selectedParams[i].id);
                selectedParamsTitles.push(controller.selectedParams[i].title);
            }

            $scope.params.filter()[controller.filterName] = selectedParamsIds;
            controller.searchTitle = selectedParamsTitles.join(', ');

            $('#' + controller.modalId).modal('hide');
        };

        controller.clean = function() {
            controller.selectedParams = [];
            controller.searchTitle = '';
            $scope.params.filter()[controller.filterName] = [];
            $('#' + controller.modalId).modal('hide');
        };

        function getRandomId() {
            return Math.floor(Math.random() * 1000000);
        }
    }
]);