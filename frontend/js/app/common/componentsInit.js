svApp.factory('componentsInit', function() {
    var componentsInit = {};

    componentsInit.initSelect = function() {
        // Bootstrap-select
        $('.bootstrap-select').selectpicker({
            style: '',
            width: '100%',
            size: 8
        });

        // Select2
        $.fn.select2.defaults.set("minimumResultsForSearch", "Infinity");

        $('.select2').select2();

        function select2Icons (state) {
            if (!state.id) { return state.text; }
            var $state = $(
                '<span class="font-icon ' + state.element.getAttribute('data-icon') + '"></span><span>' + state.text + '</span>'
            );
            return $state;
        }

        $(".select2-icon").select2({
            templateSelection: select2Icons,
            templateResult: select2Icons
        });

        $(".select2-arrow").select2({
            theme: "arrow"
        });

        $(".select2-white").select2({
            theme: "white"
        });

        function select2Photos (state) {
            if (!state.id) { return state.text; }
            var $state = $(
                '<span class="user-item"><img src="' + state.element.getAttribute('data-photo') + '"/>' + state.text + '</span>'
            );
            return $state;
        }

        $(".select2-photo").select2({
            templateSelection: select2Photos,
            templateResult: select2Photos
        });
    };

    componentsInit.initDatepicker = function (inputClass, format, onChangeCallback, defaultDate){
        $('.' + inputClass).datetimepicker({
            defaultDate: defaultDate ? defaultDate : null,
            widgetPositioning: {
                horizontal: 'right'
            },
            showTodayButton: true,
            showClear: true,
            showClose: true,
            allowInputToggle: true,
            format: format,
            debug: false
        });

        if(onChangeCallback) {
            $('.' + inputClass).on("dp.change", function (e) {
                onChangeCallback();
            });
        }
    };

    componentsInit.initTypeaheadWithObject = function (inputId, outputId, data, display, onClickCallback, defaultValue) {
        $.typeahead({
            input: "#" + inputId,
            order: "asc",
            display: display,
            minLength: 1,
            source: {
                data: data
            },
            callback: {
                onInit: function(node){
                    if(defaultValue)
                        $("#" + inputId).val(defaultValue);
                },
                onClick: function (node, a, obj, e) {
                    $("#" + outputId).val(obj.id).trigger('input');
                    if(onClickCallback && (typeof (onClickCallback) == "function"))
                        onClickCallback(obj.id);
                }
            }
        });
    };

    componentsInit.initTypeahead  = function(inputId, data){
        $.typeahead({
            input: "#" + inputId,
            order: "asc",
            minLength: 1,
            source: {
                data: data
            },
        });
    };

    componentsInit.initScrollBars = function() {
        if (!("ontouchstart" in document.documentElement)) {

            document.documentElement.className += " no-touch";

            var jScrollOptions = {
                autoReinitialise: true,
                autoReinitialiseDelay: 100
            };

            $('.box-typical-body').jScrollPane(jScrollOptions);
            $('.side-menu').jScrollPane(jScrollOptions);
            //$('.side-menu-addl').jScrollPane(jScrollOptions);
            $('.scrollable-block').jScrollPane(jScrollOptions);
        }
    };

    return componentsInit;
});